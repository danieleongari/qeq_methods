/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _EWALDPOTENTIAL_H
#define _EWALDPOTENTIAL_H

#include "definitions.h"
#include "Array1D.h"
#include "Array2D.h"
#include "String.h"
#include "Vector.h"
#include "Tensor.h"
#include "KVector.h"
#include "Geometry.h"

#ifdef COMPILE_PARALLEL
	#include "MPICommBall.h"
#endif

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#define EWALDPOTENTIAL_SUMMATION_METHOD_ATOMIC 0
#define EWALDPOTENTIAL_SUMMATION_METHOD_EWALD 1
#define EWALDPOTENTIAL_RATIO_R1 1.25
#define EWALDPOTENTIAL_BLANKSIZE 0.16

class EwaldPotential
{
	protected:
		//  1D Term variable lists
		Array1D<int> IndexI_val, IndexJ_val;
		Array1D<Vector> Rij_val;
		Array1D<FP_TYPE> MinImageFactor_val;
		Array1D<FP_TYPE> SumPair_val;
		Array1D<int> TotalTerms_val;
		
		//  1D Atom variable lists, for either one or two structure interactions
		Array1D<FP_TYPE> SumSingle_val;
		
		//  Variables for handling translations and the like
		int sumMethod_val;
		FP_TYPE realCut_val, fourierCut_val;
		FP_TYPE alpha_val, error_val, volume_val;
		FP_TYPE selfPairEnergy_val, selfSingleEnergy_val;
		FP_TYPE energyTail_val;
		
		Array1D<Vector> CellVect_val, Kvect_val;
		Array1D<Vector> RealVect_val;
		Array1D<KVector> FourierVect_val;
		Array1D<KVector> FourierVectTotal_val;
		
		//  Functions for calculating atomic energies
		void atomicEnergyR(int, FP_TYPE, FP_TYPE&) const;

		//  Functions for calculating Ewald energies, forces and hessians
		void ewaldEnergyR(FP_TYPE, FP_TYPE, FP_TYPE&) const;

	public:
		EwaldPotential();
		EwaldPotential(const EwaldPotential&);
		const EwaldPotential& operator=(const EwaldPotential&);
		int operator=(int);
		void copyTranslationData(const EwaldPotential&);
		void null();

		//  Readers for various things
		int termNo() const;
		int sumMethod() const;
		bool sumMethodGroup() const;
		int sumPower() const;
		FP_TYPE realCut() const;
		FP_TYPE fourierCut() const;
		FP_TYPE alpha() const;
		FP_TYPE volume() const;
		int cellVectNo() const;
		int kVectNo() const;
		int realNo() const;
		int fourierNo() const;
		const Vector& CellVect(int) const;
		const Vector& Kvect(int) const;
		const Vector& RealVect(int) const;
		const KVector& FourierVect(int) const;
	
		//  Writers for the static variables
		void setSumMethod(int);
		void setCutoff(FP_TYPE);
		void setError(FP_TYPE);
		
		void createTerms(const Geometry&);
		void setParamLists(const Geometry&);
		
		//  Functions to handle setting the cell stuff
		FP_TYPE realEwaldError(FP_TYPE);
		FP_TYPE realEwaldDiagError(FP_TYPE);
		FP_TYPE fourierEwaldDiagError(FP_TYPE);
		
		void setCellParam(const Geometry&);
		bool cellVectCube() const;
		void setEwaldRcut(FP_TYPE);
		void setEwaldHcut(FP_TYPE);
		void setTransParam(FP_TYPE, FP_TYPE, FP_TYPE, FP_TYPE);
		void setTransVect(FP_TYPE, FP_TYPE, FP_TYPE, FP_TYPE);

		FP_TYPE fourierFactor(FP_TYPE, FP_TYPE) const;
		int transVectNo(FP_TYPE) const;
		Array1D<Vector> TransVect(FP_TYPE, const Array1D<Vector>&) const;
		Array1D<KVector> FourierVect(bool, FP_TYPE, FP_TYPE, FP_TYPE, const Array1D<Vector>&) const;

		void setRij(const Geometry&);
		
		////  Energy interface functions
		void addEnergy(const Geometry&, FP_TYPE&);
		FP_TYPE energyRealAtomic(const Geometry&) const;
		FP_TYPE energyRealEwald(const Geometry&) const;
		FP_TYPE energyFourierGroupGeom(const Geometry&) const;
		FP_TYPE energyTail() const;
		FP_TYPE energyEwaldSelf() const;

		void loadListData(const Column&);
};


#endif


