
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "KVector.h"

KVector::KVector()	{ null(); }
KVector::KVector(const KVector& A)
{
	*this = A;
}
KVector::~KVector()
{
	return;
}
const KVector& KVector::operator=(const KVector& A)
{
	K_val = A.K_val;
	factor_val = A.factor_val;
	a_val = A.a_val;
	b_val = A.b_val;
	c_val = A.c_val;

	return *this;
}
int KVector::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in KVector::operator=(int i)");
	null();

	return i;
}
void KVector::null()
{
	K_val.zero();
	a_val = b_val = c_val = 0;
	factor_val = 0;
}

//  Readers to get at the information
FP_TYPE KVector::r() const				{ return K_val.r(); }
const Vector& KVector::K() const		{ return K_val; }
FP_TYPE KVector::factor() const			{ return factor_val; }
int KVector::a() const					{ return a_val; }
int KVector::b() const					{ return b_val; }
int KVector::c() const					{ return c_val; }

//  Single function for setting the whole thing
void KVector::setVector(const Array1D<int>& Component, const Array1D<Vector>& InvVect)
{
	int i;
	FunctionBall Func;

	K_val.zero();
	for(i = 0; i < 3; i++) K_val += InvVect[i] * (FP_TYPE)Component[i];

	a_val = Component[0];
	b_val = Component[1];
	c_val = Component[2];
}
void KVector::setKVector(FP_TYPE x, FP_TYPE y, FP_TYPE z)
{
	K_val.setCart(x, y, z);
}
void KVector::setFactor(FP_TYPE factor)				{ factor_val = factor; }
void KVector::setComponent(int a, int b, int c)
{
	a_val = a;
	b_val = b;
	c_val = c;
}

//  Functions to make sorting work
bool KVector::operator<(const KVector& A)			{ return (K_val.r() < A.r()) ? true : false; }
bool KVector::operator>(const KVector& A)			{ return (K_val.r() > A.r()) ? true : false; }

//  Function to get dot products with the position vector
FP_TYPE KVector::operator%(const Vector& A)	const		{ return K_val % A; }