
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Calculus.h"
#include "Array1D.h"
#include "Vector.h"
#include "FunctionBall.h"
#include "Column.h"
#include "Matrix.h"

Calculus::Calculus()
{
	null();
}
Calculus::Calculus(const Calculus& Template)
{
	*this = Template;
}
const Calculus& Calculus::operator=(const Calculus& A)
{ 
	return *this;
}
int Calculus::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in Calculus::operator=(int i)");
	null();

	return i;
}
void Calculus::null()
{
	return;
}

FP_TYPE Calculus::value1D(FP_TYPE x) const
{
	FP_TYPE N, alpha, result;

	N = 2;
	alpha = 0.8;
	result = N * exp(-alpha*alpha*x*x);
	return result;
	//throw ErrorBall("Function Calculus::value1D has no definition for this type of class");
	//return 0;
}
FP_TYPE Calculus::value2D(FP_TYPE x, FP_TYPE y) const
{
	FP_TYPE N, alpha, rsq, result;

	N = 2;
	alpha = 0.8;
	rsq = (x*x) + (y*y);
	result = N * exp(-alpha*alpha*rsq);
	return result;
	//throw ErrorBall("Function Calculus::value2D has no definition for this type of class");
	//return 0;
}
FP_TYPE Calculus::value3D(FP_TYPE x, FP_TYPE y, FP_TYPE z) const
{
	FP_TYPE N, alpha, rsq, result;

	N = 2;
	alpha = 0.8;
	rsq = (x*x) + (y*y) + (z*z);
	result = N * exp(-alpha*alpha*rsq);
	return result;

	//throw ErrorBall("Function Calculus::value3D has no definition for this type of class");
	//return 0;
}

//http://www.trentfguidry.net/post/2010/09/04/Numerical-differentiation-formulas.aspx
FP_TYPE Calculus::firstDerivative(int place, const Array1D<FP_TYPE>& FuncVal, FP_TYPE dVar)
{
	int i, j;
	FP_TYPE result, denom;
	Array1D<FP_TYPE> Factor;

	Factor.setSize(FuncVal.length());

	switch(FuncVal.length())
	{
		case 2:
		{
			Factor[0] = -1;
			Factor[1] = 1;

			denom = dVar;

			break;
		}
		case 3:
		{
			switch(place)
			{
				case 0:
				case 2:
				{
					Factor[0] = -3;
					Factor[1] = 4;
					Factor[2] = -1;
					break;
				}
				case 1:
				{
					Factor[0] = -1;
					Factor[1] = 0;
					Factor[2] = 1;
					break;
				}
			}

			denom = 2 * dVar;

			break;
		}
		case 4:
		{
			switch(place)
			{
				case 0:
				case 3:
				{
					Factor[0] = -11;
					Factor[1] = 18;
					Factor[2] = -9;
					Factor[3] = 2;
					break;
				}
				case 1:
				case 2:
				{
					Factor[0] = -2;
					Factor[1] = -3;
					Factor[2] = 6;
					Factor[3] = -1;
					break;
				}
			}

			denom = 6 * dVar;

			break;
		}
		case 5:
		{
			switch(place)
			{
				case 0:
				case 4:
				{
					Factor[0] = -25;
					Factor[1] = 48;
					Factor[2] = -36;
					Factor[3] = 16;
					Factor[4] = -3;
					break;
				}
				case 1:
				case 3:
				{
					Factor[0] = -3;
					Factor[1] = -10;
					Factor[2] = 18;
					Factor[3] = -6;
					Factor[4] = 1;
					break;
				}
				case 2:
				{
					Factor[0] = 1;
					Factor[1] = -8;
					Factor[2] = 0;
					Factor[3] = 8;
					Factor[4] = -1;
					break;
				}
			}

			denom = 12 * dVar;

			break;
		}
		case 6:
		{
			switch(place)
			{
				case 0:
				case 5:
				{
					Factor[0] = -137;
					Factor[1] = 300;
					Factor[2] = -300;
					Factor[3] = 200;
					Factor[4] = -75;
					Factor[5] = 12;
					break;
				}
				case 1:
				case 4:
				{
					Factor[0] = -12;
					Factor[1] = -65;
					Factor[2] = 120;
					Factor[3] = -60;
					Factor[4] = 20;
					Factor[5] = -3;
					break;
				}
				case 2:
				case 3:
				{
					Factor[0] = 3;
					Factor[1] = -30;
					Factor[2] = -20;
					Factor[3] = 60;
					Factor[4] = -15;
					Factor[5] = 2;
					break;
				}

				denom = 60 * dVar;
			}
			break;
		}
		case 7:
		{
			switch(place)
			{
				case 0:
				case 6:
				{
					Factor[0] = -147;
					Factor[1] = 360;
					Factor[2] = -450;
					Factor[3] = 400;
					Factor[4] = -225;
					Factor[5] = 72;
					Factor[6] = -10;
					break;
				}
				case 1:
				case 5:
				{
					Factor[0] = -10;
					Factor[1] = -77;
					Factor[2] = 150;
					Factor[3] = -100;
					Factor[4] = 50;
					Factor[5] = -15;
					Factor[6] = 2;
					break;
				}
				case 2:
				case 4:
				{
					Factor[0] = 2;
					Factor[1] = -24;
					Factor[2] = -35;
					Factor[3] = 80;
					Factor[4] = -30;
					Factor[5] = 8;
					Factor[6] = -1;
					break;
				}
				case 3:
				{
					Factor[0] = -1;
					Factor[1] = 9;
					Factor[2] = -45;
					Factor[3] = 0;
					Factor[4] = 45;
					Factor[5] = -9;
					Factor[6] = 1;
					break;
				}
				
			}

			denom = 60 * dVar;

			break;
		}
		case 8:
		{
			switch(place)
			{
				case 0:
				case 7:
				{
					Factor[0] = -1089;
					Factor[1] = 2940;
					Factor[2] = -4410;
					Factor[3] = 4900;
					Factor[4] = -3675;
					Factor[5] = 1764;
					Factor[6] = -490;
					Factor[7] = 60;
					break;
				}
				case 1:
				case 6:
				{
					Factor[0] = -60;
					Factor[1] = -609;
					Factor[2] = 1260;
					Factor[3] = -1050;
					Factor[4] = 700;
					Factor[5] = -315;
					Factor[6] = 84;
					Factor[7] = -10;
					break;
				}
				case 2:
				case 5:
				{
					Factor[0] = 10;
					Factor[1] = -140;
					Factor[2] = -329;
					Factor[3] = 700;
					Factor[4] = -350;
					Factor[5] = 140;
					Factor[6] = -35;
					Factor[7] = 4;
					break;
				}
				case 3:
				case 4:
				{
					Factor[0] = -4;
					Factor[1] = 42;
					Factor[2] = -252;
					Factor[3] = -105;
					Factor[4] = 420;
					Factor[5] = -126;
					Factor[6] = 28;
					Factor[7] = -3;
					break;
				}
			}
			break;

			denom = 420 * dVar;
		}
		default:
		{
			throw ErrorBall("Derivative not defined for number of points in Calculus::firstDerivative");
			break;
		}
	}

	//  Reverse the direction if I need to
	if(place >= (Factor.length()/2))
	{
		j = Factor.length()-1;
		for(i = 0; i < j; i++)
		{
			Factor.swap(i, j);
			j--;
		}
		for(i = 0; i < Factor.length(); i++) Factor[i] *= -1;
	}

	result = 0;
	for(i = 0; i < FuncVal.length(); i++) result += FuncVal[i] * Factor[i];
	result /= denom;

	return result;
}
FP_TYPE Calculus::secondDerivative(int place, const Array1D<FP_TYPE>& FuncVal, FP_TYPE dVar)
{
	int i;
	Array1D<FP_TYPE> Temp;

	Temp.setSize(FuncVal.length());
	for(i = 0; i < FuncVal.length(); i++) Temp[i] = firstDerivative(i, FuncVal, dVar);

	return firstDerivative(place, Temp, dVar);
}
FP_TYPE Calculus::orderDerivative(int order, int place, const Array1D<FP_TYPE>& FuncVal, FP_TYPE dVar)
{
	int i, j;
	Array2D<FP_TYPE> Temp;

	Temp.setSize(order, FuncVal.length());
	Temp[0] = FuncVal;

	for(i = 1; i < order; i++)
	{
		for(j = 0; j < Temp[i].length(); j++) Temp[i][j] = firstDerivative(j, Temp[i-1], dVar);
	}

	return firstDerivative(place, Temp[i], dVar);
}

FP_TYPE Calculus::firstPolyDeriv(FP_TYPE x, const Array1D<FP_TYPE>& Xval, const Array1D<FP_TYPE>& Yval)
{
	int i, n, power;
	FP_TYPE value;
	Column Factors;
	FunctionBall Func;
	
	Factors = Func.polyFactors(Xval, Yval);
	value = 0;
	n = Xval.length();
	for(i = 0; i < n-1; i++)
	{
		power = n - i - 1;
		if(power == 1) value += Factors[i];
		else value += power * pow(x, power-1) * Factors[i];
	}

	return value;
}
FP_TYPE Calculus::secondPolyDeriv(FP_TYPE x, const Array1D<FP_TYPE>& Xval, const Array1D<FP_TYPE>& Yval)
{
	int i, n, power;
	FP_TYPE value;
	Column Factors;
	FunctionBall Func;
	
	Factors = Func.polyFactors(Xval, Yval);
	value = 0;
	n = Xval.length();
	for(i = 0; i < n-2; i++)
	{
		power = n - i - 1;
		if(power == 2) value += 2 * Factors[i];
		else value += power * (power-1) * pow(x, power-2) * Factors[i];
	}

	return value;
}

