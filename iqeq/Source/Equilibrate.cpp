/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "definitions.h"
#include "String.h"
#include "Geometry.h"
#include "AtomDataBall.h"
#include "InputChargeFitBall.h"

#include <ctime>
#include <cstdlib>
#include <sys/stat.h>
#include <cmath>
#include <iostream>
#include <fstream>
//#include <iomanip>
using namespace std;

#define METHOD_QEQ 0
#define METHOD_EQEQ 1
#define METHOD_FCQEQ 2
#define METHOD_IQEQ 3

int main(int argc, char* argv[])
{
	int i, type, algorithm;
	FP_TYPE energy;
	String FileName, Variant;
	InputChargeFitBall Calc;
	Geometry Molecule;
	AtomDataBall AtomData;
	Array1D<String> Args;

	try
	{
		AtomData.loadData();

		//  Process the arguments
		if(argc < 2)
		{
			cout << "	Usage:  equilibrate [args] <file>" << endl;
			cout << endl;
			cout << "	Arguments are:" << endl;
			cout << "       -alg     Sets the base algorithm.  Option are:" << endl;
			cout << "                qeq   - Specifies the original QEq of Rappe and Goddard" << endl;
			cout << "                eqeq  - Specifies the extended QEq of Wilmer et al" << endl;
			cout << "                fcqeq - Specifies the formal charge QEq" << endl;
			cout << "                iqeq - Specifies the ionizing QEq.  This is the default" << endl;
			cout << endl;
			cout << "       -base    Sets the base charge used in the Taylor series expansion.  Options are:" << endl;
			cout << "                zero  - Use neutral atom properties" << endl;
			cout << "                metal - Use the metal formal charge only" << endl;
			cout << "                fc    - Use the formal charge of each atom" << endl;
			cout << "                ch    - Use the charge of the atom.  This results in an iterative solution" << endl;
			cout << endl;
			cout << "       -der     Sets the type of derivative used in the calculation.  Options are:" << endl;
			cout << "                atom  - Use the pre-defined electronegativity and atomic hardness for each atom" << endl;
			cout << "                diff  - Use simple three point numerical derivatives" << endl;
			cout << "                poly  - Use larger five point numerical derivatives" << endl;
			cout << "                splin - Use a five point cubic spline derivative" << endl;
			cout << endl;
			cout << "       -idem    Sets the type of idempotential for hydrogen.  Options are:" << endl;
			cout << "                atom  - Use the atomic parameters, resulting in a direct solution" << endl;
			cout << "                iter  - Set the idempotential to vary with charge as in the QEq method, resulting in an iterative solution" << endl;
			cout << endl;
			cout << "       -size    Sets the type of atom size used in the calculation.  Options are:" << endl;
			cout << "                atom  - Use the atomic Slater screen size from the QEq method" << endl;
			cout << "                zero  - Use the calculated neutral atom size" << endl;
			cout << "                fc    - Use the calculated formal charge ion size" << endl;
			cout << "                ch    - Use the calculated atomic charge ion size" << endl;
			cout << endl;
			cout << "       -int     Sets the type of integral used to calculate the charge screening.  Options are:" << endl;
			cout << "                slat  - Use the exact Slater integral" << endl;
			cout << "                gauss - Use a Guassian charge distribution" << endl;
			cout << "                nm    - Use the Nishimoto-Mataga approximation of the Slater integral" << endl;
			cout << "                nmw   - Use the Nishimoto-Mataga-Weiss approximation of the Slater integral" << endl;
			cout << "                ohno  - Use the Ohno approximation of the Slater integral" << endl;
			cout << "                ok    - Use the Ohno-Klopman approximation of the Slater integral" << endl;
			cout << "                dh    - Use the DasGupta-Huzinaga approximation of the Slater integral" << endl;
			cout << "                wil   - Use the integral equation from the E-QEq method.  Note that this equation does not use atom size" << endl;
			cout << "                none  - Don't do charge screening and just use te 1/r term" << endl;
			cout << endl;
			cout << "       -init    Sets the charges for the start of the iterative solution.  Options are:" << endl;
			cout << "                zero  - Use zero charges" << endl;
			cout << "                cur   - Use the current charge from the input file" << endl;
			cout << "                form  - Use the current charge" << endl;
			cout << endl;
			cout << "       -lamb    Sets the lambda value used to calculate that atomic charge size.  Specified as a floating point number" << endl;
			cout << endl;
			cout << "       -diel    Sets the dielectric constant value used to calculate that atomic charge size.  Specified as a floating point number" << endl;
			cout << endl;
			cout << "       -step    Sets the maximum number of steps used in finding the solution.  Specified as an integer" << endl;
			cout << endl;

			return 0;
		}

		Args.setSize(argc-2);
		for(i = 0; i < Args.length(); i++) Args[i] = argv[i+1];
		FileName = argv[argc-1];

		//  Find the default method
		algorithm = METHOD_IQEQ;
		for(i = 0; i < Args.length(); i++)
		{
			if(Args[i] == "-alg")
			{
				if(Args[i+1] == "qeq") algorithm = METHOD_QEQ;
				else if(Args[i+1] == "eqeq") algorithm = METHOD_EQEQ;
				else if(Args[i+1] == "fcqeq") algorithm = METHOD_FCQEQ;
				else if(Args[i+1] == "iqeq") algorithm = METHOD_IQEQ;
				else throw ErrorBall("Unrecognised algorithm type");
			}
		}
		switch(algorithm)
		{
			case METHOD_QEQ:
			{
				Calc.setQeqHardnessData("Goddard-Revised");
				Calc.setQeqElectronegData("Goddard-Revised");
				Calc.setQeqAtomIonData("FC-QEq");
				Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO);
				Calc.setQeqDielectric(1.0);
				Calc.setQeqScreenScale(0.5);
				Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE);
				Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM);
				Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM);
				Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER);
				break;
			}
			case METHOD_EQEQ:
			{
				Calc.setQeqHardnessData("Goddard-Revised");
				Calc.setQeqElectronegData("Goddard-Revised");
				Calc.setQeqAtomIonData("E-QEq");
				Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE);
				Calc.setQeqDielectric(1.66666);
				Calc.setQeqScreenScale(0.5);
				Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ATOM);
				Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF);
				Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM);
				Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER);
				break;
			}
			case METHOD_FCQEQ:
			{
				Calc.setQeqHardnessData("Goddard-Revised");
				Calc.setQeqElectronegData("Goddard-Revised");
				Calc.setQeqAtomIonData("FC-QEq");
				Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE);
				Calc.setQeqDielectric(1.0);
				Calc.setQeqScreenScale(0.5);
				Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ATOM);
				Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF);
				Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL);
				Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH);
				break;
			}
			case METHOD_IQEQ:
			{
				Calc.setQeqHardnessData("Goddard-Revised");
				Calc.setQeqElectronegData("Goddard-Revised");
				Calc.setQeqAtomIonData("SC-QEq");
				Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE);
				Calc.setQeqDielectric(1.0);
				Calc.setQeqScreenScale(0.8);
				Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ATOM);
				Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF);
				Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE);
				Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH);
				break;
			}
		}

		//domod: moved before so that they can be overwritten by the options
		Calc.setQeqSteps(100);
		Calc.setQeqInitialCharge(INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO);
		Calc.setQeqLimit(1E-5);

		//  Now check the rest of the arguments
		for(i = 0; i < Args.length(); i += 2)
		{
			if(Args[i] == "-alg");
			else if(Args[i] == "-base")
			{
				if(Args[i+1] == "zero") Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO);
				else if(Args[i+1] == "metal") Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE);
				else if(Args[i+1] == "fc") Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE);
				else if(Args[i+1] == "ch") Calc.setQeqBaseChargeType(INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE);
				else throw ErrorBall("Unknown atom base charge type");
			}
			else if(Args[i] == "-der")
			{	
				if(Args[i+1] == "atom") Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM);
				else if(Args[i+1] == "diff") Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF);
				else if(Args[i+1] == "poly") Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY);
				else if(Args[i+1] == "splin") Calc.setQeqDerivativeType(INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE);
				else throw ErrorBall("Unknown derivative type");
			}
			else if(Args[i] == "-idem")
			{	
				if(Args[i+1] == "atom") Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ATOM);
				else if(Args[i+1] == "iter") Calc.setQeqIdempotentialType(INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE);
				else throw ErrorBall("Unknown idempotential type");
			}
			else if(Args[i] == "-size")
			{	
				if(Args[i+1] == "atom") Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM);
				else if(Args[i+1] == "zero") Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_NEUTRAL);
				else if(Args[i+1] == "fc") Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL);
				else if(Args[i+1] == "ch") Calc.setQeqAtomSize(INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE);
				else throw ErrorBall("Unknown derivative type");
			}
			else if(Args[i] == "-int")
			{
				if(Args[i+1] == "slat") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER);
				else if(Args[i+1] == "gauss") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN);
				else if(Args[i+1] == "nm") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_NM);
				else if(Args[i+1] == "nmw") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_NMW);
				else if(Args[i+1] == "ohno") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_OHNO);
				else if(Args[i+1] == "ok") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_OK);
				else if(Args[i+1] == "dh") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH);
				else if(Args[i+1] == "wil") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER);
				else if(Args[i+1] == "none") Calc.setQeqIntegralType(INPUTCHARGEFITBALL_QEQ_INTEGRAL_NONE);
				else throw ErrorBall("Unknown integral type");
			}
			else if(Args[i] == "-init")
			{	
				if(Args[i+1] == "zero") Calc.setQeqInitialCharge(INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO);
				else if(Args[i+1] == "cur") Calc.setQeqInitialCharge(INPUTCHARGEFITBALL_QEQ_INITIALQ_CURRENT);
				else if(Args[i+1] == "form") Calc.setQeqInitialCharge(INPUTCHARGEFITBALL_QEQ_INITIALQ_FORMAL);
				else throw ErrorBall("Unknown initial charge type");
			}
			else if(Args[i] == "-lamb") Calc.setQeqScreenScale(Args[i+1].stof());
			else if(Args[i] == "-diel") Calc.setQeqDielectric(Args[i+1].stof());
			else if(Args[i] == "-step") Calc.setQeqSteps(Args[i+1].stoi());
			else throw ErrorBall("Unknown argument");
		}

		Molecule.loadXYZGeometry(FileName);
	
		cout << "---------------------------------------------------------------------------------------------" << endl;
		cout << "Equilibrate program for assigning atomic partial charges on the basis of charge equilibration" << endl;
		cout << "---------------------------------------------------------------------------------------------" << endl;
		cout << endl;

		//  Set the electronegativity if I need to
		if(Calc.QeqElectronegData() != "")
		{
			Variant = Calc.QeqElectronegData();
			for(i = 0; i < Molecule.atomNo(); i++)
			{
				type = Molecule.Atoms(i).type();
				if(AtomData.electronegDefined(Variant, type) == true)
				{
					Molecule.changeAtoms(i).setElectroNeg(AtomData.getElectroneg(Variant, type));
				}
			}
		}

		//  Set the hardness if I need to
		if(Calc.QeqHardnessData() != "")
		{
			Variant = Calc.QeqHardnessData();
			for(i = 0; i < Molecule.atomNo(); i++)
			{
				type = Molecule.Atoms(i).type();
				if(AtomData.hardnessDefined(Variant, type) == true)
				{
					Molecule.changeAtoms(i).setHardness(AtomData.getHardness(Variant, type));
				}
			}
		}

		//  Now set the initial charges
		switch(Calc.qeqInitialCharge())
		{
			case INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO:
			{
				for(i = 0; i < Molecule.atomNo(); i++)
				{
					Molecule.changeAtoms(i).setCharge(0);
				}
				break;
			}
			case INPUTCHARGEFITBALL_QEQ_INITIALQ_FORMAL:
			{
				for(i = 0; i < Molecule.atomNo(); i++)
				{
					Molecule.changeAtoms(i).setCharge(Molecule.Atoms(i).formalCharge());
				}
			}
		}

		energy = Molecule.chargeGeomEquilibrate(cout, Calc);

		cout << endl << "Charges:" << endl;
		for(i = 0; i < Molecule.atomNo(); i++)
		{
			cout << setw(10) << left << Molecule.Atoms(i).Symbol();
			cout << setw(20) << right << Molecule.Atoms(i).charge();
			cout << endl;
		}
		cout << endl;

		cout << "Solution Energy:  " << energy << " kcal/mol" << endl << endl;

		Molecule.printXYZGeometry(FileName);
	}
	catch(ErrorBall Err)
	{
		Err.print();
		cout << Err.Message() << endl;
	}


	return 0;
}
