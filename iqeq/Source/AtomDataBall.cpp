
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "AtomDataBall.h"
#include "XmlLine.h"
#include "Atom.h"
#include "definitions.h"

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

String AtomDataBall::Variant_val("Default");
Array1D<String> AtomDataBall::VariantNames_val;
Array3D<String> AtomDataBall::VariantSets_val;
Array2D<String> AtomDataBall::BlockNames_val(ATOM_DATA_TOTAL);
Array3D<bool> AtomDataBall::DataDefined_val(ATOM_DATA_TOTAL);
Array2D<String> AtomDataBall::SymbolData_val;
Array2D<String> AtomDataBall::NameData_val;
Array2D<int> AtomDataBall::ShellData_val;
Array2D<int> AtomDataBall::ValenceElecData_val;
Array2D<int> AtomDataBall::ValenceBondData_val;
Array2D<int> AtomDataBall::MinChargeData_val;
Array2D<int> AtomDataBall::MaxChargeData_val;
Array2D<FP_TYPE> AtomDataBall::MassData_val;
Array2D<FP_TYPE> AtomDataBall::ElectronegData_val;
Array2D<FP_TYPE> AtomDataBall::HardnessData_val;
Array2D<FP_TYPE> AtomDataBall::SlaterScreenData_val;
Array2D<FP_TYPE> AtomDataBall::GammaScreenData_val;
Array2D<FP_TYPE> AtomDataBall::VdwRadiusData_val;
Array2D<FP_TYPE> AtomDataBall::SurfaceRadiusData_val;
Array2D<FP_TYPE> AtomDataBall::CovalentRadiusData_val;
Array2D<FP_TYPE> AtomDataBall::SlaterRadiusData_val;
Array3D<FP_TYPE> AtomDataBall::AtomIonEnergyData_val;
Array3D<int> AtomDataBall::AtomIonEnergyStateData_val;
Array3D<FP_TYPE> AtomDataBall::AtomIonSizeData_val;
Array3D<int> AtomDataBall::AtomIonSizeStateData_val;

void AtomDataBall::setVarient(const String& Data)
{
	Variant_val = Data;
}
void AtomDataBall::loadData()
{
	String FileName;
	ifstream Input;
	XmlLine Data;
	int i, j, k, size;
	String Temp;

	FileName << ATOM_DATA_FILE_PATH & "AtomData.sad";
	Input.open(FileName.pointer(), ios::in);
	if(Input.is_open() == false) 
	{
		throw ErrorBall("Unable to open AtomData file in AtomDataBall::loadData");
	}

	Input >> Data;
	Input >> Data;
	while(Data.LineName() != "AtomData" || Data.isEnd() == false)
	{
		//  If reading in the variant data
		if(Data.LineName() == "Versions")
		{
			size = Data.TagValueInt("Number");
			VariantNames_val.setSize(size);
			VariantSets_val.setSize(size, ATOM_DATA_TOTAL);

			//  Read through all of the versions
			Input >> Data;
			for(i = 0; Data.LineName() != "Versions" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Variant")
				{
					VariantNames_val[i] = Data.TagValue("Name");

					//  Read through all of the versions
					Input >> Data;
					while(Data.LineName() != "Variant" || Data.isEnd() == false)
					{
						Temp = Data.TagValue("Name");
						if(Data.LineName() == "SymbolBlock") VariantSets_val[i][ATOM_DATA_SYMBOL].push(Temp);
						else if(Data.LineName() == "NameBlock") VariantSets_val[i][ATOM_DATA_NAME].push(Temp);
						else if(Data.LineName() == "ShellBlock") VariantSets_val[i][ATOM_DATA_SHELL].push(Temp);
						else if(Data.LineName() == "ValenceElecBlock") VariantSets_val[i][ATOM_DATA_VALENCE_ELEC].push(Temp);
						else if(Data.LineName() == "ValenceBondBlock") VariantSets_val[i][ATOM_DATA_VALENCE_BOND].push(Temp);
						else if(Data.LineName() == "MinChargeBlock") VariantSets_val[i][ATOM_DATA_MINCHARGE].push(Temp);
						else if(Data.LineName() == "MaxChargeBlock") VariantSets_val[i][ATOM_DATA_MAXCHARGE].push(Temp);
						else if(Data.LineName() == "MassBlock") VariantSets_val[i][ATOM_DATA_MASS].push(Temp);
						else if(Data.LineName() == "ElectroNegBlock") VariantSets_val[i][ATOM_DATA_ELECTRONEG].push(Temp);
						else if(Data.LineName() == "HardnessBlock") VariantSets_val[i][ATOM_DATA_HARDNESS].push(Temp);
						else if(Data.LineName() == "SlaterScreenBlock") VariantSets_val[i][ATOM_DATA_SLATER_SCREEN].push(Temp);
						else if(Data.LineName() == "GammaScreenBlock") VariantSets_val[i][ATOM_DATA_GAMMA_SCREEN].push(Temp);
						else if(Data.LineName() == "VdwRadiusBlock") VariantSets_val[i][ATOM_DATA_VDW_RADIUS].push(Temp);
						else if(Data.LineName() == "SurfaceRadiusBlock") VariantSets_val[i][ATOM_DATA_SURFACE_RADIUS].push(Temp);
						else if(Data.LineName() == "CovalentRadiusBlock") VariantSets_val[i][ATOM_DATA_COVALENT_RADIUS].push(Temp);
						else if(Data.LineName() == "SlaterRadiusBlock") VariantSets_val[i][ATOM_DATA_SLATER_RADIUS].push(Temp);
						else if(Data.LineName() == "AtomIonEnergyBlock") VariantSets_val[i][ATOM_DATA_ATOM_ION_ENERGY].push(Temp);
						else if(Data.LineName() == "AtomIonSizeBlock") VariantSets_val[i][ATOM_DATA_ATOM_ION_SIZE].push(Temp);
						
						Input >> Data;
					}
				}

				Input >> Data;
			}
		}
		else if(Data.LineName() == "SymbolList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			SymbolData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_SYMBOL].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_SYMBOL][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "SymbolList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Symbol")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_SYMBOL].push(Temp);

					Input >> Data;
					while(Data.LineName() != "Symbol" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							SymbolData_val[i][j] = Data.TagValue("Symbol");
							DataDefined_val[ATOM_DATA_SYMBOL][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "NameList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			NameData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_NAME].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_NAME][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "NameList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Name")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_NAME].push(Temp);

					Input >> Data;
					while(Data.LineName() != "Name" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							NameData_val[i][j] = Data.TagValue("Name");
							DataDefined_val[ATOM_DATA_NAME][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "ShellList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			ShellData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_SHELL].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_SHELL][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "ShellList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Shell")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_SHELL].push(Temp);

					Input >> Data;
					while(Data.LineName() != "Shell" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							ShellData_val[i][j] = Data.TagValueInt("Shell");
							DataDefined_val[ATOM_DATA_SHELL][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "ValenceElecList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			ValenceElecData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_VALENCE_ELEC].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_VALENCE_ELEC][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "ValenceElecList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "ValenceElec")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_VALENCE_ELEC].push(Temp);

					Input >> Data;
					while(Data.LineName() != "ValenceElec" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							ValenceElecData_val[i][j] = Data.TagValueInt("ValenceElec");
							DataDefined_val[ATOM_DATA_VALENCE_ELEC][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "ValenceBondList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			ValenceBondData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_VALENCE_BOND].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_VALENCE_BOND][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "ValenceBondList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "ValenceBond")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_VALENCE_BOND].push(Temp);

					Input >> Data;
					while(Data.LineName() != "ValenceBond" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							ValenceBondData_val[i][j] = Data.TagValueInt("ValenceBond");
							DataDefined_val[ATOM_DATA_VALENCE_BOND][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "MinChargeList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			MinChargeData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_MINCHARGE].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_MINCHARGE][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "MinChargeList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "MinCharge")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_MINCHARGE].push(Temp);

					Input >> Data;
					while(Data.LineName() != "MinCharge" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							MinChargeData_val[i][j] = Data.TagValueInt("MinCharge");
							DataDefined_val[ATOM_DATA_MINCHARGE][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "MaxChargeList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			MaxChargeData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_MAXCHARGE].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_MAXCHARGE][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "MaxChargeList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "MaxCharge")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_MAXCHARGE].push(Temp);

					Input >> Data;
					while(Data.LineName() != "MaxCharge" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							MaxChargeData_val[i][j] = Data.TagValueInt("MaxCharge");
							DataDefined_val[ATOM_DATA_MAXCHARGE][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "MassList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			MassData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_MASS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_MASS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "MassList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Mass")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_MASS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "Mass" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							MassData_val[i][j] = Data.TagValueDouble("Mass");
							DataDefined_val[ATOM_DATA_MASS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "ElectroNegList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			ElectronegData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_ELECTRONEG].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_ELECTRONEG][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "ElectroNegList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "ElectroNeg")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_ELECTRONEG].push(Temp);

					Input >> Data;
					while(Data.LineName() != "ElectroNeg" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							ElectronegData_val[i][j] = Data.TagValueDouble("ElectroNeg");
							DataDefined_val[ATOM_DATA_ELECTRONEG][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "HardnessList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			HardnessData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_HARDNESS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_HARDNESS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "HardnessList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "Hardness")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_HARDNESS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "Hardness" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							HardnessData_val[i][j] = Data.TagValueDouble("Hardness");
							DataDefined_val[ATOM_DATA_HARDNESS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "SlaterScreenList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			SlaterScreenData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_SLATER_SCREEN].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_SLATER_SCREEN][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "SlaterScreenList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "SlaterScreen")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_SLATER_SCREEN].push(Temp);

					Input >> Data;
					while(Data.LineName() != "SlaterScreen" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							SlaterScreenData_val[i][j] = Data.TagValueDouble("Slater");
							DataDefined_val[ATOM_DATA_SLATER_SCREEN][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "GammaScreenList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			GammaScreenData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_GAMMA_SCREEN].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_GAMMA_SCREEN][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "GammaScreenList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "GammaScreen")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_GAMMA_SCREEN].push(Temp);

					Input >> Data;
					while(Data.LineName() != "GammaScreen" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							GammaScreenData_val[i][j] = Data.TagValueDouble("Gamma");
							DataDefined_val[ATOM_DATA_GAMMA_SCREEN][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "VdwRadiusList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			VdwRadiusData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_VDW_RADIUS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_VDW_RADIUS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "VdwRadiusList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "VdwRadius")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_VDW_RADIUS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "VdwRadius" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							VdwRadiusData_val[i][j] = Data.TagValueDouble("VdwRadius");
							DataDefined_val[ATOM_DATA_VDW_RADIUS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "SurfaceRadiusList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			SurfaceRadiusData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_SURFACE_RADIUS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_SURFACE_RADIUS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "SurfaceRadiusList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "SurfaceRadius")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_SURFACE_RADIUS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "SurfaceRadius" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							SurfaceRadiusData_val[i][j] = Data.TagValueDouble("SurfaceRadius");
							DataDefined_val[ATOM_DATA_SURFACE_RADIUS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "CovalentRadiusList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			CovalentRadiusData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_COVALENT_RADIUS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_COVALENT_RADIUS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "CovalentRadiusList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "CovalentRadius")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_COVALENT_RADIUS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "CovalentRadius" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							CovalentRadiusData_val[i][j] = Data.TagValueDouble("CovalentRadius");
							DataDefined_val[ATOM_DATA_COVALENT_RADIUS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "SlaterRadiusList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			SlaterRadiusData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_SLATER_RADIUS].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_SLATER_RADIUS][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "SlaterRadiusList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "SlaterRadius")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_SLATER_RADIUS].push(Temp);

					Input >> Data;
					while(Data.LineName() != "SlaterRadius" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							SlaterRadiusData_val[i][j] = Data.TagValueDouble("SlaterRadius");
							DataDefined_val[ATOM_DATA_SLATER_RADIUS][i][j] = true;
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "AtomIonEnergyList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			AtomIonEnergyData_val.setSize(size, ATOM_ATOMNO);
			AtomIonEnergyStateData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_ATOM_ION_ENERGY].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_ATOM_ION_ENERGY][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "AtomIonEnergyList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "AtomIonEnergy")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_ATOM_ION_ENERGY].push(Temp);

					Input >> Data;
					while(Data.LineName() != "AtomIonEnergy" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							size = Data.TagValueInt("StateNo");
							AtomIonEnergyData_val[i][j].setSize(size);
							AtomIonEnergyStateData_val[i][j].setSize(size);
							DataDefined_val[ATOM_DATA_ATOM_ION_ENERGY][i][j] = true;
						
							Input >> Data;
							k = 0;
							while(Data.LineName() != "Atom" || Data.isEnd() == false)
							{
								AtomIonEnergyData_val[i][j][k] = Data.TagValueDouble("Data");
								AtomIonEnergyStateData_val[i][j][k] = Data.TagValueInt("Number");
								Input >> Data;
								k++;
							}
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		else if(Data.LineName() == "AtomIonSizeList")
		{
			//  Set the size of the arrays
			size = Data.TagValueInt("Number");
			AtomIonSizeData_val.setSize(size, ATOM_ATOMNO);
			AtomIonSizeStateData_val.setSize(size, ATOM_ATOMNO);
			DataDefined_val[ATOM_DATA_ATOM_ION_SIZE].setSize(size, ATOM_ATOMNO);
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < ATOM_ATOMNO; j++) DataDefined_val[ATOM_DATA_ATOM_ION_SIZE][i][j] = false;
			}

			Input >> Data;
			for(i = 0; Data.LineName() != "AtomIonSizeList" || Data.isEnd() == false; i++)
			{
				if(Data.LineName() == "AtomIonSize")
				{
					Temp = Data.TagValue("Name");
					BlockNames_val[ATOM_DATA_ATOM_ION_SIZE].push(Temp);

					Input >> Data;
					while(Data.LineName() != "AtomIonSize" || Data.isEnd() == false)
					{
						if(Data.LineName() == "Atom")
						{
							j = Data.TagValueInt("Number");
							size = Data.TagValueInt("StateNo");
							AtomIonSizeData_val[i][j].setSize(size);
							AtomIonSizeStateData_val[i][j].setSize(size);
							DataDefined_val[ATOM_DATA_ATOM_ION_SIZE][i][j] = true;
						
							Input >> Data;
							k = 0;
							while(Data.LineName() != "Atom" || Data.isEnd() == false)
							{
								AtomIonSizeData_val[i][j][k] = Data.TagValueDouble("Data");
								AtomIonSizeStateData_val[i][j][k] = Data.TagValueInt("Number");
								Input >> Data;
								k++;
							}
						}
						Input >> Data;
					}
				}
				Input >> Data;
			}
		}

		Input >> Data;
	}

	Input.close();
}

//  Some readers to get size information
int AtomDataBall::variantNo() const					{ return VariantNames_val.length(); }
int AtomDataBall::SymbolSetNo() const				{ return BlockNames_val[ATOM_DATA_SYMBOL].length(); }
int AtomDataBall::NameSetNo() const					{ return BlockNames_val[ATOM_DATA_NAME].length(); }
int AtomDataBall::shellSetNo() const				{ return BlockNames_val[ATOM_DATA_SHELL].length(); }
int AtomDataBall::valenceElecSetNo() const			{ return BlockNames_val[ATOM_DATA_VALENCE_ELEC].length(); }
int AtomDataBall::valenceBondSetNo() const			{ return BlockNames_val[ATOM_DATA_VALENCE_BOND].length(); }
int AtomDataBall::minChargeSetNo() const			{ return BlockNames_val[ATOM_DATA_MINCHARGE].length(); }
int AtomDataBall::maxChargeSetNo() const			{ return BlockNames_val[ATOM_DATA_MAXCHARGE].length(); }
int AtomDataBall::massSetNo() const					{ return BlockNames_val[ATOM_DATA_MASS].length(); }
int AtomDataBall::electronegSetNo() const			{ return BlockNames_val[ATOM_DATA_ELECTRONEG].length(); }
int AtomDataBall::hardnessSetNo() const				{ return BlockNames_val[ATOM_DATA_HARDNESS].length(); }
int AtomDataBall::slaterScreenSetNo() const			{ return BlockNames_val[ATOM_DATA_SLATER_SCREEN].length(); }
int AtomDataBall::gammaScreenSetNo() const			{ return BlockNames_val[ATOM_DATA_GAMMA_SCREEN].length(); }
int AtomDataBall::vdwRadiusSetNo() const			{ return BlockNames_val[ATOM_DATA_VDW_RADIUS].length(); }
int AtomDataBall::surfaceRadiusSetNo() const		{ return BlockNames_val[ATOM_DATA_SURFACE_RADIUS].length(); }
int AtomDataBall::covalentRadiusSetNo() const		{ return BlockNames_val[ATOM_DATA_COVALENT_RADIUS].length(); }
int AtomDataBall::slaterRadiusSetNo() const			{ return BlockNames_val[ATOM_DATA_SLATER_RADIUS].length(); }
int AtomDataBall::AtomIonEnergyStateSetNo() const	{ return BlockNames_val[ATOM_DATA_ATOM_ION_ENERGY].length(); }
int AtomDataBall::AtomIonSizeStateSetNo() const		{ return BlockNames_val[ATOM_DATA_ATOM_ION_SIZE].length(); }

int AtomDataBall::variantBlockNo(int variant, int blockType) const
{
	return VariantSets_val[variant][blockType].length();
}
const String& AtomDataBall::VariantBlockName(int variant, int blockType, int i) const
{
	return VariantSets_val[variant][blockType][i];
}

//  Some readers to get the block names
const String& AtomDataBall::VariantName(int i) const			{ return VariantNames_val[i]; }
const String& AtomDataBall::SymbolName(int i) const				{ return BlockNames_val[ATOM_DATA_SYMBOL][i]; }
const String& AtomDataBall::NameName(int i) const				{ return BlockNames_val[ATOM_DATA_NAME][i]; }
const String& AtomDataBall::shellName(int i) const				{ return BlockNames_val[ATOM_DATA_SHELL][i]; }
const String& AtomDataBall::valenceElecName(int i) const		{ return BlockNames_val[ATOM_DATA_VALENCE_ELEC][i]; }
const String& AtomDataBall::valenceBondName(int i) const		{ return BlockNames_val[ATOM_DATA_VALENCE_BOND][i]; }
const String& AtomDataBall::minChargeName(int i) const			{ return BlockNames_val[ATOM_DATA_MINCHARGE][i]; }
const String& AtomDataBall::maxChargeName(int i) const			{ return BlockNames_val[ATOM_DATA_MAXCHARGE][i]; }
const String& AtomDataBall::massName(int i) const				{ return BlockNames_val[ATOM_DATA_MASS][i]; }
const String& AtomDataBall::electronegName(int i) const			{ return BlockNames_val[ATOM_DATA_ELECTRONEG][i]; }
const String& AtomDataBall::hardnessName(int i) const			{ return BlockNames_val[ATOM_DATA_HARDNESS][i]; }
const String& AtomDataBall::slaterScreenName(int i) const		{ return BlockNames_val[ATOM_DATA_SLATER_SCREEN][i]; }
const String& AtomDataBall::gammaScreenName(int i) const		{ return BlockNames_val[ATOM_DATA_GAMMA_SCREEN][i]; }
const String& AtomDataBall::vdwRadiusName(int i) const			{ return BlockNames_val[ATOM_DATA_VDW_RADIUS][i]; }
const String& AtomDataBall::surfaceRadiusName(int i) const		{ return BlockNames_val[ATOM_DATA_SURFACE_RADIUS][i]; }
const String& AtomDataBall::covalentRadiusName(int i) const		{ return BlockNames_val[ATOM_DATA_COVALENT_RADIUS][i]; }
const String& AtomDataBall::slaterRadiusName(int i) const		{ return BlockNames_val[ATOM_DATA_SLATER_RADIUS][i]; }
const String& AtomDataBall::AtomIonEnergyName(int i) const		{ return BlockNames_val[ATOM_DATA_ATOM_ION_ENERGY][i]; }
const String& AtomDataBall::AtomIonSizeName(int i) const		{ return BlockNames_val[ATOM_DATA_ATOM_ION_SIZE][i]; }

//  Readers for whether or not the variable is defined for the standard orientation
bool AtomDataBall::variableDefined(int type, int atom) const
{
	int i, block, variant;
	bool result;

	variant = VariantNames_val.search(Variant_val);

	result = false;
	for(i = 0; i < VariantSets_val[variant][type].length() && result == false; i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		result = DataDefined_val[type][block][atom];
	}

	return result;
}

bool AtomDataBall::symbolDefined(int atom) const			{ return variableDefined(ATOM_DATA_SYMBOL, atom); }
bool AtomDataBall::nameDefined(int atom) const				{ return variableDefined(ATOM_DATA_NAME, atom); }
bool AtomDataBall::shellDefined(int atom) const				{ return variableDefined(ATOM_DATA_SHELL, atom); }
bool AtomDataBall::valenceElecDefined(int atom) const		{ return variableDefined(ATOM_DATA_VALENCE_ELEC, atom); }
bool AtomDataBall::valenceBondDefined(int atom) const		{ return variableDefined(ATOM_DATA_VALENCE_BOND, atom); }
bool AtomDataBall::minChargeDefined(int atom) const			{ return variableDefined(ATOM_DATA_MINCHARGE, atom); }
bool AtomDataBall::maxChargeDefined(int atom) const			{ return variableDefined(ATOM_DATA_MAXCHARGE, atom); }
bool AtomDataBall::massDefined(int atom) const				{ return variableDefined(ATOM_DATA_MASS, atom); }
bool AtomDataBall::electronegDefined(int atom) const		{ return variableDefined(ATOM_DATA_ELECTRONEG, atom); }
bool AtomDataBall::hardnessDefined(int atom) const			{ return variableDefined(ATOM_DATA_HARDNESS, atom); }
bool AtomDataBall::slaterScreenDefined(int atom) const		{ return variableDefined(ATOM_DATA_SLATER_SCREEN, atom); }
bool AtomDataBall::gammaScreenDefined(int atom) const		{ return variableDefined(ATOM_DATA_GAMMA_SCREEN, atom); }
bool AtomDataBall::vdwRadiusDefined(int atom) const			{ return variableDefined(ATOM_DATA_VDW_RADIUS, atom); }
bool AtomDataBall::surfaceRadiusDefined(int atom) const		{ return variableDefined(ATOM_DATA_SURFACE_RADIUS, atom); }
bool AtomDataBall::covalentRadiusDefined(int atom) const	{ return variableDefined(ATOM_DATA_COVALENT_RADIUS, atom); }
bool AtomDataBall::slaterRadiusDefined(int atom) const		{ return variableDefined(ATOM_DATA_SLATER_RADIUS, atom); }
bool AtomDataBall::AtomIonEnergyDefined(int atom) const		{ return variableDefined(ATOM_DATA_ATOM_ION_ENERGY, atom); }
bool AtomDataBall::AtomIonSizeDefined(int atom) const		{ return variableDefined(ATOM_DATA_ATOM_ION_SIZE, atom); }

//  Readers for the standard definition
int AtomDataBall::getSymbolIndex(const String& Symbol) const
{
	int i, j, variant, block;
	int type = ATOM_DATA_SYMBOL;
	int result = -1;

	variant = VariantNames_val.search(Variant_val);
	for(i = 0; i < VariantSets_val[variant][type].length() && result < 0; i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		for(j = 0; j < ATOM_ATOMNO && result < 0; j++)
		{
			if(Symbol == SymbolData_val[block][j]) result = j;
		}
	}

	return result;
}
const String AtomDataBall::getSymbol(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_SYMBOL;
	String Error;
	String Result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		Result = "";
	}
	else
	{
		for(i = 0; i < VariantSets_val[variant][type].length(); i++)
		{
			block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
			if(DataDefined_val[type][block][atom] == true) Result = SymbolData_val[block][atom];
		}
	}

	return Result;
}
const String AtomDataBall::getName(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_NAME;
	String Error;
	String Result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		Result = "";
	}
	else
	{
		for(i = 0; i < VariantSets_val[variant][type].length(); i++)
		{
			block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
			if(DataDefined_val[type][block][atom] == true) Result = NameData_val[block][atom];
		}
	}

	return Result;
}
int AtomDataBall::getShell(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_SHELL;
	String Error;
	int result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = ShellData_val[block][atom];
	}

	return result;
}
int AtomDataBall::getValenceElec(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_VALENCE_ELEC;
	String Error;
	int result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = ValenceElecData_val[block][atom];
	}

	return result;
}
int AtomDataBall::getValenceBond(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_VALENCE_BOND;
	String Error;
	int result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = ValenceBondData_val[block][atom];
	}

	return result;
}
int AtomDataBall::getMinCharge(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_MINCHARGE;
	String Error;
	int result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = MinChargeData_val[block][atom];
	}

	return result;
}
int AtomDataBall::getMaxCharge(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_MAXCHARGE;
	String Error;
	int result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = MaxChargeData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getMass(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_MASS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = MassData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getElectroneg(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_ELECTRONEG;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}
	
	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = ElectronegData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getHardness(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_HARDNESS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = HardnessData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getSlaterScreen(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_SLATER_SCREEN;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = SlaterScreenData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getGammaScreen(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_GAMMA_SCREEN;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = GammaScreenData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getVdwRadius(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_VDW_RADIUS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = VdwRadiusData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getSurfaceRadius(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_SURFACE_RADIUS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = SurfaceRadiusData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getCovalentRadius(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_COVALENT_RADIUS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = CovalentRadiusData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getSlaterRadius(int atom) const
{
	int i, block, variant;
	int type = ATOM_DATA_SLATER_RADIUS;
	String Error;
	FP_TYPE result;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) result = SlaterRadiusData_val[block][atom];
	}

	return result;
}
FP_TYPE AtomDataBall::getAtomIonEnergy(int atom, int state) const
{
	int i, block, variant, place;
	int type = ATOM_DATA_ATOM_ION_ENERGY;
	FP_TYPE result;
	String ErrorString;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			place = AtomIonEnergyStateData_val[block][atom].search(state);
			if(place < 0)
			{
				ErrorString << "Cannot find energy for ionisation state " & state & " for atom number " & atom+1 & " in AtomDataBall::getAtomIonEnergy(int, int)";
				throw ErrorString;
			}
			result = AtomIonEnergyData_val[block][atom][place];
		}
	}

	return result;
}
int AtomDataBall::getAtomIonEnergyStateBase(int atom) const
{
	int result, variant, block, i, j;
	int type = ATOM_DATA_ATOM_ION_ENERGY;

	variant = VariantNames_val.search(Variant_val);
	result = 100;
	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			for(j = 0; j < AtomIonEnergyStateData_val[block][atom].length(); j++)
			{
				result = (AtomIonEnergyStateData_val[block][atom][j] < result) ? AtomIonEnergyStateData_val[block][atom][j] : result;
			}
		}
	}

	return result;
}
int AtomDataBall::getAtomIonEnergyNumber(int atom) const
{
	int result, variant, block, i;
	int type = ATOM_DATA_ATOM_ION_ENERGY;

	variant = VariantNames_val.search(Variant_val);
	result = 0;
	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			result = (AtomIonEnergyStateData_val[block][atom].length() > result) ? AtomIonEnergyStateData_val[block][atom].length() : result;
		}
	}

	return result;
}
FP_TYPE AtomDataBall::getAtomIonSize(int atom, int state) const
{
	int i, block, variant, place;
	int type = ATOM_DATA_ATOM_ION_ENERGY;
	FP_TYPE result;
	String ErrorString;

	variant = VariantNames_val.search(Variant_val);
	if(variableDefined(type, atom) == false)
	{
		return 0;
	}

	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			place = AtomIonSizeStateData_val[block][atom].search(state);
			if(place < 0)
			{
				ErrorString << "Cannot find energy for ionisation state " & state & " for atom number " & atom+1 & " in AtomDataBall::getAtomIonSize(int, int)";
				throw ErrorBall(ErrorString);
			}
			result = AtomIonSizeData_val[block][atom][place];
		}
	}

	return result;
}
int AtomDataBall::getAtomIonSizeStateBase(int atom) const
{
	int result, variant, block, i, j;
	int type = ATOM_DATA_ATOM_ION_ENERGY;

	variant = VariantNames_val.search(Variant_val);
	result = 100;
	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			for(j = 0; j < AtomIonSizeStateData_val[block][atom].length(); j++)
			{
				result = (AtomIonSizeStateData_val[block][atom][j] < result) ? AtomIonSizeStateData_val[block][atom][j] : result;
			}
		}
	}

	return result;
}
int AtomDataBall::getAtomIonSizeNumber(int atom) const
{
	int result, variant, block, i;
	int type = ATOM_DATA_ATOM_ION_ENERGY;

	variant = VariantNames_val.search(Variant_val);
	result = 0;
	for(i = 0; i < VariantSets_val[variant][type].length(); i++)
	{
		block = BlockNames_val[type].search(VariantSets_val[variant][type][i]);
		if(DataDefined_val[type][block][atom] == true) 
		{
			result = (AtomIonSizeStateData_val[block][atom].length() > result) ? AtomIonSizeStateData_val[block][atom].length() : result;
		}
	}

	return result;
}

bool AtomDataBall::variableDefined(const String& BlockName, int type, int atom) const
{
	int block = BlockNames_val[type].search(BlockName);
	return DataDefined_val[type][block][atom];
}

//  Readers for whether or not the variable for a given block is defined
bool AtomDataBall::symbolDefined(const String& BlockName, int atom) const				{ return variableDefined(BlockName, ATOM_DATA_SYMBOL, atom); }
bool AtomDataBall::nameDefined(const String& BlockName, int atom) const					{ return variableDefined(BlockName, ATOM_DATA_NAME, atom); }
bool AtomDataBall::shellDefined(const String& BlockName, int atom) const				{ return variableDefined(BlockName, ATOM_DATA_SHELL, atom); }
bool AtomDataBall::valenceElecDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_VALENCE_ELEC, atom); }
bool AtomDataBall::valenceBondDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_VALENCE_BOND, atom); }
bool AtomDataBall::minChargeDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_MINCHARGE, atom); }
bool AtomDataBall::maxChargeDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_MAXCHARGE, atom); }
bool AtomDataBall::massDefined(const String& BlockName, int atom) const					{ return variableDefined(BlockName, ATOM_DATA_MASS, atom); }
bool AtomDataBall::electronegDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_ELECTRONEG, atom); }
bool AtomDataBall::hardnessDefined(const String& BlockName, int atom) const				{ return variableDefined(BlockName, ATOM_DATA_HARDNESS, atom); }
bool AtomDataBall::slaterScreenDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_SLATER_SCREEN, atom); }
bool AtomDataBall::gammaScreenDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_GAMMA_SCREEN, atom); }
bool AtomDataBall::vdwRadiusDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_VDW_RADIUS, atom); }
bool AtomDataBall::surfaceRadiusDefined(const String& BlockName, int atom) const		{ return variableDefined(BlockName, ATOM_DATA_SURFACE_RADIUS, atom); }
bool AtomDataBall::covalentRadiusDefined(const String& BlockName, int atom) const		{ return variableDefined(BlockName, ATOM_DATA_COVALENT_RADIUS, atom); }
bool AtomDataBall::slaterRadiusDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_SLATER_RADIUS, atom); }
bool AtomDataBall::AtomIonEnergyDefined(const String& BlockName, int atom) const		{ return variableDefined(BlockName, ATOM_DATA_ATOM_ION_ENERGY, atom); }
bool AtomDataBall::AtomIonSizeDefined(const String& BlockName, int atom) const			{ return variableDefined(BlockName, ATOM_DATA_ATOM_ION_SIZE, atom); }

//  Readers for the data in a specific data set
int AtomDataBall::getSymbolIndex(const String& BlockName, const String& Symbol) const
{
	int type = ATOM_DATA_SYMBOL;
	int block = BlockNames_val[type].search(BlockName);
	int result, i;

	result = -1;
	for(i = 0; i < ATOM_ATOMNO && result < 0; i++)
	{
		if(Symbol == SymbolData_val[block][i]) result = i;
	}

	return result;
}
const String& AtomDataBall::getSymbol(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_SYMBOL;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return SymbolData_val[block][atom];
}
const String AtomDataBall::getName(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_NAME;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return NameData_val[block][atom];
}
int AtomDataBall::getShell(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_SHELL;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return ShellData_val[block][atom];
}
int AtomDataBall::getValenceElec(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_VALENCE_ELEC;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getValenceElec";
		throw ErrorBall(Error);
	}

	return ValenceElecData_val[block][atom];
}
int AtomDataBall::getValenceBond(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_VALENCE_BOND;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getValenceBond";
		throw ErrorBall(Error);
	}

	return ValenceBondData_val[block][atom];
}
int AtomDataBall::getMinCharge(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_MINCHARGE;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return MinChargeData_val[block][atom];
}
int AtomDataBall::getMaxCharge(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_MAXCHARGE;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return MaxChargeData_val[block][atom];
}
FP_TYPE AtomDataBall::getMass(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_MASS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return MassData_val[block][atom];
}
FP_TYPE AtomDataBall::getElectroneg(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_ELECTRONEG;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return ElectronegData_val[block][atom];
}
FP_TYPE AtomDataBall::getHardness(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_HARDNESS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return HardnessData_val[block][atom];
}
FP_TYPE AtomDataBall::getSlaterScreen(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_SLATER_SCREEN;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return SlaterScreenData_val[block][atom];
}
FP_TYPE AtomDataBall::getGammaScreen(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_GAMMA_SCREEN;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return GammaScreenData_val[block][atom];
}
FP_TYPE AtomDataBall::getVdwRadius(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_VDW_RADIUS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return VdwRadiusData_val[block][atom];
}
FP_TYPE AtomDataBall::getSurfaceRadius(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_SURFACE_RADIUS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return SurfaceRadiusData_val[block][atom];
}
FP_TYPE AtomDataBall::getCovalentRadius(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_COVALENT_RADIUS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return CovalentRadiusData_val[block][atom];
}
FP_TYPE AtomDataBall::getSlaterRadius(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_SLATER_RADIUS;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getSymbol";
		throw ErrorBall(Error);
	}

	return SlaterRadiusData_val[block][atom];
}
FP_TYPE AtomDataBall::getAtomIonEnergy(const String& BlockName, int atom, int state) const
{
	int place;
	int type = ATOM_DATA_ATOM_ION_ENERGY;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonEnergy";
		throw ErrorBall(Error);
	}

	place = AtomIonEnergyStateData_val[block][atom].search(state);
	if(place < 0)
	{
		Error << "Cannot find energy for ionisation state " & state & " for atom number " & atom+1 & " in AtomDataBall::getAtomIonEnergy(int, int)";
		throw Error;
	}

	return AtomIonEnergyData_val[block][atom][place];
}
int AtomDataBall::getAtomIonEnergyStateBase(const String& BlockName, int atom) const
{
	int result, i;
	int type = ATOM_DATA_ATOM_ION_ENERGY;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonEnergyStateBase";
		throw ErrorBall(Error);
	}

	result = 100;
	for(i = 0; i < AtomIonEnergyStateData_val[block][atom].length(); i++)
	{
		result = (AtomIonEnergyStateData_val[block][atom][i] < result) ? AtomIonEnergyStateData_val[block][atom][i] : result;
	}

	return result;
}
int AtomDataBall::getAtomIonEnergyNumber(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_ATOM_ION_ENERGY;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonEnergyNumber";
		throw ErrorBall(Error);
	}

	return AtomIonEnergyStateData_val[block][atom].length();
}
FP_TYPE AtomDataBall::getAtomIonSize(const String& BlockName, int atom, int state) const
{
	int place;
	int type = ATOM_DATA_ATOM_ION_SIZE;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonSize";
		throw ErrorBall(Error);
	}

	place = AtomIonSizeStateData_val[block][atom].search(state);
	if(place < 0)
	{
		Error << "Cannot find energy for ionisation state " & state & " for atom number " & atom+1 & " in AtomDataBall::getAtomIonSize(int, int)";
		throw Error;
	}

	return AtomIonSizeData_val[block][atom][place];
}
int AtomDataBall::getAtomIonSizeStateBase(const String& BlockName, int atom) const
{
	int result, i;
	int type = ATOM_DATA_ATOM_ION_SIZE;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonStateBase";
		throw ErrorBall(Error);
	}

	result = 100;
	for(i = 0; i < AtomIonSizeStateData_val[block][atom].length(); i++)
	{
		result = (AtomIonSizeStateData_val[block][atom][i] < result) ? AtomIonSizeStateData_val[block][atom][i] : result;
	}

	return result;
}
int AtomDataBall::getAtomIonSizeNumber(const String& BlockName, int atom) const
{
	int type = ATOM_DATA_ATOM_ION_SIZE;
	int block = BlockNames_val[type].search(BlockName);
	String Error;

	if(variableDefined(BlockName, type, atom) == false)
	{
		Error << "Symbol undefined for atom " & atom & " in AtomDataBall::getAtomIonSizeStateBase";
		throw ErrorBall(Error);
	}

	return AtomIonSizeStateData_val[block][atom].length();
}


