
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef ATOMDATABALL_H
#define ATOMDATABALL_H

#include "definitions.h"
#include "Array1D.h"
#include "Array2D.h"
#include "Array3D.h"

#define ATOM_DATA_TOTAL 18
#define ATOM_DATA_SYMBOL 0
#define ATOM_DATA_NAME 1
#define ATOM_DATA_SHELL 2
#define ATOM_DATA_VALENCE_ELEC 3
#define ATOM_DATA_VALENCE_BOND 4
#define ATOM_DATA_MINCHARGE 5
#define ATOM_DATA_MAXCHARGE 6
#define ATOM_DATA_MASS 7
#define ATOM_DATA_ELECTRONEG 8
#define ATOM_DATA_HARDNESS 9
#define ATOM_DATA_SLATER_SCREEN 10
#define ATOM_DATA_GAMMA_SCREEN 11
#define ATOM_DATA_VDW_RADIUS 12
#define ATOM_DATA_SURFACE_RADIUS 13
#define ATOM_DATA_COVALENT_RADIUS 14
#define ATOM_DATA_SLATER_RADIUS 15
#define ATOM_DATA_ATOM_ION_ENERGY 16
#define ATOM_DATA_ATOM_ION_SIZE 17

class AtomDataBall
{
	protected:
		static String Variant_val;
		static Array1D<String> VariantNames_val;
		static Array3D<String> VariantSets_val;
		static Array2D<String> BlockNames_val;
		static Array3D<bool> DataDefined_val;
		static Array2D<String> SymbolData_val;
		static Array2D<String> NameData_val;
		static Array2D<int> ShellData_val;
		static Array2D<int> ValenceElecData_val;
		static Array2D<int> ValenceBondData_val;
		static Array2D<int> MinChargeData_val;
		static Array2D<int> MaxChargeData_val;
		static Array2D<FP_TYPE> MassData_val;
		static Array2D<FP_TYPE> ElectronegData_val;
		static Array2D<FP_TYPE> HardnessData_val;
		static Array2D<FP_TYPE> SlaterScreenData_val;
		static Array2D<FP_TYPE> GammaScreenData_val;
		static Array2D<FP_TYPE> VdwRadiusData_val;
		static Array2D<FP_TYPE> SurfaceRadiusData_val;
		static Array2D<FP_TYPE> CovalentRadiusData_val;
		static Array2D<FP_TYPE> SlaterRadiusData_val;
		static Array3D<FP_TYPE> AtomIonEnergyData_val;
		static Array3D<int> AtomIonEnergyStateData_val;
		static Array3D<FP_TYPE> AtomIonSizeData_val;
		static Array3D<int> AtomIonSizeStateData_val;

		bool variableDefined(int, int) const;
		bool variableDefined(const String&, int, int) const;

	public:

		//  Some writers
		void setVarient(const String&);
		void loadData();

		//  Some readers to get size information
		int variantNo() const;
		int SymbolSetNo() const;
		int NameSetNo() const;
		int shellSetNo() const;
		int valenceElecSetNo() const;
		int valenceBondSetNo() const;
		int minChargeSetNo() const;
		int maxChargeSetNo() const;
		int massSetNo() const;
		int electronegSetNo() const;
		int hardnessSetNo() const;
		int slaterScreenSetNo() const;
		int gammaScreenSetNo() const;
		int vdwRadiusSetNo() const;
		int surfaceRadiusSetNo() const;
		int covalentRadiusSetNo() const;
		int slaterRadiusSetNo() const;
		int AtomIonEnergyStateSetNo() const;
		int AtomIonSizeStateSetNo() const;
		int variantBlockNo(int, int) const;
		const String& VariantBlockName(int, int, int) const;

		const String& VariantName(int) const;
		const String& SymbolName(int) const;
		const String& NameName(int) const;
		const String& shellName(int) const;
		const String& valenceElecName(int) const;
		const String& valenceBondName(int) const;
		const String& minChargeName(int) const;
		const String& maxChargeName(int) const;
		const String& massName(int) const;
		const String& electronegName(int) const;
		const String& hardnessName(int) const;
		const String& slaterScreenName(int) const;
		const String& gammaScreenName(int) const;
		const String& vdwRadiusName(int) const;
		const String& surfaceRadiusName(int) const;
		const String& covalentRadiusName(int) const;
		const String& slaterRadiusName(int) const;
		const String& AtomIonEnergyName(int) const;
		const String& AtomIonSizeName(int) const;

		//  Readers for whether or not the variable is defined for the standard orientation
		bool symbolDefined(int) const;
		bool nameDefined(int) const;
		bool shellDefined(int) const;
		bool valenceElecDefined(int) const;
		bool valenceBondDefined(int) const;
		bool minChargeDefined(int) const;
		bool maxChargeDefined(int) const;
		bool massDefined(int) const;
		bool electronegDefined(int) const;
		bool hardnessDefined(int) const;
		bool slaterScreenDefined(int) const;
		bool gammaScreenDefined(int) const;
		bool vdwRadiusDefined(int) const;
		bool surfaceRadiusDefined(int) const;
		bool covalentRadiusDefined(int) const;
		bool slaterRadiusDefined(int) const;
		bool AtomIonEnergyDefined(int) const;
		bool AtomIonSizeDefined(int) const;

		//  Readers for the standard definition
		int getSymbolIndex(const String&) const;
		const String getSymbol(int) const;
		const String getName(int) const;
		int getShell(int) const;
		int getValenceElec(int) const;
		int getValenceBond(int) const;
		int getMinCharge(int) const;
		int getMaxCharge(int) const;
		FP_TYPE getMass(int) const;
		FP_TYPE getElectroneg(int) const;
		FP_TYPE getHardness(int) const;
		FP_TYPE getSlaterScreen(int) const;
		FP_TYPE getGammaScreen(int) const;
		FP_TYPE getVdwRadius(int) const;
		FP_TYPE getSurfaceRadius(int) const;
		FP_TYPE getCovalentRadius(int) const;
		FP_TYPE getSlaterRadius(int) const;
		FP_TYPE getAtomIonEnergy(int, int) const;
		int getAtomIonEnergyStateBase(int) const;
		int getAtomIonEnergyNumber(int) const;
		FP_TYPE getAtomIonSize(int, int) const;
		int getAtomIonSizeStateBase(int) const;
		int getAtomIonSizeNumber(int) const;

		//  Readers for whether or not the variable for a given block is defined
		int getSymbolIndex(const String&, const String&) const;
		bool symbolDefined(const String&, int) const;
		bool nameDefined(const String&, int) const;
		bool shellDefined(const String&, int) const;
		bool valenceElecDefined(const String&, int) const;
		bool valenceBondDefined(const String&, int) const;
		bool minChargeDefined(const String&, int) const;
		bool maxChargeDefined(const String&, int) const;
		bool massDefined(const String&, int) const;
		bool electronegDefined(const String&, int) const;
		bool hardnessDefined(const String&, int) const;
		bool slaterScreenDefined(const String&, int) const;
		bool gammaScreenDefined(const String&, int) const;
		bool vdwRadiusDefined(const String&, int) const;
		bool surfaceRadiusDefined(const String&, int) const;
		bool covalentRadiusDefined(const String&, int) const;
		bool slaterRadiusDefined(const String&, int) const;
		bool AtomIonEnergyDefined(const String&, int) const;
		bool AtomIonSizeDefined(const String&, int) const;

		//  Readers for the data in a specific data set
		const String& getSymbol(const String&, int) const;
		const String getName(const String&, int) const;
		int getShell(const String&, int) const;
		int getValenceElec(const String&, int) const;
		int getValenceBond(const String&, int) const;
		int getMinCharge(const String&, int) const;
		int getMaxCharge(const String&, int) const;
		FP_TYPE getMass(const String&, int) const;
		FP_TYPE getElectroneg(const String&, int) const;
		FP_TYPE getHardness(const String&, int) const;
		FP_TYPE getSlaterScreen(const String&, int) const;
		FP_TYPE getGammaScreen(const String&, int) const;
		FP_TYPE getVdwRadius(const String&, int) const;
		FP_TYPE getSurfaceRadius(const String&, int) const;
		FP_TYPE getCovalentRadius(const String&, int) const;
		FP_TYPE getSlaterRadius(const String&, int) const;
		FP_TYPE getAtomIonEnergy(const String&, int, int) const;
		int getAtomIonEnergyStateBase(const String&, int) const;
		int getAtomIonEnergyNumber(const String&, int) const;
		FP_TYPE getAtomIonSize(const String&, int, int) const;
		int getAtomIonSizeStateBase(const String&, int) const;
		int getAtomIonSizeNumber(const String&, int) const;
};


#endif
