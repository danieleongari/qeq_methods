
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef INTPUCHARGEFITBALL_H
#define INPUTCHARGEFITBALL_H

#include "definitions.h"
#include "String.h"

#define INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO 0
#define INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE 1
#define INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE 2
#define INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE 3

#define INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM 0
#define INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF 1
#define INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY 2
#define INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE 3

#define INPUTCHARGEFITBALL_QEQ_IDEM_ATOM 0
#define INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE 1

#define INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM 0
#define INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_NEUTRAL 1
#define INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL 2
#define INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE 3

#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_NONE 0
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER 1
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN 2
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_NM 3
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_NMW 4
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_OHNO 5
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_OK 6
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH 7
#define INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER 8

#define INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO 0
#define INPUTCHARGEFITBALL_QEQ_INITIALQ_CURRENT 1
#define INPUTCHARGEFITBALL_QEQ_INITIALQ_FORMAL 2

#define INPUTCHARGEFITBALL_LAMBDA (FP_TYPE)0.5


class InputChargeFitBall
{
	friend class MPICommBall;

	protected:
		int charge_val;
		FP_TYPE ewaldError_val;
		FP_TYPE shiftForceCutoff_val;
		FP_TYPE shiftForceDamp_val;
		bool useChargeWeight_val;
		bool usePolarWeight_val;
		FP_TYPE chargeWeight_val;
		FP_TYPE polarWeight_val;
		FP_TYPE vdwRangeLow_val;
		FP_TYPE vdwRangeHigh_val;

		//  Values for the electronegativity equilibration stuff
		String QeqHardnessData_val;
		String QeqElectronegData_val;
		String QeqAtomIonData_val;
		int qeqBaseChargeType_val;
		FP_TYPE qeqDielectric_val;
		FP_TYPE qeqScreenScale_val;
		int qeqIdempotentialType_val;
		int qeqDerivativeType_val;
		int qeqAtomSize_val;
		int qeqIntegralType_val;
		int qeqSteps_val;
		int qeqInitialCharge_val;
		FP_TYPE qeqLimit_val;

	public:
		InputChargeFitBall();
		InputChargeFitBall(const InputChargeFitBall&);
		void operator=(const InputChargeFitBall&);
		int operator=(int);
		void null();

		String QeqHardnessData() const;
		String QeqElectronegData() const;
		String QeqAtomIonData() const;
		int qeqBaseChargeType() const;
		FP_TYPE qeqDielectric() const;
		FP_TYPE qeqScreenScale() const;
		int qeqIdempotentialType() const;
		int qeqDerivativeType() const;
		int qeqIntegralType() const;
		int qeqSteps() const;
		int qeqAtomSize() const;
		int qeqInitialCharge() const;
		FP_TYPE qeqLimit() const;

		void setQeqHardnessData(String);
		void setQeqElectronegData(String);
		void setQeqAtomIonData(String);
		void setQeqBaseChargeType(int);
		void setQeqDielectric(FP_TYPE);
		void setQeqScreenScale(FP_TYPE);
		void setQeqIdempotentialType(int);
		void setQeqDerivativeType(int);
		void setQeqAtomSize(int);
		void setQeqIntegralType(int);
		void setQeqSteps(int);
		void setQeqInitialCharge(int);
		void setQeqLimit(FP_TYPE);

		void loadFile(const String&);
		void printCommand(const String&) const;
		void printCommand(ostream&) const;
};
#endif