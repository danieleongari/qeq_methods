/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _KVECTOR_H
#define _KVECTOR_H

#include "definitions.h"
#include "Vector.h"
#include "Array1D.h"
#include "FunctionBall.h"


class KVector
{
	friend class MPICommBall;

	protected:
		Vector K_val;
		FP_TYPE factor_val;
		int a_val, b_val, c_val;

	public:
		//  Necessary functions
		KVector();
		KVector(const KVector&);
		~KVector();
		const KVector& operator=(const KVector&);
		int operator=(int);
		void null();

		//  Readers to get at the information
		FP_TYPE r() const;
		const Vector& K() const;
		FP_TYPE factor() const;
		int a() const;
		int b() const;
		int c() const;

		//  Single function for setting the whole thing
		void setVector(const Array1D<int>&, const Array1D<Vector>&);
		void setKVector(FP_TYPE, FP_TYPE, FP_TYPE);
		void setFactor(FP_TYPE);
		void setComponent(int, int, int);

		//  Functions to make sorting work
		bool operator<(const KVector&);
		bool operator>(const KVector&);

		//  Function to get dot products with the position vector
		FP_TYPE operator%(const Vector&) const;
};

#endif
