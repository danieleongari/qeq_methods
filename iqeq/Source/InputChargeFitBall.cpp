
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "InputChargeFitBall.h"
#include "ErrorBall.h"

#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

InputChargeFitBall::InputChargeFitBall()
{
	null();
}
InputChargeFitBall::InputChargeFitBall(const InputChargeFitBall& A)
{
	*this = A;
}
void InputChargeFitBall::operator=(const InputChargeFitBall& A)
{
	charge_val = A.charge_val;
	ewaldError_val = A.ewaldError_val;
	shiftForceCutoff_val = A.shiftForceCutoff_val;
	shiftForceDamp_val = A.shiftForceDamp_val;
	useChargeWeight_val = A.useChargeWeight_val;
	usePolarWeight_val = A.usePolarWeight_val;
	chargeWeight_val = A.chargeWeight_val;
	polarWeight_val = A.polarWeight_val;
	vdwRangeLow_val = A.vdwRangeLow_val;
	vdwRangeHigh_val = A.vdwRangeHigh_val;

	QeqHardnessData_val = A.QeqHardnessData_val;
	QeqElectronegData_val = A.QeqElectronegData_val;
	QeqAtomIonData_val = A.QeqAtomIonData_val;
	qeqBaseChargeType_val = A.qeqBaseChargeType_val;
	qeqDielectric_val = A.qeqDielectric_val;
	qeqScreenScale_val = A.qeqScreenScale_val;
	qeqIdempotentialType_val = A.qeqIdempotentialType_val;
	qeqDerivativeType_val = A.qeqDerivativeType_val;
	qeqAtomSize_val = A.qeqAtomSize_val;
	qeqIntegralType_val = A.qeqIntegralType_val;
	qeqSteps_val = A.qeqSteps_val;
	qeqInitialCharge_val = A.qeqInitialCharge_val;
	qeqLimit_val = A.qeqLimit_val;
}
int InputChargeFitBall::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in InputChargeFitBall::operator=(int i)");
	null();

	return i;
}
void InputChargeFitBall::null()
{
	charge_val = 0;
	ewaldError_val = 1E-5;
	shiftForceCutoff_val = 20;
	shiftForceDamp_val = 0;
	useChargeWeight_val = false;
	usePolarWeight_val = false;
	chargeWeight_val = 1;
	polarWeight_val = 1;
	vdwRangeLow_val = 1.3;
	vdwRangeHigh_val = 2.0;

	QeqHardnessData_val = "";
	QeqElectronegData_val = "";
	QeqAtomIonData_val = "Default";
	qeqBaseChargeType_val = INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE;
	qeqDielectric_val = 1.0;
	qeqScreenScale_val = INPUTCHARGEFITBALL_LAMBDA;
	qeqIdempotentialType_val = INPUTCHARGEFITBALL_QEQ_IDEM_ATOM;
	qeqDerivativeType_val = INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF;
	qeqAtomSize_val = INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM;
	qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH;
	qeqSteps_val = 100;
	qeqInitialCharge_val = INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO;
	qeqLimit_val = 1E-5;
}

String InputChargeFitBall::QeqHardnessData() const					{ return QeqHardnessData_val; }
String InputChargeFitBall::QeqElectronegData() const				{ return QeqElectronegData_val; }
String InputChargeFitBall::QeqAtomIonData() const					{ return QeqAtomIonData_val; }
int InputChargeFitBall::qeqBaseChargeType() const					{ return qeqBaseChargeType_val; }
FP_TYPE InputChargeFitBall::qeqDielectric() const					{ return qeqDielectric_val; }
FP_TYPE InputChargeFitBall::qeqScreenScale() const					{ return qeqScreenScale_val; }
int InputChargeFitBall::qeqIdempotentialType() const				{ return qeqIdempotentialType_val; }
int InputChargeFitBall::qeqDerivativeType() const					{ return qeqDerivativeType_val; }
int InputChargeFitBall::qeqAtomSize() const							{ return qeqAtomSize_val; }
int InputChargeFitBall::qeqIntegralType() const						{ return qeqIntegralType_val; }
int InputChargeFitBall::qeqSteps() const							{ return qeqSteps_val; }
int InputChargeFitBall::qeqInitialCharge() const					{ return qeqInitialCharge_val; }
FP_TYPE InputChargeFitBall::qeqLimit() const						{ return qeqLimit_val; }

void InputChargeFitBall::setQeqHardnessData(String Data)			{ QeqHardnessData_val = Data; }
void InputChargeFitBall::setQeqElectronegData(String Data)			{ QeqElectronegData_val = Data; }
void InputChargeFitBall::setQeqAtomIonData(String Data)				{ QeqAtomIonData_val = Data; }
void InputChargeFitBall::setQeqBaseChargeType(int data)				{ qeqBaseChargeType_val = data; }
void InputChargeFitBall::setQeqDielectric(FP_TYPE data)				{ qeqDielectric_val = data; }
void InputChargeFitBall::setQeqScreenScale(FP_TYPE data)			{ qeqScreenScale_val = data; }
void InputChargeFitBall::setQeqIdempotentialType(int data)			{ qeqIdempotentialType_val = data; }
void InputChargeFitBall::setQeqDerivativeType(int data)				{ qeqDerivativeType_val = data; }
void InputChargeFitBall::setQeqAtomSize(int data)					{ qeqAtomSize_val = data; }
void InputChargeFitBall::setQeqIntegralType(int data)				{ qeqIntegralType_val = data; }
void InputChargeFitBall::setQeqSteps(int data)						{ qeqSteps_val = data; }
void InputChargeFitBall::setQeqInitialCharge(int data)				{ qeqInitialCharge_val = data; }
void InputChargeFitBall::setQeqLimit(FP_TYPE data)					{ qeqLimit_val = data; }

void InputChargeFitBall::loadFile(const String& FileName)
{
	bool commandFound;
	String Line, Message, LineType, Token;
	ifstream Input;

	//  Open the file
	Input.open(FileName.pointer(), ios::in);
	Message << "Unable to open input file \"" & FileName & "\" in InputChargeFitBall::loadFile";
	if(!Input.is_open()) throw ErrorBall(Message);

	//  Now go through and read all of the lines
	while(!Input.eof())
	{
		//  Get the line
		Input >> Line;
		
		//  Get the line type, if it is not a blank line
		if(Line.tokenNo() > 0 && Line[0] != '#') LineType = Line.token(1).toUpper();
		else LineType = "";

		commandFound = false;

		if(LineType == "")
		{
			commandFound = true;
		}
		if(LineType == "CHARGE")
		{
			charge_val = Line.token(2).stoi();
			commandFound = true;
		}
		if(LineType == "EWALDERROR")
		{
			ewaldError_val = Line.toktof(2);
			commandFound = true;
		}
		if(LineType == "SHIFTFORCECUTOFF")
		{
			shiftForceCutoff_val = Line.toktof(2);
			commandFound = true;
		}
		if(LineType == "SHIFTFORCEDAMP")
		{
			shiftForceDamp_val = Line.toktof(2);
			commandFound = true;
		}
		if(LineType == "USECHARGEWEIGHT")
		{
			Token = Line.token(2).toUpper();
			if(Token[0] == 'Y' || Token[0] == 'T') 
			{
				useChargeWeight_val = true;
				chargeWeight_val = Line.toktof(3);
			}
			else useChargeWeight_val = false;
			commandFound = true;
		}
		if(LineType == "USEPOLARWEIGHT")
		{
			Token = Line.token(2).toUpper();
			if(Token[0] == 'Y' || Token[0] == 'T') 
			{
				usePolarWeight_val = true;
				polarWeight_val = Line.toktof(3);
			}
			else usePolarWeight_val = false;
			commandFound = true;
		}
		if(LineType == "VDWRANGE")
		{
			vdwRangeLow_val = Line.toktof(2);
			vdwRangeHigh_val = Line.toktof(3);
			commandFound = true;
		}

		if(LineType == "QEQHARDNESSDATA")
		{
			QeqHardnessData_val = Line.token(2);
			commandFound = true;
		}
		if(LineType == "QEQELECTRONEGDATA")
		{
			QeqElectronegData_val = Line.token(2);
			commandFound = true;
		}
		if(LineType == "QEQATOMIONDATA")
		{
			QeqAtomIonData_val = Line.token(2);
			commandFound = true;
		}
		if(LineType == "QEQBASECHARGE")
		{
			Token = Line.token(2).toUpper();
			if(Token == "ZERO") qeqBaseChargeType_val = INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO;
			else if(Token == "FORMAL") qeqBaseChargeType_val = INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE;
			else if(Token == "ATOM") qeqBaseChargeType_val = INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE;
			else if(Token == "METAL") qeqBaseChargeType_val = INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE;
			else
			{
				throw ErrorBall("Unknown base charge type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQDIELECTRIC")
		{
			qeqDielectric_val = Line.toktof(2);
			commandFound = true;
		}
		if(LineType == "QEQSCREENSCALE")
		{
			qeqScreenScale_val = Line.toktof(2);
			commandFound = true;
		}
		if(LineType == "QEQIDEMPOTENTIAL")
		{
			Token = Line.token(2).toUpper();
			if(Token == "ATOM") qeqIdempotentialType_val = INPUTCHARGEFITBALL_QEQ_IDEM_ATOM;
			else if(Token == "ITERATE") qeqIdempotentialType_val = INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE;
			else
			{
				throw ErrorBall("Unknown idempotential type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQDERIVATIVE")
		{
			Token = Line.token(2).toUpper();
			if(Token == "ATOM") qeqDerivativeType_val = INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM;
			else if(Token == "DIFF") qeqDerivativeType_val = INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF;
			else if(Token == "POLY") qeqDerivativeType_val = INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY;
			else if(Token == "SPLINE") qeqDerivativeType_val = INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE;
			else
			{
				throw ErrorBall("Unknown derivative type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQATOMSIZE")
		{
			Token = Line.token(2).toUpper();
			if(Token == "ATOM") qeqAtomSize_val = INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM;
			else if(Token == "NEUTRAL") qeqAtomSize_val = INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_NEUTRAL;
			else if(Token == "FORMAL") qeqAtomSize_val = INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL;
			else if(Token == "CHARGE") qeqAtomSize_val = INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE;
			else
			{
				throw ErrorBall("Unknown atom size type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQINTEGRAL")
		{
			Token = Line.token(2).toUpper();
			if(Token == "NONE") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_NONE;
			else if(Token == "SLATER") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER;
			else if(Token == "GAUSSIAN") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN;
			else if(Token == "NM") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_NM;
			else if(Token == "NMW") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_NMW;
			else if(Token == "OHNO") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_OHNO;
			else if(Token == "OK") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_OK;
			else if(Token == "DH") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH;
			else if(Token == "WILMER") qeqIntegralType_val = INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER;
			else
			{
				throw ErrorBall("Unknown integral type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQINITIAL")
		{
			Token = Line.token(2).toUpper();
			if(Token == "ZERO") qeqInitialCharge_val = INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO;
			else if(Token == "CURRENT") qeqInitialCharge_val = INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO;
			else if(Token == "FORMAL") qeqInitialCharge_val = INPUTCHARGEFITBALL_QEQ_INITIALQ_FORMAL;
			else
			{
				throw ErrorBall("Unknown initial charge type in InputChargeFitBall::loadFile");
			}
			commandFound = true;
		}
		if(LineType == "QEQSTEPS") 
		{
			qeqSteps_val = Line.token(2).stoi();
			commandFound = true;
		}
		if(LineType == "QEQLIMIT") 
		{
			qeqLimit_val = Line.toktof(2);
			commandFound = true;
		}
		if(commandFound == false)
		{
			Message << "Could not find linetype " & LineType & " in InputChargeFitBall::loadFile";
			throw ErrorBall(Message);
		}
	}
}

void InputChargeFitBall::printCommand(const String& FileName) const
{
	ofstream Output;

	//  Open the output file
	Output.open(FileName.pointer(), ios::out);
	if(Output.is_open() == false)
	{
		throw ErrorBall("Could not open file in InputChargeFitBall::printCommand");
	}

	//  Now use that for the other function
	printCommand(Output);

	Output.close();
}

void InputChargeFitBall::printCommand(ostream& Output) const
{
	Output.precision(6);
	Output.setf(ios::fixed | ios::showpoint);

	//  Print the common stuff
	Output << endl;
	Output << setw(30) << left << "Algorithm";

	//  Now print the stuff for the electrostatic fitting methods, if I need
	if(QeqHardnessData_val != "") Output << setw(30) << "QeqHardnessData" << QeqHardnessData_val << endl;
	if(QeqElectronegData_val != "") Output << setw(30) << "QeqElectronegData" << QeqElectronegData_val << endl;
	if(QeqAtomIonData_val != "") Output << setw(30) << "QeqAtomIonData" << QeqAtomIonData_val << endl;
	switch(qeqBaseChargeType_val)
	{
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO:
			Output << setw(30) << "QeqBaseCharge" << "Zero" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE:
			Output << setw(30) << "QeqBaseCharge" << "Formal" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE:
			Output << setw(30) << "QeqBaseCharge" << "Atom" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE:
			Output << setw(30) << "QeqBaseCharge" << "Metal" << endl;
			break;
	}
	Output << setw(30) << "QeqDielectric" << qeqDielectric_val << endl;
	Output << setw(30) << "QeqScreenScale" << qeqScreenScale_val << endl;
	switch(qeqIdempotentialType_val)
	{
		case INPUTCHARGEFITBALL_QEQ_IDEM_ATOM:
			Output << setw(30) << "QeqIdempotential" << "Atom" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE:
			Output << setw(30) << "QeqIdempotential" << "Iterate" << endl;
			break;
	}
	switch(qeqDerivativeType_val)
	{
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM:
			Output << setw(30) << "QeqDerivative" << "Atom" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF:
			Output << setw(30) << "QeqDerivative" << "Diff" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY:
			Output << setw(30) << "QeqDerivative" << "Poly" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE:
			Output << setw(30) << "QeqDerivative" << "Spline" << endl;
			break;
	}
	switch(qeqAtomSize_val)
	{
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM:
			Output << setw(30) << "QeqAtomSize" << "Atom" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_NEUTRAL:
			Output << setw(30) << "QeqAtomSize" << "Neutral" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL:
			Output << setw(30) << "QeqAtomSize" << "Formal" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE:
			Output << setw(30) << "QeqAtomSize" << "Charge" << endl;
			break;
	}
	switch(qeqIntegralType_val)
	{
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NONE:
			Output << setw(30) << "QeqIntegral" << "None" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER:
			Output << setw(30) << "QeqIntegral" << "Slater" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN:
			Output << setw(30) << "QeqIntegral" << "Gaussian" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NM:
			Output << setw(30) << "QeqIntegral" << "NM" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NMW:
			Output << setw(30) << "QeqIntegral" << "NMW" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_OHNO:
			Output << setw(30) << "QeqIntegral" << "OHNO" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_OK:
			Output << setw(30) << "QeqIntegral" << "OK" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH:
			Output << setw(30) << "QeqIntegral" << "DH" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER:
			Output << setw(30) << "QeqIntegral" << "Wilmer" << endl;
			break;
	}
	switch(qeqInitialCharge_val)
	{
		case INPUTCHARGEFITBALL_QEQ_INITIALQ_ZERO:
			Output << setw(30) << "QeqInitial" << "Zero" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INITIALQ_CURRENT:
			Output << setw(30) << "QeqInitial" << "Current" << endl;
			break;
		case INPUTCHARGEFITBALL_QEQ_INITIALQ_FORMAL:
			Output << setw(30) << "QeqInitial" << "Formal" << endl;
			break;
	}
	Output << setw(30) << "QeqSteps" << qeqSteps_val << endl;
	Output << setw(30) << "QeqLimit" << qeqLimit_val << endl;
	
	Output << endl;
}