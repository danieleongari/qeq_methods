
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Matrix.h"
#include "definitions.h"
#include "ErrorBall.h"
#include "Column.h"
#include "FunctionBall.h"
#include "Tensor.h"
#include "Lapack.h"

#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;

//  The constructor
Matrix::Matrix()
{
	rowNo_val = columnNo_val = 0;
	rowSize_val = columnSize_val = 0;
	Data_val = NULL;
	null(); 
}
Matrix::Matrix(int rowSize, int columnSize)
{
	int i;

	rowNo_val = columnNo_val = 0;
	rowSize_val = columnSize_val = 0;
	Data_val = NULL;

	setSize(columnSize, rowSize);
	for(i = 0; i < totalSize(); i++) Data_val[i] = 0;
}
Matrix::Matrix(Matrix const& A)
{
	rowNo_val = columnNo_val = 0;
	rowSize_val = columnSize_val = 0;
	Data_val = NULL;

	*this = A;
}
Matrix& Matrix::operator=(Matrix const& A)
{
	int i, total;

	null();

	rowNo_val = A.rowNo();
	columnNo_val = A.columnNo();
	rowSize_val = A.rowSize();
	columnSize_val = A.columnSize();

	total = rowSize_val * columnSize_val;
	if(total > 0) Data_val = new FP_TYPE[total];
	for(i = 0; i < total; i++) Data_val[i] = A.Data_val[i];

	return *this;
}
Matrix& Matrix::operator=(const Tensor& A)
{
	int i, j;

	//  Set the size of the matrix
	setSize(3, 3);

	for(i = 0; i < 3; i++) 
	{
		for(j = 0; j < 3; j++) Data_val[i+(j*3)] = A.element(i,j);
	}

	return *this;
}
int Matrix::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in Matrix::operator=(int i)");
	null();

	return i;
}
void Matrix::null()
{
	if(totalSize() > 0) delete [] Data_val;
	Data_val = NULL;
	rowNo_val = columnNo_val = 0;
	rowSize_val = columnSize_val = 0;
}

////  The destructor
Matrix::~Matrix()	{ null(); }

int Matrix::rowNo() const					{ return rowNo_val; }
int Matrix::columnNo() const				{ return columnNo_val; }
int Matrix::rowSize() const					{ return rowSize_val; }
int Matrix::columnSize() const				{ return columnSize_val; }
int Matrix::totalSize() const				{ return rowSize_val*columnSize_val; }

FP_TYPE& Matrix::operator[](int i) const
{ 
	if(i >= totalSize() || i < 0) throw ErrorBall("Out of bounds array access in function Matrix::element(int)");
	return Data_val[i];
}
FP_TYPE& Matrix::operator()(int i, int j) const
{ 
	int index;

	if(i >= rowSize_val || j >= columnSize_val || i < 0 || j < 0) 
		throw ErrorBall("Out of bounds array access in function Matrix::element(int,int)");

	index = (j*rowSize_val) + i; 
	return Data_val[index];
}

void Matrix::zero()
{
	int i, total;

	total = rowSize_val * columnSize_val;
	for(i = 0; i < total; i++) Data_val[i] = 0;          
}
Matrix Matrix::identity()
{
	int i;
	Matrix Result;

	if(rowNo_val != columnNo_val)
		throw ErrorBall("Identity only defined for square matrices in function Matrix::identity()\n");

	Result.setSize(rowNo_val, columnNo_val);
	Result.zero();

	for(i = 0; i < rowNo_val; i++) Result(i, i) = 1;

	return Result;
}
void Matrix::setSize(int rowNo, int columnNo)
{
	int i, total;

	null();

	rowNo_val = rowNo;
	columnNo_val = columnNo;

	rowSize_val = rowNo;
	columnSize_val = columnNo;

	total = rowSize_val*columnSize_val;
	Data_val = new FP_TYPE[total];
	for(i = 0; i < total; i++) Data_val[i] = 0;
}

Matrix& Matrix::operator+=(const Matrix& A)
{
	int i, total;

	if(rowNo_val != A.rowNo() || columnNo_val != A.columnNo())
			throw ErrorBall("Cannot add matrices of different sizes in function Matrix::operator+=\n");

	total = totalSize();
	for(i = 0; i < total; i++) Data_val[i] += A[i];

	return *this;
}

Matrix Matrix::operator*(const Matrix& A) const
{
	Matrix Result(rowNo_val, A.columnNo());

	if(columnNo_val != A.rowNo()) throw ErrorBall("Cannot multiply matrices of different sizes in function operator*(const& Matrix A)");

	LapackLib::fgemm('N', 'N', rowNo_val, A.columnNo(), columnNo_val, 1, Data_val, rowNo_val, A.Data_val, A.rowNo(), 0, Result.Data_val, rowNo_val);

	return Result;
}
Matrix & Matrix::operator*=(FP_TYPE value) 
{
	int i, total;

	total = totalSize();
	for(i = 0; i < total; i++) Data_val[i] *= value;

	return *this;
}

Matrix& Matrix::operator/=(FP_TYPE value) 
{
	int i, total;

	total = totalSize();
	for(i = 0; i < total; i++) Data_val[i] /= value;

	return *this;
}
Column Matrix::operator*(const Column& A) const
{
	Column Result;

	if(columnNo_val != A.elementNo()) throw ErrorBall("Mismatched array sizes in function Column::operator*(const Matrix&)");
	
	Result.setSize(rowNo_val);
	LapackLib::fgemv('N', rowNo_val, columnNo_val, 1, Data_val, rowNo_val, A.Data_val, 1, 0, Result.Data_val, 1);

	return Result;
}
Column Matrix::linearSolve(const Column& Bcolumn)
{
	int info, *ipiv;
	String Error;
	Column Result;
	Matrix Temp;

	if(rowNo_val != columnNo_val) throw ErrorBall("Matrix must be square in function Matrix::linearSolve\n");
	
	ipiv = new int[rowNo_val];
	Temp = *this;
	Result = Bcolumn;

	info = LapackLib::fgesv(rowNo_val, 1, Temp.Data_val, rowNo_val, ipiv, Result.Data_val, Result.elementNo());
	if(info > 0)
	{
		Error << "Problem with LAPACK routine in Matrix::linearSolve giving error code " & info;
		throw ErrorBall(Error);
	}

	//  Now clean out the arrays
	delete [] ipiv;

	//  Done
	return Result;
}

