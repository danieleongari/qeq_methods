
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Column.h"
#include "Lapack.h"

Column::Column()
{
	elementNo_val = 0;
	Data_val = NULL;
	null();
}
Column::Column(int size)
{
	int i;

	elementNo_val = 0;
	Data_val = NULL;

	setSize(size);
	for(i = 0; i < elementNo_val; i++) Data_val[i] = 0;
}
Column::Column(int type, int size)
{
	int i;

	elementNo_val = 0;
	Data_val = NULL;

	setSize(size);
	for(i = 0; i < elementNo_val; i++) Data_val[i] = 0;
}
Column::Column(const Column& A)
{
	elementNo_val = 0;
	Data_val = NULL;

	*this = A;
}

//  The assignment overload
Column& Column::operator=(const Column& A)
{
	int i;

	null();

	elementNo_val = A.elementNo();
	if(elementNo_val > 0) Data_val = new FP_TYPE[elementNo_val];
	for(i = 0; i < elementNo_val; i++) Data_val[i] = A.Data_val[i];

	return *this;
}
Column& Column::operator=(const Vector& A)
{
	int i;

	//  Set the size of the matrix
	setSize(3);
	for(i = 0; i < 3; i++) Data_val[i] = A.cartesian(i);

	return *this;
}
Column& Column::operator=(const Array1D<FP_TYPE>& A)
{
	int i;

	setSize(A.length());
	for(i = 0; i < elementNo_val; i++) Data_val[i] = A[i];

	return *this;
}

void Column::null()
{
	if(elementNo_val > 0) delete [] Data_val;
	elementNo_val = 0;
}
//  The destructor
Column::~Column()
{
	null();
}

//  The readers
FP_TYPE& Column::operator[](int i) const		
{
	#ifdef _ERROR_REPORT
		if(i >= dataSize_val || i < 0) 
			throw ErrorBall("Out of bounds array access in function Column::operator[](int)");
	#endif
	return Data_val[i];
}
int Column::elementNo() const							{ return elementNo_val; }
void Column::setSize(int size)
{
	null();

	elementNo_val = size;
	Data_val = new FP_TYPE[elementNo_val];
}
Column& Column::operator+=(const Column& A)
{
	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator+=(const Column&)");
	LapackLib::faxpy(elementNo_val, 1, A.Data_val, 1, Data_val, 1);

	return *this;
}
Column Column::operator+(const Column& A) const
{
	Column Result(elementNo_val);

	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator+(const Column&)");

	LapackLib::fcopy(elementNo_val, Data_val, 1, Result.Data_val, 1);
	LapackLib::faxpy(elementNo_val, 1, A.Data_val, 1, Result.Data_val, 1);

	return Result;
}
Column& Column::operator-=(const Column& A)
{
	int i_one = 1;
	FP_TYPE neg_one = -1;
	Column Result(elementNo_val);
	
	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator-=(const Column&)");
	LapackLib::faxpy(elementNo_val, -1, A.Data_val, 1, Data_val, 1);

	return *this;
}
Column Column::operator-(const Column& A) const
{
	Column Result(elementNo_val);

	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator+(const Column&)");

	LapackLib::fcopy(elementNo_val, Data_val, 1, Result.Data_val, 1);
	LapackLib::faxpy(elementNo_val, -1, A.Data_val, 1, Result.Data_val, 1);

	return Result;
}

////  Overloads for multiplication - dot, cross and outer
FP_TYPE Column::operator%(const Column& A) const
{
	FP_TYPE result;

	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator%(const Column&)");
	result = LapackLib::fdot(elementNo_val, Data_val, 1, A.Data_val, 1);

	return result;
}
Matrix Column::operator&(const Column& A)
{
	Matrix Result;

	if(elementNo_val != A.elementNo()) throw ErrorBall("Mismatched Column lengths in function Column::operator&(const Column&)");

	Result.setSize(elementNo_val, elementNo_val);
	LapackLib::fgemm('N', 'N', elementNo_val, elementNo_val, 1, 1, Data_val, elementNo_val, A.Data_val, 1, 0, Result.Data_val, elementNo_val);

	return Result;
}

////  Overloads for matrices
Column Column::operator*(const Matrix& A) const
{
	Column Result;

	if(elementNo_val != A.rowNo()) throw ErrorBall("Mismatched array sizes in function Column::operator*(const Matrix&)");
	Result.setSize(A.columnNo());
	LapackLib::fgemv('T', A.rowNo(), A.columnNo(), 1, A.Data_val, A.rowNo(), Data_val, 1, 0, Result.Data_val, 1);

	return Result;
}

////  Overloads for regular values
Column& Column::operator*=(FP_TYPE value)
{
	LapackLib::fscal(elementNo_val, value, Data_val, 1);
	
	return *this;
}
Column Column::operator*(FP_TYPE value) const
{
	Column Result(elementNo_val);

	LapackLib::fcopy(elementNo_val, Data_val, 1, Result.Data_val, 1);
	LapackLib::fscal(Result.elementNo_val, value, Result.Data_val, 1);

	return Result;
}
FP_TYPE Column::length() const
{
	FP_TYPE result;

	result = LapackLib::fnrm2(elementNo_val, Data_val, 1);
	return result;
}
FP_TYPE Column::rms() const
{
	FP_TYPE norm, result;

	norm = LapackLib::fnrm2(elementNo_val, Data_val, 1);
	result = sqrt(norm*norm/elementNo_val);

	return result;
}
