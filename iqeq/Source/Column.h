/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _COLUMN_H
#define _COLUMN_H

#include "definitions.h"
#include "ErrorBall.h"
#include "Matrix.h"
#include "Vector.h"
#include "FunctionBall.h"


#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;

#define COLUMN_DESC_SIZE 9

class Column
{
	friend class MPICommBall;
	friend class Matrix;

	protected:
		FP_TYPE *Data_val;
		int elementNo_val;

	public:

		//  The constructor
		Column();
		Column(int);
		Column(int, int);
		Column(const Column&);
		Column& operator=(const Column&);  
		Column& operator=(const Vector&);
		Column& operator=(const Array1D<FP_TYPE>&);
		int operator=(int);
		void null();

		//  The destructor
		~Column();

		//  The readers
		FP_TYPE& operator[](int) const;
		int elementNo() const;

		//  The writer
		void setSize(int);
		
		//  Overloads for addition and subtraction
		Column& operator+=(const Column&);  
		Column operator+(const Column&) const;     
		Column& operator-=(const Column&); 
		Column operator-(const Column&) const;    
		
		//  Overloads for multiplication - dot and outer
		FP_TYPE operator%(const Column&) const;
		Matrix operator&(const Column&);
		
		//  Overloads for matrices
		Column operator*(const Matrix&) const;
		//Column parallelMult(const Matrix&);
		
		//  Overloads for regular values
		Column& operator*=(FP_TYPE);    
		Column operator*(FP_TYPE) const;	

		//  Functions to deal with the length of the Column
		FP_TYPE length() const;
		FP_TYPE rms() const;
};

#endif



