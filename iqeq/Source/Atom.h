/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/


#ifndef _ATOM_H
#define _ATOM_H

#include "definitions.h"
#include "String.h"
#include "Vector.h"
#include "Array1D.h"

#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;

#define ATOM_ATOMNO 109

#define ATOM_DUMMY 0
#define ATOM_HYDROGEN 1
#define ATOM_HELIUM 2
#define ATOM_LITHIUM 3
#define ATOM_BERYLLIUM 4
#define ATOM_BORON 5
#define ATOM_CARBON 6
#define ATOM_NITROGEN 7
#define ATOM_OXYGEN 8
#define ATOM_FLUORINE 9
#define ATOM_NEON 10
#define ATOM_SODIUM 11
#define ATOM_MAGNESIUM 12
#define ATOM_ALUMINIUM 13
#define ATOM_SILICON 14
#define ATOM_PHOSPHORUS 15
#define ATOM_SULFUR 16
#define ATOM_CHLORINE 17
#define ATOM_ARGON 18
#define ATOM_POTASSIUM 19
#define ATOM_CALCIUM 20
#define ATOM_SCANDIUM 21
#define ATOM_TITANIUM 22
#define ATOM_VANADIUM 23
#define ATOM_CHROMIUM 24
#define ATOM_MANGANESE 25
#define ATOM_IRON 26
#define ATOM_COBALT 27
#define ATOM_NICKEL 28
#define ATOM_COPPER 29
#define ATOM_ZINC 30
#define ATOM_GALLIUM 31
#define ATOM_GERMANIUM 32
#define ATOM_ARSENIC 33
#define ATOM_SELENIUM 34
#define ATOM_BROMINE 35
#define ATOM_KRYPTON 36
#define ATOM_RUBIDIUM 37
#define ATOM_STRONTIUM 38
#define ATOM_YTTRIUM 39
#define ATOM_ZIRCONIUM 40
#define ATOM_NIOBIUM 41
#define ATOM_MOLYBDENUM 42
#define ATOM_TECHNETIUM 43
#define ATOM_RUTHENIUM 44
#define ATOM_RHODIUM 45
#define ATOM_PALLADIUM 46
#define ATOM_SILVER 47
#define ATOM_CADMIUM 48
#define ATOM_INDIUM 49
#define ATOM_TIN 50
#define ATOM_ANTIMONY 51
#define ATOM_TELLURIUM 52
#define ATOM_IODINE 53
#define ATOM_XENON 54
#define ATOM_CESIUM 55
#define ATOM_BARIUM 56
#define ATOM_LANTHANUM 57
#define ATOM_CERIUM 58
#define ATOM_PRASEODYMIUM 59
#define ATOM_NEODYMIUM 60
#define ATOM_PROMETHIUM 61
#define ATOM_SAMARIUM 62
#define ATOM_EUROPIUM 63
#define ATOM_GADOLINIUM 64
#define ATOM_TERBIUM 65
#define ATOM_DYSPROSIUM 66
#define ATOM_HOLMIUM 67
#define ATOM_ERBIUM 68
#define ATOM_THULIUM 69
#define ATOM_YTTERBIUM 70
#define ATOM_LUTETIUM 71
#define ATOM_HAFNIUM 72
#define ATOM_TANTALUM 73
#define ATOM_TUNGSTEN 74
#define ATOM_RHENIUM 75
#define ATOM_OSMIUM 76
#define ATOM_IRIDIUM 77
#define ATOM_PLATINUM 78
#define ATOM_GOLD 79
#define ATOM_MERCURY 80
#define ATOM_THALLIUM 81
#define ATOM_LEAD 82
#define ATOM_BISMUTH 83
#define ATOM_POLONIUM 84
#define ATOM_ASTATINE 85
#define ATOM_RADON 86
#define ATOM_FRANCIUM 87
#define ATOM_RADIUM 88
#define ATOM_ACTINIUM 89
#define ATOM_THORIUM 90
#define ATOM_PROTACTINIUM 91
#define ATOM_URANIUM 92
#define ATOM_NEPTUNIUM 93
#define ATOM_PLUTONIUM 94
#define ATOM_AMERICIUM 95
#define ATOM_CURIUM 96
#define ATOM_BERKELIUM 97
#define ATOM_CALIFORNIUM 98
#define ATOM_EINSTEINIUM 99
#define ATOM_FERMIUM 100
#define ATOM_MENDELEVIUM 101
#define ATOM_NOBELIUM 102
#define ATOM_LAWRENCIUM 103
#define ATOM_RUTHERFORDIUM 104
#define ATOM_DEUTERIUM 105
#define ATOM_LONE_PAIR 106
#define ATOM_PI_ORBITAL 107
#define ATOM_VIRTUAL_HYDROGEN 108

#define ATOM_HYBRID_NONE 0
#define ATOM_HYBRID_LINEAR 1
#define ATOM_HYBRID_TRIGONAL 2
#define ATOM_HYBRID_TETRAHEDRAL 3
#define ATOM_HYBRID_SQUARE_PLANAR 4
#define ATOM_HYBRID_TRIGONAL_BIPYRAMID 5
#define ATOM_HYBRID_SQUARE_PYRAMID 6 
#define ATOM_HYBRID_TRIGONAL_ANTIPRISM 7
#define ATOM_HYBRID_TRIGONAL_PRISM 8
#define ATOM_HYBRID_OCTAHEDRAL 9
#define ATOM_HYBRID_PENTAGONAL_BIPYRAMID 10
#define ATOM_HYBRID_SQUARE_ANTIPRISM 11
#define ATOM_HYBRID_OTHER 12

class Atom
{
	friend class MPICommBall;

	protected:
		String Name_val;		//  String that stores the user-set atom name
		String Symbol_val;		//  String that stores the element symbol
		int type_val;				//  Element number
		String FFType_val;		//  Forcefield type, in String form	
		FP_TYPE mass_val;			//  Atomic mass
		FP_TYPE charge_val;			//  Atomic charge
		FP_TYPE spin_val;			//  Atomic spin
		Vector Position_val;		//  Cartesian position
		Vector Force_val;			//  Cartesian force
		Vector Velocity_val;		//  Cartesian velocity
		FP_TYPE friction_val;		//  Value for friction needed for some thermostats
		bool frozen_val;			//  Boolean for whether or not the atom is frozen
		bool ghost_val;				//  Boolean for whether or not the atom is a ghost atom in DFT calculations
		int hybrid_val;
		int ringSize_val;
		bool aromatic_val;
		bool ringBridge_val;
		int shell_val, valenceElec_val, valenceBond_val;				
		int minCharge_val, maxCharge_val;
		FP_TYPE vdwRadius_val, covalentRadius_val, slaterRadius_val, surfaceRadius_val;
		FP_TYPE electroNeg_val, hardness_val;
		FP_TYPE gammaScreen_val, slaterScreen_val;
		FP_TYPE formalCharge_val;
		int formalSpin_val;

	public:
		
		//  The constructors
		Atom();
		Atom(Atom const&);
		Atom& operator=(Atom const&);
		int operator=(int);
		void null();

		//  Readers for the type, position and force on the atom
		const String& Name() const;
		const String& Symbol() const;
		int type() const;
		const String& FFType() const;
		FP_TYPE mass() const;
		FP_TYPE charge() const;
		FP_TYPE spin() const;
		const Vector& Position() const;
		const Vector& Force() const;
		const Vector& Velocity() const;
		FP_TYPE friction() const;

		bool frozen() const;
		bool ghost() const;
		int hybrid() const;
		int ringSize() const;
		bool isRing() const;
		bool aromatic() const;
		bool ringBridge() const;
		int shell() const;
		int valenceElec() const;
		int valenceBond() const;
		int minCharge() const;
		int maxCharge() const;
		FP_TYPE vdwRadius() const;
		FP_TYPE covalentRadius() const;
		FP_TYPE slaterRadius() const;
		FP_TYPE surfaceRadius() const;
		FP_TYPE electroNeg() const;
		FP_TYPE hardness() const;
		FP_TYPE slaterScreen() const;
		FP_TYPE gammaScreen() const;
		FP_TYPE formalCharge() const;
		int formalSpin() const;


		//  Some writer functions
		void setName(String const&);
		void setSymbol(String const&);
		void setType(int);
		void setFFType(String const&);
		void setMass(FP_TYPE);
		void setCharge(FP_TYPE);
		void setSpin(FP_TYPE);
		void setPosition(Vector const&);
		void setForce(Vector const&);
		void setVelocity(const Vector&);
		void setFriction(FP_TYPE);
		void setFrozen(bool);
		void setGhost(bool);
		void setShell(int);
		void setValenceElec(int);
		void setValenceBond(int);
		void setRingSize(int);
		void setHybrid(int);
		void setAromatic(bool);
		void setRingBridge(bool);
		void setMinCharge(int);
		void setMaxCharge(int);
		void setVdwRadius(FP_TYPE);
		void setCovalentRadius(FP_TYPE);
		void setSlaterRadius(FP_TYPE);
		void setSurfaceRadius(FP_TYPE);
		void setElectroNeg(FP_TYPE);
		void setHardness(FP_TYPE);
		void setSlaterScreen(FP_TYPE);
		void setGammaScreen(FP_TYPE);
		void setFormalCharge(FP_TYPE);
		void setFormalSpin(int);

		//  The comparison operators
		//  Note that the &= is translated as "roughly equal", that is the types are the same
		bool operator==(Atom const&);
		bool operator!=(Atom const&);
		bool operator%=(Atom const&);
		bool operator>(const Atom&);
		bool operator>=(const Atom&);
		bool operator<(const Atom&);
		bool operator<=(const Atom&);

		//  Operator overloads for translation and placement with vectors 
		//  And also functions for adding / subtracting forces
		Atom& operator+=(Vector const&);
		Atom& operator-=(Vector const&);
		void addForce(Vector const&);
		void subForce(Vector const&);
		void addVelocity(const Vector&);
		void subVelocity(const Vector&);
		void addFriction(FP_TYPE);

		//  A function to spin the atom, which just pretty much calls the vector functions
		void spin(Vector const&, FP_TYPE);
		void spin(Vector const&, Vector const&, FP_TYPE);
		void eulerSpin(FP_TYPE, FP_TYPE, FP_TYPE);
		void eulerSpin(FP_TYPE, FP_TYPE, FP_TYPE, const Vector&);

		//  Functions to handle stuff with the periodic table
		int periodicTableRow();
		int periodicTableColumn();
		int perodicTableGroup();

		bool isOrganicCovalent() const;
		bool isMetal() const;
		int loneFormalCharge() const;
		

		//  An overload to output the details of the atom
		friend ostream & operator<<(ostream &, Atom const&);
};


#endif

		
