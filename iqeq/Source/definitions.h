
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H

#ifdef COMPILE_PARALLEL
	#include <mpi.h>
#endif

#if defined ENV_WINDOWS
	#pragma warning ( disable : 4996 )
	#pragma warning ( disable : 4985 )
#endif

#define FP_SINGLE 0
#define FP_DOUBLE 1
#define FP_PRECISION FP_DOUBLE

#if FP_PRECISION == FP_SINGLE
	#define FP_TYPE float
	#define MPI_FP_TYPE MPI_FLOAT
	#define MPI_FP_TYPE_INT MPI_FLOAT_INT
	#define MACHINE_ZERO 1E-8
	#define ZERO (FP_TYPE)1E-25 
	#define INF (FP_TYPE)1E30
	#define LN_INF 88
	#define ZEPS (FP_TYPE)1E-6 
#else
	#define FP_TYPE double
	#define MPI_FP_TYPE MPI_DOUBLE
	#define MPI_FP_TYPE_INT MPI_DOUBLE_INT
	#define MACHINE_ZERO 1E-15
	#define ZERO (FP_TYPE)1E-300 
	#define INF (FP_TYPE)1E307
	#define LN_INF (FP_TYPE) 708
	#define ZEPS (FP_TYPE)1E-10 
#endif

#define ATOM_DATA_FILE_PATH ""

#define PI (FP_TYPE)3.1415926535897932384626433832795
#define TWOPI (FP_TYPE)6.283185307179586476925286766559
#define HALFPI (FP_TYPE)1.5707963267948966192313216916398
#define ONE_HALF (FP_TYPE)0.5
#define EULER 0.5772156649015328606065120900824024310421
#define TWO_THIRD (FP_TYPE)2.0/(FP_TYPE)3.0

#define RAD_TO_DEG (FP_TYPE)57.295779513082320876798154814105
#define DEG_TO_RAD (FP_TYPE)0.017453292519943295769236907684886

#define BOHR_TO_ANGSTROM (FP_TYPE)0.5291772
#define ANGSTROM_TO_BOHR (FP_TYPE)1.8897162
#define COULOMB (FP_TYPE)332.0637090025480
#define BOLTZMANN_VELOCITY (FP_TYPE)8.3144648E-07
#define AVAGADRO (FP_TYPE)6.0221415E23
#define CALORIE (FP_TYPE)4.184
#define EV_TO_KCAL (FP_TYPE)23.06054194532934

#define CELLSYMM_GENERIC 0
#define CELLSYMM_XYZ_NONE 1
#define CELLSYMM_ZYX_NONE 2
#define CELLSYMM_XYZ_TRIG 3
#define CELLSYMM_ZYX_TRIG 4
#define CELLSYMM_SQUARE 5
#define CELLSYMM_NONSTANDARD 6

#define PERIODIC_TABLE_GROUP_NONE 0
#define PERIODIC_TABLE_GROUP_S 1
#define PERIODIC_TABLE_GROUP_P 2
#define PERIODIC_TABLE_GROUP_D 3
#define PERIODIC_TABLE_GROUP_F 4

//  Forward class definitions to get me out of trouble with the compiler
class Atom;
class AtomDataBall;
class Calculus;
class ChargeEquilibrator;
class Column;
class Complex;
class ErrorBall;
class EwaldPotential;
class FunctionBall;
class Geometry;
class InputChargeFitBall;
class KVector;
class Lapack;
class Matrix;
class String;
class Structure;
class Tensor;
class Vector;
class XmlLine;

#endif
