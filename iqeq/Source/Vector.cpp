
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Vector.h"
#include "ErrorBall.h"
#include "definitions.h"
#include "Tensor.h"

#include <mkl.h>

#include <cmath>
#include <iomanip>

//  The constructor that just sets everything to 0
Vector::Vector() { null(); }

//  The copy constructor
Vector::Vector(const Vector& Source)
{
	x_val = Source.x();
	y_val = Source.y();
	z_val = Source.z();
}

//  A cartesian constructor
Vector::Vector(FP_TYPE x, FP_TYPE y, FP_TYPE z)
{
	x_val = x;
	y_val = y;
	z_val = z;
}
void Vector::null()
{
	x_val = 0;
	y_val = 0;
	z_val = 0;
}


//  The wrapper functions that allow read only access to the vector internals
FP_TYPE Vector::x() const { return x_val; }
FP_TYPE Vector::y() const { return y_val; }
FP_TYPE Vector::z() const { return z_val; }

FP_TYPE Vector::r() const 
{ 
	return sqrt((x_val*x_val)+(y_val*y_val)+(z_val*z_val));
}
FP_TYPE Vector::theta() const
{
	FP_TYPE radius;
	FP_TYPE theta;

	radius = r();
	if(radius <= 0) theta = 0;
	else theta = acos(z_val/radius);

	return theta;
}
FP_TYPE Vector::phi() const
{
	FP_TYPE phi, rdash;

	phi = 0;
	rdash = sqrt((x_val*x_val)+(y_val*y_val));
	if(fabs(rdash) < ZERO) phi = 0;
	else if(y_val >= 0) phi = acos(x_val/rdash);
	else phi = TWOPI - acos(x_val/rdash);

	return phi;
}

FP_TYPE Vector::xSq() const			{ return x_val*x_val; }
FP_TYPE Vector::ySq() const			{ return y_val*y_val; }
FP_TYPE Vector::zSq() const			{ return z_val*z_val; }
FP_TYPE Vector::rSq() const			{ return (x_val*x_val) + (y_val*y_val) + (z_val*z_val); }

Vector Vector::xUnit() const
{
	Vector Result;
	Result.setCart(1, 0, 0);
	return Result;
}
Vector Vector::yUnit() const
{
	Vector Result;
	Result.setCart(0, 1, 0);
	return Result;
}
Vector Vector::zUnit() const
{
	Vector Result;
	Result.setCart(0, 0, 1);
	return Result;
}

//  Curvilinear unit vectors based on
//  http://en.wikipedia.org/wiki/Del_in_cylindrical_and_spherical_coordinates
Vector Vector::rUnit() const
{
	Vector Result;

	Result = xUnit() * x_val;
	Result += yUnit() * y_val;
	Result += zUnit() * z_val;
	Result /= r();
	
	return Result;
}
Vector Vector::thetaUnit() const
{
	FP_TYPE rho;
	Vector Result;

	rho = sqrt((x_val*x_val)+(y_val*y_val));

	Result = xUnit() * x_val * z_val;
	Result += yUnit() * y_val * z_val;
	Result += zUnit() * -1 * rho * rho;
	Result /= r() * rho;
	
	return Result;
}
Vector Vector::phiUnit() const
{
	FP_TYPE rho;
	Vector Result;

	rho = sqrt((x_val*x_val)+(y_val*y_val));

	Result = xUnit() * -y_val;
	Result += yUnit() * x_val;
	Result /= rho;
	
	return Result;
}
FP_TYPE Vector::rMax(FP_TYPE rMin) const
{
	FP_TYPE rr;

	rr = r();
	if(rr < rMin) rr = rMin;

	return rr;
}

//  An operator overload so that I can access X,Y,Z like an array
FP_TYPE& Vector::operator[](int index)
{
	if(index == 0) return x_val;
	else if(index == 1) return y_val;
	else if(index == 2) return z_val;
	else throw ErrorBall("Array out of bounds access in function Vector::operator[]\n");
}
FP_TYPE Vector::cartesian(int index) const
{
	if(index == 0) return x_val;
	else if(index == 1) return y_val;
	else if(index == 2) return z_val;
	else throw ErrorBall("Array out of bounds access in function Vector::operator[]\n");
}

//  Function to set a new vector using cartesian co-ords
void Vector::setCart(FP_TYPE x, FP_TYPE y, FP_TYPE z)
{
	x_val = x;
	y_val = y;
	z_val = z;
}

//  Function for setting a new vector using the polar coordinates
void Vector::setPolar(FP_TYPE r, FP_TYPE theta, FP_TYPE phi)
{
	x_val = r * sin(theta) * cos(phi);
	y_val = r * sin(theta) * sin(phi);
	z_val = r * cos(theta);
}

//  Function for setting cylindrical polar coordinates
void Vector::setCylinderPolar(FP_TYPE r, FP_TYPE theta, FP_TYPE z)
{
	x_val = r * cos(theta);
	y_val = r * sin(theta);
	z_val = z;
}

//  Function to set an individual element
void Vector::setElement(int i, FP_TYPE value)
{
	if(i == 0) x_val = value;
	else if(i == 1) y_val = value;
	else if(i == 2) z_val = value;
}

//  A function for setting the vector to zero
void Vector::zero()
{
	x_val = 0;
	y_val = 0;
	z_val = 0;
}

//  A function for setting the current vector to the unit vector
Vector Vector::unit() const
{
	return *this / r();
}

bool Vector::operator==(const Vector& Source) const
{
	bool flag;
	
	flag = true;

	if(fabs(x_val-Source.x()) > ZERO) flag = false;
	if(fabs(y_val-Source.y()) > ZERO) flag = false;
	if(fabs(z_val-Source.z()) > ZERO) flag = false;

	return flag;
}

bool Vector::operator!=(const Vector& Source) const
{
	bool flag;
	
	flag = false;

	if(fabs(x_val-Source.x()) > ZERO) flag = true;
	if(fabs(y_val-Source.y()) > ZERO) flag = true;
	if(fabs(z_val-Source.z()) > ZERO) flag = true;

	return flag;
}
bool Vector::operator<(const Vector& Source) const
{
	if(r() < Source.r()) return true;
	else return false;
}
bool Vector::operator>(const Vector& Source) const
{
	if(r() > Source.r()) return true;
	else return false;
}

//  Overload to test if a vector is a null vector or not
bool Vector::operator!()
{
	bool flag;
	
	flag = true;

	if(fabs(x_val) > ZERO) flag = false;
	if(fabs(y_val) > ZERO) flag = false;
	if(fabs(z_val) > ZERO) flag = false;

	return flag;
}

Vector& Vector::operator=(const Vector& Source)
{
	x_val = Source.x();
	y_val = Source.y();
	z_val = Source.z();

	return *this;
}
Vector& Vector::operator=(Vector* Source)
{
	x_val = Source->x();
	y_val = Source->y();
	z_val = Source->z();

	delete Source;	
	return *this;
}
int Vector::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in Vector::operator=(int i)");
	null();

	return i;
}

Vector& Vector::operator+=(const Vector& Source)
{
	x_val += Source.x();
	y_val += Source.y();
	z_val += Source.z();
	return *this;
}


Vector Vector::operator+(const Vector& Source) const
{
	Vector Result;
	
	Result.setCart(x_val+Source.x(), y_val+Source.y(), z_val+Source.z());
	return Result;
}


Vector& Vector::operator-=(const Vector& Source)
{
	x_val -= Source.x();
	y_val -= Source.y();
	z_val -= Source.z();
	return *this;
}

Vector Vector::operator-(const Vector& Source) const
{	
	Vector Result;
	
	Result.setCart(x_val-Source.x(), y_val-Source.y(), z_val-Source.z());
	return Result;
}

//  Functions to multiply and divide by scalars, done with operator overloading
Vector& Vector::operator*=(FP_TYPE scale)
{
	x_val *= scale;
	y_val *= scale;
	z_val *= scale;
	return *this;
}

Vector Vector::operator*(FP_TYPE scale) const
{
	Vector Result;
	
	Result.setCart(x_val*scale, y_val*scale, z_val*scale);
	return Result;
}

Vector& Vector::operator/=(FP_TYPE scale)
{
	x_val /= scale;
	y_val /= scale;
	z_val /= scale;
	return *this;
}

Vector Vector::operator/(FP_TYPE scale) const
{
	Vector Result;
	
	Result.setCart(x_val/scale, y_val/scale, z_val/scale);
	return Result;
}

Vector Vector::operator-() const
{
	return *this * -1;
}

Vector Vector::operator*(Vector const& Source) const
{
	Vector Result;
	FP_TYPE x, y, z;
	
	x = y_val*Source.z() - z_val*Source.y();
	y = z_val*Source.x() - x_val*Source.z();
	z = x_val*Source.y() - y_val*Source.x();
	Result.setCart(x, y, z);

	return Result;
}

Vector Vector::unitCross(Vector const& Source) const
{
	Vector Result;
	FP_TYPE x, y, z;
	FP_TYPE angle, mag;

	angle = *this |= Source;
	mag = sin(angle);
	
	//  Do the one for vectors very close to paralell
	if(fabs(mag) < 1E-3)
	{
		x = y = z = 0;
		if(fabs(Source.x()) > 1E-6 && fabs(Source.z()) > 1E-6)
		{
			x = -Source.z();
			z = Source.x();
		}
		else if(fabs(Source.x()) > 1E-6 && fabs(Source.y()) > 1E-6)
		{
			x = -Source.y();
			y = Source.x();
		}
		else if(fabs(Source.y()) > 1E-6 && fabs(Source.z()) > 1E-6)
		{
			y = -Source.z();
			z = Source.y();
		}
		else if(fabs(Source.x()) > 1E-6) y = 1;
		else if(fabs(Source.y()) > 1E-6) z = 1;
		else x = 1;

	}
	else
	{	
		x = y_val*Source.z() - z_val*Source.y();
		y = z_val*Source.x() - x_val*Source.z();
		z = x_val*Source.y() - y_val*Source.x();
	}

	Result.setCart(x, y, z);
	Result = Result.unit();

	return Result;
}


FP_TYPE Vector::operator%(const Vector& Source) const
{
	FP_TYPE result;

	result = x_val * Source.x();
	result += y_val * Source.y();
	result += z_val * Source.z();

	return result;
}

Tensor Vector::operator&(const Vector& Source) const
{
	Tensor Result;
	
	Result(0, 0) = x_val*Source.x();
	Result(0, 1) = x_val*Source.y();
	Result(0, 2) = x_val*Source.z();
	Result(1, 0) = y_val*Source.x();
	Result(1, 1) = y_val*Source.y();
	Result(1, 2) = y_val*Source.z();
	Result(2, 0) = z_val*Source.x();
	Result(2, 1) = z_val*Source.y();
	Result(2, 2) = z_val*Source.z();

	return Result;
}

Vector Vector::operator|(const Vector& B) const
{
	Vector Result;
	Result.setCart(x_val*B.x(), y_val*B.y(), z_val*B.z());
	return Result;
}

Vector Vector::bracketProduct(const Vector& B) const
{
	FP_TYPE xx, yy, zz;
	Vector Result;

	xx = (y_val * B.y()) + (z_val * B.z());
	yy = (x_val * B.x()) + (z_val * B.z());
	zz = (x_val * B.x()) + (y_val * B.y());

	Result.setCart(xx, yy, zz);
	return Result;
}

Tensor Vector::operator&(const Tensor& T) const
{
	Tensor Result;
	int s, u, a, b;

	for(a = 0; a < 3; a++)
	{
		for(b = 0; b < 3; b++)
		{
			s = (b+1) % 3;
			u = (b+2) % 3;

			Result(a, b) = (*this).cartesian(s) * T.element(a, u);
			Result(a, b) -= (*this).cartesian(u) * T.element(a, s);
		}
	}

	return Result;
}

FP_TYPE Vector::operator|=(const Vector& Source) const
{
	FP_TYPE dotProd, angle;
	FP_TYPE r1, r2;

	r1 = this->r();
	r2 = Source.r();
	if(r1 <= 0 || r2 <= 0) 
		throw ErrorBall("Attempting to take angle of null vector in function Vector::operator|=");
	else
	{
		dotProd = x_val * Source.x();
		dotProd += y_val * Source.y();
		dotProd += z_val * Source.z_val;
		dotProd /= (r1 * r2);
		if(dotProd >= 1) angle = 0;
		else if(dotProd <= -1) angle = PI;
		else angle = acos(dotProd);
	}

	return angle;
}


Vector Vector::operator||(const Vector& Source) const
{
	Vector Result;

	Result.x_val = (x_val+Source.x())/2;
	Result.y_val = (y_val+Source.y())/2;
	Result.z_val = (z_val+Source.z())/2;

	return Result;
}


Vector Vector::spin(const Vector& Axis, FP_TYPE angle) const
{
	Tensor Transform;
	FP_TYPE x, y, z, x2, y2, z2, ct, st, t;
	Vector Temp;
	Vector newPoint;
	Vector Result;

	Temp = Axis.unit();
	
	x = Temp.x_val;	y = Temp.y_val;	z = Temp.z_val; 
	x2 = pow(x,2); y2 = pow(y,2); z2 = pow(z,2); 
	st = sin(angle);  ct = cos(angle); t = 1-ct;
	
	Transform(0,0) = t*x2 + ct;
	Transform(0,1) = t*x*y - st*z;
	Transform(0,2) = t*x*z + st*y;
	Transform(1,0) = t*x*y + st*z;
	Transform(1,1) = t*y2  + ct;
	Transform(1,2) = t*y*z - st*x;
	Transform(2,0) = t*x*z - st*y;
	Transform(2,1) = t*y*z + st*x;
	Transform(2,2) = t*z2  + ct;

	newPoint = (Transform * *this);

	Result.setCart(newPoint.x(), newPoint.y(), newPoint.z());
	return Result;
}

Vector Vector::spin(const Vector& Axis, const Vector& RotPoint, FP_TYPE angle) const
{
	Tensor Transform;
	FP_TYPE x, y, z, x2, y2, z2, ct, st, t;
	Vector Temp;
	Vector newPoint;
	Vector Result;

	Result.setCart(x_val-RotPoint.x(), y_val-RotPoint.y(), z_val-RotPoint.z());

	Temp = Axis.unit();
	
	x = Temp.x_val;	y = Temp.y_val;	z = Temp.z_val; 
	x2 = pow(x,2); y2 = pow(y,2); z2 = pow(z,2); 
	st = sin(angle);  ct = cos(angle); t = 1-ct;
	
	Transform(0,0) = t*x2 + ct;
	Transform(0,1) = t*x*y - st*z;
	Transform(0,2) = t*x*z + st*y;
	Transform(1,0) = t*x*y + st*z;
	Transform(1,1) = t*y2  + ct;
	Transform(1,2) = t*y*z - st*x;
	Transform(2,0) = t*x*z - st*y;
	Transform(2,1) = t*y*z + st*x;
	Transform(2,2) = t*z2  + ct;

	newPoint = Transform * Result;

	Result.setCart(newPoint.x()+RotPoint.x(), newPoint.y()+RotPoint.y(), newPoint.z()+RotPoint.z());
	return Result;
}
//  Information from the page
//http://en.wikipedia.org/wiki/Euler_angles
Vector Vector::eulerSpin(FP_TYPE phi, FP_TYPE theta, FP_TYPE psi) const
{
	Vector Zero(0,0,0);
	return eulerSpin(phi, theta, psi, Zero);
}
Vector Vector::eulerSpin(FP_TYPE phi, FP_TYPE theta, FP_TYPE psi, const Vector& RotPoint) const
{
	FP_TYPE c1, c2, c3, s1, s2, s3;
	Tensor Transform;
	Vector Result, NewPoint;

	Result.setCart(x_val-RotPoint.x(), y_val-RotPoint.y(), z_val-RotPoint.z());

	c1 = cos(phi);
	c2 = cos(theta);
	c3 = cos(phi);
	s1 = sin(phi);
	s2 = sin(theta);
	s3 = sin(phi);

	Transform(0,0) = c2;
	Transform(0,1) = -c3*s2;
	Transform(0,2) = s2*s3;
	Transform(1,0) = c1*s2;
	Transform(1,1) = (c1*c2*c3) - (s1*s3);
	Transform(1,2) = (-c3*s1) - (c1*c2*s3);
	Transform(2,0) = s1*s2;
	Transform(2,1) = (c1*s3) + (c2*c3*s1);
	Transform(2,2) = (c1*c3) - (c2*s1*s3);

	NewPoint = Transform * Result;
	Result.setCart(NewPoint.x()+RotPoint.x(), NewPoint.y()+RotPoint.y(), NewPoint.z()+RotPoint.z());

	return NewPoint;
}

Vector Vector::project(const Vector& Axis) const
{
	FP_TYPE factor;
	Vector TempA, TempB, Result;

	TempA = *this;
	TempB = Axis.unit();
	factor = (TempA % TempB);

	Result = TempB * factor;
	return Result;
}

Vector Vector::project(const Vector& PlaneA, const Vector& PlaneB) const
{
	Vector Norm, Result;
	Tensor Project;
	FP_TYPE x, y, z;
	
	//  Get the normal to the plane, which defines it
	Norm = (PlaneA * PlaneB).unit();
	x = Norm.x();
	y = Norm.y();
	z = Norm.z();

	Project(0, 0) = pow(y,2) + pow(z,2);
	Project(1, 1) = pow(x,2) + pow(z,2);
	Project(2, 2) = pow(x,2) + pow(y,2);
	Project.setSymmetricElement(0, 1, -x*y);
	Project.setSymmetricElement(0, 2, -x*z);
	Project.setSymmetricElement(1, 2, -y*z);

	Result = Project * *this;
	return Result;
}


//  An overload for output, so that I can easily test values of vectors
ostream& operator<<(ostream &output, const Vector& A)
{
	output.precision(4);
	output.setf(ios::showpoint | ios::fixed);
	FP_TYPE theta, phi;

	theta = A.theta() * RAD_TO_DEG;
	phi = A.phi() * RAD_TO_DEG;

	output << setw(4) << right << "X=" << setw(7) << right << A.x();
	output << setw(4) << right << "Y=" << setw(7) << right << A.y();
	output << setw(4) << right << "Z=" << setw(7) << right << A.z();
	output << setw(4) << right << "R=" << setw(7) << right << A.r();
	output << setw(4) << right << "T=" << setw(10) << right << theta;
	output << setw(4) << right << "P=" << setw(10) << right << phi;

	output << endl;

	return output;
}









	






