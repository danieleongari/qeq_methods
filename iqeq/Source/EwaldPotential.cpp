
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "definitions.h"
#include "EwaldPotential.h"
#include "FunctionBall.h"
#include "Complex.h"

EwaldPotential::EwaldPotential()	{ null(); }
EwaldPotential::EwaldPotential(const EwaldPotential& A)
{
	*this = A;
}
const EwaldPotential& EwaldPotential::operator=(const EwaldPotential& A)
{
	IndexI_val = A.IndexI_val;
	IndexJ_val = A.IndexJ_val;
	Rij_val = A.Rij_val;
	SumPair_val = A.SumPair_val;
	MinImageFactor_val = A.MinImageFactor_val;
	TotalTerms_val = A.TotalTerms_val;

	//  1D variable lists
	SumSingle_val = A.SumSingle_val;

	sumMethod_val = A.sumMethod_val;
	realCut_val = A.realCut_val;
	alpha_val = A.alpha_val;
	error_val = A.error_val;
	volume_val = A.volume_val;
	fourierCut_val = A.fourierCut_val;
	selfPairEnergy_val = A.selfPairEnergy_val;
	selfSingleEnergy_val = A.selfSingleEnergy_val;
	energyTail_val = A.energyTail_val;

	CellVect_val = A.CellVect_val;
	Kvect_val = A.Kvect_val;
	RealVect_val = A.RealVect_val;
	FourierVect_val = A.FourierVect_val;
	FourierVectTotal_val = A.FourierVectTotal_val;

	return *this;
}
int EwaldPotential::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in EwaldPotential::operator=(int i)");
	null();

	return i;
}
void EwaldPotential::null()
{
	IndexI_val.setSize(0);
	IndexJ_val.setSize(0);
	Rij_val.setSize(0);
	MinImageFactor_val.setSize(0);
	SumPair_val.setSize(0);
	TotalTerms_val.setSize(0);

	//  1D variable lists
	SumSingle_val.setSize(0);

	//  Translation variables definitions
	sumMethod_val = EWALDPOTENTIAL_SUMMATION_METHOD_ATOMIC;
	realCut_val = 18.5;
	alpha_val = 0;
	error_val = (FP_TYPE)1E-5;
	volume_val = 0;
	fourierCut_val = 0;
	selfPairEnergy_val = 0;
	selfSingleEnergy_val = 0;
	energyTail_val = 0;

	CellVect_val.setSize(3);
	Kvect_val.setSize(3);
	RealVect_val.setSize(0);
	FourierVect_val.setSize(0);
	FourierVectTotal_val.setSize(0);

	selfPairEnergy_val = selfSingleEnergy_val = 0;
}
void EwaldPotential::copyTranslationData(const EwaldPotential& A)
{
	int i;

	sumMethod_val = A.sumMethod_val;
	realCut_val = A.realCut_val;
	fourierCut_val = A.fourierCut_val;
	alpha_val = A.alpha_val;
	error_val = A.error_val;
	volume_val = A.volume_val;
	selfPairEnergy_val = A.selfPairEnergy_val;
	selfSingleEnergy_val = A.selfSingleEnergy_val;
	energyTail_val = A.energyTail_val;

	CellVect_val.setSize(A.cellVectNo());
	Kvect_val.setSize(A.kVectNo());
	RealVect_val.setSize(A.realNo());
	FourierVect_val.setSize(A.fourierNo());
	for(i = 0; i < CellVect_val.length(); i++) CellVect_val[i] = A.CellVect(i);
	for(i = 0; i < Kvect_val.length(); i++) Kvect_val[i] = A.Kvect(i);
	for(i = 0; i < RealVect_val.length(); i++) RealVect_val[i] = A.RealVect(i);
	for(i = 0; i < FourierVect_val.length(); i++) FourierVect_val[i] = A.FourierVect(i);
}

//  Readers for various things
int EwaldPotential::termNo() const											{ return IndexI_val.length(); }
int EwaldPotential::sumMethod() const										{ return sumMethod_val; }
bool EwaldPotential::sumMethodGroup() const
{
	switch(sumMethod_val)
	{
		case EWALDPOTENTIAL_SUMMATION_METHOD_EWALD:
		{
			return true;
		}
		default:
		{
			return false;
		}
	}
}
int EwaldPotential::sumPower() const
{
	return 1;
}
FP_TYPE EwaldPotential::realCut() const										{ return realCut_val; }
FP_TYPE EwaldPotential::fourierCut() const									{ return fourierCut_val; }
FP_TYPE EwaldPotential::alpha() const										{ return alpha_val; }
FP_TYPE EwaldPotential::volume() const										{ return volume_val; }
int EwaldPotential::cellVectNo() const										{ return CellVect_val.length(); }
int EwaldPotential::kVectNo() const											{ return Kvect_val.length(); }
int EwaldPotential::realNo() const											{ return RealVect_val.length(); }
int EwaldPotential::fourierNo() const										{ return FourierVect_val.length(); }

const Vector& EwaldPotential::CellVect(int i) const							{ return CellVect_val[i]; }
const Vector& EwaldPotential::Kvect(int i) const							{ return Kvect_val[i]; }
const Vector& EwaldPotential::RealVect(int i) const							{ return RealVect_val[i]; }
const KVector& EwaldPotential::FourierVect(int k) const						{ return FourierVect_val[k]; }

void EwaldPotential::setSumMethod(int data)									{ sumMethod_val = data; }
void EwaldPotential::setCutoff(FP_TYPE data)								{ realCut_val = data; }
void EwaldPotential::setError(FP_TYPE data)									{ error_val = data; }

//  Functions for creating terms and setting up the object
void EwaldPotential::createTerms(const Geometry& Geom)
{
	int i, j, place, size;
	int start;
	Vector Distance;
	bool termFound;

	//  Now count the number of VDW terms needed and alloc the memory
	place = 0;
	for(i = 0; i < Geom.atomNo(); i++)
	{
		start = (Geom.period() > 0) ? i : i+1;
		for(j = start; j < Geom.atomNo(); j++) place++;
	}
	size = place;

	IndexI_val.setSize(size);
	IndexJ_val.setSize(size);
	MinImageFactor_val.setSize(size);

	place = 0;
	for(i = 0; i < Geom.atomNo(); i++)
	{
		start = (Geom.period() > 0) ? i : i+1;
		for(j = start; j < Geom.atomNo(); j++)
		{
			termFound = false;
			MinImageFactor_val[place] = 1;

			//  Trans only if the same translated atom
			if(i == j) 
			{
				MinImageFactor_val[place] = 0;
				termFound = true;
			}
				
			//  Set all of the list memory stuff
			IndexI_val[place] = i;
			IndexJ_val[place] = j;
			place++;
		}
	}

	//  Now alloc up the atomic array lists
	setParamLists(Geom);
}
void EwaldPotential::setParamLists(const Geometry& Geom)
{
	int i, size;
	FP_TYPE lambda = 0.5;

	size = IndexI_val.length();

	//  Set the single variable lists
	SumSingle_val.setSize(Geom.atomNo());
	for(i = 0; i < Geom.atomNo(); i++) 
	{
		SumSingle_val[i] = 0;
	}

	//  Set the pair variable lists
	SumPair_val.setSize(size);
	Rij_val.setSize(size);
	TotalTerms_val.setSize(size);
	for(i = 0; i < size; i++)
	{
		SumPair_val[i] = 0;
		TotalTerms_val[i] = i;
	}
}

FP_TYPE EwaldPotential::realEwaldError(FP_TYPE sumB)
{
	return realEwaldDiagError(sumB);
}
FP_TYPE EwaldPotential::realEwaldDiagError(FP_TYPE sumB)
{
	int n;
	FP_TYPE result, ac, aac, rc, temp;
	FunctionBall Func;

	ac = alpha_val * realCut_val;
	aac = ac * ac;
	rc = realCut_val;

	n = sumPower();

	result = sqrt(PI) * sumB * pow(alpha_val, n-(FP_TYPE)1.5);
	result /= Func.halfGamma(n) * sqrt(2*volume_val);
	temp = Func.halfUpperIncompleteGamma(-1, 2*aac);
	result = (temp < 0) ? 0 : result * sqrt(temp);

	return result;
}
FP_TYPE EwaldPotential::fourierEwaldDiagError(FP_TYPE sumB)
{
	int n;
	FP_TYPE result, bc, bbc, hc;
	FunctionBall Func;

	bc = fourierCut_val / (2*alpha_val);
	bbc = bc * bc;
	hc = fourierCut_val;

	n = sumPower();

	result = 2 * pow(alpha_val, n) * sumB;
	result /= Func.halfGamma(n) * sqrt(volume_val) * pow(hc,(FP_TYPE)1.5);
	result *= exp(-bbc);

	return result;
}
//
void EwaldPotential::setCellParam(const Geometry& Geom)
{
	int i;
	//  If the structure is non-periodic
	if(Geom.period() == 0)
	{
		CellVect_val.setSize(0);
		Kvect_val.setSize(0);
		RealVect_val.setSize(0);
		FourierVect_val.setSize(0);

		volume_val = -1;
	}
	else
	{
		CellVect_val.setSize(3);
		for(i = 0; i < 3; i++) CellVect_val[i] = Geom.Translation(i);
		//CellVect_val = Geom.Translation();
		volume_val = fabs(CellVect_val[0] % (CellVect_val[1]*CellVect_val[2]));

		Kvect_val.setSize(3);
		Kvect_val[0] = CellVect_val[1] * CellVect_val[2];
		Kvect_val[1] = CellVect_val[2] * CellVect_val[0];
		Kvect_val[2] = CellVect_val[0] * CellVect_val[1];
		Kvect_val[0] *= 2 * PI / volume_val;
		Kvect_val[1] *= 2 * PI / volume_val;
		Kvect_val[2] *= 2 * PI / volume_val;
	}
}
bool EwaldPotential::cellVectCube() const
{
	bool cube;
	FP_TYPE alpha, beta, gamma, tolerance;

	tolerance  = 0.1 * DEG_TO_RAD;
	alpha = CellVect_val[1] |= CellVect_val[2];
	beta = CellVect_val[0] |= CellVect_val[2];
	gamma = CellVect_val[0] |= CellVect_val[1];

	cube = true;
	if(fabs(CellVect_val[0].r()-CellVect_val[1].r()) > tolerance) cube = false;
	if(fabs(CellVect_val[0].r()-CellVect_val[2].r()) > tolerance) cube = false;
	if(fabs(CellVect_val[1].r()-CellVect_val[2].r()) > tolerance) cube = false;

	return cube;
}
void EwaldPotential::setEwaldRcut(FP_TYPE totalSum)
{
	FP_TYPE rcutLow, rcutHigh, error;

	rcutLow = (FP_TYPE)0.1;		
	rcutHigh = (FP_TYPE)200;
	while((rcutHigh-rcutLow) > 1E-2)
	{
		realCut_val = (rcutLow + rcutHigh) / 2;
		error = realEwaldError(totalSum);

		if(error < error_val) rcutHigh = realCut_val;
		else rcutLow = realCut_val;
	}
}
void EwaldPotential::setEwaldHcut(FP_TYPE totalSum)
{
	FP_TYPE hcutLow, hcutHigh, error;

	hcutLow = (FP_TYPE)0.01;		
	hcutHigh = (FP_TYPE)100;
	while((hcutHigh-hcutLow) > 1E-2)
	{
		fourierCut_val = (hcutHigh+hcutLow) / 2;
		error = fourierEwaldDiagError(totalSum);
		if(error < error_val) hcutHigh = fourierCut_val;
		else hcutLow = fourierCut_val;
	}
}
void EwaldPotential::setTransParam(FP_TYPE atoms, FP_TYPE terms, FP_TYPE totalSum, FP_TYPE totalGroup)
{ 
	FP_TYPE alphaLow, alphaHigh, realNum, fourierNum, effectNum, blankNum, ratio, ratioTarget;

	switch(sumMethod())
	{
		case EWALDPOTENTIAL_SUMMATION_METHOD_ATOMIC:
		{
			break;
		}
		case EWALDPOTENTIAL_SUMMATION_METHOD_EWALD:
		{
			ratioTarget = EWALDPOTENTIAL_RATIO_R1;

			//  Loop for the right alpha value
			alphaLow = (FP_TYPE)0.001;		
			alphaHigh = (FP_TYPE)20;
		
			while((alphaHigh-alphaLow) > 1E-4)
			{
				alpha_val = (alphaHigh+alphaLow) / 2;

				//  Now get the right cutoffs for this eta
				setEwaldRcut(totalSum);
				setEwaldHcut(totalSum);

				//  Now get the expected number of terms and the ratio of them
				effectNum = 4 * PI * pow(realCut_val, 3) * terms / (3 * volume_val);
				blankNum = (transVectNo(realCut_val)*terms) - effectNum;
				realNum = effectNum + (EWALDPOTENTIAL_BLANKSIZE*blankNum);

				fourierNum = (pow(fourierCut_val, 3) * volume_val / (12*PI*PI)) - 1;
				fourierNum *= atoms;

				ratio = fourierNum / realNum;
				if(ratio > ratioTarget) alphaHigh = alpha_val;
				else alphaLow = alpha_val;
			}

			break;
		}
	}
}
void EwaldPotential::setTransVect(FP_TYPE atoms, FP_TYPE terms, FP_TYPE totalSum, FP_TYPE totalGroup)
{
	setTransParam(atoms, terms, totalSum, totalGroup);

	RealVect_val = TransVect(realCut_val, CellVect_val);
	FourierVect_val = FourierVect(false, fourierCut_val, alpha_val, volume_val, Kvect_val);
}

FP_TYPE EwaldPotential::fourierFactor(FP_TYPE b, FP_TYPE h) const
{
	int n;
	FP_TYPE factor;
	FunctionBall Func;

	n = sumPower();
	
	factor = pow(h, n-3) * Func.halfUpperIncompleteGamma(3-n, b*b);
	factor *= pow(PI, (FP_TYPE)1.5);
	factor /= pow((FP_TYPE)2, n-3) * volume_val * Func.halfGamma(n);

	return factor;
}
int EwaldPotential::transVectNo(FP_TYPE cutoff) const
{
	int totalX, totalY, totalZ;
	FP_TYPE maxLength;
	int i, j, k, count;
	Vector Trans;
	FunctionBall Func;

	if(CellVect_val.length() > 0)
	{
		maxLength = (CellVect_val[0] + CellVect_val[1] + CellVect_val[2]).r() / 2;
		totalX = (int)Func.round(cutoff / CellVect_val[0].r());
		totalY = (int)Func.round(cutoff / CellVect_val[1].r());
		totalZ = (int)Func.round(cutoff / CellVect_val[2].r());

		count = 0;
		for(i = -totalX; i <= totalX; i++)
		{
			for(j = -totalY; j <= totalY; j++)
			{
				for(k = -totalZ; k <= totalZ; k++)
				{
					Trans = (CellVect_val[0]*(FP_TYPE)i) + (CellVect_val[1]*(FP_TYPE)j) + (CellVect_val[2]*(FP_TYPE)k);
					if((Trans.r()-maxLength) < cutoff) count++;
				}
			}
		}
	}
	else count = 0;

	return count;
}
Array1D<Vector> EwaldPotential::TransVect(FP_TYPE cutoff, const Array1D<Vector>& Cell) const
{
	int totalX, totalY, totalZ, total;
	FP_TYPE maxLength;
	int i, j, k, count;
	Vector Trans;
	FunctionBall Func;
	Array1D<Vector> Result;

	if(Cell.length() > 0)
	{
		maxLength = (Cell[0] + Cell[1] + Cell[2]).r() / 2;
		totalX = (int)Func.round(cutoff / Cell[0].r());
		totalY = (int)Func.round(cutoff / Cell[1].r());
		totalZ = (int)Func.round(cutoff / Cell[2].r());
		total = ((2*totalX)+1) * ((2*totalY)+1) * ((2*totalZ)+1);
		Result.setSize(total);

		count = 0;
		for(i = -totalX; i <= totalX; i++)
		{
			for(j = -totalY; j <= totalY; j++)
			{
				for(k = -totalZ; k <= totalZ; k++)
				{
					Result[count] = (Cell[0]*(FP_TYPE)i) + (Cell[1]*(FP_TYPE)j) + (Cell[2]*(FP_TYPE)k);
					if((Result[count].r()-maxLength) < cutoff) count++;
				}
			}
		}
		Result.resize(count);
		Result.reverseQuicksort();
	}
	else
	{
		Trans.zero();
		Result.setSize(1);
		Result.setElement(0, Trans);
	}

	return Result;
}
Array1D<KVector> EwaldPotential::FourierVect(bool totalVect, FP_TYPE cutoff, FP_TYPE converge, FP_TYPE volume, const Array1D<Vector>& Cell) const
{
	int totalX, totalY, totalZ, total;
	Array1D<int> Index(3);
	Vector Trans;
	int count, place, size;
	FunctionBall Func;
	Array1D<KVector> Result;
	FP_TYPE b, h;
	int myNode, totalNode;

	myNode = 0;
	totalNode = 1;

	totalX = (int)Func.round(cutoff / Cell[0].r());
	totalY = (int)Func.round(cutoff / Cell[1].r());
	totalZ = (int)Func.round(cutoff / Cell[2].r());
	total = ((2*totalX)+1) * ((2*totalY)+1) * ((2*totalZ)+1) / 2;
	
	size = total / totalNode;
	if((total%totalNode) > myNode) size++;
	Result.setSize(size);

	count = place = 0;
	for(Index[0] = 1; Index[0] <= totalX; Index[0]++)
	{
		for(Index[1] = -totalY; Index[1] <= totalY; Index[1]++)
		{
			for(Index[2] = -totalZ; Index[2] <= totalZ; Index[2]++)
			{
				Trans = (Cell[0]*(FP_TYPE)Index[0]) + (Cell[1]*(FP_TYPE)Index[1]) + (Cell[2]*(FP_TYPE)Index[2]);
				if(Trans.r() < cutoff) 
				{
					if((count%totalNode) == myNode)
					{
						Result[place].setVector(Index, Cell);
						h = Result[place].K().r();
						b = h / (2*converge);
						Result[place].setFactor(fourierFactor(b, h));
						place++;
					}
					count++;
				}
			}
		}
	}

	Index[0] = 0;
	for(Index[1] = 1; Index[1] <= totalY; Index[1]++)
	{
		for(Index[2] = -totalZ; Index[2] <= totalZ; Index[2]++)
		{
			Trans = (Cell[1]*(FP_TYPE)Index[1]) + (Cell[2]*(FP_TYPE)Index[2]);
			if(Trans.r() < cutoff) 
			{
				if((count%totalNode) == myNode)
				{		
					Result[place].setVector(Index, Cell);
					h = Result[place].K().r();
					b = h / (2*converge);
					Result[place].setFactor(fourierFactor(b, h));
					place++;
				}
				count++;
			}
		}
	}

	Index[0] = 0;
	Index[1] = 0;
	for(Index[2] = 1; Index[2] <= totalZ; Index[2]++)
	{
		Trans = (Cell[2]*(FP_TYPE)Index[2]);
		if(Trans.r() < cutoff) 
		{
			if((count%totalNode) == myNode)
			{
				Result[place].setVector(Index, Cell);
				h = Result[place].K().r();
				b = h / (2*converge);
				Result[place].setFactor(fourierFactor(b, h));
				place++;
			}
			count++;
		}
	}

	Result.resize(place);
	Result.reverseQuicksort();

	return Result;
}

void EwaldPotential::setRij(const Geometry& Geom)														
{
	int i;

	//  Calculate all of the terms if I need to
	for(i = 0; i < termNo(); i++)
	{
		Rij_val[i] = Geom.bondVector(IndexI_val[i], Geom.Atoms(IndexJ_val[i]).Position());
	}
}

//  Functions for calculating atomic energies
void EwaldPotential::atomicEnergyR(int term, FP_TYPE rij, FP_TYPE& energy) const
{
	FP_TYPE B;

	B = SumPair_val[term];
	energy = B / rij;
}

void EwaldPotential::ewaldEnergyR(FP_TYPE B, FP_TYPE rij, FP_TYPE& energy) const
{
	FP_TYPE a, gamma, factor;
	FunctionBall Func;

	a = alpha_val * rij;
	factor = B / Func.halfGamma(1);
	gamma = Func.halfUpperIncompleteGamma(1, a*a);

	energy = factor * gamma / rij;
}

////  Energy interface functions
void EwaldPotential::addEnergy(const Geometry& Geom, FP_TYPE& energy)		
{ 
	FP_TYPE erg;

	erg = 0;

	//  If no cell vects, then just do a single molecule
	if(CellVect_val.length() == 0) 
	{
		erg = energyRealAtomic(Geom);
	}

	//  If cell vects, then do it with translations
	else if(sumMethod_val == EWALDPOTENTIAL_SUMMATION_METHOD_ATOMIC)
	{
		erg = energyRealAtomic(Geom);
		erg += energyTail();
	}

	else
	{
		erg = 0;
		erg += energyRealEwald(Geom);
		
		if(sumMethodGroup() == true)
		{
			erg += energyFourierGroupGeom(Geom);
		}

		erg += energyEwaldSelf();
	}

	energy += erg;

}
FP_TYPE EwaldPotential::energyRealAtomic(const Geometry& GeomA) const
{
	int term, i, j, b, end;
	FP_TYPE energy, rij, result;
	FunctionBall Func;

	//  Set the translation position to start from
	end = RealVect_val.length()-1;

	//  Now loop over all terms and their translation vectors
	energy = 0;
	for(term = 0; term < termNo(); term++)
	{
		i = IndexI_val[term];
		j = IndexJ_val[term];

		if(Rij_val[term].r() < realCut_val)
		{
			for(b = 0; b <= end; b++)
			{
				rij = (RealVect_val[b] + Rij_val[term]).r(); 

				if(rij < realCut_val && rij > 1E-6) 
				{
					result = 0;

					atomicEnergyR(term, rij, result);

					//  Deal with scale factors for minimum images
					if(b == end) result *= MinImageFactor_val[term];

					//  Now deal with same translated atom is structure
					if(i == j) result /= 2;

					//  Now add it in to the buffrers
					energy += result;
				}
			}
		}
	}

	return energy;
}

FP_TYPE EwaldPotential::energyRealEwald(const Geometry& Geom) const
{
	int n, trans, term;
	FP_TYPE energy, rij, rijTrans, thisErg, atomErg, factor;
	Vector Rij;
	FunctionBall Func;

	if(realCut_val < 0.5) return 0;

	//  Work out the power of the potential
	n = sumPower();

	//  Now loop over all terms and their translation vectors
	energy = 0;
	for(term = 0; term < termNo(); term++)
	{
		rij = Rij_val[term].r();

		if(rij < realCut_val)
		{	
			if(IndexI_val[term] != IndexJ_val[term])
			{
				factor = 1;
				ewaldEnergyR(SumPair_val[term], rij, thisErg);
				atomicEnergyR(term, rij, atomErg);
				energy += thisErg + ((MinImageFactor_val[term]-1)*atomErg);
			}
			else factor = 0.5;

			for(trans = 0; trans < RealVect_val.length()-1; trans++)
			{
				rijTrans = (RealVect_val[trans] + Rij_val[term]).r(); 
				if(rijTrans < realCut_val)
				{
					ewaldEnergyR(SumPair_val[term], rijTrans, thisErg);
					energy += thisErg * factor;
				}
			}
		}
	}		

	return energy;
}
FP_TYPE EwaldPotential::energyFourierGroupGeom(const Geometry& Geom) const
{
	int i, k;
	Array1D<Vector> PosArray;
	Array1D<Complex> Zarray;
	FP_TYPE Bi;
	Complex ThisPhasor;
	FP_TYPE energy, dotProd;

	//  The case for just one structure being valid
	Zarray.setSize(FourierVect_val.length());

	
	for(i = 0; i < Geom.atomNo(); i++)
	{
		if(Geom.Atoms(i).frozen() == false)
		{
			Bi = SumSingle_val[i];
			for(k = 0; k < FourierVect_val.length(); k++)
			{
				dotProd = FourierVect_val[k] % Geom.Atoms(i).Position();
				ThisPhasor.setPhasor(Bi, dotProd);
				Zarray[k] += ThisPhasor;
			}
		}
	}

	energy = 0;
	for(k = 0; k < FourierVect_val.length(); k++)
	{
		energy += FourierVect_val[k].factor() * Zarray[k].conjugateSquare();
	}

	return energy;
}

FP_TYPE EwaldPotential::energyTail() const
{
	return energyTail_val;
}
FP_TYPE EwaldPotential::energyEwaldSelf() const
{
	return selfPairEnergy_val+selfSingleEnergy_val;
}
void EwaldPotential::loadListData(const Column& List)
{
	int term, i, j;

	for(i = 0; i < List.elementNo(); i++)
	{
		SumSingle_val[i] = List[i] * sqrt(COULOMB);
	}

	for(term = 0; term < List.elementNo(); term++)
	{
		i = IndexI_val[term];
		j = IndexJ_val[term];

		SumPair_val[term] = List[i] * List[j] * COULOMB;
	}
}
