
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Complex.h"
#include "ErrorBall.h"

//  The constructor
Complex::Complex()	{ null(); }
Complex::Complex(FP_TYPE r, FP_TYPE i)
{
	real_val = r;
	imag_val = i;
}
Complex::Complex(Complex const& A)
{
	real_val = A.real();
	imag_val = A.imag();
}

void Complex::null()
{
	real_val = 0;
	imag_val = 0;
}
Complex::~Complex()
{
	return;
}

//  The readers
FP_TYPE Complex::real() const { return real_val; }
FP_TYPE Complex::imag() const { return imag_val; }
FP_TYPE Complex::mod() const { return sqrt(pow(real_val,2)+pow(imag_val,2)); }
FP_TYPE Complex::arg() const 
{ 
	if(fabs(real_val) > ZERO) return atan(imag_val/real_val);
	else return 0;	
}

//  The writer
void Complex::setComplex(FP_TYPE real, FP_TYPE imag)
{
	real_val = real;
	imag_val = imag;
}
void Complex::setPhasor(FP_TYPE arg, FP_TYPE angle)
{
	//FP_TYPE fangle, sign;

	/*
	fangle = fabs(angle);
	sign = (angle < 0) ? -1 : 1;
	if(angle < 0) sign = -1;
	else sign = 1;
	
	real_val = arg * cos(fangle);
	imag_val = sign * arg * sin(fangle);
	*/

	real_val = arg * cos(angle);
	imag_val = arg * sin(angle);
}
void Complex::zero()
{
	real_val = 0;
	imag_val = 0;
}

//  The comparison operator
bool Complex::operator==(Complex const& a) const
{
	bool flag;

	flag = true;
	if(fabs(real_val-a.real_val) > ZERO) flag = false;
	if(fabs(imag_val-a.imag_val) > ZERO) flag = false;

	return flag;
}
bool Complex::operator!=(Complex const& a) const
{
	bool flag;

	flag = false;
	if(fabs(real_val-a.real_val) > ZERO) flag = true;
	if(fabs(imag_val-a.imag_val) > ZERO) flag = true;

	return flag;
}
bool Complex::operator>(const Complex& A) const
{
	if(mod() > A.mod()) return true;
	else return false;
}
bool Complex::operator<(const Complex& A) const
{
	if(mod() < A.mod()) return true;
	else return false;
}
bool Complex::operator>(FP_TYPE a) const
{
	if(mod() > a) return true;
	else return false;
}
bool Complex::operator<(FP_TYPE a) const
{
	if(mod() < a) return true;
	else return false;
}

//  The assignment operator
Complex& Complex::operator=(Complex const& a)
{
	real_val = a.real_val;
	imag_val = a.imag_val;

	return *this;
}
Complex& Complex::operator=(Complex *a)
{
	real_val = a->real_val;
	imag_val = a->imag_val;
	delete a;

	return *this;
}

//  The assignment operator
Complex& Complex::operator=(FP_TYPE a)
{
	real_val = a;
	imag_val = 0;

	return *this;
}
Complex& Complex::operator=(int a)
{
	real_val = (FP_TYPE)a;
	imag_val = 0;

	return *this;
}
//  The addition operator overloads
Complex& Complex::operator+=(Complex const& a)
{
	real_val += a.real_val;
	imag_val += a.imag_val;
	return *this;
}
Complex& Complex::operator+=(FP_TYPE a)
{
	real_val += a;
	return *this;
}
Complex Complex::operator+(Complex const& a) const
{
	Complex Result;

	Result.real_val = real_val + a.real_val;
	Result.imag_val = imag_val + a.imag_val;
	return Result;
}
Complex Complex::operator+(FP_TYPE a) const
{
	Complex Result;

	Result.real_val = real_val + a;
	Result.imag_val = imag_val;
	return Result;
}

//  The subtraction operator overloads
Complex& Complex::operator-=(Complex const& a)
{
	real_val -= a.real_val;
	imag_val -= a.imag_val;
	return *this;
}
Complex& Complex::operator-=(FP_TYPE a)
{
	real_val -= a;
	return *this;
}
Complex Complex::operator-(Complex const& a) const
{
	Complex Result;

	Result.real_val = real_val - a.real_val;
	Result.imag_val = imag_val - a.imag_val;
	return Result;
}
Complex Complex::operator-(FP_TYPE a) const
{
	Complex Result;

	Result.real_val = real_val - a;
	Result.imag_val = imag_val;
	return Result;
}

//  The multiplication operator overloads
Complex& Complex::operator*=(Complex const& a)
{
	FP_TYPE temp;
	temp = (real_val*a.real_val) - (imag_val*a.imag_val);
	imag_val = (imag_val*a.real_val) + (real_val*a.imag_val);
	real_val = temp;
	return *this;
}
Complex& Complex::operator*=(FP_TYPE a)
{
	real_val = real_val*a;
	imag_val = imag_val*a;
	return *this;
}
Complex Complex::operator*(Complex const& a) const
{
	Complex Result;

	Result.real_val = (real_val*a.real_val) - (imag_val*a.imag_val);
	Result.imag_val = (imag_val*a.real_val) + (real_val*a.imag_val);
	return Result;
}
Complex Complex::operator*(FP_TYPE a) const
{
	Complex Result;

	Result.real_val = real_val*a;
	Result.imag_val = imag_val*a;
	return Result;
}

//  The division operator overloads
Complex& Complex::operator/=(const Complex& A)
{
	Complex Result;

	*this *= (~A) / A.conjugateSquare();
	return *this;
}
Complex& Complex::operator/=(FP_TYPE a)
{
	real_val /= a;
	imag_val /= a;
	return *this;
}
Complex Complex::operator/(const Complex& A) const
{
	Complex Result;

	Result = *this * (~A) / A.conjugateSquare();
	return Result;
}
Complex Complex::operator/(FP_TYPE a) const
{
	Complex Result;

	Result.real_val = real_val/a;
	Result.imag_val = imag_val/a;
	return Result;
}
//  Function for multiplication by -1
Complex Complex::operator-()
{
	Complex Result;

	Result.setComplex(-real_val, -imag_val);
	return Result;
}

//  Function for multiplication by i
Complex Complex::operator!()
{
	Complex Result;

	Result.setComplex(-imag_val, real_val);
	return Result;
}

//  The complex conjugate function
Complex Complex::operator~() const
{
	Complex Result;

	Result.setComplex(real_val, -1*imag_val);
	return Result;
}
FP_TYPE Complex::conjugateSquare() const
{
	return (real_val*real_val) + (imag_val*imag_val);
}

//  A function to get the root to an arbitrary power
Complex Complex::power(FP_TYPE power)
{
	FP_TYPE newMod, newArg;
	Complex Result;

	if(fabs(power) > ZERO)
	{
		newMod = pow(mod(), power);
		newArg = arg() * power;

		Result.setComplex(newMod*cos(newArg), newMod*sin(newArg));
	}
	else
	{
		Result.setComplex(1, 0);
	}

	return Result;
}
Complex Complex::root(FP_TYPE power)
{
	FP_TYPE newMod, newArg;
	Complex Result;

	if(fabs(power) > ZERO)
	{
		newMod = pow(mod(), 1/power);
		newArg = arg() / power;

		Result.setComplex(newMod*cos(newArg), newMod*sin(newArg));
	}
	else
	{
		Result.setComplex(1, 0);
	}

	return Result;
}

//  An overload to output complex numbers 
ostream & operator<<(ostream &output, Complex const& A)
{
	output.precision(6);
	output.setf(ios::showpoint | ios::fixed);
	
	output << setw(12) << A.real() << setw(12) << A.imag() << "i";

	return output;
}





