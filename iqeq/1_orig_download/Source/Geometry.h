
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _GEOMETRY_H
#define _GEOMETRY_H

#include "definitions.h"
#include "Atom.h"
#include "String.h"
#include "Vector.h"
#include "Matrix.h"
#include "Column.h"
#include "Array1D.h"
#include "Array2D.h"
#include "Tensor.h"

#include <iostream>
using namespace std;



#define GEOMETRY_BOND_NONE 0
#define GEOMETRY_BOND_SINGLE 1
#define GEOMETRY_BOND_DOUBLE 2
#define GEOMETRY_BOND_TRIPLE 3
#define GEOMETRY_BOND_AROMATIC 4
#define GEOMETRY_BOND_AROMATIC_DOUBLE 5
#define GEOMETRY_BOND_HYDROGEN 6

#define GEOMETRY_BADER_UNDEFINED -2
#define GEOMETRY_BADER_ZERO_DENSITY -1

#define GEOMETRY_MIN_REF_CHARGE -4
#define GEOMETRY_MAX_REF_CHARGE 4

class Geometry
{
	friend class MPICommBall;

	protected:
		int type_val;
		Array1D<Atom> Atoms_val;
		Array2D<int> BondType_val;
		Array2D<int> BondConnect_val;
		Array2D<FP_TYPE> BondOrder_val;

		//  Variables that control the periodicity of the structure
		Array1D<Vector> Translation_val;
		FP_TYPE minLength_val;
		int cellOrientation_val;
		Tensor Stress_val;
		Matrix Hessian_val;

	public:
		
		//  Constructor and destructor functions
		Geometry();
		Geometry(Geometry const&);
		Geometry& operator=(Geometry const&);
		int operator=(int);
		void null();

		//  A readers for various stuff
		int atomNo() const;
		const Atom& Atoms(int) const;
		int bondConnect(int, int) const;
		int bondType(int, int) const;
		FP_TYPE bondOrder(int, int) const;
		int bondNumber(int) const;
		int bondCovalentNumber(int) const;
		FP_TYPE bondTotalValence(int) const;
		int period() const;
		const Vector& Translation(int) const;
		const Array1D<Vector>& Translation() const;
		Tensor TransTensor() const;
		FP_TYPE cellParamA() const;
		FP_TYPE cellParamB() const;
		FP_TYPE cellParamC() const;
		FP_TYPE cellParamAlpha() const;
		FP_TYPE cellParamGamma() const;
		FP_TYPE cellParamBeta() const;

		Vector FracAtomPosition(int) const;
		Vector CartAtomPosition(int) const;

		//  Writers for a few things
		void setAtomNumber(int);
		void setBondType(int, int, int);
		void setBondOrder(int, int, FP_TYPE);
		void setCellParamZYX(FP_TYPE, FP_TYPE, FP_TYPE, FP_TYPE, FP_TYPE, FP_TYPE);

		Atom& changeAtoms(int) const;
		void setAtomPosFractional();
		void setAtomPosCartesian();

		//  Functions to get internal coordinatates
		Vector bondVector(int, int) const;
		Vector bondVector(int, const Vector&) const;
		Vector distance(const Vector&, const Vector&) const;
		FP_TYPE angle(int, int, int) const;

		//  Functions to set details about the geometry
		void deleteBonds(const Array1D<int>&);
		void setBonds();
		void setBonds(bool, bool, bool, FP_TYPE, const Array1D<int>&);
		void setRingSize();
		void setRingSize(const Array1D<int>&);
		void setAromatic();
		void setAromatic(const Array1D<int>&);
		void setHybrid();
		void setHybrid(const Array1D<int>&);
		void setFormalCharge();
		void setFormalCharge(const Array1D<int>&);
		
		void loadXYZGeometry(const String&);
		void printXYZGeometry(const String&);

		FP_TYPE chargeGeomEquilibrate(ostream&, const InputChargeFitBall&);

		//  Function to add standard data to the atoms
		void setAtomData();
		void setAtomData(const Array1D<int>&);
};

#endif



		
