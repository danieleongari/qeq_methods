/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _ARRAY1D_H
#define _ARRAY1D_H

#include "definitions.h"
#include "ErrorBall.h"
#include "String.h"
#include "Complex.h"

#include <iostream>
#include <iomanip>
using namespace std;

template <class T>
class Array1D
{
	friend class MPICommBall;
	friend class EwaldPotential;

	protected:
		T* Data_val;
		int length_val;

		void quicksort(int l, int r, Array1D<int>& OrderList)
		{	
			int i, j;
			T partition;

			if(r > l)
			{
				partition = (*this)[r];
				i = l-1;
				j = r;
				while(i < j)
				{
					while((*this)[++i] < partition);
					while(j > l && (*this)[--j] > partition);

					if(i < j) 
					{
						swap(i, j);
						OrderList.swap(i, j);
					}
				}
				if(i != r)
				{
					swap(i, r);
					OrderList.swap(i, r);
				}

				quicksort(l, i-1, OrderList);
				quicksort(i+1, r, OrderList);
			}
		}
		void reverseQuicksort(int l, int r, Array1D<int>& OrderList)
		{	
			int i, j;
			T partition;
	
			if(r > l)
			{
				partition = Data_val[r];
				i = l-1;
				j = r;
				while(i < j)
				{
					while(Data_val[++i] > partition);
					while(j > l && Data_val[--j] < partition);
					if(i < j) 
					{
						swap(i, j);
						OrderList.swap(i, j);
					}
				}
				swap(i, r);
				OrderList.swap(i, r);

				reverseQuicksort(l, i-1, OrderList);
				reverseQuicksort(i+1, r, OrderList);
			}
		}

	public:
		Array1D(int size)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(size);
		}
		Array1D(const Array1D<T>& A)
		{
			int i;

			setSize(A.length());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];
		}
		Array1D()
		{
			length_val = 0;
			Data_val = NULL;
		}
		~Array1D()
		{
			if(length_val > 0) delete [] Data_val;
			length_val = 0;
		}
		void null()
		{
			int i;

			for(i = 0; i < length_val; i++) Data_val[i] = 0;
		}

		//  Function to return the pointer to the data array
		//  Designed mainly for MPI Communication, to be used with caution
		T* pointer()
		{ 
			return Data_val; 
		}

		//  Operator overloads
		const Array1D<T>& operator=(const Array1D<T>& A)
		{
			int i;

			setSize(A.length());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];

			return *this;
		}
		const Array1D<T>& operator=(const Column& A)
		{
			int i;

			setSize(A.elementNo());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];

			return *this;
		}
		int operator=(int i)
		{
			if(i != 0) 
				throw ErrorBall("Misformed clearing of atom data in Array1D<T>::operator=(int i)");
			null();

			return i;
		}

		bool operator==(const Array1D<T>& A) const
		{
			bool flag;
			int i;

			if(length_val != A.length()) return false;

			flag = true;
			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i] != A[i]) flag = false;
			}

			return flag;
		}
		bool operator!=(const Array1D<T>& A) const
		{
			return !(*this == A);
		}
		T& operator[](int i) const
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val) 
				{
					String Message;
					Message << "Out of bounds Array1D accessing element " & i & " in Array1D::operator[]";
					throw ErrorBall(Message);
				}
			#endif 
			
			return Data_val[i];
		}
		const T& operator()(int i, const T& Element)
		{
			if(i < 0 || i >= length_val) throw ErrorBall("Out of bounds Array1D access in Array1D::operator()");
			else Data_val[i] = Element;

			return Element;
		}

		//http://www.webelements.com/
		//  Explicit reading and writing functions
		T& element(int i) const
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val) throw ErrorBall("Out of bounds Array1D access in Array1D::element(int)");
			#endif
			
			return Data_val[i];
		}
		const T& setElement(int i, const T& Element)
		{
			if(i < 0 || i >= length_val) throw ErrorBall("Out of bounds Array1D access in Array1D::setElement(int, const T&)");
			else Data_val[i] = Element;

			return Element;
		}		
		int length() const
		{
			return length_val;
		}

		//  Size and re-sizing function
		void setSize(int size)
		{
			int i;
			String Error;

			try
			{
				#ifdef _ERROR_REPORT
					if(size < 0) 
						throw ErrorBall("Cannot size negative Array1D in function Array1D::resize(int)");
					if(size > 100000000)
						throw ErrorBall("Cannot size massive Array1D in function Array1D::resize(int)");
				#endif 

				if(length_val > 0) delete [] Data_val;
				if(size > 0) Data_val = new T[size];
				else Data_val = NULL;
				length_val = size;
				for(i = 0; i < length_val; i++) Data_val[i] = 0;
			}
			catch(bad_alloc ba)
			{
				Error << ba.what() & "\nCaught bad memory allocation in Array1D::setSize(int)";
				throw ErrorBall(Error);
			}
		}
		void resize(int size)
		{
			Array1D<T> CopySpace;
			int i;

			#ifdef _ERROR_REPORT
				if(size < 0) 
					throw ErrorBall("Cannot resize negative Array1D in function Array1D::resize(int)");
			#endif

			CopySpace = *this;
			setSize(size);
			for(i = 0; i < length_val && i < CopySpace.length(); i++) Data_val[i] = CopySpace[i];
		}

		//  Function to sort the array, assuming that the comparison operators for that class are defined
		void swap(int i, int j)
		{	
			if(i != j)
			{
				T Temp;
				Temp = Data_val[i];
				Data_val[i] = Data_val[j];
				Data_val[j] = Temp;
			}
		}
		Array1D<int> quicksort()
		{ 
			Array1D<int> OrderList;
			int i;

			OrderList.setSize(length_val);
			for(i = 0; i < OrderList.length(); i++) OrderList[i] = i;

			quicksort(0, length_val-1, OrderList);
			return OrderList;
		}
		Array1D<int> reverseQuicksort()
		{ 
			Array1D<int> OrderList;
			int i;

			OrderList.setSize(length_val);
			for(i = 0; i < OrderList.length(); i++) OrderList[i] = i;

			reverseQuicksort(0, length_val-1, OrderList); 

			return OrderList;
		}
		void orderArray(const Array1D<int>& OrderList)
		{
			int i;
			Array1D<T> Buffer;

			Buffer = (*this);
			for(i = 0; i < length_val; i++) Data_val[i] = Buffer[OrderList[i]];
		}

		//  Functions to search the array in a normal and binary way, assuming that the array is sorted
		//  Index reveals the position of the element
		int search(const T& value) const
		{
			int i;

			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i] == value) return i;
			}
			return -1;
		}
		int binarySearch(const T& value) const
		{
			int low, high, middle;

			if(length_val <= 0) return -1;
			if(value < Data_val[0] || value > Data_val[length_val-1]) return -1;

			low = 0;
			high = length_val -1;
			while((high-low) > 1)
			{
				middle = low + ((high-low)/2);
				if(value == Data_val[middle]) return middle;
				else if(value < Data_val[middle]) high = middle;
				else low = middle;
			}

			if(value == Data_val[low]) return low;
			else if(value == Data_val[high]) return high;
			else return -1;
		}

		//  Functions that approximate a filo que stack
		void push(const T& Value)
		{
			resize(length_val+1);
			Data_val[length_val-1] = Value;
		}
		T pop()
		{
			T Value;

			#ifdef _ERROR_REPORT
				if(length_val <= 0) throw ErrorBall("Trying to pop element from 0 element list in Array1D<T>::pop");
			#endif

			Value = Data_val[length_val-1];
			resize(length_val-1);
			return Value;
		}

		//  Function to insert into and delete from a 1D list
		void addElement(int place, const T& Data)
		{
			int i;
			
			#ifdef _ERROR_REPORT
				if(place < 0 || place > length_val) 
					throw ErrorBall("Array out of bounds error in Array1D<T>::addElement");
			#endif

			resize(length_val+1);
			for(i = length_val-1; i > place; i--)
			{
				Data_val[i] = Data_val[i-1];
			}
			Data_val[place] = Data;
		}
		void deleteElement(int place)
		{
			#ifdef _ERROR_REPORT
				if(place < 0 || place > length_val) 
					throw ErrorBall("Array out of bounds error in Array1D<T>::deleteElement");
			#endif

			swap(place, length_val-1);
			resize(length_val-1);
		}
		void operator+=(const Array1D<T>& Data)
		{
			int i, offset;

			offset = length_val;
			resize(length_val+Data.length());
			for(i = 0; i < Data.length(); i++)
			{
				Data_val[i+offset] = Data[i];
			}
		}

		//  Functions to get the positions of the values of the minimum and maximum values in the array
		//  Again assumes the definition of the comparison operators
		const T& minValue() const
		{
			int place;

			place = minPos();
			
			#ifdef _ERROR_REPORT
				if(place == -1) throw ErrorBall("Cannot get maximum value in an empty array in Array1D<T>::min()");
			#endif
			return Data_val[place];
		}
		const T& maxValue() const
		{
			int place;

			place = maxPos();
			#ifdef _ERROR_REPORT
				if(place == -1) throw ErrorBall("Cannot get maximum value in an empty array in Array1D<T>::max()");
			#endif
			return Data_val[place];
		}
		int minPos() const
		{
			int i, place;

			if(length_val <= 0) return -1;
	
			place = 0;
			for(i = 1; i < length_val; i++)
			{
				if(Data_val[i] < Data_val[place]) place = i;
			}
			return place;
		}
		int maxPos() const
		{
			int i, place;

			if(length_val <= 0) return -1;
	
			place = 0;
			for(i = 1; i < length_val; i++)
			{
				if(Data_val[i] > Data_val[place]) place = i;
			}
			return place;
		}

		//  Functions for sorting the start of strings assuming that the data contained there in is atomic types
		bool atomTypeSort(int);

		bool bondTypeSort()
		{ 
			if(Data_val[1] < Data_val[0]) 
			{
				swap(0, 1);
				return true;
			}
			else return false;
		}
		bool angleTypeSort()
		{ 
			if(Data_val[2] < Data_val[0])
			{
				swap(0, 2); 
				return true;
			}
			else return false;
		}
		bool torsionTypeSort()
		{
			if(Data_val[2] < Data_val[1] || (Data_val[1] == Data_val[2] && Data_val[3] < Data_val[0]))
			{
				swap(0, 3);
				swap(1, 2);
				return true;
			}

			else return false;
		}
		void inversionTypeSort(bool single)
		{
			if(single == false)
			{
				if(Data_val[2] < Data_val[1]) swap(2, 1);
			}
			else
			{
				if(Data_val[2] < Data_val[1]) swap(2, 1);
				if(Data_val[3] < Data_val[2]) swap(3, 2);
				if(Data_val[2] < Data_val[1]) swap(2, 1);
			}
		}	
		bool aaCrossTypeSort()
		{
			if(Data_val[3] < Data_val[2])
			{
				swap(2, 3);
				return true;
			}
			else return false;
		}

		//  An overload for output, so that I can easily test values of arraysa
		friend ostream & operator<<(ostream &output, const Array1D<T>& A)
		{
			String Index;
			int i;

			output.precision(4);
			output.setf(ios::showpoint | ios::fixed);
			
			for(i = 0; i < A.length(); i++)
			{
				Index << "[" & i & "]";
				output << setw(10) << right << Index;
				output << setw(20) << right << A[i] << endl;
			}

			output << endl;

			return output;
		}

};


#endif


