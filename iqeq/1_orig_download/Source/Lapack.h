/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _LAPACK_H
#define _LAPACK_H

#include "definitions.h"
#include <mkl.h>

#define LAPACK_DESC_SIZE 9

namespace LapackLib
{
	// http://software.intel.com/en-us/node/468378
	//  BLAS Level One Routines
	void faxpy(int n, FP_TYPE a, FP_TYPE* x, int incx, FP_TYPE* y, int incy);
	void fcopy(int n, FP_TYPE* x, int incx, FP_TYPE* y, int incy);
	FP_TYPE fdot(int n, FP_TYPE *x, int incx, FP_TYPE *y, int incy);
	void fscal(int n, FP_TYPE a, FP_TYPE* x, int incx);
	FP_TYPE fnrm2(int n, FP_TYPE* x, int incx);

	//  BLAS Level Two Routines
	void fgemv(char trans, int m, int n, FP_TYPE alpha, FP_TYPE* a, int lda, FP_TYPE* x, int incx, FP_TYPE beta, FP_TYPE* y, int incy);

	//  BLAS Level Three Routines
	void fgemm(char transa, char transb, int m, int n, int k, FP_TYPE alpha, FP_TYPE* a, int lda, FP_TYPE* b, int ldb, FP_TYPE beta, FP_TYPE* c, int ldc);

	// http://software.intel.com/en-us/node/501008
	//  LAPACK routines
	int fgetri(int n, FP_TYPE* a, int lda, int *ipiv, FP_TYPE *work, int lwork);
	int fgesv(int n, int nrhs, FP_TYPE *a, int lda, int* ipiv, FP_TYPE* b, int ldb);
	int fgeev(char jobvl, char jobvr, int n, FP_TYPE *a, int lda, FP_TYPE* wr, FP_TYPE* wi, FP_TYPE* vl, int ldvl, FP_TYPE* vr, int ldvr, FP_TYPE* work, int lwork);
	int fsyev(char jobz, char uplo, int n, FP_TYPE* a, int lda, FP_TYPE* w, FP_TYPE* work, int lwork);
	int fstev(char jobz, int n, FP_TYPE* d, FP_TYPE* e, FP_TYPE* z, int ldz, FP_TYPE* work);
	
	//  http://software.intel.com/en-us/node/469300
	// Auxilary Routines
	FP_TYPE flange(char norm, int m, int n, FP_TYPE* a, int lda, FP_TYPE* work);
}

#endif _LAPACK_H