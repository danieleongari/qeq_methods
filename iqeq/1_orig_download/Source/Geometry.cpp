
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Geometry.h"
#include "definitions.h"
#include "ErrorBall.h"
#include "Matrix.h"
#include "FunctionBall.h"
#include "XmlLine.h"
#include "AtomDataBall.h"
#include "InputChargeFitBall.h"
#include "ChargeEquilibrator.h"

#include <ctime>
#include <cmath>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

/*
	private:
		int atomNumber;
		Atom* atoms;
		int *bondType;
*/
//  Constructor and destructor functions
Geometry::Geometry()	{ null(); }
Geometry::Geometry(Geometry const& Geom)
{
	*this = Geom;
}
void Geometry::null()
{
	type_val = -1;
	Atoms_val.setSize(0);
	BondType_val.setSize(0,0);
	BondOrder_val.setSize(0,0);
	BondConnect_val.setSize(0,0);
	minLength_val = 0;
	Translation_val.setSize(0);
	cellOrientation_val = CELLSYMM_XYZ_NONE;
}

// Overload for assigning different stuff
Geometry& Geometry::operator=(Geometry const& Geom)
{
	type_val = Geom.type_val;
	Atoms_val = Geom.Atoms_val;
	BondType_val = Geom.BondType_val;
	BondConnect_val = Geom.BondConnect_val;
	BondOrder_val = Geom.BondOrder_val;
	
	Translation_val = Geom.Translation_val;
	minLength_val = Geom.minLength_val;
	cellOrientation_val = Geom.cellOrientation_val;
	Stress_val = Geom.Stress_val;
	Hessian_val = Geom.Hessian_val;

	return *this;
}
int Geometry::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in Geometry::operator=(int i)");
	null();

	return i;
}
	
//  A readers for various stuff
int Geometry::atomNo() const { return Atoms_val.length(); }
const Atom& Geometry::Atoms(int place) const		{ return Atoms_val[place]; }
int Geometry::bondConnect(int i, int j) const		{ return BondConnect_val[i][j]; }
int Geometry::bondType(int i, int j) const			
{
	int place;

	place = BondConnect_val[i].search(j);
	if(place < 0) return GEOMETRY_BOND_NONE;
	return BondType_val[i][place]; 
}
FP_TYPE Geometry::bondOrder(int i, int j) const	
{
	int place;

	place = BondConnect_val[i].search(j);
	if(place < 0) return 0;
	return BondOrder_val[i][place]; 
}
int Geometry::bondNumber(int i) const					{ return BondConnect_val[i].length(); } 
int Geometry::bondCovalentNumber(int i) const
{
	int j, k, result;

	result = 0;

	if(Atoms_val[i].valenceBond() != 0) 
	{
		result = 0;
		for(k = 0; k < BondConnect_val[i].length(); k++)
		{
			j = BondConnect_val[i][k];
			if(Atoms_val[j].valenceBond() != 0) result++;
		}
	}

	return result;
}
FP_TYPE Geometry::bondTotalValence(int atom) const
{
	int i, j;
	FP_TYPE total;

	total = 0;
	for(i = 0; i < BondConnect_val[atom].length(); i++)
	{
		j = BondConnect_val[atom][i];
		if(Atoms_val[j].valenceBond() > 0)
		{
			total += bondOrder(atom, BondConnect_val[atom][i]);
		}
	}

	return total;
}

int Geometry::period() const							{ return Translation_val.length(); }
//FP_TYPE Geometry::minLength() const						{ return minLength_val; }
const Vector& Geometry::Translation(int i) const		{ return Translation_val[i]; }
const Array1D<Vector>& Geometry::Translation() const	{ return Translation_val; }
Tensor Geometry::TransTensor() const
{
	int i, j;
	Tensor Result;

	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++) Result(i, j) = (Translation_val[j])[i];
	}

	return Result;
}
FP_TYPE Geometry::cellParamA() const					{ return Translation_val[0].r(); }
FP_TYPE Geometry::cellParamB() const					{ return Translation_val[1].r(); }
FP_TYPE Geometry::cellParamC() const					{ return Translation_val[2].r(); }
FP_TYPE Geometry::cellParamAlpha() const				{ return Translation_val[1] |= Translation_val[2]; }
FP_TYPE Geometry::cellParamBeta() const					{ return Translation_val[0] |= Translation_val[2]; }
FP_TYPE Geometry::cellParamGamma() const				{ return Translation_val[0] |= Translation_val[1]; }

Vector Geometry::FracAtomPosition(int i) const				{ return TransTensor().inverse() * Atoms_val[i].Position(); }

Vector Geometry::CartAtomPosition(int i) const				{ return TransTensor() * Atoms_val[i].Position(); }

Atom& Geometry::changeAtoms(int place) const { return Atoms_val[place]; }
//void Geometry::setAtom(int place, Atom const& Replace) { Atoms_val[place] = Replace; }
//void Geometry::setType(int i) { type_val = i; }
void Geometry::setAtomNumber(int atomNo)
{
	//  Allocate the new memory
	Atoms_val.setSize(atomNo);
	BondConnect_val.setSize(atomNo, 0);
	BondType_val.setSize(atomNo, 0);
	BondOrder_val.setSize(atomNo, 0);
}

void Geometry::setBondType(int i, int j, int type)
{
	int place;

	place = BondConnect_val[i].search(j);
	if(place < 0)
	{
		BondConnect_val[i].push(j);
		BondType_val[i].push(type);
		BondOrder_val[i].push(1.0);
	}
	else BondType_val[i][place] = type;

	place = BondConnect_val[j].search(i);
	if(place < 0)
	{
		BondConnect_val[j].push(i);
		BondType_val[j].push(type);
		BondOrder_val[j].push(1.0);
	}
	else BondType_val[j][place] = type;
}
void Geometry::setBondOrder(int i, int j, FP_TYPE order)
{
	int place;

	place = BondConnect_val[i].search(j);
	if(place < 0)
	{
		BondConnect_val[i].push(j);
		BondType_val[i].push(GEOMETRY_BOND_SINGLE);
		BondOrder_val[i].push(order);
	}
	else BondOrder_val[i][place] = order;

	place = BondConnect_val[j].search(i);
	if(place < 0)
	{
		BondConnect_val[j].push(i);
		BondType_val[j].push(GEOMETRY_BOND_SINGLE);
		BondOrder_val[j].push(order);
	}
	else BondOrder_val[j][place] = order;
}

void Geometry::setAtomPosFractional()
{
	int i;

	//  First deal with the atoms
	for(i = 0; i < Atoms_val.length(); i++) Atoms_val[i].setPosition(FracAtomPosition(i));
}
void Geometry::setAtomPosCartesian()
{
	int i;

	for(i = 0; i < Atoms_val.length(); i++) Atoms_val[i].setPosition(CartAtomPosition(i));
}

void Geometry::setCellParamZYX(FP_TYPE a, FP_TYPE b, FP_TYPE c, FP_TYPE alpha, FP_TYPE beta, FP_TYPE gamma)
{
	FP_TYPE x, y, z;
	FunctionBall Func;
	FP_TYPE square, trig;

	Translation_val.setSize(3);

	//  Now use that to set the translation vectors
	Translation_val[2].setCart(0, 0, c);
	z = b * cos(alpha);
	y = b * sin(alpha);
	Translation_val[1].setCart(0, y, z);
	z = a * cos(beta);
	y = (cos(gamma) * b * a) - (Translation_val[1].z() * z);
	y /= Translation_val[1].y();
	x = sqrt((a*a) - (z*z) - (y*y));
	Translation_val[0].setCart(x, y, z);

	//  Now set the minimum length and periodicity
	minLength_val = Func.minValue(Func.minValue(a,b), c);

	//  Now set the squareness
	square = trig = 0;
	square += fabs(cellParamAlpha()-HALFPI);
	square += fabs(cellParamBeta()-HALFPI);
	square += fabs(cellParamGamma()-HALFPI);
	trig += fabs(cellParamAlpha()-HALFPI);
	trig += fabs(cellParamBeta()-HALFPI);
	trig += fabs(cellParamGamma()-(TWO_THIRD*PI));

	if(square < 1E-5) cellOrientation_val = CELLSYMM_SQUARE;
	else if(trig < 1E-5) cellOrientation_val = CELLSYMM_ZYX_TRIG;
	else cellOrientation_val = CELLSYMM_ZYX_NONE;
}

Vector Geometry::bondVector(int i, int j) const
{
	return distance(Atoms_val[i].Position(), Atoms_val[j].Position());
}

//  Functions to get internal coordinatates
Vector Geometry::bondVector(int i, const Vector& Position) const
{
	return distance(Atoms_val[i].Position(), Position);
}
Vector Geometry::distance(const Vector& PosA, const Vector& PosB) const
{
	Vector FracDiff, Diff, MinDiff;
	Vector Test, Min, Total;
	Vector VA, VB, VC, VD;
	//int altA, altB, testB, testC;
	//FP_TYPE minR, testR;
	FunctionBall Func;
	FP_TYPE x, y, z, xx, xy, xz, yx, yy, yz, zx, zy, zz, a, b, c, var, vbr, vcr, vdr;
	FP_TYPE xa, xb, xc, xd, ya, yb, yc, yd, za, zb, zc, zd;

	Diff = (PosA - PosB);

	if(Translation_val.length() > 0 && Diff.r() > (minLength_val/2))
	{
		switch(cellOrientation_val)
		{
			case CELLSYMM_SQUARE:
			{
				x = Diff.x() - (Translation_val[0].x()*Func.round(Diff.x()/Translation_val[0].x()));
				y = Diff.y() - (Translation_val[1].y()*Func.round(Diff.y()/Translation_val[1].y()));
				z = Diff.z() - (Translation_val[2].z()*Func.round(Diff.z()/Translation_val[2].z()));
				Diff.setCart(x,y,z);
				break;
			}
			case CELLSYMM_XYZ_TRIG:
			{
				zz = Translation_val[2].z();
				yy = Translation_val[1].y();
				yx = Translation_val[1].x();
				xx = Translation_val[0].x();

				c = Func.round(-Diff.z()/zz);
				z = Diff.z() + (c*zz);

				b = floor(-Diff.y()/yy);
				a = Func.round(((-b*yx)-Diff.x())/xx);
				ya = Diff.y() + (b*yy);
				xa = Diff.x() + (a*xx) + (b*yx);
				var = (xa*xa) + (ya*ya);

				b = ceil(-Diff.y()/yy);
				a = Func.round(((-b*yx)-Diff.x())/xx);
				yb = Diff.y() + (b*yy);
				xb = Diff.x() + (a*xx) + (b*yx);
				vbr = (xb*xb) + (yb*yb);

				if(var < vbr) Diff.setCart(xa, ya, z);
				else Diff.setCart(xb, yb, z);
				break;
			}
			case CELLSYMM_ZYX_TRIG:
			{
				xx = Translation_val[0].x();
				xy = Translation_val[0].y();
				yy = Translation_val[1].y();
				zz = Translation_val[2].z();

				c = Func.round(-Diff.z()/zz);
				z = Diff.z() + (c*zz);

				a = floor(-Diff.x()/xx);
				b = Func.round(((-a*xy)-Diff.y())/yy);
				xa = Diff.x() + (a*xx);
				ya = Diff.y() + (a*xy) + (b*yy);
				var = (xa*xa) + (ya*ya);

				a = ceil(-Diff.x()/xx);
				b = Func.round(((-a*xy)-Diff.y())/yy);
				xb = Diff.x() + (a*xx);
				yb = Diff.y() + (a*xy) + (b*yy);
				vbr = (xb*xb) + (yb*yb);

				if(var < vbr) Diff.setCart(xa, ya, z);
				else Diff.setCart(xb, yb, z);
				break;
			}
			case CELLSYMM_XYZ_NONE:
			{
				zz = Translation_val[2].z();
				zy = Translation_val[2].y();
				zx = Translation_val[2].x();
				yy = Translation_val[1].y();
				yx = Translation_val[1].x();
				xx = Translation_val[0].x();

				c = floor(-Diff.z()/zz);
				b = floor(((-c*zy)-Diff.y())/yy);
				a = Func.round(((-b*yx)-(c*zx) - Diff.x())/xx);
				za = Diff.z() + (c*zz);
				ya = Diff.y() + (b*yy) + (c*zy);
				xa = Diff.x() + (a*xx) + (b*yx) + (c*zx);
				var = (xa*xa) + (ya*ya) + (za*za);

				b = ceil(((-c*zy)-Diff.y())/yy);
				a = Func.round(((-b*yx)-(c*zx) - Diff.x())/xx);
				yb = Diff.y() + (b*yy) + (c*zy);
				xb = Diff.x() + (a*xx) + (b*yx) + (c*zx);
				vbr = (xb*xb) + (yb*yb) + (za*za);

				c = ceil(-Diff.z()/zz);
				b = floor(((-c*zy)-Diff.y())/yy);
				a = Func.round(((-b*yx)-(c*zx) - Diff.x())/xx);
				zb = Diff.z() + (c*zz);
				yc = Diff.y() + (b*yy) + (c*zy);
				xc = Diff.x() + (a*xx) + (b*yx) + (c*zx);
				vcr = (xc*xc) + (yc*yc) + (zb*zb);

				b = ceil(((-c*zy)-Diff.y())/yy);
				a = Func.round(((-b*yx)-(c*zx) - Diff.x())/xx);
				yd = Diff.y() + (b*yy) + (c*zy);
				xd = Diff.x() + (a*xx) + (b*yx) + (c*zx);
				vdr = (xd*xd) + (yd*yd) + (zb*zb);

				if(var < vbr && var < vcr && var < vdr) Diff.setCart(xa, ya, za);
				else if(vbr < vcr && vbr < vdr) Diff.setCart(xb, yb, za);
				else if(vcr < vdr) Diff.setCart(xc, yc, zb);
				else Diff.setCart(xd, yd, zb);
				break;
			}
			case CELLSYMM_ZYX_NONE:
			{
				xx = Translation_val[0].x();
				xy = Translation_val[0].y();
				xz = Translation_val[0].z();
				yy = Translation_val[1].y();
				yz = Translation_val[1].z();
				zz = Translation_val[2].z();

				a = floor(-Diff.x()/xx);
				b = floor(((-a*xy)-Diff.y())/yy);
				c = Func.round(((-a*xz)-(b*yz)-Diff.z())/zz);
				xa = Diff.x() + (a*xx);
				ya = Diff.y() + (a*xy) + (b*yy);
				za = Diff.z() + (a*xz) + (b*yz) + (c*zz);
				var = (xa*xa) + (ya*ya) + (za*za);

				b = ceil(((-a*xy)-Diff.y())/yy);
				c = Func.round(((-a*xz)-(b*yz)-Diff.z())/zz);
				yb = Diff.y() + (a*xy) + (b*yy);
				zb = Diff.z() + (a*xz) + (b*yz) + (c*zz);
				vbr = (xa*xa) + (yb*yb) + (zb*zb);

				a = ceil(-Diff.x()/xx);
				b = floor(((-a*xy)-Diff.y())/yy);
				c = Func.round(((-a*xz)-(b*yz)-Diff.z())/zz);
				xb = Diff.x() + (a*xx);
				yc = Diff.y() + (a*xy) + (b*yy);
				zc = Diff.z() + (a*xz) + (b*yz) + (c*zz);
				vcr = (xb*xb) + (yc*yc) + (zc*zc);

				b = ceil(((-a*xy)-Diff.y())/yy);
				c = Func.round(((-a*xz)-(b*yz) - Diff.z())/zz);
				yd = Diff.y() + (a*xy) + (b*yy);
				zd = Diff.z() + (a*xz) + (b*yz) + (c*zz);
				vdr = (xb*xb) + (yd*yd) + (zd*zd);

				if(var < vbr && var < vcr && var < vdr) Diff.setCart(xa, ya, za);
				else if(vbr < vcr && vbr < vdr) Diff.setCart(xa, yb, zb);
				else if(vcr < vdr) Diff.setCart(xb, yc, zc);
				else Diff.setCart(xb, yd, zd);
				break;
			}
			case CELLSYMM_NONSTANDARD:
			{
		
				Min = Diff;
				for(a = -1; a <= 1; a++)
				{
					VA = Translation_val[0] * a;

					for(b = -1; b <= 1; b++)
					{
						VB = Translation_val[1] * b;

						for(c = -1; c <= 1; c++)
						{
							VC = Translation_val[2] * c;

							Test = VA + VB + VC + Diff;
							if(Test.r() < Min.r()) Min = Test;
						}
					}
				}

				Diff = Min;
				break;
			}
			default:
			{
		
				Min = Diff;
				for(a = -1; a <= 1; a++)
				{
					VA = Translation_val[0] * a;

					for(b = -1; b <= 1; b++)
					{
						VB = Translation_val[1] * b;

						for(c = -1; c <= 1; c++)
						{
							VC = Translation_val[2] * c;

							Test = VA + VB + VC + Diff;
							if(Test.r() < Min.r()) Min = Test;
						}
					}
				}

				Diff = Min;
				break;
			}
		}
	}

	return Diff;
}

FP_TYPE Geometry::angle(int i, int j, int k) const
{
	Vector A, B;

	A = bondVector(i, j);
	B = bondVector(k, j);

	return A |= B;
}

void Geometry::deleteBonds(const Array1D<int>& List)
{
	int count, i, j, k, place;

	for(count = 0; count < List.length(); count++)
	{
		i = List[count];

		for(k = 0; k < BondConnect_val[i].length(); k++)
		{
			j = BondConnect_val[i][k];
			place = BondConnect_val[j].search(i);
			BondConnect_val[j].deleteElement(place);
			BondType_val[j].deleteElement(place);
			BondOrder_val[j].deleteElement(place);
		}

		BondConnect_val[i].setSize(0);
		BondType_val[i].setSize(0);
		BondOrder_val[i].setSize(0);

		//  Set all of the other data that relies on bonding data
		Atoms_val[i].setAromatic(false);
		Atoms_val[i].setRingSize(0);
		Atoms_val[i].setRingBridge(false);
		Atoms_val[i].setHybrid(ATOM_HYBRID_NONE);
	}
}
void Geometry::setBonds()
{
	int i;
	Array1D<int> List;

	List.setSize(Atoms_val.length());
	for(i = 0; i < List.length(); i++) List[i] = i;
	
	setBonds(true, true, false, 1.2, List);
}

void Geometry::setBonds(bool includeS, bool includeD, bool includeF, FP_TYPE limit, const Array1D<int>& AtomList)
{
	int i, j, k, count, possNo, bondIncrease, stepUp;
	FP_TYPE radius, rij, upper;
	bool change;
	FunctionBall Func;
	Array1D<int> CurrentBondValence;
	int group;
	bool useI, useJ;

	deleteBonds(AtomList);
	
	//  First of all get the single bonds
	for(count = 0; count < AtomList.length(); count++)
	{
		i = AtomList[count];
		useI = true;
		group = Atoms_val[i].perodicTableGroup();
		if(group == PERIODIC_TABLE_GROUP_S && includeS == false && Atoms_val[i].type() != ATOM_HYDROGEN) useI = false;
		if(group == PERIODIC_TABLE_GROUP_D && includeD == false) useI = false;
		if(group == PERIODIC_TABLE_GROUP_F && includeF == false) useI = false;
		
		for(j = 0; j < Atoms_val.length() && useI == true; j++)
		{
			useJ = true;
			group = Atoms_val[j].perodicTableGroup();
			if(group == PERIODIC_TABLE_GROUP_S && includeS == false && Atoms_val[j].type() != ATOM_HYDROGEN) useJ = false;
			if(group == PERIODIC_TABLE_GROUP_D && includeD == false) useJ = false;
			if(group == PERIODIC_TABLE_GROUP_F && includeF == false) useJ = false;

			if(i != j && useJ == true)
			{
				radius = Atoms_val[i].covalentRadius() + Atoms_val[j].covalentRadius();
				radius /= 2;
				upper = radius * limit;
				rij = bondVector(i, j).r();

				if(rij < upper)
				{
					setBondType(i, j, GEOMETRY_BOND_SINGLE);
					setBondOrder(i, j, 1);
				}
			}
		}
	}

	//  Now that I have the connections set the sizes of all rings
	setRingSize(AtomList);

	//  Now go through and add "necessary" double and triple bonds
	change = true;
	while(change == true)
	{
		change = false;

		for(count = 0; count < AtomList.length(); count++)
		{
			i = AtomList[count];

			stepUp = Atoms_val[i].valenceBond() - (int)Func.round(bondTotalValence(i));
			if(stepUp > 0)
			{
				if(bondCovalentNumber(i) == 0) possNo = 0;
				else if(bondCovalentNumber(i) == 1)
				{
					possNo = 1;
					
					j = 0;
					bondIncrease = bondConnect(i, j);
					while(Atoms_val[bondIncrease].valenceBond() == 0)
					{
						j++;
						bondIncrease = bondConnect(i, j);
					}
				}
				else
				{
					possNo = 0;
					for(j = 0; j < bondNumber(i); j++)
					{
						k = bondConnect(i, j);
						if(Atoms_val[i].valenceBond() == 0);
						else if(Atoms_val[k].valenceBond() > (int)Func.round(bondTotalValence(k)))
						{
							possNo++;
							bondIncrease = k;
						}
						else if(Atoms_val[k].valenceBond() == (int)Func.round(bondTotalValence(k)) 
							&& Atoms_val[i].ringSize() == 5 
							&& Atoms_val[k].ringSize() == 5 
							&& (Atoms_val[k].type() == ATOM_NITROGEN || Atoms_val[k].type() == ATOM_OXYGEN || Atoms_val[k].type() == ATOM_PHOSPHORUS || Atoms_val[k].type() == ATOM_SULFUR))
						{
							//  This condition is here to doctor the rules for 5 atom hetero rings
							possNo++;
							bondIncrease = k;
						}
					}
				}

				if(possNo == 1)
				{
					if(stepUp == 1)
					{
						if(bondType(i, bondIncrease) == GEOMETRY_BOND_SINGLE)
						{
							change = true;
							setBondType(i, bondIncrease, GEOMETRY_BOND_DOUBLE);
							setBondOrder(i, bondIncrease, 2);
						}
						else if(bondType(i, bondIncrease) == GEOMETRY_BOND_DOUBLE)
						{
							change = true;
							setBondType(i, bondIncrease, GEOMETRY_BOND_TRIPLE);
							setBondOrder(i, bondIncrease, 3);
						}
					}
					else if(stepUp > 1 && bondType(i, bondIncrease) != GEOMETRY_BOND_TRIPLE)
					{
						change = true;
						setBondType(i, bondIncrease, GEOMETRY_BOND_TRIPLE);
						setBondOrder(i, bondIncrease, 3);
					}
				}
			}
		}
	}

	//  Now downscale double bonds to aromatic if they overplay the bonding on a particular atom
	for(count = 0; count < AtomList.length(); count++)
	{
		i = AtomList[count];
		
		if((int)Func.round(bondTotalValence(i)) > Atoms_val[i].valenceBond())
		{
			for(k = 0; k < BondConnect_val[i].length(); k++)
			{
				j = BondConnect_val[i][k];
				if((int)Func.round(bondTotalValence(j)) >= Atoms_val[j].valenceBond())
				{
					if(bondType(i,j) == GEOMETRY_BOND_DOUBLE)
					{
						setBondType(i, j, GEOMETRY_BOND_AROMATIC);
						setBondOrder(i, j, 1.5);
					}
					else if(bondType(i,j) == GEOMETRY_BOND_TRIPLE)
					{
						setBondType(i, j, GEOMETRY_BOND_AROMATIC_DOUBLE);
						setBondOrder(i, j, 2.5);
					}
				}
			}
		}
	}

	//  Set the list of current bond valences, and if they are satisfied
	CurrentBondValence.setSize(atomNo());
	for(i = 0; i < atomNo(); i++)
	{
		CurrentBondValence[i] = Atoms_val[i].valenceBond() - (int)Func.round(bondTotalValence(i));
	}

	//  Now presume that the rest of the bonds, if they are not necessary, must be aromatic
	for(count = 0; count < AtomList.length(); count++)
	{
		i = AtomList[count];
		
		if(CurrentBondValence[i] > 0)
		{
			//  First count the possible number of aromatic bonds, to try and not overasign biphenyl rings
			possNo = 0;
			for(k = 0; k < bondNumber(i); k++)
			{
				j = BondConnect_val[i][k];

				//  Test to see if that atom is also underdone
				if(CurrentBondValence[j] > 0)
				{
					possNo++;
				}
				else if(Atoms_val[i].ringSize() == 5 && Atoms_val[j].ringSize() == 5 && CurrentBondValence[j] == 0
					&& (Atoms_val[j].type() == ATOM_NITROGEN || Atoms_val[j].type() == ATOM_OXYGEN || Atoms_val[j].type() == ATOM_PHOSPHORUS || Atoms_val[j].type() == ATOM_SULFUR))
				{
					//  This condition is here to doctor the rules for 5 atom hetero rings
					possNo++;
				}
			}

			if(possNo <= 2 || (possNo == 3 && Atoms_val[i].ringBridge() == true))
			{
				for(k = 0; k < bondNumber(i); k++)
				{
					j = BondConnect_val[i][k];

					//  Test to see if that atom is also underdone
					if(CurrentBondValence[j] > 0)
					{
						setBondType(i, j, GEOMETRY_BOND_AROMATIC);
						setBondOrder(i, j, 1.5);
					}
					else if(Atoms_val[i].ringSize() == 5 && Atoms_val[j].ringSize() == 5 && CurrentBondValence[j] == 0 
						&& (Atoms_val[j].type() == ATOM_NITROGEN || Atoms_val[j].type() == ATOM_OXYGEN || Atoms_val[j].type() == ATOM_PHOSPHORUS || Atoms_val[j].type() == ATOM_SULFUR))
					{
						//  This condition is here to doctor the rules for 5 atom hetero rings
						setBondType(i, j, GEOMETRY_BOND_AROMATIC);
						setBondOrder(i, j, 1.5);
					}
				}
			}
		}
	}
}
void Geometry::setRingSize()
{
	int i;
	Array1D<int> List;

	List.setSize(Atoms_val.length());
	for(i = 0; i < List.length(); i++) List[i] = i;
	
	setRingSize(List);
}
void Geometry::setRingSize(const Array1D<int>& AtomList)
{
	int atom, i, j, k, a, b, count, loop, armNo, testIn, testOut, ringSize, same, limit;
	bool addNone;
	Array2D<int> Lists;
	Array1D<bool> FoundRing;
	bool bridge;

	for(count = 0; count < AtomList.length(); count++)
	{
		atom = AtomList[count];
		ringSize = 0;
		bridge = false;

		if(bondNumber(atom) >= 2  && Atoms_val[atom].valenceBond() > 0)
		{
			armNo = 0;
			for(i = 0; i < bondNumber(atom); i++) 
			{
				if(Atoms_val[bondConnect(atom,i)].valenceBond() > 0) armNo++;
			}
			Lists.setSize(armNo, 0);
			FoundRing.setSize(armNo);
			
			loop = 0;
			for(i = 0; i < bondNumber(atom); i++) 
			{
				if(Atoms_val[bondConnect(atom,i)].valenceBond() > 0)
				{
					Lists[loop].push(bondConnect(atom, i));
					FoundRing[loop] = false;
					loop++;
				}
			}
			
			addNone = false;

			for(loop = 2; loop <= 6 && addNone == false; loop++)
			{
				addNone = true;

				//  First add the necessary atoms to each arm for this level of rings
				for(i = 0; i < armNo; i++)
				{
					if(FoundRing[i] == false)
					{
						limit = Lists[i].length();
						for(j = 0; j < limit; j++)
						{
							testIn = Lists[i][j];
							for(k = 0; k < bondNumber(testIn); k++)
							{
								testOut = bondConnect(testIn, k);
								if(testOut != atom && Lists[i].search(testOut) < 0 && bondNumber(testOut) > 1 && Atoms_val[testOut].valenceBond() > 0) 
								{
									Lists[i].push(testOut);
									addNone = false;
								}
							}
						}
					}
				}

				//  Quicksort the lists for efficiency
				for(i = 0; i < armNo; i++) Lists[i].quicksort();
				
				//  Now check for any similarities
				for(i = 0; i < Lists.length(); i++)
				{
					for(j = i+1; j < Lists.length(); j++)
					{
						if(FoundRing[i] == false || FoundRing[j] == false)
						{
							same = 0;
							for(k = 0; k < Lists[i].length(); k++)
							{
								if(Lists[j].binarySearch(Lists[i][k]) >= 0) same++;
							}

							if(same > 0)
							{
								if(ringSize == 0) ringSize = loop*2;
								FoundRing[i] = true;
								FoundRing[j] = true;
							}
						}
					}
				}

				//  Now check to see if any atoms from different arms are bonded together
				for(i = 0; i < Lists.length(); i++)
				{
					for(j = i+1; j < Lists.length(); j++)
					{
						if(FoundRing[i] == false || FoundRing[j] == false)
						{
							for(a = 0; a < Lists[i].length(); a++)
							{
								for(b = 0; b < Lists[j].length(); b++)
								{
									if(bondType(Lists[i][a], Lists[j][b]) != GEOMETRY_BOND_NONE)
									{
										if(ringSize == 0) ringSize = (loop*2)+1;
										FoundRing[i] = true;
										FoundRing[j] = true;
									}
								}
							}
						}
					}
				}

				//  Now count the number of rings found
				same = 0;
				for(i = 0; i < FoundRing.length(); i++)
				{
					if(FoundRing[i] == true) same++;
				}
				if(same >= 3) bridge = true;
			}
		}
		Atoms_val[atom].setRingSize(ringSize);
		Atoms_val[atom].setRingBridge(bridge);
	}
}
void Geometry::setAromatic()
{
	int i;
	Array1D<int> List;

	List.setSize(Atoms_val.length());
	for(i = 0; i < List.length(); i++) List[i] = i;
	
	setAromatic(List);
}
void Geometry::setAromatic(const Array1D<int>& AtomList)
{
	int atom, i, j, k, count, loop, armNo, testIn, testOut, ringSize, same, maxSame, limit;
	bool addNone;
	Array2D<int> Lists;

	for(count = 0; count < AtomList.length(); count++)
	{
		atom = AtomList[count];

		if(bondNumber(atom) < 2) Atoms_val[atom].setRingSize(0);
		else
		{
			ringSize = 0;
			armNo = bondNumber(atom);
			Lists.setSize(armNo, 0);
			for(i = 0; i < armNo; i++) Lists[i].push(bondConnect(atom, i));
			addNone = false;

			for(loop = 2; addNone == false && ringSize == 0; loop++)
			{
				addNone = true;

				//  First add the necessary atoms to each arm for this level of rings
				for(i = 0; i < armNo; i++)
				{
					limit = Lists[i].length();
					for(j = 0; j < limit; j++)
					{
						testIn = Lists[i][j];
						for(k = 0; k < bondNumber(testIn); k++)
						{
							testOut = bondConnect(testIn, k);
							if(testOut != atom && Lists[i].search(testOut) < 0 && bondType(testIn, testOut) == GEOMETRY_BOND_AROMATIC) 
							{
								Lists[i].push(testOut);
								addNone = false;
							}
						}
					}
				}

				//  Quicksort the lists for efficiency
				for(i = 0; i < armNo; i++) Lists[i].quicksort();
				
				//  Now check for any similarities
				maxSame = 0;
				for(i = 0; i < Lists.length(); i++)
				{
					for(j = i+1; j < Lists.length(); j++)
					{
						same = 0;
						for(k = 0; k < Lists[i].length(); k++)
						{
							if(Lists[j].binarySearch(Lists[i][k]) >= 0) same++;
						}

						if(same > maxSame) maxSame = same;
					}
				}

				//  Now set the ring size if I can
				if(maxSame == 1) ringSize = loop*2;
				else if(maxSame > 1) ringSize = (loop*2)-1;
				else ringSize = 0;
			}

			if(ringSize > 0) Atoms_val[atom].setAromatic(true);
			else Atoms_val[atom].setAromatic(false);
		}
	}
}
void Geometry::setHybrid()
{
	int i;
	Array1D<int> List;

	List.setSize(Atoms_val.length());
	for(i = 0; i < List.length(); i++) List[i] = i;
	
	setHybrid(List);
}
void Geometry::setHybrid(const Array1D<int>& AtomList)
{
	bool bondFlag;
	int atom, i, k, j;
	FP_TYPE avgAngle;

	for(i = 0; i < AtomList.length(); i++)
	{
		atom = AtomList[i];

		switch(bondNumber(atom))
		{
			case 0:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_NONE);
				break;
			}
			case 1:
			{
				switch(Atoms_val[atom].type())
				{
					case ATOM_CARBON:
					case ATOM_SILICON:
					case ATOM_GERMANIUM:
					case ATOM_TIN:
					case ATOM_NITROGEN:
					case ATOM_PHOSPHORUS:
					case ATOM_ARSENIC:
					case ATOM_ANTIMONY:
					case ATOM_BISMUTH:
					case ATOM_OXYGEN:
					case ATOM_SULFUR:
					case ATOM_SELENIUM:
					case ATOM_TELLURIUM:
					case ATOM_POLONIUM:
					{
						j = bondConnect(atom, 0);
						if(bondType(atom, j) == GEOMETRY_BOND_DOUBLE || bondType(atom, j) == GEOMETRY_BOND_AROMATIC)
						{
							Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						}
						if(bondType(i, j) == GEOMETRY_BOND_TRIPLE)
						{
							Atoms_val[atom].setHybrid(ATOM_HYBRID_LINEAR);
						}
						else 
						{
							Atoms_val[atom].setHybrid(ATOM_HYBRID_TETRAHEDRAL);
						}
						break;
					}
					default:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_NONE);
						break;
					}
				}
			}
			case 2:
			{
				switch(Atoms_val[atom].type())
				{
					case ATOM_NITROGEN:
					case ATOM_PHOSPHORUS:
					case ATOM_ARSENIC:
					case ATOM_ANTIMONY:
					case ATOM_BISMUTH:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						break;
					}
					case ATOM_OXYGEN:
					case ATOM_SULFUR:
					case ATOM_SELENIUM:
					case ATOM_TELLURIUM:
					case ATOM_POLONIUM:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_TETRAHEDRAL);

						//  Test to see if there are any non-single bonds, indicating that there is SP2 hybridisation going on
						for(k = 0; k < bondNumber(i); k++)
						{
							j = bondConnect(i, k);
							if(bondType(i, j) == GEOMETRY_BOND_DOUBLE || bondType(i, j) == GEOMETRY_BOND_AROMATIC)
							{
								Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
							}
						}

						break;
					}
					default:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_LINEAR);
						break;
					}
				}
				break;
			}
			case 3:
			{
				switch(Atoms_val[atom].type())
				{
					case ATOM_NITROGEN:
					case ATOM_PHOSPHORUS:
					case ATOM_ARSENIC:
					case ATOM_ANTIMONY:
					case ATOM_BISMUTH:
					{
						bondFlag = false;
						if(BondType_val[atom][0] == GEOMETRY_BOND_DOUBLE || BondType_val[atom][0] == GEOMETRY_BOND_AROMATIC) bondFlag = true;
						if(BondType_val[atom][1] == GEOMETRY_BOND_DOUBLE || BondType_val[atom][1] == GEOMETRY_BOND_AROMATIC) bondFlag = true;
						if(BondType_val[atom][2] == GEOMETRY_BOND_DOUBLE || BondType_val[atom][2] == GEOMETRY_BOND_AROMATIC) bondFlag = true;

						//  Get the average bond angle to see if it is closer to 109 or 120 degrees
						avgAngle = 0;
						avgAngle += angle(BondConnect_val[atom][0], atom, BondConnect_val[atom][1]);
						avgAngle += angle(BondConnect_val[atom][0], atom, BondConnect_val[atom][2]);
						avgAngle += angle(BondConnect_val[atom][1], atom, BondConnect_val[atom][2]);
						avgAngle *= RAD_TO_DEG / 3;

						if(bondFlag == true) Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						else if(avgAngle > 115) Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						else Atoms_val[atom].setHybrid(ATOM_HYBRID_TETRAHEDRAL);
						break;
					}
					case ATOM_OXYGEN:
					case ATOM_SULFUR:
					case ATOM_SELENIUM:
					case ATOM_TELLURIUM:
					case ATOM_POLONIUM:
					{
						//  Get the average bond angle to see if it is closer to 109 or 120 degrees
						avgAngle = 0;
						avgAngle += angle(BondConnect_val[atom][0], atom, BondConnect_val[atom][1]);
						avgAngle += angle(BondConnect_val[atom][0], atom, BondConnect_val[atom][2]);
						avgAngle += angle(BondConnect_val[atom][1], atom, BondConnect_val[atom][2]);
						avgAngle *= RAD_TO_DEG / 3;

						if(avgAngle > 115) Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						else Atoms_val[atom].setHybrid(ATOM_HYBRID_TETRAHEDRAL);
						break;
					}
					default:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL);
						break;
					}
				}
				break;
			}
			case 4:
			{
				switch(Atoms_val[atom].type())
				{
					case ATOM_NICKEL:
					case ATOM_PALLADIUM:
					case ATOM_PLATINUM:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_SQUARE_PLANAR);
						break;
					}
					default:
					{
						Atoms_val[atom].setHybrid(ATOM_HYBRID_TETRAHEDRAL);
						break;
					}
				}
				break;
			}
			case 5:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_TRIGONAL_BIPYRAMID);
				break;
			}
			case 6:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_OCTAHEDRAL);
				break;
			}
			case 7:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_PENTAGONAL_BIPYRAMID);
				break;
			}
			case 8:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_SQUARE_ANTIPRISM);
				break;
			}
			default:
			{
				Atoms_val[atom].setHybrid(ATOM_HYBRID_NONE);
				break;
			}
		}
	}
}
void Geometry::setFormalCharge() 
{
	int i;
	Array1D<int> List;

	List.setSize(Atoms_val.length());
	for(i = 0; i < List.length(); i++) List[i] = i;
	
	setFormalCharge(List);
}
void Geometry::setFormalCharge(const Array1D<int>& AtomList)
{
	bool change;
	int i, j, k, a, b, c, place;
	FP_TYPE tempCharge, shiftCharge, value;
	Array1D<bool> RingDone;
	Array1D<int> RingList;

	//  Get the base charges
	for(i = 0; i < Atoms_val.length(); i++) Atoms_val[i].setFormalCharge(Atoms_val[i].loneFormalCharge());

	//  Now correct of the "covalent" bonds
	for(i = 0; i < AtomList.length(); i++) 
	{
		a = AtomList[i];
		if(Atoms_val[a].isOrganicCovalent())
		{
			tempCharge = Atoms_val[i].formalCharge();

			for(j = 0; j < bondNumber(a); j++)
			{
				b = bondConnect(a, j);
				if(Atoms_val[b].isOrganicCovalent())
				{
					if(Atoms_val[a].formalCharge() < 0) tempCharge += bondOrder(a, b);
					else tempCharge -= bondOrder(a, b);
				}
			}

			if(Atoms_val[a].aromatic() == true && Atoms_val[a].ringBridge() == true && fabs(tempCharge-0.5) < 1E-6) tempCharge = 0;

			Atoms_val[i].setFormalCharge(tempCharge);
		}
	}

	//  Now test for all of the 5 membered ring complexes
	RingDone.setSize(AtomList.length());
	for(i = 0; i < RingDone.length(); i++) RingDone[i] = false;
	for(i = 0; i < AtomList.length(); i++)
	{
		a = AtomList[i];

		if(RingDone[i] == false && Atoms_val[a].aromatic() == true)
		{
			//  Get all of the atoms
			RingList.setSize(1);
			RingList[0] = a;
			change = true;
			while(change == true)
			{
				change = false;
				for(j = 0; j < RingList.length(); j++)
				{
					b = RingList[j];
					for(k = 0; k < bondNumber(b); k++)
					{
						c = BondConnect_val[b][k];
						if(BondType_val[b][k] == GEOMETRY_BOND_AROMATIC && RingDone[c] == false)
						{
							place = RingList.search(c);
							if(place == -1)
							{
								RingList.push(c);
								change = true;
								RingDone[c] = true;
							}
						}
					}
				}
			}

			//  If I have an odd number of electrons in the ring
			if(RingList.length() % 2 == 1)
			{
				//  Now go through and delete the bridging atoms
				j = 0;
				while(j < RingList.length())
				{
					if(Atoms_val[RingList[j]].ringBridge() == true) RingList.deleteElement(j);
					else j++;
				}

				//  Take out those with lower electronegativity
				value = Atoms_val[RingList[0]].electroNeg();
				for(j = 1; j < RingList.length(); j++)
				{
					if(value < Atoms_val[RingList[j]].electroNeg()) value = Atoms_val[RingList[j]].electroNeg();
				}
				j = 0;
				while(j < RingList.length())
				{
					if(fabs(Atoms_val[RingList[j]].electroNeg()-value)>1E-6) RingList.deleteElement(j);
					else j++;
				}

				//  Take out those with lower formal charge
				value = Atoms_val[RingList[0]].formalCharge();
				for(j = 1; j < RingList.length(); j++)
				{
					if(value < Atoms_val[RingList[j]].formalCharge()) value = Atoms_val[RingList[j]].formalCharge();
				}
				j = 0;
				while(j < RingList.length())
				{
					if(fabs(Atoms_val[RingList[j]].formalCharge()-value)>1E-6) RingList.deleteElement(j);
					else j++;
				}

				//  Now shift the charge of all of the relevant atoms
				shiftCharge = 1 / (FP_TYPE)RingList.length();
				for(j = 0; j < RingList.length(); j++)
				{
					b = RingList[j];
					tempCharge = Atoms_val[b].formalCharge() - shiftCharge;
					Atoms_val[b].setFormalCharge(tempCharge);
				}
			}
		}
	}
}


void Geometry::loadXYZGeometry(const String& FileStem) 
{
	bool isFrac, setFormal;
	int i, atomNo;
	FP_TYPE a, b, c, alpha, beta, gamma;
	String Line, File, ErrorMessage, Tag;
	Vector Pos;
	ifstream Input;

	//  Open the ARC file
	File = FileStem;
	if((FileStem %= ".XYZ") || (FileStem %= ".xyz"));
	else File += ".xyz";

	Input.open(File.pointer(), ios::in);
	if(!Input.is_open()) 
	{
		ErrorMessage << "Unable to open file \"" & File & "\" to read in ARC information";
		throw ErrorBall(ErrorMessage);
	}


	//  Read in the comment line, and do nothing with it
	Input >> Line;

	//  Now get the type of coordinates
	Input >> Line;
	Tag = Line.token(1).toUpper();
	if((Tag == "CART") || (Tag == "FRAC"))
	{
		a = Line.toktof(2);
		b = Line.toktof(3);
		c = Line.toktof(4);
		alpha = Line.toktof(5) * DEG_TO_RAD;
		beta = Line.toktof(6) * DEG_TO_RAD;
		gamma = Line.toktof(7) * DEG_TO_RAD;
		isFrac = (Tag == "FRAC") ? true : false;
		setCellParamZYX(a, b, c, alpha, beta, gamma);
	}
	else
	{
		a = -1;
		isFrac = false;
	}

	//  Now get the number of atoms
	Input >> Line;
	atomNo = Line.token(1).stoi();
	this->setAtomNumber(atomNo);

	//  Now go through and get the data for each atom
	setFormal = true;
	for(i = 0; i < atomNo; i++)
	{
		Input >> Line;
		Atoms_val[i].setSymbol(Line.token(1));
		Pos.setCart(Line.toktof(2), Line.toktof(3), Line.toktof(4));
		Atoms_val[i].setPosition(Pos);
		if(Line.tokenNo() > 4) Atoms_val[i].setFFType(Line.token(5));
		if(Line.tokenNo() > 5) Atoms_val[i].setCharge(Line.toktof(6));
		if(Line.tokenNo() > 6) 
		{
			setFormal = false;
			Atoms_val[i].setFormalCharge(Line.toktof(7));
		}
	}

	//  Now modify the cell so that it is in the ZYX orientation
	if(isFrac == true) setAtomPosCartesian();

	//  Now set all of the atom data
	setAtomData();
	setBonds();
	setRingSize();
	setAromatic();
	setHybrid();
	if(setFormal == true) setFormalCharge();
}
void Geometry::printXYZGeometry(const String& FileStem)
{
	int i;
	String FileName, ErrorMessage, Type;
	ofstream Output;

	FileName = FileStem;
	if(!(FileName %= ".xyz")) FileName += ".xyz";
	Output.open(FileName.pointer(), ios::out);
	if(!Output.is_open()) 
	{
		ErrorMessage << "Unable to open file \"" & FileName & "\" to write out XYZ information";
		throw ErrorBall(ErrorMessage);
	}
	Output.setf(ios::showpoint | ios::fixed);
	Output.precision(6);

	//  Comment line;
	Output << FileStem << endl;

	//  Output the cell parameters if they are there
	if(period() == 3)
	{
		Output << setw(10) << "FRAC";
		Output << setw(15) << this->cellParamA();
		Output << setw(15) << this->cellParamB();
		Output << setw(15) << this->cellParamC();
		Output << setw(15) << this->cellParamAlpha() * RAD_TO_DEG;
		Output << setw(15) << this->cellParamBeta() * RAD_TO_DEG;
		Output << setw(15) << this->cellParamGamma() * RAD_TO_DEG;
		Output << endl;
	}
	else
	{
		Output << "MOLECULE" << endl;
	}

	//  Now output the atom number
	Output << atomNo() << endl;

	//  Now output the data for each atom
	if(period() == 3) setAtomPosFractional();
	for(i = 0; i < atomNo(); i++)
	{
		Type = Atoms_val[i].FFType();
		if(Type == "") Type = "xx";

		Output << setw(5) << Atoms_val[i].Symbol();
		Output << setw(15) << Atoms_val[i].Position().x();
		Output << setw(15) << Atoms_val[i].Position().y();
		Output << setw(15) << Atoms_val[i].Position().z();
		Output << setw(10) << Type;
		Output << setw(15) << Atoms_val[i].charge();
		Output << setw(15) << Atoms_val[i].formalCharge();
		Output << endl;
	}
	Output << endl;
	if(period() == 3) setAtomPosCartesian();

	Output.close();
}	

void Geometry::setAtomData()
{
	int i;
	Array1D<int> AtomList;

	AtomList.setSize(Atoms_val.length());
	for(i = 0; i < AtomList.length(); i++) AtomList[i] = i;

	setAtomData(AtomList);
}

void Geometry::setAtomData(const Array1D<int>& AtomList)
{
	int i, atom, type;
	AtomDataBall AtomData;

	for(i = 0; i < AtomList.length(); i++)
	{
		atom = AtomList[i];

		//  Get the type natively or from the symbol, with preference from the symbol
		if(Atoms_val[atom].Symbol() == "")
		{
			type = Atoms_val[atom].type();
			Atoms_val[atom].setSymbol(AtomData.getSymbol(type));
		}
		else
		{
			type = AtomData.getSymbolIndex(Atoms_val[atom].Symbol());
			Atoms_val[atom].setType(type);
		}

		//  Now set the rest of the data
		Atoms_val[atom].setName(AtomData.getName(type));
		Atoms_val[atom].setShell(AtomData.getShell(type));
		Atoms_val[atom].setValenceElec(AtomData.getValenceElec(type));
		Atoms_val[atom].setValenceBond(AtomData.getValenceBond(type));
		Atoms_val[atom].setMinCharge(AtomData.getMinCharge(type));
		Atoms_val[atom].setMaxCharge(AtomData.getMaxCharge(type));
		Atoms_val[atom].setMass(AtomData.getMass(type));
		Atoms_val[atom].setElectroNeg(AtomData.getElectroneg(type));
		Atoms_val[atom].setHardness(AtomData.getHardness(type));
		Atoms_val[atom].setSlaterScreen(AtomData.getSlaterScreen(type));
		Atoms_val[atom].setGammaScreen(AtomData.getGammaScreen(type));
		Atoms_val[atom].setVdwRadius(AtomData.getVdwRadius(type));
		Atoms_val[atom].setSurfaceRadius(AtomData.getSurfaceRadius(type));
		Atoms_val[atom].setCovalentRadius(AtomData.getCovalentRadius(type));
		Atoms_val[atom].setSlaterRadius(AtomData.getSlaterRadius(type));
	}
}

FP_TYPE Geometry::chargeGeomEquilibrate(ostream& Output, const InputChargeFitBall& Calc)
{
	int i;
	FP_TYPE energy;
	ChargeEquilibrator QEq;

	QEq.equilibrate(Output, *this, Calc);
	energy = QEq.solutionEnergy(*this, Calc);

	for(i = 0; i < atomNo(); i++)
	{
		Atoms_val[i].setCharge(QEq.charge(i));
	}
	QEq.null();

	return energy;
}

