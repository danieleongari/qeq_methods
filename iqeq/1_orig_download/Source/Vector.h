/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _VECTOR_H
#define _VECTOR_H

#include "definitions.h"

#include <iostream>
using namespace std;

class Vector
{
	friend class MPICommBall;

	protected: 
		FP_TYPE x_val, y_val, z_val;
	
		//  Functions for updating the polar and cartesian internal coordinates
		void setCart();

	public:
		
		//  Constructor functions that take in a nothing and a vector as the input
		Vector();
		Vector(Vector const&);
		Vector(FP_TYPE, FP_TYPE, FP_TYPE);
		void null();

		//  The wrapper functions
		FP_TYPE x() const;
		FP_TYPE y() const;
		FP_TYPE z() const;
		FP_TYPE r() const;
		FP_TYPE theta() const;
		FP_TYPE phi() const;

		//  Functions for some squares, because that helps as well
		FP_TYPE xSq() const;
		FP_TYPE ySq() const;
		FP_TYPE zSq() const;
		FP_TYPE rSq() const;

		Vector xUnit() const;
		Vector yUnit() const;
		Vector zUnit() const;
		Vector rUnit() const;
		Vector thetaUnit() const;
		Vector phiUnit() const;

		//  Function that returns the maximum of the two values
		FP_TYPE rMax(FP_TYPE) const;

		//  Overload in the index operator so that I can access it like an array
		//  but only for the cartesians
		FP_TYPE& operator[](int);
		FP_TYPE cartesian(int) const;

		//  Writer functions that allow for setting the vector from cartesian or polar coords
		void setPolar(FP_TYPE, FP_TYPE, FP_TYPE);
		void setCart(FP_TYPE, FP_TYPE, FP_TYPE);
		void setCylinderPolar(FP_TYPE, FP_TYPE, FP_TYPE);
		void setElement(int, FP_TYPE);

		//  Unary vector functions to set the unit vector and to zero the vector
		void zero();
		Vector unit() const;

		//  Do a comparison with the == operator and also a not equal
		bool operator==(const Vector&) const; 
		bool operator!=(const Vector&) const; 
		bool operator<(const Vector&) const;
		bool operator>(const Vector&) const;

		//  Overload to test if a vector is a null vector or not
		bool operator!();

		//  Function to copy using the overloaded equals operatot
		Vector& operator=(const Vector&);
		Vector& operator=(Vector*);
		int operator=(int);

		//  Functions to add and subtract, based on operator overloading
		Vector& operator+=(const Vector&);
		Vector operator+(const Vector&) const;
		Vector& operator-=(const Vector&);
		Vector operator-(const Vector&) const;

		//  Functions to multiply and divide by scalars, done with operator overloading
		Vector& operator*=(FP_TYPE);
		Vector operator*(FP_TYPE) const;
		Vector& operator/=(FP_TYPE);
		Vector operator/(FP_TYPE) const;
		
		//  Operator for the negative of the vector
		Vector operator-() const;

		//  Functions to do the dot prod and the cross product
		//  Note that cross prod is done with "*" and dot prod is done with "%"
		Vector operator*(const Vector&) const;
		Vector unitCross(const Vector&) const;
		FP_TYPE operator%(const Vector&) const;
		Tensor operator&(const Vector&) const;
		Vector operator|(const Vector&) const;
		Vector bracketProduct(const Vector&) const;

		//  Function to get the funny cross product between vector and tensor
		Tensor operator&(const Tensor&) const;

		//  Function to get the angle between this vector and another
		FP_TYPE operator|=(const Vector&) const;

		//  Function to get the midpoint between two vectors
		Vector operator||(const Vector&) const;

		//  Function to spin vectors about an arbitrary axis
		Vector spin(const Vector&, FP_TYPE) const;
		Vector spin(const Vector&, const Vector&, FP_TYPE) const;
		Vector eulerSpin(FP_TYPE, FP_TYPE, FP_TYPE) const;
		Vector eulerSpin(FP_TYPE, FP_TYPE, FP_TYPE, const Vector&) const;

		//  Function to project this vector along another vector, or along a plane
		Vector project(const Vector&) const;
		Vector project(const Vector&, const Vector&) const;

		//  An overload for output, so that I can easily test values of vectors
		friend ostream& operator<<(ostream&, const Vector&);
};

#endif



