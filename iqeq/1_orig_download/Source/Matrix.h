
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _MATRIX_H
#define _MATRIX_H

#include "definitions.h"
#include "mkl.h"
#include "Lapack.h"

#include <iostream>
using namespace std;

/*  Here rowNo refers to the length of each column, and columnNo refers to the length of each row
 *  Referencing should always be Column,Row indexes in that order
 *  Allocating space should always be RowNo,ColumnNo in that order
 */

/*  Note that the notation and idea of these matrices are that they are stored in a column major format
 *  Indices refer first to column (with the first number) and then to row
 *  This is the typical FORTRAN storage pattern so that it links in with LAPACK and BLAS
 */

#define MATRIX_NORMAL 0
#define MATRIX_TRANSPOSE 1

class Matrix
{
	friend class Column;

	protected:
		FP_TYPE* Data_val;
		int rowNo_val, columnNo_val;
		int rowSize_val, columnSize_val;
		
	public:
		//  The constructor
		Matrix();
		Matrix(int, int);
		Matrix(Matrix const&);
		~Matrix();
		Matrix& operator=(const Matrix&); 
		Matrix& operator=(const Tensor&); 
		int operator=(int);
		void null();

		//  The readers
		int rowNo() const;
		int columnNo() const;
		int rowSize() const;
		int columnSize() const;
		int totalSize() const;

		//  The master writer
		FP_TYPE& operator[](int) const;
		FP_TYPE& operator()(int, int) const;

		//  The writer
		void zero();
		Matrix identity();
		void setSize(int, int);

		//  Overloads for addition and subtraction
		Matrix& operator+=(const Matrix&);  

		//  Overloads for multiplication
		Matrix operator*(const Matrix&) const;   
		Matrix& operator*=(FP_TYPE);    
		Matrix& operator/=(FP_TYPE);		

		//  Overload for multipication by a column
		Column operator*(const Column&) const;
		
		//  Functions to solve linear equations
		Column linearSolve(const Column&);
};

#endif
