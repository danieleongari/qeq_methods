
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "XmlLine.h"


XmlLine::XmlLine()
{
	null();
}
XmlLine::XmlLine(const XmlLine& A)
{
	*this = A;
}
void XmlLine::null()
{
	TagName_val.setSize(0);
	TagValue_val.setSize(0);
	end_val = false;
	LineName_val = "";
	printEnd_val = false;
	printTabLevel_val = 0;
	precision_val = 6;
}
const XmlLine& XmlLine::operator=(const XmlLine& A)
{
	TagName_val = A.TagName_val;
	TagValue_val = A.TagValue_val;
	end_val = A.end_val;
	LineName_val = A.LineName_val;
	printEnd_val = A.printEnd_val;
	printTabLevel_val = A.printTabLevel_val;
	precision_val = A.precision_val;

	return *this;
}
int XmlLine::tagNo() const { return TagName_val.length(); }
const String& XmlLine::LineName() const { return LineName_val; }
const String& XmlLine::TagName(int a) const 
{
	if(a >= TagName_val.length()) throw ErrorBall("Called for a XML tag number that exceeded the number of tags\n");
	return (TagName_val)[a];
}
const String& XmlLine::TagValue(int a) const
{
	if(a >= TagValue_val.length()) throw ErrorBall("Called for a XML tag number that exceeded the number of tags\n");
	return (TagValue_val)[a];
}
int XmlLine::TagValueInt(int a) const
{
	String Temp;
	Temp = TagValue(a);
	return Temp.stoi();
}
FP_TYPE XmlLine::TagValueDouble(int a) const
{
	String Temp;
	Temp = TagValue(a);
	return Temp.stof();
}
bool XmlLine::TagValueBool(int a) const
{
	String Temp;
	Temp = TagValue(a);
	if(Temp[0] == 't' || Temp[0] == 'T') return true;
	else return false;
}

const String& XmlLine::TagValue(String const& Seeker) const
{
	int i;
	String ErrorMessage;

	i = TagName_val.search(Seeker);
	if(i == -1) 
	{
		ErrorMessage = "Called for unknown tag \"";
		ErrorMessage += Seeker;
		ErrorMessage += "\" in XML line \"";
		ErrorMessage += LineName_val;
		ErrorMessage += "\"\n";
		throw ErrorBall(ErrorMessage);
	}
	else return TagValue_val[i];
}
int XmlLine::TagValueInt(String const& Seeker) const
{
	String Temp;
	Temp = TagValue(Seeker);
	return Temp.stoi();
}
FP_TYPE XmlLine::TagValueDouble(String const& Seeker) const
{
	String Temp;
	Temp = TagValue(Seeker);
	return Temp.stof();
}
bool XmlLine::TagValueBool(String const& Seeker) const
{
	String Temp;
	Temp = TagValue(Seeker);
	if(Temp[0] == 't' || Temp[0] == 'T') return true;
	else return false;
}


const String& XmlLine::TagValue(char* seek) const
{
	String Seeker;

	Seeker = seek;
	return TagValue(Seeker);
}
int XmlLine::TagValueInt(char* seek) const
{
	String Seeker;

	Seeker = seek;
	return TagValueInt(Seeker);
}
FP_TYPE XmlLine::TagValueDouble(char* seek) const
{
	String Seeker;

	Seeker = seek;
	return TagValueDouble(Seeker);
}
bool XmlLine::TagValueBool(char* seek) const
{
	String Seeker;

	Seeker = seek;
	return TagValueBool(Seeker);
}

bool XmlLine::isStart() const { return !end_val; }
bool XmlLine::isStart(const char* tag) const
{
	String Test;

	Test << tag;
	if(LineName_val == Test && end_val == false) return true;
	else return false;
}
bool XmlLine::isStart(String const& Test) const
{
	if(LineName_val == Test && end_val == false) return true;
	else return false;
}
bool XmlLine::isEnd() const { return end_val; }
bool XmlLine::isEnd(const char* tag) const
{
	String Test;

	Test << tag;
	if(LineName_val == Test && end_val == true) return true;
	else return false;
}
bool XmlLine::isEnd(String const& Test) const
{
	if(LineName_val == Test && end_val == true) return true;
	else return false;
}

bool XmlLine::tagExists(const String& Tag) const
{
	int i;

	for(i = 0; i < TagName_val.length() && TagName_val[i] != Tag; i++);
	if(i == TagName_val.length()) return false;
	else return true;
}
bool XmlLine::tagExists(char* tag) const
{
	int i;

	for(i = 0; i < TagName_val.length() && TagName_val[i] != tag; i++);
	if(i == TagName_val.length()) return false;
	else return true;
}
bool XmlLine::printEnd() const
{
	return printEnd_val;
}
int XmlLine::printTabLevel() const
{
	return printTabLevel_val;
}
int XmlLine::precision() const
{
	return precision_val;
}

void XmlLine::setLineName(const String& Name)
{
	LineName_val = Name;
}
void XmlLine::setTag(const String& TagName, const String& TagValue)
{
	TagName_val.push(TagName);
	TagValue_val.push(TagValue);
}
void XmlLine::setTag(const String& TagName, const char* TagValue)
{
	TagName_val.push(TagName);
	TagValue_val.push(TagValue);
}
void XmlLine::setTag(const String& TagName, int tagValue)
{
	String TagStringValue;

	TagStringValue << tagValue;

	TagName_val.push(TagName);
	TagValue_val.push(TagStringValue);
}
void XmlLine::setTag(const String& TagName, FP_TYPE tagValue)
{
	String TagStringValue;

	TagStringValue.setFloat(precision_val, tagValue);

	TagName_val.push(TagName);
	TagValue_val.push(TagStringValue);
}
void XmlLine::setTag(const String& TagName, bool tagValue)
{
	String TagStringValue;

	if(tagValue == true) TagStringValue << "True";
	else TagStringValue << "False";

	TagName_val.push(TagName);
	TagValue_val.push(TagStringValue);
}
void XmlLine::setEnd(bool isEnd)
{
	end_val = isEnd;
}
void XmlLine::setPrintState(bool printEnd, int tabLevel)
{
	printEnd_val = printEnd;
	printTabLevel_val = tabLevel;
	TagName_val.setSize(0);
	TagValue_val.setSize(0);
	end_val = false;
}
void XmlLine::setPrecision(int value)
{
	precision_val = value;
}

XmlLine & XmlLine::operator=(String const& Line)
{
	int i, place, count, start, end;
	String ThisTok, Temp;
	String Test;
	String NewLine;
	int tagNo;

	//  Copy out line, so that it is not destroyed
	NewLine = Line;

	tagNo = 0;
	end_val = false;
	LineName_val = "";

	//  Now count the number of tags by the number of FP_TYPE quotes, then alloc memory
	tagNo = 0;
	for(i = 0; i < NewLine.length(); i++)
	{
		if(NewLine[i] == '\"') tagNo++;
	}
	tagNo /= 2;
	TagName_val.setSize(tagNo);
	TagValue_val.setSize(tagNo);

	//  Now get the name of the line
	ThisTok = NewLine.token(1);
	if(ThisTok %= "/") 
	{
		end_val = true;
		ThisTok.translate("</", "");
		ThisTok.translate(">", "");
		LineName_val = ThisTok;
	}
	
	else
	{
		end_val = false;
		ThisTok.translate("<", "");
		ThisTok.translate(">", "");
		LineName_val = ThisTok;
	
		//  Now check for the immediate ending block
		Test = "</";
		Test += LineName_val;
		Test += ">";
		NewLine.translate(Test.pointer(), "");

		//  Now get all of the tokens
		if(tagNo > 0)
		{
			//  Find the start of the line
			for(place = 0; NewLine[place] != '<'; place++);
			for(; NewLine[place] != ' '; place++);
			place++;
		
			//j = 2;
			for(i = 0; i < tagNo; i++)
			{
				while(NewLine[place] == ' ') place++;
				start = place;
				count = 0;
				while(count < 2)
				{
					if(NewLine[place] == '\"') count++;
					place++;
				}
				end = place-1;
				place++;
				ThisTok = NewLine(start, end);
				/*
				ThisTok = NewLine.token(j);
				while(ThisTok[ThisTok.length()-1] != '\"' && ThisTok[ThisTok.length()-1] != '>')
				{
					j++;
					Temp = NewLine.token(j);
					ThisTok += " ";
					ThisTok += Temp;
				}
				*/

				(TagName_val)[i] = ThisTok.token(1, "=");
				if(ThisTok %= "\"\"") (TagValue_val)[i] = "";
				else 
				{
					(TagValue_val)[i] = ThisTok.token(2, "\"");
					if(ThisTok[ThisTok.length()-1] == '>') (TagValue_val)[i].translate("\">", "");
					(TagValue_val)[i].translate("\"", "");
				}
				//j++;
			}
		}
	}
	
	return *this;
}
int XmlLine::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in XmlLine::operator=(int i)");
	null();

	return i;
}

istream & operator>>(istream &Input, XmlLine &A)
{
	String Line;

	Input >> Line;
	A = Line;
	return Input;
}

ostream& operator<<(ostream &Output, XmlLine &A)
{
	int i;

	Output.precision(12);

	//  Print out the necessary tabs
	for(i = 0; i < A.printTabLevel(); i++) Output << "\t";

	if(A.isEnd() == true)
	{
		Output << "</" << A.LineName() << ">";
	}
	else
	{
		Output << "<" << A.LineName();
		for(i = 0; i < A.tagNo(); i++)
		{
			Output << " " << A.TagName(i) << "=\"" << A.TagValue(i) << "\"";
		}
		Output << ">";
		
		if(A.printEnd() == true)
		{
			Output << "</" << A.LineName() << ">";
		}
	}

	return Output;
}

