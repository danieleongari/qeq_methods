
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "String.h"
#include "definitions.h"
#include "ErrorBall.h"
#include "FunctionBall.h"
#include "Vector.h"

#include <cstring>
#include <cstdlib>
#include <cctype>
#include <cstdio>
#include <cmath>
#include <iostream>
using namespace std;

//  The resize function, which is private
void String::resize(int newSize)
{
	char* buffer;
	int i, bufferSize;
	FunctionBall Func;

	bufferSize = Func.minValue(length_val, newSize);
	buffer = new char[bufferSize+1];
	for(i = 0; i < bufferSize; i++) buffer[i] = pointer_val[i];
	buffer[i] = '\0';

	delete [] pointer_val;
	pointer_val = new char[newSize+1];
	strcpy(pointer_val, buffer);
	delete [] buffer;
}

//  The constructors and destructors
String::String()
{
	pointer_val = new char[1];
	strcpy(pointer_val, "");
	length_val = 0;
}
String::String(String const& A)
{
	length_val = A.length();
	pointer_val = new char[length_val+1];
	strcpy(pointer_val, A.pointer_val);
}
String::String(const char *A)
{
	if(A != NULL)
	{	
		length_val = (int)strlen(A);
		pointer_val = new char[length_val+1];
		strcpy(pointer_val, A);
	}
	else
	{	
		pointer_val = new char[1];
		strcpy(pointer_val, "");
		length_val = 0;
	}
}
String::~String()
{
	if(pointer_val != NULL) delete [] pointer_val;
}
void String::null()
{
	*this = "";
}

char* String::pointer() const
{
	return pointer_val;
}

//  A reader for getting a character out of it
char& String::operator[](int i) const
{
	if(i < 0 || i >= length_val) 
		throw ErrorBall("String out of bounds access in String::operator[]\n");
	return pointer_val[i] ;
}

//  Function for getting the length of the string or a reference to the memory
int String::length() const
{
	return length_val;
}

//  Assignment operator
String& String::operator=(String const& A)
{
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = A.length();
	pointer_val = new char[length_val+1];
	strcpy(pointer_val, A.pointer_val);

	return *this;
}
String& String::operator=(const char *A)
{
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = (int)strlen(A);
	pointer_val = new char[length_val+1];
	strcpy(pointer_val, A);
	
	return *this;
}
String& String::operator=(char A)
{
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = 1;
	pointer_val = new char[length_val+1];
	pointer_val[0] = A;
	pointer_val[1] = '\0';
	
	return *this;
}
int String::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in String::operator=(int i)");
	null();

	return i;
}

//  Left associative assignment
String& String::operator<<(String const& A)
{
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = A.length();
	pointer_val = new char[length_val+1];
	strcpy(pointer_val, A.pointer_val);

	return *this;
}
String & String::operator<<(const char *A)
{
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = (int)strlen(A);
	pointer_val = new char[length_val+1];
	strcpy(pointer_val, A);
	
	return *this;
}
String & String::operator<<(const int i)
{
	FunctionBall Func;
	
	if(pointer_val != NULL) delete [] pointer_val;
	length_val = Func.intCharLength(i);
	pointer_val = new char[length_val];
	sprintf(pointer_val, "%d", i);
	
	return *this;
}
String & String::operator<<(const FP_TYPE data)
{
	if(fabs(data) > 0.001 && fabs(data) < 1000) 
	{
		setFloat(6, data);
	}
	else 
	{
		setFloatScientific(6, data);
	}
	
	return *this;
}

void String::setFloat(FP_TYPE value)
{
	int sigFig;

	if(fabs(value) < ZERO || fabs(value) > 1) sigFig = 0;
	else sigFig = -1*(int)floor(log10(fabs(value)));
	setFloat(sigFig, value);
}
void String::setFloat(int precision, FP_TYPE value)
{
	FP_TYPE limit;
	char number[100];
	char format[10];

	limit = pow(10, (FP_TYPE)-precision);

	sprintf(format, "%%.%d", precision);
	#if FP_PRECISION == FP_DOUBLE
		strcat(format, "l");
	#endif
	strcat(format, "f");
	sprintf(number, format, value);
	*this = number;
}
void String::setFloatScientific(int precision, FP_TYPE value)
{
	char number[100];
	char format[10];
	String Temp, Result;
	int power;

	sprintf(format, "%%.%d", precision);
	#if FP_PRECISION == FP_DOUBLE
		strcat(format, "l");
	#endif
	strcat(format, "e");
	sprintf(number, format, value);

	Temp = number;
	Result = Temp.token(1, "e");
	Result += "e";
	power = Temp.token(2, "e").stoi();
	if(power >= 0) Result += "+";
	Result += Temp.token(2, "e").stoi();

	*this = Result;
}
void String::setFloat(int precision, const Vector& Data)
{
	String Temp[3];

	Temp[0].setFloat(precision, Data.x());
	Temp[1].setFloat(precision, Data.y());
	Temp[2].setFloat(precision, Data.z());

	*this << "< " & Temp[0] & " , " & Temp[1] & " , " & Temp[2] & " >";
}
void String::setFloatScientific(int precision, const Vector& Data)
{
	String Temp[3];

	Temp[0].setFloatScientific(precision, Data.x());
	Temp[1].setFloatScientific(precision, Data.y());
	Temp[2].setFloatScientific(precision, Data.z());

	*this << "< " & Temp[0] & " , " & Temp[1] & " , " & Temp[2] & " >";
}

//  Comparison operators
bool String::operator==(String const& A) const
{
	if(strcmp(pointer_val, A.pointer_val) == 0) return true;
	else return false;
}
bool String::operator==(char *A) const
{
	if(strcmp(pointer_val, A) == 0) return true;
	else return false;
}
bool String::operator!=(String const& A) const
{
	if(strcmp(pointer_val, A.pointer_val) == 0) return false;
	else return true;
}
bool String::operator!=(char *A) const
{
	if(strcmp(pointer_val, A) == 0) return false;
	else return true;
}

//  Operator for the empty string
bool String::operator!()
{
	if(length_val == 0) return true;
	else return false;
}

//  Operator for the contains
bool String::operator%=(String const& A) const
{
	if(strstr(pointer_val, A.pointer_val) != NULL) return true;
	else return false;
}
bool String::operator%=(char *A) const
{
	if(strstr(pointer_val, A) != NULL) return true;
	else return false;
}

//  Operator for String comparisons
bool String::operator<(String const& A) const
{
	if(strcmp(pointer_val, A.pointer_val) < 0) return true;
	else return false;
}
bool String::operator<(char *A) const
{
	if(strcmp(pointer_val, A) < 0) return true;
	else return false;
}
bool String::operator>(String const& A) const
{
	if(strcmp(pointer_val, A.pointer_val) > 0) return true;
	else return false;
}
bool String::operator>(char *A) const
{
	if(strcmp(pointer_val, A) > 0) return true;
	else return false;
}

//  Functions for getting sub bits of strings
String String::token(int i) const
{
	return this->token(i, " \t\n");
}
String String::token(int i, String const& A) const
{
	return this->token(i, A.pointer_val);
}
String String::token(int i, char *A) const
{
	String Result;
	char* tok;
	int j;
	char* temp;

	//  Copy the string so that it doesn't get destroyed
	temp = new char[length_val+1];
	strcpy(temp, pointer_val);
	tok = NULL;
	for(j = 0; j < i && (j == 0 || tok != NULL); j++)
	{
		if(j == 0) tok = strtok(pointer_val, A);
		else tok = strtok(NULL, A);
	}
 
	if(tok != NULL) Result = tok;
	else Result = "";
	strcpy(pointer_val, temp);
	
	delete [] temp;
	return Result;
}
FP_TYPE String::toktof(int i) const
{
	return this->toktof(i, " \t\n");
}
FP_TYPE String::toktof(int i, String const& A) const
{
	return this->toktof(i, A.pointer_val);
}
FP_TYPE String::toktof(int i, char* A) const
{
	String Token;
	FP_TYPE result;

	Token = this->token(i, A);
	result = Token.stof();
	return result;
}
int String::tokenNo() const
{
	return this->tokenNo(" \t\n");
}
int String::tokenNo(String const& A) const
{
	return this->tokenNo(A.pointer());
}
int String::tokenNo(char* A) const
{
	int size, count;
	char *temp;
	char* tok;

	size = (int)strlen(pointer_val);
	temp = new char[size+10];
	strcpy(temp, pointer_val);
	tok = strtok(temp, A);
	for(count = 0; tok != NULL; count++) tok = strtok(NULL, A);

	delete [] temp;
	return count;
}

String String::tokenStrict(int tokNo, const String& Token) const
{
	bool found;
	int i;
	String Result, Test, Data;
	int start, end, count, startMark, endMark;

	if(length_val == 0) return "";

	Data = *this;

	//  Find the right token
	if(tokNo == 1) startMark = 0;
	else
	{
		count = 1;
		start = 0;
		i = 0;
		while(i < length_val && count < tokNo)
		{
			start = i;
			end = i + Token.length() - 1;
			Test = Data(start, end);
			if(Test == Token) 
			{
				count++;
				i += Token.length();
			}
			else i++;
		}
		if(i >= length_val) return "";
		startMark = start + Token.length();
	}

	//  Now find the end
	found = false;
	for(i = startMark; found == false && i < length_val; i++)
	{
		start = i;
		end = i + Token.length() - 1;
		Test = Data(start, end);
		if(Test == Token) found = true;
	}
	endMark = (i >= length_val) ? length_val-1 : start-1;

	if(endMark < startMark) Result = "";
	else Result = Data(startMark, endMark);

	return Result;
}
FP_TYPE String::toktofStrict(int i, const String& Token) const
{
	return tokenStrict(i, Token).stof();
}
int String::tokenNoStrict(const String& Token) const
{
	int i, start, end, count;
	String Test, Data;

	Data = *this;

	if(length_val == 0) return 0;

	count = 1;
	i = 0;
	while(i < length_val)
	{
		start = i;
		end = i+Token.length()-1;
		Test = Data(start, end);

		if(Test == Token) 
		{
			count++;
			i += Token.length();
		}
		else i++;
	}

	return count;
}

//  Overload for getting a range of characters
String String::operator()(int i, int j) const
{
	String Result;
	char* temp;
	int k;

	if(j < i || i < 0 || j >= length_val) 
		throw ErrorBall("Array out of bounds in String::operator() function\n");
	temp = new char[j-i+2];
	for(k = i; k <= j; k++)
	{
		temp[k-i] = pointer_val[k];
	}
	temp[k-i] = '\0';

	Result = temp;
	delete [] temp;
	return Result;
}

//  Operators for concatenation
String & String::operator+=(String const& A)
{
	resize(length_val+A.length());
	length_val += A.length();
	strcat(pointer_val, A.pointer_val);

	return *this;
}
String & String::operator+=(char* A)
{
	resize(length_val+(int)strlen(A));
	length_val += (int)strlen(A);
	strcat(pointer_val, A);

	return *this;
}
String & String::operator+=(char A)
{
	char temp[2];

	resize(length_val+1);
	length_val++;
	temp[0] = A;
	temp[1] = '\0';
	strcat(pointer_val, temp);

	return *this;
}
String & String::operator+=(int const i)
{
	int extra;
	char number[100];
	FunctionBall Func;

	extra = Func.intCharLength(i);
	resize(length_val+extra);
	length_val += extra;
	sprintf(number, "%d", i);
	strcat(pointer_val, number);

	return *this;
}
String & String::operator&(String const& A)
{
	resize(length_val+A.length());
	length_val += A.length();
	strcat(pointer_val, A.pointer_val);

	return *this;
}
String & String::operator&(char* A)
{
	resize(length_val+(int)strlen(A));
	length_val += (int)strlen(A);
	strcat(pointer_val, A);

	return *this;
}
String & String::operator&(char A)
{
	char temp[2];

	resize(length_val+1);
	length_val++;
	temp[0] = A;
	temp[1] = '\0';
	strcat(pointer_val, temp);

	return *this;
}
String & String::operator&(int const i)
{
	int extra;
	char number[100];
	FunctionBall Func;

	extra = Func.intCharLength(i);
	resize(length_val+extra);
	length_val += extra;
	sprintf(number, "%d", i);
	strcat(pointer_val, number);

	return *this;
}
String& String::operator&(const FP_TYPE data)
{
	FunctionBall Func;
	char buffer[30];
	int i, extra;

	if(fabs(data) < ZERO) 
	{
		strcpy(buffer, "0.0");
	}
	else if(fabs(data) > 0.001 && fabs(data) < 1000) 
	{
		#if FP_PRECISION == FP_DOUBLE
			sprintf(buffer, "%.9lf", data);
		#else
			sprintf(buffer, "%.9f", data);
		#endif 

		//  Now get rid of trailing zeros
		for(i = (int)strlen(buffer)-1; buffer[i] == '0'; i--);
		if(buffer[i] != '.') i++;
		buffer[i] = '\0';
	}
	else 
	{
		#if FP_PRECISION == FP_DOUBLE
			sprintf(buffer, "%.8lE", data);
		#else
			sprintf(buffer, "%.8E", data);
		#endif 
	}

	extra = (int)strlen(buffer);
	resize(length_val+extra);
	length_val += extra;
	strcat(pointer_val, buffer);
	
	return *this;
}

String String::operator+(const String& A) const
{
	String Result;
	char* temp;

	temp = new char[length_val+A.length()+1];
	strcpy(temp, pointer_val);
	strcat(temp, A.pointer_val);

	Result = temp;
	delete [] temp;
	return Result;
}
String String::operator+(const char *A) const
{
	String Result;
	char* temp;

	temp = new char[length_val+(int)strlen(A)+1];
	strcpy(temp, pointer_val);
	strcat(temp, A);

	Result = temp;
	delete [] temp;
	return Result;
}
String String::operator+(const char A) const
{
	String Result;
	char* temp;
	char str[2];

	str[0] = A;
	str[1] = '\0';

	temp = new char[length_val+2];
	strcpy(temp, pointer_val);
	strcat(temp, str);

	Result = temp;
	delete [] temp;
	return Result;
}
String String::operator+(const int i) const
{
	String Result;
	char* temp;
	int numberLength;
	char number[100];
	FunctionBall Func;

	numberLength = Func.intCharLength(i);
	temp = new char[length_val+numberLength];
	strcpy(temp, pointer_val);
	sprintf(number, "%d", i);
	strcat(temp, number);

	Result = temp;
	delete [] temp;
	return Result;
}
//  Function for converting one thing into another in a string
void String::translate(const String& Reference, const String& NewText)
{
	int start, end;
	int step;
	String Result;
	String Test;
	int newSize;

	//  Do a test to see if the reference is even in the string
	if(*this %= Reference)
	{
		Result = "";
		step = Reference.length();
		newSize = 0;
		start = 0;
		end = step-1;

		while(end < length_val)
		{
			Test = this->operator()(start, end);
			if(Test == Reference)
			{
				Result += NewText;
				start += step;
				end += step;
			}
			else 
			{
				Result += pointer_val[start];
				start++;
				end++;
			}
		}
		if(step > 1 && end <= length_val) Result += this->operator()(start, end-1);
		*this = Result;
	}
}

/*
void String::translate(char* reference, char* newText)
{
	int start, end;
	int step;
	String Result;
	String Test;
	int newSize;

	//  Do a test to see if the reference is even in the string
	if(*this %= reference)
	{
		Result = "";
		step = (int)strlen(reference);
		newSize = 0;
		start = 0;
		end = step-1;

		while(end < length_val)
		{
			Test = this->operator()(start, end);
			if(Test == reference)
			{
				Result += newText;
				start += step;
				end += step;
			}
			else 
			{
				Result += pointer_val[start];
				start++;
				end++;
			}
		}
		if(step > 1 && end <= length_val) Result += this->operator()(start, end-1);
		*this = Result;
	}
}
*/

//  Some functions to determine the type of thing stored in the string
bool String::isInt()
{
	bool result;
	int i;

	//  Read past the whitespace
	for(i = 0; pointer_val[i] == ' ' || pointer_val[i] == '\t'; i++);
	
	//  Now check for a minus sign
	if(pointer_val[i] == '-') i++;

	//  Now test that all the characters are digits
	result = true;
	for(; i < length_val && result == true; i++)
	{
		if(!isdigit(pointer_val[i])) result = false;
	}

	return result;
}
bool String::isAlpha()
{
	bool result;
	int i;

	//  Read past the whitespace
	for(i = 0; pointer_val[i] == ' ' || pointer_val[i] == '\t'; i++);

	//  Now test that all the characters are alphabetic
	result = true;
	for(; i < length_val && result == true; i++)
	{
		if(!isalpha(pointer_val[i])) result = false;
	}

	return result;
}
bool String::isFloat()
{
	bool result;
	int i;

	result = false;

	//  Read past the whitespace
	for(i = 0; pointer_val[i] == ' ' || pointer_val[i] == '\t'; i++);

	//  Now check for a minus sign
	if(pointer_val[i] == '-') i++;

	//  Now read through the starting digits
	for(; isdigit(pointer_val[i]); i++);

	//  Now for each ending case
	if(pointer_val[i] == '\0') result = true;
	else if(pointer_val[i] == '.')
	{
		i++;
		for(; isdigit(pointer_val[i]); i++);
		if(pointer_val[i] == '\0') result = true;
		else if(pointer_val[i] == 'E' || pointer_val[i] == 'e')
		{
			i++;
			if(pointer_val[i] == '+' || pointer_val[i] == '-') i++;
			for(; isdigit(pointer_val[i]); i++);
			if(pointer_val[i] == '\0') result = true;
			else result = false;
		}
	}
	else if(pointer_val[i] == 'E' || pointer_val[i] == 'e')
	{
		i++;
		if(pointer_val[i] == '+' || pointer_val[i] == '-') i++;
		for(; isdigit(pointer_val[i]); i++);
		if(pointer_val[i] == '\0') result = true;
		else result = false;
	}
	else result = false;

	return result;
}

//  Functions for converting to numbers
int String::stoi() const
{
	if(length_val > 0) return atoi(pointer_val);
	else return 0;
} 
FP_TYPE String::stof() const
{
	char* place;

	if(length_val > 0) 
	{
		place = strstr(pointer_val, "D");
		while(place != NULL)
		{
			place[0] = 'E';
			place = strstr(pointer_val, "D");
		}
		return (FP_TYPE)atof(pointer_val);
	}
	else return 0;
} 

//  Functions for chaging cases
String String::toUpper() const
{
	int i;
	String Result;

	Result = *this;
	for(i = 0; i < length_val; i++) Result[i] = (char)toupper(pointer_val[i]);
	return Result;
}
String String::toLower() const
{
	int i;
	String Result;

	Result = *this;
	for(i = 0; i < length_val; i++) Result[i] = (char)tolower(pointer_val[i]);
	return Result;
}

//  Definitions for reading and writing strings
ostream & operator<<(ostream &output, String const& A)
{
	output << A.pointer();
	return output;
}
istream & operator>>(istream &input, String& A)
{
	char temp[1000];

	input.getline(temp, 1000);
	A = (char*)temp;
	return input;
}

