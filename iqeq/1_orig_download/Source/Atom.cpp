
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Atom.h"
#include "ErrorBall.h"
#include "Calculus.h"
#include "Array1D.h"
#include "FunctionBall.h"

//  The constructors
Atom::Atom()
{
	null();
}
Atom::Atom(Atom const& A)
{
	*this = A;
}
void Atom::null()									
{
	Name_val = "";
	Symbol_val = "";
	type_val = -1;
	FFType_val = "";
	mass_val = 0;		
	spin_val = 0;
	charge_val = 0;
	Position_val.zero();
	Force_val.zero();
	Velocity_val.zero();
	friction_val = 0;
	frozen_val = false;
	ghost_val = false;
	shell_val = 0;
	valenceElec_val = 0;
	valenceBond_val = 0;
	hybrid_val = ATOM_HYBRID_NONE;
	ringSize_val = 0;
	aromatic_val = false;
	ringBridge_val = false;
	minCharge_val = 0;
	maxCharge_val = 0;
	vdwRadius_val = 0;
	covalentRadius_val = 0;
	slaterRadius_val = 0;
	surfaceRadius_val = 0;
	electroNeg_val = 0;
	hardness_val = 0;
	slaterScreen_val = 0;
	gammaScreen_val = 1;
	formalCharge_val = 0;
	formalSpin_val = 0;
}

//  Readers for the type, position and force on the atom
const String& Atom::Name() const						{ return Name_val; }
const String& Atom::Symbol() const						{ return Symbol_val; }
int Atom::type() const									{ return type_val; }
const String& Atom::FFType() const						{ return FFType_val; }
FP_TYPE Atom::mass() const								{ return mass_val; }
FP_TYPE Atom::charge() const							{ return charge_val;}
FP_TYPE Atom::spin() const								{ return spin_val; }
const Vector& Atom::Position() const					{ return Position_val; }
const Vector& Atom::Force() const						{ return Force_val; }
const Vector& Atom::Velocity() const					{ return Velocity_val; }
FP_TYPE Atom::friction() const							{ return friction_val; }
bool Atom::frozen() const								{ return frozen_val;}
bool Atom::ghost() const								{ return ghost_val;}
int Atom::shell() const									{ return shell_val; }
int Atom::valenceElec() const							{ return valenceElec_val; }
int Atom::valenceBond() const							{ return valenceBond_val; }
int Atom::hybrid() const								{ return hybrid_val; }
int Atom::ringSize() const								{ return ringSize_val; }
bool Atom::isRing() const								{ return ringSize_val > 0 ? true : false; }
bool Atom::aromatic() const								{ return aromatic_val; }
bool Atom::ringBridge() const							{ return ringBridge_val; }
int Atom::minCharge() const								{ return minCharge_val; }
int Atom::maxCharge() const								{ return maxCharge_val; }
FP_TYPE Atom::vdwRadius() const							{ return vdwRadius_val;}
FP_TYPE Atom::covalentRadius() const					{ return covalentRadius_val;}
FP_TYPE Atom::slaterRadius() const						{ return slaterRadius_val; }
FP_TYPE Atom::surfaceRadius() const						{ return surfaceRadius_val; }
FP_TYPE Atom::electroNeg() const						{ return electroNeg_val;}
FP_TYPE Atom::hardness() const							{ return hardness_val;}
FP_TYPE Atom::slaterScreen() const						{ return slaterScreen_val;}
FP_TYPE Atom::gammaScreen() const						{ return gammaScreen_val;}
FP_TYPE Atom::formalCharge() const						{ return formalCharge_val; }
int Atom::formalSpin() const							{ return formalSpin_val; }

//  Some writer functions
void Atom::setName(String const& Name)				{ Name_val = Name; }
void Atom::setSymbol(String const& Symbol)			{ Symbol_val = Symbol; }
void Atom::setType(int newfloat)						{ type_val = newfloat; }
void Atom::setFFType(String const& newFFType)		{ FFType_val = newFFType; }
void Atom::setMass(FP_TYPE newMass)						{ mass_val = newMass; }
void Atom::setCharge(FP_TYPE newCharge)					{ charge_val = newCharge; }
void Atom::setSpin(FP_TYPE newSpin)						{ spin_val = newSpin; }
void Atom::setPosition(Vector const& NewPosition)		{ Position_val = NewPosition; }
void Atom::setForce(Vector const& NewForce)				{ Force_val = NewForce; }
void Atom::setVelocity(const Vector& NewVelocity)		{ Velocity_val = NewVelocity; }
void Atom::setFriction(FP_TYPE newFriction)				{ friction_val = newFriction; }
void Atom::setFrozen(bool freeze)						{ frozen_val = freeze;}
void Atom::setGhost(bool freeze)						{ ghost_val = freeze;}
void Atom::setShell(int shell)							{ shell_val = shell; }
void Atom::setValenceElec(int elec)						{ valenceElec_val = elec; }
void Atom::setValenceBond(int bond)						{ valenceBond_val = bond; }
void Atom::setHybrid(int hybrid)						{ hybrid_val = hybrid; }
void Atom::setRingSize(int size)						{ ringSize_val = size; }
void Atom::setAromatic(bool data)						{ aromatic_val = data; }
void Atom::setRingBridge(bool data)						{ ringBridge_val = data; }
void Atom::setMinCharge(int charge)						{ minCharge_val = charge; }
void Atom::setMaxCharge(int charge)						{ maxCharge_val = charge; }
void Atom::setVdwRadius(FP_TYPE value)					{ vdwRadius_val = value; }
void Atom::setCovalentRadius(FP_TYPE value)				{ covalentRadius_val = value; }
void Atom::setSlaterRadius(FP_TYPE value)				{ slaterRadius_val = value; }
void Atom::setSurfaceRadius(FP_TYPE value)				{ surfaceRadius_val = value; }
void Atom::setElectroNeg(FP_TYPE value)					{ electroNeg_val = value; }
void Atom::setHardness(FP_TYPE value)					{ hardness_val = value; }
void Atom::setSlaterScreen(FP_TYPE value)				{ slaterScreen_val = value; }
void Atom::setGammaScreen(FP_TYPE value)				{ gammaScreen_val = value; }
void Atom::setFormalCharge(FP_TYPE value)				{ formalCharge_val = value; }
void Atom::setFormalSpin(int value)						{ formalSpin_val = value; }

//  The comparison operators
//  Note that the =~ is translated as "roughly equal", that is the types are the same
bool Atom::operator==(Atom const& A)
{
	bool flag;

	flag = true;
	if(Name_val != A.Name()) flag = false;
	if(type_val != A.type()) flag = false;
	if(FFType_val != A.FFType()) flag = false;
	if(fabs(mass_val-A.mass()) > ZERO) flag = false;
	if(fabs(charge_val-A.charge()) > ZERO) flag = false;
	if(Position_val != A.Position()) flag = false;
	if(Force_val != A.Force()) flag = false;
	if(Velocity_val != A.Velocity()) flag = false;

	return flag;
}

bool Atom::operator!=(Atom const& A)
{
	bool flag;

	flag = false;
	if(Name_val != A.Name()) flag = true;
	if(type_val != A.type()) flag = true;
	if(FFType_val != A.FFType()) flag = true;
	if(fabs(mass_val-A.mass()) > ZERO) flag = true;
	if(fabs(charge_val-A.charge()) > ZERO) flag = true;
	if(Position_val != A.Position()) flag = true;
	if(Force_val != A.Force()) flag = true;
	if(Velocity_val != A.Velocity()) flag = true;

	return flag;
}
bool Atom::operator%=(Atom const& A)
{
	bool flag;

	flag = true;
	if(Name_val != A.Name()) flag = false;
	if(type_val != A.type()) flag = false;
	if(fabs(mass_val-A.mass()) > ZERO) flag = false;

	return flag;
}
bool Atom::operator>(const Atom& A)
{
	return (type_val > A.type()) ? true : false;
}
bool Atom::operator>=(const Atom& A)
{
	return (type_val >= A.type()) ? true : false;
}
bool Atom::operator<(const Atom& A)
{
	return (type_val < A.type()) ? true : false;
}
bool Atom::operator<=(const Atom& A)
{
	return (type_val <= A.type()) ? true : false;
}

//  The assignment operator
Atom & Atom::operator=(Atom const& A)
{
	Name_val = A.Name();
	Symbol_val = A.Symbol();
	type_val = A.type();
	FFType_val = A.FFType();
	mass_val = A.mass();		
	charge_val = A.charge();
	spin_val = A.spin();
	Position_val = A.Position();
	Force_val = A.Force();
	Velocity_val = A.Velocity();
	friction_val = A.friction();
	shell_val = A.shell();
	valenceElec_val = A.valenceElec();
	valenceBond_val = A.valenceBond();
	hybrid_val = A.hybrid();
	ringSize_val = A.ringSize();
	aromatic_val = A.aromatic();
	ringBridge_val = A.ringBridge();
	minCharge_val = A.minCharge();
	maxCharge_val = A.maxCharge();
	vdwRadius_val = A.vdwRadius();
	covalentRadius_val = A.covalentRadius();
	slaterRadius_val = A.slaterRadius();
	surfaceRadius_val = A.surfaceRadius();
	electroNeg_val = A.electroNeg();
	hardness_val = A.hardness();
	slaterScreen_val = A.slaterScreen();
	gammaScreen_val = A.gammaScreen();
	formalCharge_val = A.formalCharge();
	formalSpin_val = A.formalSpin();
	frozen_val = A.frozen();
	ghost_val = A.ghost();

	return *this;
}
int Atom::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in Atom::operator=(int i)");
	null();

	return i;
}

//  Operator overloads for translation with vectors and adding forces
Atom & Atom::operator+=(Vector const& A)
{
	Position_val += A;
	return *this;
}
Atom & Atom::operator-=(Vector const& A)
{
	Position_val -= A;
	return *this;
}
void Atom::addForce(Vector const& Add)
{
	Force_val += Add;
}
void Atom::subForce(Vector const& Sub)
{
	Force_val -= Sub;
}
void Atom::addVelocity(Vector const& Add)
{
	Velocity_val += Add;
}
void Atom::subVelocity(Vector const& Sub)
{
	Velocity_val -= Sub;
}
void Atom::addFriction(FP_TYPE add)				
{ 
	friction_val += add; 
}

//  A function to spin the atom, which just pretty much calls the vector functions
void Atom::spin(Vector const& Axis, FP_TYPE angle)
{
	Position_val = Position_val.spin(Axis, angle);
}
void Atom::spin(Vector const& Axis, Vector const& Center, FP_TYPE angle)
{
	Position_val = Position_val.spin(Axis, Center, angle);
}
void Atom::eulerSpin(FP_TYPE phi, FP_TYPE theta, FP_TYPE psi)
{
	Position_val = Position_val.eulerSpin(phi, theta, psi);
}
void Atom::eulerSpin(FP_TYPE phi, FP_TYPE theta, FP_TYPE psi, const Vector& Center)
{
	Position_val = Position_val.eulerSpin(phi, theta, psi, Center);
}

int Atom::periodicTableRow()
{
	if(type_val < ATOM_HYDROGEN) return 0;
	else if(type_val <= ATOM_HELIUM) return 1;
	else if(type_val <= ATOM_NEON) return 2;
	else if(type_val <= ATOM_ARGON) return 3;
	else if(type_val <= ATOM_KRYPTON) return 4;
	else if(type_val <= ATOM_XENON) return 5;
	else if(type_val <= ATOM_RADON) return 6;
	else return 7;
}
int Atom::periodicTableColumn()
{
	switch(type_val)
	{
		case ATOM_HYDROGEN:
		case ATOM_LITHIUM:
		case ATOM_SODIUM:
		case ATOM_POTASSIUM:
		case ATOM_RUBIDIUM:
		case ATOM_CESIUM:
		case ATOM_FRANCIUM:
		{
			return 1;
		}

		case ATOM_BERYLLIUM:
		case ATOM_MAGNESIUM:
		case ATOM_CALCIUM:
		case ATOM_STRONTIUM:
		case ATOM_BARIUM:
		case ATOM_RADIUM:
		{
			return 2;
		}

		case ATOM_SCANDIUM:
		case ATOM_YTTRIUM:
		case ATOM_LUTETIUM:
		case ATOM_LAWRENCIUM:
		{
			return 3;
		}

		case ATOM_TITANIUM:
		case ATOM_ZIRCONIUM:
		case ATOM_HAFNIUM:
		case ATOM_RUTHERFORDIUM:
		{
			return 4;
		}

		case ATOM_VANADIUM:
		case ATOM_NIOBIUM:
		case ATOM_TANTALUM:
		{
			return 5;
		}
		
		case ATOM_CHROMIUM:
		case ATOM_MOLYBDENUM:
		case ATOM_TUNGSTEN:
		{
			return 6;
		}

		case ATOM_MANGANESE:
		case ATOM_TECHNETIUM:
		case ATOM_RHENIUM:
		{
			return 7;
		}
		
		case ATOM_IRON:
		case ATOM_RUTHENIUM:
		case ATOM_OSMIUM:
		{
			return 8;
		}
		
		case ATOM_COBALT:
		case ATOM_RHODIUM:
		case ATOM_IRIDIUM:
		{
			return 9;
		}
		
		case ATOM_NICKEL:
		case ATOM_PALLADIUM:
		case ATOM_PLATINUM:
		{
			return 10;
		}
		
		case ATOM_COPPER:
		case ATOM_SILVER:
		case ATOM_GOLD:
		{
			return 11;
		}
		
		case ATOM_ZINC:
		case ATOM_CADMIUM:
		case ATOM_MERCURY:
		{
			return 12;
		}

		case ATOM_BORON:
		case ATOM_ALUMINIUM:
		case ATOM_GALLIUM:
		case ATOM_INDIUM:
		case ATOM_THALLIUM:
		{
			return 13;
		}

		case ATOM_CARBON:
		case ATOM_SILICON:
		case ATOM_GERMANIUM:
		case ATOM_TIN:
		case ATOM_LEAD:
		{
			return 14;
		}

		case ATOM_NITROGEN:
		case ATOM_PHOSPHORUS:
		case ATOM_ARSENIC:
		case ATOM_ANTIMONY:
		case ATOM_BISMUTH:
		{
			return 15;
		}

		case ATOM_OXYGEN:
		case ATOM_SULFUR:
		case ATOM_SELENIUM:
		case ATOM_TELLURIUM:
		case ATOM_POLONIUM:
		{
			return 16;
		}

		case ATOM_FLUORINE:
		case ATOM_CHLORINE:
		case ATOM_BROMINE:
		case ATOM_IODINE:
		case ATOM_ASTATINE:
		{
			return 17;
		}

		case ATOM_NEON:
		case ATOM_ARGON:
		case ATOM_KRYPTON:
		case ATOM_XENON:
		case ATOM_RADON:
		{
			return 18;
		}

		case ATOM_LANTHANUM:
		case ATOM_ACTINIUM:
		{
			return 19;
		}
		case ATOM_CERIUM:
		case ATOM_THORIUM:
		{
			return 20;
		}
		case ATOM_PRASEODYMIUM:
		case ATOM_PROTACTINIUM:
		{
			return 21;
		}
		case ATOM_NEODYMIUM:
		case ATOM_URANIUM:
		{
			return 22;
		}
		case ATOM_PROMETHIUM:
		case ATOM_NEPTUNIUM:
		{
			return 23;
		}
		case ATOM_SAMARIUM:
		case ATOM_PLUTONIUM:
		{
			return 24;
		}
		case ATOM_EUROPIUM:
		case ATOM_AMERICIUM:
		{
			return 25;
		}
		case ATOM_GADOLINIUM:
		case ATOM_CURIUM:
		{
			return 26;
		}
		case ATOM_TERBIUM:
		case ATOM_BERKELIUM:
		{
			return 27;
		}
		case ATOM_DYSPROSIUM:
		case ATOM_CALIFORNIUM:
		{
			return 28;
		}
		case ATOM_HOLMIUM:
		case ATOM_EINSTEINIUM:
		{
			return 29;
		}
		case ATOM_ERBIUM:
		case ATOM_FERMIUM:
		{
			return 30;
		}
		case ATOM_THULIUM:
		case ATOM_MENDELEVIUM:
		{
			return 31;
		}
		case ATOM_YTTERBIUM:
		case ATOM_NOBELIUM:
		{
			return 32;
		}
		default:
		{
			return 0;
		}
	}
}
int Atom::perodicTableGroup()
{
	int column;

	column = periodicTableColumn();

	if(column <= 2) return PERIODIC_TABLE_GROUP_S;
	else if(column >= 13 && column <= 18) return PERIODIC_TABLE_GROUP_P;
	else if(column >= 3 && column <= 12) return PERIODIC_TABLE_GROUP_D;
	else if(column > 18) return PERIODIC_TABLE_GROUP_F;
	else return PERIODIC_TABLE_GROUP_NONE;
}

bool Atom::isOrganicCovalent() const
{
	switch(type_val)
	{
		case ATOM_HYDROGEN:
		case ATOM_BORON:
		case ATOM_CARBON:
		case ATOM_SILICON:
		case ATOM_NITROGEN:
		case ATOM_PHOSPHORUS:
		case ATOM_ARSENIC:
		case ATOM_OXYGEN:
		case ATOM_SULFUR:
		case ATOM_SELENIUM:
		case ATOM_TELLURIUM:
		case ATOM_FLUORINE:
		case ATOM_CHLORINE:
		case ATOM_BROMINE:
		case ATOM_IODINE:
		case ATOM_ASTATINE:
		{
			return true;
		}
		default:
		{
			return false;
		}
	}

	return false;
}
bool Atom::isMetal() const
{
	if(type_val >= ATOM_LITHIUM && type_val <= ATOM_BERYLLIUM) return true;
	else if(type_val >= ATOM_SODIUM && type_val <= ATOM_ALUMINIUM) return true;
	else if(type_val >= ATOM_POTASSIUM && type_val <= ATOM_ZINC) return true;
	else if(type_val >= ATOM_RUBIDIUM && type_val <= ATOM_TIN) return true;
	else if(type_val >= ATOM_CESIUM && type_val <= ATOM_POLONIUM) return true;
	else if(type_val >= ATOM_FRANCIUM && type_val <= ATOM_RUTHERFORDIUM) return true;
	else return false;
}
int Atom::loneFormalCharge() const
{
	switch(type_val)
	{
		case ATOM_DUMMY:			return 0;
		case ATOM_HYDROGEN:			return 1;
		case ATOM_HELIUM:			return 0;
		case ATOM_LITHIUM:			return 1;
		case ATOM_BERYLLIUM:		return 2;
		case ATOM_BORON:			return 3;
		case ATOM_CARBON:			return -4;
		case ATOM_NITROGEN:			return -3;
		case ATOM_OXYGEN:			return -2;
		case ATOM_FLUORINE:			return -1;
		case ATOM_NEON:				return 0;
		case ATOM_SODIUM:			return 1;
		case ATOM_MAGNESIUM:		return 2;
		case ATOM_ALUMINIUM:		return 3;
		case ATOM_SILICON:			return -4;
		case ATOM_PHOSPHORUS:		return -3;
		case ATOM_SULFUR:			return -2;
		case ATOM_CHLORINE:			return -1;
		case ATOM_ARGON:			return 0;
		case ATOM_POTASSIUM:		return 1;
		case ATOM_CALCIUM:			return 2;
		case ATOM_SCANDIUM:			return 3;
		case ATOM_TITANIUM:			return 4;
		case ATOM_VANADIUM:			return 4;
		case ATOM_CHROMIUM:			return 3;
		case ATOM_MANGANESE:		return 2;
		case ATOM_IRON:				return 2;
		case ATOM_COBALT:			return 2;
		case ATOM_NICKEL:			return 2;
		case ATOM_COPPER:			return 2;
		case ATOM_ZINC:				return 2;
		case ATOM_GALLIUM:			return 3;
		case ATOM_GERMANIUM:		return 4;
		case ATOM_ARSENIC:			return -3;
		case ATOM_SELENIUM:			return -2;
		case ATOM_BROMINE:			return -1;
		case ATOM_KRYPTON:			return 0;
		case ATOM_RUBIDIUM:			return 1;
		case ATOM_STRONTIUM:		return 2;
		case ATOM_YTTRIUM:			return 3;
		case ATOM_ZIRCONIUM:		return 4;
		case ATOM_NIOBIUM:			return 4;
		case ATOM_MOLYBDENUM:		return 4;
		case ATOM_TECHNETIUM:		return 4;
		case ATOM_RUTHENIUM:		return 3;
		case ATOM_RHODIUM:			return 3;
		case ATOM_PALLADIUM:		return 2;
		case ATOM_SILVER:			return 1;
		case ATOM_CADMIUM:			return 2;
		case ATOM_INDIUM:			return 3;
		case ATOM_TIN:				return 4;
		case ATOM_ANTIMONY:			return 0;
		case ATOM_TELLURIUM:		return -2;
		case ATOM_IODINE:			return -1;
		case ATOM_XENON:			return 0;
		case ATOM_CESIUM:			return 1;
		case ATOM_BARIUM:			return 2;
		case ATOM_LANTHANUM:		return 3;
		case ATOM_CERIUM:			return 3;
		case ATOM_PRASEODYMIUM:		return 3;
		case ATOM_NEODYMIUM:		return 3;
		case ATOM_PROMETHIUM:		return 3;
		case ATOM_SAMARIUM:			return 3;
		case ATOM_EUROPIUM:			return 3;
		case ATOM_GADOLINIUM:		return 3;
		case ATOM_TERBIUM:			return 3;
		case ATOM_DYSPROSIUM:		return 3;
		case ATOM_HOLMIUM:			return 3;
		case ATOM_ERBIUM:			return 3;
		case ATOM_THULIUM:			return 3;
		case ATOM_YTTERBIUM:		return 3;
		case ATOM_LUTETIUM:			return 3;
		case ATOM_HAFNIUM:			return 4;
		case ATOM_TANTALUM:			return 4;
		case ATOM_TUNGSTEN:			return 4;
		case ATOM_RHENIUM:			return 4;
		case ATOM_OSMIUM:			return 3;
		case ATOM_IRIDIUM:			return 3;
		case ATOM_PLATINUM:			return 4;
		case ATOM_GOLD:				return 3;
		case ATOM_MERCURY:			return 2;
		case ATOM_THALLIUM:			return 1;
		case ATOM_LEAD:				return 2;
		case ATOM_BISMUTH:			return 0;
		case ATOM_POLONIUM:			return 0;
		case ATOM_ASTATINE:			return -1;
		case ATOM_RADON:			return 0;
		case ATOM_FRANCIUM:			return 1;
		case ATOM_RADIUM:			return 2;
		case ATOM_ACTINIUM:			return 3;
		case ATOM_THORIUM:			return 3;
		case ATOM_PROTACTINIUM:		return 3;
		case ATOM_URANIUM:			return 3;
		case ATOM_NEPTUNIUM:		return 3;
		case ATOM_PLUTONIUM:		return 3;
		case ATOM_AMERICIUM:		return 3;
		case ATOM_CURIUM:			return 3;
		case ATOM_BERKELIUM:		return 3;
		case ATOM_CALIFORNIUM:		return 3;
		case ATOM_EINSTEINIUM:		return 3;
		case ATOM_FERMIUM:			return 3;
		case ATOM_MENDELEVIUM:		return 3;
		case ATOM_NOBELIUM:			return 3;
		case ATOM_LAWRENCIUM:		return 3;
		case ATOM_RUTHERFORDIUM:	return 4;
	}

	return 0;
}

//  An overload to output the details of the atom
ostream & operator<<(ostream &output, const Atom& A)
{
	output.precision(4);
	output.setf(ios::showpoint | ios::fixed | ios::left);

	output << std::setw(4) << A.Name();
	output << std::setw(4) << A.type();
	output << std::setw(10) << A.mass();
	output << std::setw(8) << A.charge();
	output << std::setw(8) << (A.Position()).x() << setw(8) << (A.Position()).y() << setw(8) << (A.Position()).z();
	output << endl;

	return output;
}


