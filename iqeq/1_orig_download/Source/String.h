
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _MYSTRING_H
#define _MYSTRING_H

#include "definitions.h"

#include <iostream>
using namespace std;

class String
{
	friend class MPICommBall;

	protected:
		char* pointer_val;
		int length_val;

		void resize(int);

	public:
		
		//  The constructors and destructors
		String();
		String(String const&);
		String(const char*);
		~String();
		void null();

		//  Function for getting at the memory address 
		char* pointer() const;

		//  A reader for getting a character out of it
		char& operator[](int) const;

		//  Function for getting the length of the string
		int length() const;

		//  Assignment operator
		String& operator=(String const&);
		String& operator=(const char*);
		String& operator=(const char);
		int operator=(int);
		String& operator<<(String const&);
		String& operator<<(const char*);
		String& operator<<(const int);
		String& operator<<(const FP_TYPE);

		//  Functions for converting a float to a string, based on basic criteria
		void setFloat(FP_TYPE);
		void setFloat(int, FP_TYPE);
		void setFloat(int, const Vector&);
		void setFloatScientific(int, FP_TYPE);
		void setFloatScientific(int, const Vector&);

		//  Comparison operators
		bool operator==(String const&) const;
		bool operator==(char*) const;
		bool operator!=(String const&) const;
		bool operator!=(char*) const;

		//  Operator for the empty string
		bool operator!();

		//  Operator for the contains
		bool operator%=(String const&) const;
		bool operator%=(char*) const;

		//  Operator for comparisons
		bool operator>(String const&) const;
		bool operator>(char*) const;
		bool operator<(String const&) const;
		bool operator<(char*) const;

		//  Functions for getting sub bits of strings
		String token(int) const;
		String token(int, String const&) const;
		String token(int, char*) const;
		FP_TYPE toktof(int) const;
		FP_TYPE toktof(int, String const&) const;
		FP_TYPE toktof(int, char*) const;
		int tokenNo() const;
		int tokenNo(String const&) const;
		int tokenNo(char*) const;

		//  Functions for getting sub bits of strings
		String tokenStrict(int, const String&) const;
		FP_TYPE toktofStrict(int, const String&) const;
		int tokenNoStrict(const String&) const;

		//  Overload for getting a range of characters
		String operator()(int, int) const;

		//  Operators for concatenation
		String& operator+=(String const&);
		String& operator+=(char*);
		String& operator+=(char);
		String& operator+=(int);
		String& operator&(String const&);
		String& operator&(char*);
		String& operator&(char);
		String& operator&(int);
		String& operator&(FP_TYPE);
		String operator+(const String&) const;
		String operator+(const char*) const;
		String operator+(const char) const;
		String operator+(const int) const;

		//  Function for converting one thing into another in a string
		//void translate(char*, char*);
		void translate(const String&, const String&);

		//  Some functions to determine the type of thing stored in the string
		bool isInt();
		bool isAlpha();
		bool isFloat();

		//  Functions for converting to numbers
		int stoi() const;
		FP_TYPE stof() const;

		//  Functions for changing case
		String toUpper() const;
		String toLower() const;

		//  Overloads for putting in and out strings
		friend ostream & operator<<(ostream&, const String&);
		friend istream & operator>>(istream&, String&);
};

#endif


