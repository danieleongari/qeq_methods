
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _FUNCTIONBALL_H
#define _FUNCTIONBALL_H

#include "definitions.h"
#include "Array1D.h"
#include "Array2D.h"
#include "Array3D.h"

#include <cmath>

#include <mkl.h>

using namespace std;

#define FUNCTIONBALL_CUBIC_SPLINE_FAST 0
#define FUNCTIONBALL_CUBIC_SPLINE_NATURAL 1
#define FUNCTIONBALL_CUBIC_SPLINE_QUAD_RUNOFF 2
#define FUNCTIONBALL_CUBIC_SPLINE_CUBIC_RUNOFF 3

#define FUNCTIONBALL_RANDOM_NUMBER_GENERATOR VSL_BRNG_MRG32K3A
#define FUNCTIONBALL_RANDOM_NUMBER_UNIFORM VSL_RNG_METHOD_UNIFORM_STD
#define FUNCTIONBALL_RANDOM_NUMBER_NORMAL VSL_RNG_METHOD_GAUSSIAN_ICDF

class FunctionBall 
{
	private:
		static unsigned long long random_u, random_v, random_w;
		static VSLStreamStatePtr randomStream_val;
		int progress_val;

	public:
		FunctionBall();
		FunctionBall(const FunctionBall&);
		const FunctionBall& operator=(const FunctionBall&);
		int operator=(int);
		void null();

		FP_TYPE factoral(int) const;
		FP_TYPE logFactoral(int) const;
		FP_TYPE gamma(int) const;
		FP_TYPE halfGamma(int) const;
		FP_TYPE halfUpperIncompleteGamma(int, FP_TYPE) const;
		FP_TYPE binomial(int, int) const;
		template<class T> T minValue(const T& a, const T& b)			{ return (a < b) ? a : b; }
		int signPower(int);
		FP_TYPE round(FP_TYPE);
		FP_TYPE erfc(FP_TYPE) const;
		FP_TYPE expIntEn(int, FP_TYPE) const;
		int intCharLength(int);
		FP_TYPE polyInterpolate(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		Column polyFactors(const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineInterpolate(int, FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineFirstDerivative(int, FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineSecondDerivative(int, FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineInterpolate(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineFirstDerivative(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE cubicSplineSecondDerivative(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		Array1D<FP_TYPE> cubicSplineDeriv(int, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
};


#endif



