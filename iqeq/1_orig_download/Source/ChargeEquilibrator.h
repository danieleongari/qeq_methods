/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _CHARGEEQUILIBRATOR_H
#define _CHARGEEQUILIBRATOR_H

#include "definitions.h"
#include "Array1D.h"
#include "Array2D.h"
#include "Column.h"
#include "Vector.h"
#include "EwaldPotential.h"

#define CHARGEEQUILIBRATOR_FR (FP_TYPE)1.2
#define CHARGEEQUILIBRATOR_KLONDIKE (FP_TYPE)0.4
#define CHARGEEQUILIBRATOR_MIX 0.3

class ChargeEquilibrator
{
	friend class MPICommBall;

	protected:
		int size_val;
		Column Q_val, BaseQ_val, ElectroNeg_val, Chi_val, D_val;
		Matrix PenetrateJ_val, InteractionJ_val, J_val, C_val;
		Array1D<Vector> RealTrans_val;
		Array1D<KVector> FourierTrans_val;
		Array2D<Vector> Rij_val;
		Array2D<FP_TYPE> AtomIonEnergy_val;
		Array1D<int> AtomIonEnergyBase_val;
		Array2D<FP_TYPE> AtomIonSize_val;
		Array1D<int> AtomIonSizeBase_val;
		EwaldPotential Coulomb_val;

	public:
		ChargeEquilibrator();
		ChargeEquilibrator(const ChargeEquilibrator&);
		ChargeEquilibrator& operator=(const ChargeEquilibrator&);
		int operator=(int);
		void null();

		FP_TYPE charge(int) const;

		void setSize(const Geometry&);

		void initCoulombTerms(const Geometry&, const InputChargeFitBall&);
		void initAtomData(const Geometry&, const InputChargeFitBall&);

		FP_TYPE getPenetratePotential(int i, int j, const Geometry&, const InputChargeFitBall&);
		FP_TYPE getIdemPotential(int i, const Geometry&, const InputChargeFitBall&);

		FP_TYPE getSelfSlater(int, FP_TYPE) const;
		FP_TYPE getDistSlater(int, int, FP_TYPE, FP_TYPE, FP_TYPE) const;
		FP_TYPE slaterReducedOverlap(int, int, FP_TYPE, FP_TYPE) const;
		FP_TYPE slaterAFunc(int, FP_TYPE) const;
		FP_TYPE slaterBFunc(int, FP_TYPE) const;
		FP_TYPE slaterZFunc(int, int, int) const;
		FP_TYPE getDistGaussian(FP_TYPE, FP_TYPE, FP_TYPE) const;

		bool calculateIntegral(int i, int j, const Geometry&, const InputChargeFitBall&);
		FP_TYPE getIntegral(int i, int j, FP_TYPE rij, const Geometry& Geom, const InputChargeFitBall&);

		FP_TYPE atomIonEnergy(int, int) const;
		FP_TYPE atomIonEnergy(int, FP_TYPE) const;
		FP_TYPE atomIonSize(int, int) const;
		FP_TYPE atomIonSize(int, FP_TYPE) const;
		FP_TYPE atomIonFirstGrad(int, int) const;
		FP_TYPE atomIonFirstGrad(int, FP_TYPE) const;
		FP_TYPE atomIonSecondGrad(int, int) const;
		FP_TYPE atomIonSecondGrad(int, FP_TYPE) const;

		FP_TYPE atomIonEnergyPoly(int, FP_TYPE) const;
		FP_TYPE atomIonFirstGradPoly(int, FP_TYPE) const;
		FP_TYPE atomIonSecondGradPoly(int, FP_TYPE) const;
		FP_TYPE atomIonEnergySpline(int, FP_TYPE) const;
		FP_TYPE atomIonFirstGradSpline(int, FP_TYPE) const;
		FP_TYPE atomIonSecondGradSpline(int, FP_TYPE) const;

		void selectAtomData(int, int, Array1D<FP_TYPE>&, Array1D<FP_TYPE>&) const;
		FP_TYPE atomIonFirstGradPoly(int, int) const;
		FP_TYPE atomIonSecondGradPoly(int, int) const;
		FP_TYPE atomIonFirstGradSpline(int, int) const;
		FP_TYPE atomIonSecondGradSpline(int, int) const;

		void setBaseCharge(const Geometry&, const InputChargeFitBall&);
		void setElectroneg(const Geometry&, const InputChargeFitBall&);
		void setJTerms(const Geometry&, const InputChargeFitBall&);
		
		void setMatrix(const Geometry&, const InputChargeFitBall&);
		void equilibrate(ostream& Output, const Geometry&, const InputChargeFitBall&);

		void solveBroyden(ostream&, const Geometry&, const InputChargeFitBall&);
		void solveMix(ostream&, const Geometry&, const InputChargeFitBall&);

		FP_TYPE solutionEnergy(const Geometry&, const InputChargeFitBall&);
};


#endif
