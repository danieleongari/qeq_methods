
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "ChargeEquilibrator.h"

#include "InputChargeFitBall.h"
#include "Geometry.h"
#include "Column.h"
#include "Matrix.h"
#include "AtomDataBall.h"
#include "Calculus.h"

ChargeEquilibrator::ChargeEquilibrator()
{
	null();
}
ChargeEquilibrator::ChargeEquilibrator(const ChargeEquilibrator& Data)
{
	*this = Data;
}
ChargeEquilibrator& ChargeEquilibrator::operator=(const ChargeEquilibrator& A)
{
	size_val = A.size_val;
	Q_val = A.Q_val;
	BaseQ_val = A.BaseQ_val;
	ElectroNeg_val = A.ElectroNeg_val;
	Chi_val = A.Chi_val;
	D_val = A.D_val;
	PenetrateJ_val = A.PenetrateJ_val;
	InteractionJ_val = A.InteractionJ_val;
	J_val = A.J_val;
	C_val = A.C_val;
	Coulomb_val = A.Coulomb_val;
	RealTrans_val = A.RealTrans_val;
	FourierTrans_val = A.FourierTrans_val;
	Rij_val = A.Rij_val;

	return *this;
}
int ChargeEquilibrator::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of ChargeEquilibrator data in ChargeEquilibrator::operator=(int i)");
	null();

	return i;
}

void ChargeEquilibrator::null()
{
	size_val = 0;
	Q_val.setSize(0);
	BaseQ_val.setSize(0);
	ElectroNeg_val.setSize(0);
	Chi_val.setSize(0);
	D_val.setSize(0);

	PenetrateJ_val.setSize(0, 0);
	InteractionJ_val.setSize(0, 0);
	J_val.setSize(0, 0);
	C_val.setSize(0, 0);
	
	Coulomb_val.null();
	RealTrans_val.setSize(0);
	FourierTrans_val.setSize(0);
	Rij_val.setSize(0);
}

FP_TYPE ChargeEquilibrator::charge(int i) const
{
	return Q_val[i];
}

void ChargeEquilibrator::setSize(const Geometry& Geom)
{
	size_val = Geom.atomNo();

	Q_val.setSize(size_val);
	BaseQ_val.setSize(size_val);
	ElectroNeg_val.setSize(size_val);
	Chi_val.setSize(size_val);
	D_val.setSize(size_val);

	PenetrateJ_val.setSize(size_val, size_val);
	InteractionJ_val.setSize(size_val, size_val);
	J_val.setSize(size_val, size_val);
	C_val.setSize(size_val, size_val);

	AtomIonEnergy_val.setSize(size_val);
	AtomIonEnergyBase_val.setSize(size_val);

	AtomIonSize_val.setSize(size_val);
	AtomIonSizeBase_val.setSize(size_val);
}


void ChargeEquilibrator::initCoulombTerms(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, j, n;
	Array1D<Vector> RecipVect(3);
	FunctionBall Func;

	Coulomb_val.createTerms(Geom);
	if(Geom.period() == 0)
	{
		Coulomb_val.setCutoff(100);
		RealTrans_val.setSize(1);
		RealTrans_val[0].zero();
		FourierTrans_val.setSize(0);
	}
	else
	{
		//  Now get the possible translation vectors
		Coulomb_val.setCellParam(Geom);
		Coulomb_val.setError((FP_TYPE)1E-5);
		Coulomb_val.setSumMethod(EWALDPOTENTIAL_SUMMATION_METHOD_EWALD);
		Coulomb_val.setTransVect((FP_TYPE)size_val, (FP_TYPE)Coulomb_val.termNo(), (FP_TYPE)size_val*COULOMB, 1);
		
		RecipVect[0] = Geom.Translation(1) * Geom.Translation(2);
		RecipVect[1] = Geom.Translation(2) * Geom.Translation(0);
		RecipVect[2] = Geom.Translation(0) * Geom.Translation(1);
		RecipVect[0] *= 2 * PI / Coulomb_val.volume();
		RecipVect[1] *= 2 * PI / Coulomb_val.volume();
		RecipVect[2] *= 2 * PI / Coulomb_val.volume();
		
		RealTrans_val = Coulomb_val.TransVect(Coulomb_val.realCut(), Geom.Translation());
		FourierTrans_val = Coulomb_val.FourierVect(false, Coulomb_val.fourierCut(), Coulomb_val.alpha(), Coulomb_val.volume(), RecipVect);
	}
	
	//  Get all of the distance vectors
	n = size_val;
	Rij_val.setSize(n, n);
	for(i = 0; i < n; i++) Rij_val[i][i].zero();
	for(i = 0; i < n; i++)
	{
		for(j = i+1; j < n; j++)
		{
			Rij_val[i][j] = Geom.bondVector(i, j);
			Rij_val[j][i] = Rij_val[i][j] * -1;
		}
	}
}
void ChargeEquilibrator::initAtomData(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, j, n, state, type;
	String Variant;
	FunctionBall Func;
	AtomDataBall AtomData;

	n = size_val;

	for(i = 0; i < size_val; i++) Q_val[i] = Geom.Atoms(i).charge();

	Variant = Calc.QeqAtomIonData();
	for(i = 0; i < Geom.atomNo(); i++)
	{
		type = Geom.Atoms(i).type();
		if(AtomData.AtomIonEnergyDefined(Variant, type) == true)
		{
			AtomIonEnergyBase_val[i] = AtomData.getAtomIonEnergyStateBase(Variant, type);
			AtomIonEnergy_val[i].setSize(AtomData.getAtomIonEnergyNumber(Variant, type));
			for(j = 0; j < AtomIonEnergy_val[i].length(); j++)
			{
				state = AtomData.getAtomIonEnergyStateBase(Variant, type) + j;
				AtomIonEnergy_val[i][j] = AtomData.getAtomIonEnergy(Variant, type, state);
			}	
		}
		else
		{
			throw ErrorBall("Undefined atom ion energy data in ChargeEquilibrator::initAtomData");
		}
	}

	for(i = 0; i < Geom.atomNo(); i++)
	{
		type = Geom.Atoms(i).type();
		if(AtomData.AtomIonSizeDefined(type) == true)
		{
			AtomIonSizeBase_val[i] = AtomData.getAtomIonSizeStateBase(type);
			AtomIonSize_val[i].setSize(AtomData.getAtomIonSizeNumber(type));
			for(j = 0; j < AtomIonSize_val[i].length(); j++)
			{
				state = AtomData.getAtomIonSizeStateBase(type) + j;
				AtomIonSize_val[i][j] = AtomData.getAtomIonSize(type, state);
			}	
		}
		else
		{
			throw ErrorBall("Undefined atom ion size data in ChargeEquilibrator::initAtomData");
		}
	}

	setBaseCharge(Geom, Calc);
	setElectroneg(Geom, Calc);

	for(i = 0; i < n; i++)
	{
		for(j = i+1; j < n; j++)
		{
			PenetrateJ_val(i, j) = getPenetratePotential(i, j, Geom, Calc);
			PenetrateJ_val(j, i) = PenetrateJ_val(i, j);
		}
	}
	PenetrateJ_val /= Calc.qeqDielectric();

	setJTerms(Geom, Calc);
	InteractionJ_val /= Calc.qeqDielectric();
}

FP_TYPE ChargeEquilibrator::getPenetratePotential(int i, int j, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int k, end;
	FP_TYPE penetrate, rij, cutoff;

	penetrate = 0;

	cutoff = (Geom.Atoms(i).vdwRadius() + Geom.Atoms(j).vdwRadius());
	//cutoff *= 2;
	cutoff = 1E99;

	end = (i==j) ? RealTrans_val.length()-1 : RealTrans_val.length();
	for(k = 0; k < end; k++)
	{
		rij = (Rij_val[i][j]+RealTrans_val[k]).r();
		if(rij < cutoff)
		{
			penetrate += getIntegral(i, j, rij, Geom, Calc);
			penetrate -= (COULOMB/EV_TO_KCAL) / rij;
		}
	}

	return penetrate;
}
FP_TYPE ChargeEquilibrator::getIdemPotential(int i, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	FP_TYPE Jii, zetaH;

	switch(Calc.qeqDerivativeType())
	{
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM:
		{
			Jii = 2 * Geom.Atoms(i).hardness();
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF:
		{
			Jii = atomIonSecondGrad(i, BaseQ_val[i]);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY:
		{
			Jii = atomIonSecondGradPoly(i, BaseQ_val[i]);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE:
		{
			Jii = atomIonSecondGradSpline(i, BaseQ_val[i]);
			break;
		}
	}

	if(Calc.qeqIdempotentialType() == INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE && Geom.Atoms(i).type() == ATOM_HYDROGEN)
	{
		zetaH = 0.5 * ((2*Geom.Atoms(i).shell())+1) / (2*Geom.Atoms(i).slaterScreen()*ANGSTROM_TO_BOHR);
		Jii *= 1 + (Q_val[i]/zetaH);
	}

	return Jii;
}


FP_TYPE ChargeEquilibrator::getSelfSlater(int na, FP_TYPE alpha) const
{
	int tna, tnb, l;
	FP_TYPE result, sum, temp;
	FunctionBall Func;

	tna = 2 * na;
	tnb = 2 * na;

	sum = 0;
	for(l = 1; l <= tnb; l++)
	{
		temp = l * pow(2*alpha, tnb-l) * Func.factoral(tna+tnb-l-1);
		temp /= Func.factoral(tnb-l) * tnb * pow(4*alpha, tna+tnb-l);
		sum += temp;
	}

	temp = Func.factoral(tna-1) / pow(2*alpha, tna);
	temp -= sum;
	result = pow(2*alpha, tna+1) * temp / Func.factoral(tna);
	
	return result;
}
FP_TYPE ChargeEquilibrator::getDistSlater(int na, int nb, FP_TYPE alpha, FP_TYPE beta, FP_TYPE r) const
{
	int tna, tnb, l;
	FP_TYPE result, sum, temp;
	FunctionBall Func;

	tna = 2 * na;
	tnb = 2 * nb;

	sum = 0;
	for(l = 1; l <= tnb; l++)
	{
		temp = l * pow(2*beta, tnb-l) * pow((FP_TYPE)(0.5*r), (FP_TYPE)(tna+tnb-l));
		temp /= Func.factoral(tnb-l) * tnb;
		temp *= slaterReducedOverlap(tna-1, tnb-l, 2*alpha*r, 2*beta*r);
		sum += temp;
	}
	temp = pow((FP_TYPE)(0.5*r), (FP_TYPE)tna) * slaterReducedOverlap(tna-1, 0, 2*alpha*r, 0);
	temp -= sum;
	result = pow(2*alpha, tna+1) * temp / Func.factoral(tna);
	
	return result;
}
FP_TYPE ChargeEquilibrator::slaterReducedOverlap(int nna, int nnb, FP_TYPE zetaA, FP_TYPE zetaB) const
{
	int k, na, nb;
	FP_TYPE result, alpha, beta, a, b, z, temp;

	if(zetaA < zetaB)
	{
		na = nnb;
		nb = nna;
		alpha = zetaB;
		beta = zetaA;
	}
	else
	{
		na = nna;
		nb = nnb;
		alpha = zetaA;
		beta = zetaB;
	}

	result = 0;
	for(k = 0; k <= na + nb; k++)
	{
		z = slaterZFunc(k, na, nb);
		a = slaterAFunc(k, (FP_TYPE)0.5*(alpha+beta));
		b = slaterBFunc(na+nb-k, (FP_TYPE)0.5*(alpha-beta));

		temp = a * b * z;
		result += temp;
	}
	result *= 0.5;

	return result;
}	
FP_TYPE ChargeEquilibrator::slaterAFunc(int k, FP_TYPE rho) const
{
	FP_TYPE result;

	result = exp(-rho) / rho;
	
	if(k > 0)
	{
		result += k * slaterAFunc(k-1, rho) / rho;
	}
	
	return result;
}
FP_TYPE ChargeEquilibrator::slaterBFunc(int k, FP_TYPE rho) const
{
	FP_TYPE part, result;
	int nu;
	FunctionBall Func;

	if(rho < 1E-5)
	{
		if(k%2 == 0) result = 2 / (FP_TYPE)(k+1);
		else result = 0;
	}
	else if(k%2 == 0)
	{
		result = 0;
		part = 1;
		for(nu = 0; fabs(part) > 1E-30 && nu < 1000; nu++)
		{
			part = 2 * nu * log(rho);
			part -= Func.logFactoral(2*nu) + log((FP_TYPE)(2*nu)+k+1);
			part = exp(part);

			result += part;
		}
		result *= 2;
	}
	else
	{
		result = 0;
		part = 1;
		for(nu = 0; fabs(part) > 1E-30 && nu < 1000; nu++)
		{
			part = ((2*nu)+1) * log(rho);
			part -= Func.logFactoral((2*nu)+1) + log((FP_TYPE)(2*nu)+k+2);
			part = exp(part);

			result += part;
		}
		result *= -2;
	}

	return result;
}
FP_TYPE ChargeEquilibrator::slaterZFunc(int k, int na, int nb) const
{
	int i, j;
	FP_TYPE result, term;
	FunctionBall Func;

	term = Func.factoral(na) + Func.factoral(nb);

	result = 0;
	for(i = 0; i <= k; i++)
	{
		j = k - i;

		result += Func.signPower(nb-j) * Func.binomial(na, i) * Func.binomial(nb, j);
	}

	return result;
}
FP_TYPE ChargeEquilibrator::getDistGaussian(FP_TYPE alpha, FP_TYPE beta, FP_TYPE r) const
{
	FP_TYPE na, nb, gamma, result;

	na = pow(2*alpha/PI, (FP_TYPE)0.75);
	nb = pow(2*beta/PI, (FP_TYPE)0.75);

	gamma = alpha * beta / (alpha+beta);
	result = na * nb * pow(PI/(alpha+beta), (FP_TYPE)1.5) * exp(-gamma*r*r);

	return result;
}

bool ChargeEquilibrator::calculateIntegral(int i, int j, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	if(Calc.qeqIdempotentialType() == INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE)
	{
		if(Geom.Atoms(i).type() == ATOM_HYDROGEN) return true;
		else if(Geom.Atoms(j).type() == ATOM_HYDROGEN) return true;
		else return false;
	}

	return false;	
}
FP_TYPE ChargeEquilibrator::getIntegral(int i, int j, FP_TYPE rij, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	FP_TYPE zetaI, zetaJ, alphaI, Jaa, Jbb, Jab, Jij, K, rI, rJ;

	switch(Calc.qeqAtomSize())
	{
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_ATOM:
		{
			rI = Geom.Atoms(i).slaterScreen();
			rJ = Geom.Atoms(j).slaterScreen();
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_NEUTRAL:
		{
			rI = atomIonSize(i, 0);
			rJ = atomIonSize(j, 0);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_FORMAL:
		{
			rI = atomIonSize(i, BaseQ_val[i]);
			rJ = atomIonSize(j, BaseQ_val[j]);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE:
		{
			rI = atomIonSize(i, Q_val[i]);
			rJ = atomIonSize(j, Q_val[j]);
			break;
		}
	}

	if(Calc.qeqIntegralType() == INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN)
	{
		zetaI = Calc.qeqScreenScale() * sqrt(2/(PI*rI));
		zetaJ = Calc.qeqScreenScale() * sqrt(2/(PI*rJ));
	}
	else
	{
		zetaI = Calc.qeqScreenScale() * ((2*Geom.Atoms(i).shell())+1) / (2*rI);
		zetaJ = Calc.qeqScreenScale() * ((2*Geom.Atoms(j).shell())+1) / (2*rJ);
	}

	if(Calc.qeqIdempotentialType() == INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE)
	{
		if(Geom.Atoms(i).type() == ATOM_HYDROGEN) zetaI += Q_val[i] / BOHR_TO_ANGSTROM;
		if(Geom.Atoms(j).type() == ATOM_HYDROGEN) zetaJ += Q_val[j] / BOHR_TO_ANGSTROM;
	}

	switch(Calc.qeqIntegralType())
	{
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NONE:
		{
			Jij = 1 / rij;
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_SLATER:
		{
			Jij = getDistSlater(Geom.Atoms(i).shell(), Geom.Atoms(j).shell(), zetaI, zetaJ, rij);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_GAUSSIAN:
		{
			Jij = getDistGaussian(zetaI, zetaJ, rij);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NM:
		{
			Jaa = getSelfSlater(Geom.Atoms(i).shell(), zetaI);
			Jbb = getSelfSlater(Geom.Atoms(j).shell(), zetaJ);
			Jab = (Jaa+Jbb) / 2;

			Jij = 1 / (rij + (1/Jab));

			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_NMW:
		{
			Jaa = getSelfSlater(Geom.Atoms(i).shell(), zetaI);
			Jbb = getSelfSlater(Geom.Atoms(j).shell(), zetaJ);
			Jab = (Jaa+Jbb) / 2;

			Jij = CHARGEEQUILIBRATOR_FR / (rij + (CHARGEEQUILIBRATOR_FR/Jab));
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_OHNO:
		{
			Jaa = getSelfSlater(Geom.Atoms(i).shell(), zetaI);
			Jbb = getSelfSlater(Geom.Atoms(j).shell(), zetaJ);
			Jab = (Jaa+Jbb) / 2;

			Jij = 1 / sqrt(pow(rij,2) + pow(1/Jab,2));

			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_OK:
		{
			Jaa = getSelfSlater(Geom.Atoms(i).shell(), zetaI);
			Jbb = getSelfSlater(Geom.Atoms(j).shell(), zetaJ);

			Jij = 1 / sqrt(pow(rij,2) + pow((2/Jaa)+(2/Jbb),2));

			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_DH:
		{
			Jaa = getSelfSlater(Geom.Atoms(i).shell(), zetaI);
			Jbb = getSelfSlater(Geom.Atoms(j).shell(), zetaJ);
			Jab = (Jaa+Jbb) / 2;

			Jij = 1 / (rij + (1/(Jab*exp(CHARGEEQUILIBRATOR_KLONDIKE*rij))));

			break;
		}
		case INPUTCHARGEFITBALL_QEQ_INTEGRAL_WILMER:
		{
			switch(Calc.qeqDerivativeType())
			{
				case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM:
				{
					Jaa = 2 * Geom.Atoms(i).hardness();
					Jbb = 2 * Geom.Atoms(j).hardness();
					break;
				}
				case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF:
				{
					Jaa = atomIonSecondGrad(i, BaseQ_val[i]);
					Jbb = atomIonSecondGrad(j, BaseQ_val[j]);
					break;
				}
				case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY:
				{
					Jaa = atomIonSecondGradPoly(i, BaseQ_val[i]);
					Jbb = atomIonSecondGradPoly(j, BaseQ_val[j]);
					break;
				}
				
				case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE:
				{
					Jaa = atomIonSecondGradSpline(i, BaseQ_val[i]);
					Jbb = atomIonSecondGradSpline(j, BaseQ_val[j]);
					break;
				}
			}
			Jab = sqrt(Jaa*Jbb);

			K = COULOMB / EV_TO_KCAL;
			alphaI = Jab * rij / K;
			Jij = 2 * Jab / K;
			Jij -= Jab * Jab * rij / (K*K);
			Jij -= 1 / rij;
			Jij *= exp(-alphaI*alphaI);
			Jij += 1 / rij;		//  Added this line in to cover the later subtraction of the 1/rij term

			break;
		}
	}

	Jij *= COULOMB/EV_TO_KCAL;
	return Jij;
}

FP_TYPE ChargeEquilibrator::atomIonEnergy(int atom, int state) const			
{ 
	return AtomIonEnergy_val[atom][state-AtomIonEnergyBase_val[atom]]; 
}
FP_TYPE ChargeEquilibrator::atomIonEnergy(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	return ((1-alpha)*AtomIonEnergy_val[atom][ionLow-AtomIonEnergyBase_val[atom]]) + (alpha*AtomIonEnergy_val[atom][ionHigh-AtomIonEnergyBase_val[atom]]);
}
FP_TYPE ChargeEquilibrator::atomIonSize(int atom, int state) const			
{ 
	return AtomIonSize_val[atom][state-AtomIonSizeBase_val[atom]]; 
}
FP_TYPE ChargeEquilibrator::atomIonSize(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	return ((1-alpha)*AtomIonSize_val[atom][ionLow-AtomIonSizeBase_val[atom]]) + (alpha*AtomIonSize_val[atom][ionHigh-AtomIonSizeBase_val[atom]]);
}

FP_TYPE ChargeEquilibrator::atomIonFirstGrad(int atom, int state) const
{
	int lowPlace, highPlace, i, pos, start;
	FP_TYPE result;
	Array1D<FP_TYPE> EnergyStates(3);
	Calculus Differentiator;

	lowPlace = state - 1 - AtomIonEnergyBase_val[atom];
	highPlace = state + 1 - AtomIonEnergyBase_val[atom];
	if(lowPlace >= 0 && highPlace < AtomIonEnergy_val[atom].length()) 
	{
		start = -1;
		pos = 1;
	}
	else if(lowPlace < 0)
	{
		start = 0;
		pos = 0;
	}
	else
	{
		start = -2;
		pos = 2;
	}

	for(i = 0; i < 3; i++) EnergyStates[i] = atomIonEnergy(atom, state+i+start);
	result = Differentiator.firstDerivative(pos, EnergyStates, 1.0);

	return result;
}
FP_TYPE ChargeEquilibrator::atomIonFirstGrad(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE result, alpha;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	result = ((1-alpha)*atomIonFirstGrad(atom, ionLow)) + (alpha*atomIonFirstGrad(atom, ionHigh));
	return result;
}
FP_TYPE ChargeEquilibrator::atomIonSecondGrad(int atom, int state) const
{
	int lowPlace, highPlace, i, start, pos;
	FP_TYPE result;
	Array1D<FP_TYPE> EnergyStates(3);
	Calculus Differentiator;

	lowPlace = state - 1 - AtomIonEnergyBase_val[atom];
	highPlace = state + 1 - AtomIonEnergyBase_val[atom];
	if(lowPlace >= 0 && highPlace < AtomIonEnergy_val[atom].length()) 
	{
		start = -1;
		pos = 1;
	}
	else if(lowPlace < 0)
	{
		start = 0;
		pos = 0;
	}
	else
	{
		start = -2;
		pos = 2;
	}

	for(i = 0; i < 3; i++) EnergyStates[i] = atomIonEnergy(atom, state+i+start);
	result = Differentiator.secondDerivative(pos, EnergyStates, 1.0);

	return result;
}
FP_TYPE ChargeEquilibrator::atomIonSecondGrad(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE result, alpha;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	result = ((1-alpha)*atomIonSecondGrad(atom, ionLow)) + (alpha*atomIonSecondGrad(atom, ionHigh));
	return result;
}

FP_TYPE ChargeEquilibrator::atomIonEnergyPoly(int atom, FP_TYPE state) const
{
	int i;
	Array1D<FP_TYPE> Xval;
	FunctionBall Func;

	Xval.setSize(AtomIonEnergy_val[atom].length());
	for(i = 0; i < Xval.length(); i++) Xval[i] = i + AtomIonEnergyBase_val[atom];

	return Func.polyInterpolate(state, Xval, AtomIonEnergy_val[atom]);
}
FP_TYPE ChargeEquilibrator::atomIonFirstGradPoly(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha, lowVal, highVal;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	lowVal = atomIonFirstGradPoly(atom, ionLow);
	highVal = atomIonFirstGradPoly(atom, ionHigh);
	return ((1-alpha)*lowVal) + (alpha*highVal);
}
FP_TYPE ChargeEquilibrator::atomIonSecondGradPoly(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha, lowVal, highVal;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	lowVal = atomIonSecondGradPoly(atom, ionLow);
	highVal = atomIonSecondGradPoly(atom, ionHigh);
	return ((1-alpha)*lowVal) + (alpha*highVal);
}
FP_TYPE ChargeEquilibrator::atomIonEnergySpline(int atom, FP_TYPE state) const
{
	int i;
	Array1D<FP_TYPE> Xval;
	FunctionBall Func;

	Xval.setSize(AtomIonEnergy_val[atom].length());
	for(i = 0; i < Xval.length(); i++) Xval[i] = i + AtomIonEnergyBase_val[atom];

	return Func.cubicSplineInterpolate(FUNCTIONBALL_CUBIC_SPLINE_FAST, state, Xval, AtomIonEnergy_val[atom]);
}
FP_TYPE ChargeEquilibrator::atomIonFirstGradSpline(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha, lowVal, highVal;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	lowVal = atomIonFirstGradSpline(atom, ionLow);
	highVal = atomIonFirstGradSpline(atom, ionHigh);
	return ((1-alpha)*lowVal) + (alpha*highVal);
}
FP_TYPE ChargeEquilibrator::atomIonSecondGradSpline(int atom, FP_TYPE state) const
{
	int ionLow, ionHigh;
	FP_TYPE alpha, lowVal, highVal;

	ionLow = (int)floor(state);
	ionHigh = (int)ceil(state);
	alpha = (state-ionLow);

	lowVal = atomIonSecondGradSpline(atom, ionLow);
	highVal = atomIonSecondGradSpline(atom, ionHigh);
	return ((1-alpha)*lowVal) + (alpha*highVal);
}

void ChargeEquilibrator::selectAtomData(int atom, int state, Array1D<FP_TYPE>& Xval, Array1D<FP_TYPE>& Yval) const
{
	int i, start, end;

	Xval.setSize(0);
	Yval.setSize(0);

	start = state - 2 - AtomIonEnergyBase_val[atom];
	end = state + 2 - AtomIonEnergyBase_val[atom];
	while(start < 0) start++;
	while(end >= AtomIonEnergy_val[atom].length()) end--;
	
	/*
	if(start <= -2 && end < AtomIonEnergy_val[atom].length()-2) 
	{
		start += 2;
		end += 2;
	}
	else if(start <= -1 && end < AtomIonEnergy_val[atom].length()-1) 
	{
		start += 1;
		end += 1;
	}
	else if(start >= 2 && end >= AtomIonEnergy_val[atom].length()+1) 
	{
		start -= 2;
		end -= 2;
	}
	else if(start >= 1 && end >= AtomIonEnergy_val[atom].length()) 
	{
		start -= 1;
		end -= 1;
	}
	*/

	for(i = start; i <= end; i++)
	{
		if(i >= 0 && i <= AtomIonEnergy_val[atom].length())
		{
			Xval.push(i+AtomIonEnergyBase_val[atom]);
			Yval.push(AtomIonEnergy_val[atom][i]);
		}
	}
}

FP_TYPE ChargeEquilibrator::atomIonFirstGradPoly(int atom, int state) const
{
	FP_TYPE result;
	Array1D<FP_TYPE> Xval, Yval;
	Calculus Diff;

	selectAtomData(atom, state, Xval, Yval);
	result = Diff.firstPolyDeriv(state, Xval, Yval);

	return result;
}
FP_TYPE ChargeEquilibrator::atomIonSecondGradPoly(int atom, int state) const
{
	FP_TYPE result;
	Array1D<FP_TYPE> Xval, Yval;
	Calculus Diff;

	selectAtomData(atom, state, Xval, Yval);
	result = Diff.secondPolyDeriv(state, Xval, Yval);

	return result;
}
FP_TYPE ChargeEquilibrator::atomIonFirstGradSpline(int atom, int state) const
{
	FP_TYPE result;
	Array1D<FP_TYPE> Xval, Yval;
	FunctionBall Func;

	selectAtomData(atom, state, Xval, Yval);
	result = Func.cubicSplineFirstDerivative(FUNCTIONBALL_CUBIC_SPLINE_FAST, state, Xval, Yval);

	return result;
}
FP_TYPE ChargeEquilibrator::atomIonSecondGradSpline(int atom, int state) const
{
	FP_TYPE result;
	Array1D<FP_TYPE> Xval, Yval;
	FunctionBall Func;

	selectAtomData(atom, state, Xval, Yval);
	result = Func.cubicSplineSecondDerivative(FUNCTIONBALL_CUBIC_SPLINE_FAST, state, Xval, Yval);

	return result;
}

void ChargeEquilibrator::setBaseCharge(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i;

	switch(Calc.qeqBaseChargeType())
	{
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_METAL_CHARGE:
		{
			for(i = 0; i < BaseQ_val.elementNo(); i++)
			{
				BaseQ_val[i] = (Geom.Atoms(i).isMetal() == true) ? BaseQ_val[i] = Geom.Atoms(i).loneFormalCharge() : 0;
			}
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_FORMAL_CHARGE:
		{
			for(i = 0; i < BaseQ_val.elementNo(); i++)
			{
				BaseQ_val[i] = Geom.Atoms(i).formalCharge();
			}
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE:
		{
			for(i = 0; i < BaseQ_val.elementNo(); i++) 
			{
				BaseQ_val[i] = Geom.Atoms(i).charge();
			}
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_BASECHARGE_ZERO:
		{
			for(i = 0; i < BaseQ_val.elementNo(); i++) 
			{
				BaseQ_val[i] = 0;
			}
			break;
		}
	}
}
void ChargeEquilibrator::setElectroneg(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i;

	switch(Calc.qeqDerivativeType())
	{
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_ATOM:
		{
			for(i = 0; i < ElectroNeg_val.elementNo(); i++) ElectroNeg_val[i] = Geom.Atoms(i).electroNeg();
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_DIFF:
		{
			for(i = 0; i < ElectroNeg_val.elementNo(); i++) ElectroNeg_val[i] = atomIonFirstGrad(i, BaseQ_val[i]);
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_POLY:
		{
			for(i = 0; i < ElectroNeg_val.elementNo(); i++) 
			{
				ElectroNeg_val[i] = atomIonFirstGradPoly(i, BaseQ_val[i]);
			}
			break;
		}
		case INPUTCHARGEFITBALL_QEQ_DERIVATIVE_SPLINE:
		{
			for(i = 0; i < ElectroNeg_val.elementNo(); i++) ElectroNeg_val[i] = atomIonFirstGradSpline(i, BaseQ_val[i]);
			break;
		}
	}
}
void ChargeEquilibrator::setJTerms(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, j, k, n, end;
	FP_TYPE realCut, fourierCut, alpha, volume, rij, Jij, temp, real, fourier, self, h, factor;
	Vector Rij;
	Array1D<FP_TYPE> TaperTerms(8);
	FunctionBall Func;
	Array1D<FP_TYPE> Factor;
	Array2D<FP_TYPE> CosList, SinList;

	n = size_val;

	if(Geom.period() > 0)
	{
		realCut = Coulomb_val.realCut();
		fourierCut = Coulomb_val.fourierCut();
		alpha = Coulomb_val.alpha();
		volume = Coulomb_val.volume();

		Factor.setSize(FourierTrans_val.length());
		CosList.setSize(FourierTrans_val.length(), n);
		SinList.setSize(FourierTrans_val.length(), n);

		for(k = 0; k < FourierTrans_val.length(); k++)
		{
			h = FourierTrans_val[k].K().r();
			Factor[k] = 8 * PI * exp(-h*h/(4*alpha*alpha)) / (h*h*volume);

			for(i = 0; i < n; i++)
			{
				CosList[k][i] = cos(Geom.Atoms(i).Position()%FourierTrans_val[k].K());
				SinList[k][i] = sin(Geom.Atoms(i).Position()%FourierTrans_val[k].K());
			}
		}

		for(i = 0; i < n; i++)
		{
			for(j = i; j < n; j++)
			{
				Jij = 0;
				end = (i == j) ? RealTrans_val.length()-1 : RealTrans_val.length();
				factor = (i == j) ? ONE_HALF : 1;
				real = fourier = self = 0;
				Rij = Rij_val[i][j];
				
				for(k = 0; k < end; k++)
				{
					rij = (Rij+RealTrans_val[k]).r();
					real += factor * Func.erfc(alpha*rij) / rij;
				}

				fourier = 0;
				for(k = 0; k < FourierTrans_val.length(); k++)
				{
					temp = CosList[k][i] * CosList[k][j];
					temp += SinList[k][i] * SinList[k][j];
					fourier += temp * Factor[k];
				}

				if(i == j) self = 2 * alpha / sqrt(PI);

				InteractionJ_val(i, j) = real + fourier - self;
			}
		}
	}
	else
	{
		for(i = 0; i < n; i++)
		{
			for(j = i+1; j < n; j++)
			{		
				InteractionJ_val(i, j) = 1 / Rij_val[i][j].r();
			}
		}
	}

	for(i = 0; i < n; i++)
	{
		for(j = 0; j < i; j++) InteractionJ_val(i, j) = InteractionJ_val(j, i);
	}

	InteractionJ_val *= (COULOMB/EV_TO_KCAL);
}

void ChargeEquilibrator::setMatrix(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, j;
	Column JTemp;
	FunctionBall Func;

	JTemp.setSize(size_val);

	if(Calc.qeqBaseChargeType() == INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE)
	{
		BaseQ_val = Q_val;
		setElectroneg(Geom, Calc);
	}

	for(i = 0; i < size_val; i++)
	{
		JTemp[i] = getIdemPotential(i, Geom, Calc);
		Chi_val[i] = ElectroNeg_val[i] - (BaseQ_val[i]*JTemp[i]);
	}

	//  Update the penetration potential
	for(i = 0; i < size_val; i++)
	{
		for(j = i+1; j < size_val; j++)
		{
			if(calculateIntegral(i, j, Geom, Calc) == true)
			{
				PenetrateJ_val(i, j) = getPenetratePotential(i, j, Geom, Calc);
			}
		}
	}

	for(i = 0; i < size_val; i++)
	{
		for(j = 0; j < size_val; j++)
		{
			if(i == j) J_val(i, j) = JTemp[i] + InteractionJ_val(i, j);
			else J_val(i, j) = InteractionJ_val(i, j) + PenetrateJ_val(i, j);
		}
	}
		
	//  Now set the D vector
	D_val[0] = 0;
	for(i = 1; i < size_val; i++) D_val[i] = Chi_val[i] - Chi_val[0];
		
	//  Now set the C matrix
	for(i = 0; i < size_val; i++) C_val(0, i) = 1;
	for(i = 1; i < size_val; i++)
	{
		for(j = 0; j < size_val; j++) C_val(i, j) = J_val(0, j) - J_val(i, j);
	}
}

void ChargeEquilibrator::equilibrate(ostream& Output, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	bool doIteration;
	int i;
	FunctionBall Func;
	
	setSize(Geom);
	initCoulombTerms(Geom, Calc);
	initAtomData(Geom, Calc);
	
	//  Determine if iterations are necessary
	doIteration = false;
	if(Calc.qeqSteps() > 1)
	{
		if(Calc.qeqIdempotentialType() == INPUTCHARGEFITBALL_QEQ_IDEM_ITERATE)
		{
			for(i = 0; i < size_val && doIteration == false; i++)
			{
				if(Geom.Atoms(i).type() == ATOM_HYDROGEN) doIteration = true;
			}
		}
		else if(Calc.qeqBaseChargeType() == INPUTCHARGEFITBALL_QEQ_BASECHARGE_ATOM_CHARGE) doIteration = true;
		else if(Calc.qeqAtomSize() == INPUTCHARGEFITBALL_QEQ_ATOM_SIZE_CHARGE) doIteration = true;
	}

	if(doIteration == false || Calc.qeqSteps() == 1)
	{
		setMatrix(Geom, Calc);
		Q_val = C_val.linearSolve(D_val);
	}
	else
	{
		//solveBroyden(Output, Geom, Calc);
		solveMix(Output, Geom, Calc);
	}
}

void ChargeEquilibrator::solveBroyden(ostream& Output, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, cycle;
	FP_TYPE dQ;
	Column F, OldQ, OldF, DQ, DF;
	Matrix InvJacob, Update;

	Output << setw(10) << "Step" << setw(20) << "Func" << setw(20) << "dCharge" << setw(20) << "dFunc" << endl;

	//  Initialise the interation solving things
	InvJacob.setSize(size_val, size_val);
	InvJacob = InvJacob.identity();
	InvJacob *= 0.001;

	setMatrix(Geom, Calc);
	F = (Q_val*C_val) - D_val;
	dQ = 1;
		
	for(cycle = 0; cycle < Calc.qeqSteps() && (dQ > Calc.qeqLimit()); cycle++)
	{
		//  Get new charge coordinates
		OldQ = Q_val;
		Q_val -= InvJacob*F;
		for(i = 0; i < Q_val.elementNo(); i++)
		{
			if(Q_val[i] > Geom.Atoms(i).maxCharge()) Q_val[i] = Geom.Atoms(i).maxCharge();
			if(Q_val[i] < Geom.Atoms(i).minCharge()) Q_val[i] = Geom.Atoms(i).minCharge();
		}

		//  Now get the new Function values
		OldF = F;
		setMatrix(Geom, Calc);
		F = (Q_val*C_val) - D_val;

		//  Now update the inverse Jacobian
		DQ = Q_val - OldQ;
		dQ = DQ.rms();
		DF = F - OldF;
		//Update = (DQ - (InvJacob*DF)) & (DQ*InvJacob);
		//Update /= ((DQ*InvJacob)%DQ);
		Update = (DQ - (InvJacob*DF)) & DF;
		Update /= (DF%DF);
		InvJacob += Update;

		Output << setw(10) << cycle << setw(20) << F.rms() << setw(20) << dQ << setw(20) << DF.rms() << endl;
	}
}

void ChargeEquilibrator::solveMix(ostream& Output, const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int fixNo, cycle;
	FP_TYPE dQ, maxStep;
	Column OldQ, Dfix, Qfix, Step;
	Matrix Cfix;
	Array1D<int> Fixed;
	FunctionBall Func;

	Output << setw(10) << "Step" << setw(20) << "dCharge" << endl;

	dQ = 1;
	OldQ = Q_val;
	fixNo = 0;
	maxStep = 0.1*sqrt((FP_TYPE)size_val);
	for(cycle = 0; cycle < Calc.qeqSteps() && (dQ > Calc.qeqLimit()); cycle++)
	{
		setMatrix(Geom, Calc);
		OldQ = Q_val;
		Q_val = C_val.linearSolve(D_val);
		dQ = (Q_val-OldQ).rms();
		Q_val *= CHARGEEQUILIBRATOR_MIX;
		Q_val += OldQ * (1-CHARGEEQUILIBRATOR_MIX);

		Output << setw(10) << cycle << setw(20) << dQ << endl;
	}
	if(cycle == Calc.qeqSteps())
	{
		throw ErrorBall("Equilibration not converged by iteration limit in Geometry::chargeGeomEquilibrate");
	}
}

FP_TYPE ChargeEquilibrator::solutionEnergy(const Geometry& Geom, const InputChargeFitBall& Calc)
{
	int i, j;
	FP_TYPE energy, dQ;

	//  Add in the coulomb/ewald energy
	energy = 0;
	Coulomb_val.loadListData(Q_val);
	Coulomb_val.setRij(Geom);
	Coulomb_val.addEnergy(Geom, energy);
	energy /= Calc.qeqDielectric(); 

	//  Add in the ionisation energies
	for(i = 0; i < Geom.atomNo(); i++)
	{
		dQ = (Q_val[i]-BaseQ_val[i]);
		energy += atomIonEnergy(i, BaseQ_val[i]);
		energy += dQ * atomIonFirstGrad(i, BaseQ_val[i]);
		energy += ONE_HALF * dQ * dQ * atomIonSecondGrad(i, BaseQ_val[i]);
	}

	//  Fix the penetration potential
	for(i = 0; i < Geom.atomNo(); i++)
	{
		for(j = i+1; j < Geom.atomNo(); j++)
		{
			energy += 2 * getPenetratePotential(i, j, Geom, Calc);
		}
	}

	return energy;
}

