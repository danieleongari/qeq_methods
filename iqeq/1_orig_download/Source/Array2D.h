/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _ARRAY2D_H
#define _ARRAY2D_H

#include "definitions.h"
#include "Array1D.h"

#include <iostream>
#include <iomanip>
using namespace std;

template <class T>
class Array2D
{
	friend class MPICommBall;

	protected:
		Array1D<T> *Data_val;
		int length_val;

		//  Internal quicksort function
		void quicksort(int data, int l, int r)
		{	
			int i, j;
			Array1D<T> Partition;
	
			if(r > l)
			{
				Partition = Data_val[r];
				i = l-1;
				j = r;
				while(i < j)
				{
					while(Data_val[++i][data] < Partition[data]);
					while(j > l && Data_val[--j][data] > Partition[data]);
					if(i < j) swap(i, j);
				}
				swap(i, r);

				quicksort(data, l, i-1);
				quicksort(data, i+1, r);
			}
		}

	public:
		Array2D(int sizeI)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(sizeI);
		}
		Array2D(int sizeI, int sizeJ)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(sizeI, sizeJ);
		}
		Array2D(const Array2D<T>& A)
		{
			int i;

			setSize(A.length());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];
		}
		Array2D()
		{
			length_val = 0;
			Data_val = NULL;
		}
		~Array2D()
		{
			if(length_val > 0) delete [] Data_val;
	
			Data_val = NULL;
			length_val = 0;
		}
		void null()
		{
			int i;

			for(i = 0; i < length_val; i++) Data_val[i].null();
		}

		//  Operator overloads
		const Array2D<T>& operator=(const Array2D<T>& A)
		{
			int i;

			setSize(A.length());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];

			return *this;
		}
		int operator=(int i)
		{
			if(i != 0) 
				throw ErrorBall("Misformed clearing of atom data in Array2D<T>::operator=(int i)");
			null();

			return i;
		}
		bool operator==(const Array2D<T>& A) const
		{
			int i, j;

			if(length_val != A.length()) return false;

			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i].length() != A[i].length()) return false;
				for(j = 0; j < Data_val[i].length(); j++)
				{
					if(Data_val[i][j] != A[i][j]) return false;
				}
			}

			return true;
		}
		bool operator!=(const Array2D<T>& A) const
		{
			return !(*this == A);
		}
		Array1D<T>& operator[](int i) const
		{
			#ifdef _ERROR_REPORT
			if(i < 0 || i >= length_val) 
				{
					throw ErrorBall("Out of bounds Array2D access in Array2D::operator[]");
				}
			#endif

			return Data_val[i];
		}
		const T& operator()(int i, int j, const T& Element)
		{
			if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length()) throw ErrorBall("Out of bounds Array2D access in Array2D::operator()");
			else Data_val[i].setElement(j, Element);

			return Element;
		}

		//  Explicit reading and writing functions
		T& element(int i, int j) const
		{
			if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length()) throw ErrorBall("Out of bounds Array2D access in Array2D::element(int)");
			else return Data_val[i][j];
		}
		const T& setElement(int i, int j, const T& Element)
		{
			if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length()) throw ErrorBall("Out of bounds Array2D access in Array2D::setElement(int, const T&)");
			else Data_val[i].setElement(j, Element);

			return Element;
		}	
		int length() const
		{
			return length_val;
		}

		//  Re-sizing function
		void setSize(int size)
		{
			int i;
			String Error;
			try
			{
				#ifdef _ERROR_REPORT
					if(size < 0) 
						throw ErrorBall("Cannot size negative Array2D in function Array2D::resize(int)");
					if(size > 100000000)
						throw ErrorBall("Cannot size massive Array2D in function Array2D::resize(int)");
				#endif

				if(length_val > 0) delete [] Data_val;
				if(size > 0) Data_val = new Array1D<T>[size];
				else Data_val = NULL;
				length_val = size;
				for(i = 0; i < length_val; i++) Data_val[i].setSize(0);
			}
			catch(bad_alloc ba)
			{
				Error << ba.what() & "\nCaught bad memory allocation in Array2D::setSize(int)";
				throw ErrorBall(Error);
			}
		}
		void setSize(int sizeI, int sizeJ)
		{
			int i;

			#ifdef _ERROR_REPORT
				if(sizeI < 0) 
					throw ErrorBall("Cannot size negative Array2D in function Array2D::resize(int, int)");
			#endif

			if(length_val > 0) delete [] Data_val;
			if(sizeI > 0) Data_val = new Array1D<T>[sizeI];
			else Data_val = NULL;
			length_val = sizeI;
			for(i = 0; i < length_val; i++) Data_val[i].setSize(sizeJ);
		}
		void resize(int sizeI)
		{
			Array2D<T> CopySpace;
			int i;

			#ifdef _ERROR_REPORT
				if(sizeI < 0) 
					throw ErrorBall("Cannot resize negative Array2D in function Array2D::resize(int)");
			#endif

			CopySpace = *this;
			setSize(sizeI);
			for(i = 0; i < length_val && i < CopySpace.length(); i++) Data_val[i] = CopySpace[i];
		}
		void resize(int sizeI, int sizeJ)
		{
			Array2D<T> CopySpace;
			int i;

			#ifdef _ERROR_REPORT
				if(sizeI < 0) 
					throw ErrorBall("Cannot resize negative Array2D in function Array2D::resize(int, int)");
			#endif

			CopySpace = *this;
			setSize(sizeI);
			for(i = 0; i < length_val && i < CopySpace.length(); i++) Data_val[i] = CopySpace[i];
			for(i = CopySpace.length(); i < length_val; i++) Data_val[i].resize(sizeJ);
		}

		//  Function to sort the array, assuming that the comparison operators for that class are defined
		void swap(int i, int j)
		{	
			Array1D<T> Temp;

			if(i != j)
			{
				Temp = Data_val[i];
				Data_val[i] = Data_val[j];
				Data_val[j] = Temp;
			}
		}
		void quicksort()
		{	
			quicksort(0, 0, length_val-1);
		}
		void quicksort(int data)
		{
			quicksort(data, 0, length_val-1);
		}

		//  Functions to search the array in a normal and binary way, assuming that the array is sorted
		//  Index reveals the position of the element
		void search(int* pi, int* pj, const T& value) const
		{
			int i;

			*pi = *pj = -1;
			for(i = 0; i < length_val && *pi == -1; i++)
			{
				*pj = Data_val[i].search(value);
				if(*pj >= 0)  *pi = i;
			}
			if(*pi == -1) *pj == -1;
		}
		void binarySearch(int* pi, int* pj, const T& value) const
		{
			int i;

			*pi = *pj = -1;
			for(i = 0; i < length_val && *pi == -1; i++)
			{
				*pj = Data_val[i].binarySearch(value);
				if(*pj >= 0)  *pi = i;
			}
			if(*pi == -1) *pj == -1;
		}
		int search(const T& value) const
		{
			int i;

			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i][0] == value) return i;
			}
			return -1;
		}
		int binarySearch(const T& value) const
		{
			int low, high, middle;

			if(length_val == 0 || value < Data_val[0][0] || value > Data_val[length_val-1][0]) return -1;

			low = 0;
			high = length_val -1;
			while((high-low) > 1)
			{
				middle = low + ((high-low)/2);
				if(value == Data_val[middle][0]) return middle;
				else if(value < Data_val[middle][0]) high = middle;
				else low = middle;
			}

			if(value == Data_val[low][0]) return low;
			else if(value == Data_val[high][0]) return high;
			else return -1;
		}

		//  Functions that approximate a filo que stack
		void push(const Array1D<T>& Value)
		{
			resize(length_val+1);
			Data_val[length_val-1] = Value;
		}
		Array1D<T> pop()
		{
			T Value;

			#ifdef _ERROR_REPORT
				if(length_val <= 0) throw ErrorBall("Trying to pop element from 0 element list in Array1D<T>::pop");
			#endif

			Value = Data_val[length_val-1];
			resize(length_val-1);
			return Value;
		}
		void addElement(int place, const Array1D<T>& Data)
		{
			int i;
			
			#ifdef _ERROR_REPORT
				if(place < 0 || place > length_val) 
					throw ErrorBall("Array out of bounds error in Array2D<T>::addElement");
			#endif

			resize(length_val+1);
			for(i = length_val-1; i > place; i--)
			{
				Data_val[i] = Data_val[i-1];
			}
			Data_val[place] = Data;
		}
		void deleteElement(int place)
		{
			#ifdef _ERROR_REPORT
				if(place < 0 || place > length_val) 
					throw ErrorBall("Array out of bounds error in Array1D<T>::deleteElement");
			#endif

			swap(place, length_val-1);
			resize(length_val-1);
		}

		//  Functions to get the positions of the values of the minimum and maximum values in the array
		//  Again assumes the definition of the comparison operators
		const T& minValue() const
		{
			int pi, pj;

			minPos(&pi, &pj);
			
			#ifdef _ERROR_REPORT
				if(pi == -1) 
				{
					throw ErrorBall("Cannot get minimum value in an empty array in Array2D<T>::min()");
				}
			#endif

			return Data_val[pi][pj];
		}
		const T& maxValue() const
		{
			int pi, pj;

			maxPos(&pi, &pj);

			#ifdef _ERROR_REPORT
				if(pi == -1) 
				{
					throw ErrorBall("Cannot get maximum value in an empty array in Array2D<T>::min()");
				}
			#endif

			return Data_val[pi][pj];
		}
		void minPos(int* pi, int* pj) const
		{
			int i, j;

			*pi = *pj = -1;
			for(i = 0; i < length_val; i++)
			{
				j = Data_val[i].minPos();
				if(j != -1 && (*pi == -1 || Data_val[i][j] < Data_val[*pi][*pj]))
				{
					*pi = i;
					*pj = j;
				}
			}
			if(*pi == -1) *pj = -1;
		}
		void maxPos(int* pi, int* pj) const
		{
			int i, j;

			*pi = *pj = -1;
			for(i = 0; i < length_val; i++)
			{
				j = Data_val[i].maxPos();
				if(j != -1 && (*pi == -1 || Data_val[i][j] > Data_val[*pi][*pj]))
				{
					*pi = i;
					*pj = j;
				}
			}
			if(*pi == -1) *pj = -1;
		}

		//  An overload for output, so that I can easily test values of arrays
		friend ostream & operator<<(ostream &output, const Array2D<T>& A)
		{
			String Index;
			int i, j;

			output.precision(4);
			output.setf(ios::showpoint | ios::fixed);

			for(i = 0; i < A.length(); i++)
			{
				for(j = 0; j < A[i].length(); j++)
				{
					Index << "[" & i & "," & j & "]";
					output << setw(10) << right << Index;
					output << setw(20) << right << A[i][j] << endl;
				}
			}

			output << endl;
			return output;
		}
};


#endif


