
/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "Tensor.h"
#include "Vector.h"
#include "Complex.h"
#include "definitions.h"
#include "ErrorBall.h"
#include "Matrix.h"
#include "Column.h"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iomanip>

#include "mkl.h"

//  The constructor
Tensor::Tensor()
{
	int i, j;

	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++) elements_val[i][j] = 0; 
	}
}
Tensor::Tensor(const Tensor &A)
{
	int i, j;

	for(i = 0; i < 3; i++) 
	{
		for(j = 0; j < 3; j++) elements_val[i][j] = A.elements_val[i][j];
	}
}
Tensor::~Tensor()
{
	return;
}

//  The readers
FP_TYPE Tensor::element(int i, int j) const		{ return elements_val[i][j]; }

FP_TYPE& Tensor::operator()(int i, int j)
{ 
	if (i < 0 || j < 0 || i > 2 || j > 2)
		throw ErrorBall("Out of bounds array access in function Tensor::operator(int,int)");

	return elements_val[i][j];
}

void Tensor::setSymmetricElement(int i, int j, FP_TYPE value)
{
	if(i < 0 || i >= 3 || j < 0 || j >= 3) throw ErrorBall("Array out of bounds access in function Tensor::setElement\n");
	elements_val[i][j] = value;
	elements_val[j][i] = value;
}

Tensor Tensor::operator*(FP_TYPE value) const
{
	Tensor Result;
	int i, j;

	for(i = 0; i < 3; i++) 
	{
		for(j = 0; j < 3; j++) Result(i, j) = elements_val[i][j] * value;
	}

	return Result;
}

Tensor Tensor::operator/(FP_TYPE value) const
{
	Tensor Result;

	if(fabs(value) < ZERO) 
		throw ErrorBall("Divide by zero in function Tensor::operator/\n");
	Result = *this * (1/value);

	return Result;
}
//
//  An overload for multiplication with a Vector
Vector Tensor::operator*(const Vector& V) const
{
	Vector Result;
	int i, j;
	FP_TYPE temp[3];
	
	for(i = 0; i < 3; i++)
	{
		temp[i] = 0;
		for(j = 0; j < 3; j++) temp[i] +=  element(i,j) * V.cartesian(j);
	}

	Result.setCart(temp[0], temp[1], temp[2]);
	return Result;
}

FP_TYPE Tensor::deter() const
{
	FP_TYPE determ;

	determ =  element(0,0) * cofactor(0,0);
	determ += element(0,1) * cofactor(0,1);
	determ += element(0,2) * cofactor(0,2);

	return determ;
}


Tensor Tensor::transpose() const
{
	int i, j;
	Tensor Result;

	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++) Result(j, i) = element(i, j);
	}

	return Result;
}
//
//  A function to get the inverse
Tensor Tensor::inverse() const
{
	FP_TYPE determinant;
	Tensor Inv;
	int i, j;
	FP_TYPE temp;

	//  Check the determinant
	determinant = deter();
	if(fabs(determinant) < ZERO) 
	{
		throw ErrorBall("Tensor has no inverse in function Tensor::inverse");
	}

	//  Now try and find the inverse
	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			temp = cofactor(i, j);
			Inv(i, j) =  temp;
		}
	}
	Inv = Inv.transpose() / determinant;

	return Inv;
}

//  A function to get the co-factor
FP_TYPE Tensor::cofactor(int i, int j) const
{
	FP_TYPE temp[2][2];
	int a, b, c, d;
	FP_TYPE result;

	if(i<0 || j<0 || i>2 || j>2) throw ErrorBall("Out of bounds access in function Tensor::cofactor\n");
	c = 0;
	for(a = 0; a < 3; a++)
	{
		d = 0;
		if(a != i)
		{
			for(b = 0; b < 3; b++)
			{
				if(b != j)
				{
					temp[c][d] = element(a,b);
					d++;
				}
			}
			c++;
		}
	}

	result = temp[0][0] * temp[1][1];
	result -= temp[0][1] * temp[1][0];
	result *= pow(-1, (FP_TYPE)(i+j));
	return result;
}

