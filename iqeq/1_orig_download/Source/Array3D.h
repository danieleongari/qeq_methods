/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _ARRAY3D_H
#define _ARRAY3D_H

#include "definitions.h"
#include "Array2D.h"


#include <iostream>
#include <iomanip>
using namespace std;


template<class T>
class Array3D
{
	friend class MPICommBall;

	protected:
		Array2D<T> *Data_val;
		int length_val;

	public:
		Array3D(int sizeI)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(sizeI);
		}
		Array3D(int sizeI, int sizeJ)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(sizeI, sizeJ);
		}
		Array3D(int sizeI, int sizeJ, int sizeK)
		{
			length_val = 0;
			Data_val = NULL;
			setSize(sizeI, sizeJ, sizeK);
		}
		Array3D()
		{
			length_val = 0;
			Data_val = NULL;
		}
		Array3D(const Array3D<T>& A)
		{
			*this = A;
		}
		~Array3D()
		{

			if(length_val > 0) delete [] Data_val;
		}
		void null()
		{
			int i;

			for(i = 0; i < length_val; i++) Data_val[i].null();
		}

		//  Operator overloads
		int operator=(const Array3D<T>& A)
		{
			int i;

			setSize(A.length());
			for(i = 0; i < length_val; i++) Data_val[i] = A[i];

			return i;
		}
		const Array3D<T>& operator=(int i)
		{
			if(i != 0) 
				throw ErrorBall("Misformed clearing of atom data in Array3D<T>::operator=(int i)");
			null();

			return *this;
		}
		bool operator==(const Array3D<T>& A) const
		{
			int i, j, k;

			if(length_val != A.length()) return false;

			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i].length() != A[i].length()) return false;
				for(j = 0; j < Data_val[i].length(); j++)
				{
					if(Data_val[i][j].length() != A[i][j].length()) return false;
					for(k = 0; k < Data_val[i][j].length(); k++)
					{
						if(Data_val[i][j][k] != A[i][j][k]) return false;
					}
				}
			}

			return true;
		}
		bool operator!=(const Array3D<T>& A) const
		{
			return !(*this == A);
		}
		Array2D<T>& operator[](int i) const
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val) 
					throw ErrorBall("Out of bounds Array3D access in Array3D::operator[]");
			#endif
			return Data_val[i];
		}
		const T& operator()(int i, int j, int k, const T& Element)
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length() || k < 0 || k >= Data_val[i][j].length()) 
					throw ErrorBall("Out of bounds Array3D access in Array3D::operator()");
			#endif
			Data_val[i][j].setElement(k, Element);

			return Element;
		}

		//  Explicit reading and writing functions
		T& element(int i, int j, int k) const
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length() || k < 0 || k >= Data_val[i][j].length()) 
					throw ErrorBall("Out of bounds Array2D access in Array3D::element(int)");
			#endif
			return Data_val[i][j][k];
		}
		const T& setElement(int i, int j, int k, const T& Element)
		{
			#ifdef _ERROR_REPORT
				if(i < 0 || i >= length_val || j < 0 || j >= Data_val[i].length() || k < 0 || k >= Data_val[i][j].length()) 
					throw ErrorBall("Out of bounds Array2D access in Array3D::setElement(int, const T&)");
			#endif
			Data_val[i][j].setElement(j, Element);

			return Element;
		}	
		int length() const
		{
			return length_val;
		}

		//  Re-sizing function
		void setSize(int size)
		{
			int i;

			#ifdef _ERROR_REPORT
				if(size < 0) throw ErrorBall("Cannot size negative Array3D in function Array2D::resize(int)");
			#endif

			if(length_val > 0) delete [] Data_val;
			if(size > 0) Data_val = new Array2D<T>[size];
			else Data_val = NULL;
			length_val = size;
			for(i = 0; i < length_val; i++) Data_val[i].setSize(0);
		}
		void setSize(int sizeI, int sizeJ)
		{
			int i;

			#ifdef _ERROR_REPORT
			if(sizeI < 0) throw ErrorBall("Cannot size negative Array3D in function Array3D::resize(int, int)");
			#endif

			if(length_val > 0) delete [] Data_val;
			if(sizeI > 0) Data_val = new Array2D<T>[sizeI];
			else Data_val = NULL;
			length_val = sizeI;
			for(i = 0; i < length_val; i++) Data_val[i].setSize(sizeJ);
		}
		void setSize(int sizeI, int sizeJ, int sizeK)
		{
			int i;

			#ifdef _ERROR_REPORT
				if(sizeI < 0) throw ErrorBall("Cannot size negative Array3D in function Array3D::resize(int, int, int)");
			#endif

			if(length_val > 0) delete [] Data_val;
			if(sizeI > 0) Data_val = new Array2D<T>[sizeI];
			else Data_val = NULL;
			length_val = sizeI;
			for(i = 0; i < length_val; i++) Data_val[i].setSize(sizeJ, sizeK);
		}
		void resize(int sizeI, int sizeJ, int sizeK)
		{
			Array3D<T> CopySpace;
			int i;

			#ifdef _ERROR_REPORT
				if(sizeI < 0) throw ErrorBall("Cannot resize negative Array3D in function Array2D::resize(int, int, int)");
			#endif

			CopySpace = *this;
			setSize(sizeI);
			for(i = 0; i < length_val && i < CopySpace.length(); i++) Data_val[i] = CopySpace[i];
			Data_val[i].resize(sizeJ, sizeK);
		}

		//  Function to sort the array, assuming that the comparison operators for that class are defined
		void swap(int i, int j)
		{	
			Array2D<T> Temp;

			Temp = Data_val[i];
			Data_val[i] = Data_val[j];
			Data_val[j] = Temp;
		}
		void quicksort()
		{	
			quicksort(0, length_val-1);
		}
		void quicksort(int l, int r)
		{	
			int i, j;
			Array2D<T> Partition;
	
			if(r > l)
			{
				Partition = Data_val[r];
				i = l-1;
				j = r;
				while(i < j)
				{
					while(Data_val[++i][0] < Partition[0]);
					while(j > l && Data_val[--j][0] > Partition[0]);
					if(i < j) swap(i, j);
				}
				swap(i, r);

				quicksort(l, i-1);
				quicksort(i+1, r);
			}
		}

		//  Functions to search the array in a normal and binary way, assuming that the array is sorted
		//  Index reveals the position of the element
		void search(int* pi, int* pj, int* pk, const T& value)
		{
			int i;

			*pi = *pj = *pk = -1;
			for(i = 0; i < length_val && *pi == -1; i++)
			{
				Data_val[i].search(pj, pk, value);
				if(*pj >= 0) *pi = i;
			}
		}
		void binarySearch(int* pi, int* pj, int* pk, const T& value)
		{
			int i;

			*pi = *pj = *pk = -1;
			for(i = 0; i < length_val && *pi == -1; i++)
			{
				Data_val[i].binarySearch(pj, pk, value);
				if(*pj >= 0) *pi = i;
			}
		}
		int search(const T& value)
		{
			int i;

			for(i = 0; i < length_val; i++)
			{
				if(Data_val[i][0][0] == value) return i;
			}
			return -1;
		}
		int binarySearch(const T& value)
		{
			int low, high, middle;

			if(value < Data_val[0][0][0] || value > Data_val[length_val-1][0][0]) return -1;

			low = 0;
			high = length_val -1;
			while((high-low) > 1)
			{
				middle = low + ((high-low)/2);
				if(value == Data_val[middle][0][0]) return middle;
				else if(value < Data_val[middle][0][0]) high = middle;
				else low = middle;
			}

			if(value == Data_val[low][0][0]) return low;
			else if(value == Data_val[high][0][0]) return high;
			else return -1;
		}
		
		//  Functions to get the positions of the values of the minimum and maximum values in the array
		//  Again assumes the definition of the comparison operators
		const T& minValue()
		{
			int pi, pj, pk;

			minPos(&pi, &pj, &pk);
			#ifdef _ERROR_REPORT
				if(pi == -1) throw ErrorBall("Cannot get minimum value in an empty array in Array3D<T>::min()");
			#endif

			return Data_val[pi][pj][pk];
		}
		const T& maxValue()
		{
			int pi, pj, pk;

			maxPos(&pi, &pj, pk);
			#ifdef _ERROR_REPORT
				if(pi == -1) throw ErrorBall("Cannot get maximum value in an empty array in Array3D<T>::min()");
			#endif
			return Data_val[pi][pj][pk];
		}
		void minPos(int* pi, int* pj, int* pk)
		{
			int i;
			int tempJ, tempK;

			*pi = *pj = *pk = -1;
			for(i = 0; i < length_val; i++)
			{
				Data_val[i].minPos(&tempJ, &tempK);
				if(tempJ != -1 && (*pi == -1 || Data_val[i][tempJ][tempK] < Data_val[*pi][*pj][*pk]))
				{
					*pi = i;
					*pj = tempJ;
					*pk = tempK;
				}
			}
		}
		void maxPos(int* pi, int* pj, int* pk)
		{
			int i;
			int tempJ, tempK;

			*pi = *pj = *pk = -1;
			for(i = 0; i < length_val; i++)
			{
				Data_val[i].maxPos(&tempJ, &tempK);
				if(tempJ != -1 && (*pi == -1 || Data_val[i][tempJ][tempK] > Data_val[*pi][*pj][*pk]))
				{
					*pi = i;
					*pj = tempJ;
					*pk = tempK;
				}
			}
		}

		//  An overload for output, so that I can easily test values of arrays
		friend ostream & operator<<(ostream &output, const Array3D<T>& A)
		{
			String Index;
			int i, j, k;

			output.precision(4);
			output.setf(ios::showpoint | ios::fixed);

			for(i = 0; i < A.length(); i++)
			{
				Index << "[" & i & "," & j & "," & k & "]";
				output << setw(10) << right << Index;
				output << setw(20) << right << A[i][j] << endl;
			}

			output << endl;
			return output;
		}

};


#endif


