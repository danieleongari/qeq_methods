/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _CALCULUS_H
#define _CALCULUS_H

#include "definitions.h"
#include "Array1D.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

#define CALCULUS_GAUSS_LEGENDRE 0
#define CALCULUS_GAUSS_JACOBI 1
#define CALCULUS_GAUSS_CHEBYSHEV_FIRST 2
#define CALCULUS_GAUSS_CHEBYSHEV_SECOND 3
#define CALCULUS_GAUSS_LAGUERRE 4
#define CALCULUS_GAUSS_HERMITE 5

class Calculus
{
	public:
		Calculus();
		Calculus(const Calculus&);
		const Calculus& operator=(const Calculus&);
		int operator=(int);
		void null();

		virtual FP_TYPE value1D(FP_TYPE) const;
		virtual FP_TYPE value2D(FP_TYPE, FP_TYPE) const;
		virtual FP_TYPE value3D(FP_TYPE, FP_TYPE, FP_TYPE) const;
		
		//  Function for differentiation
		FP_TYPE firstDerivative(int, const Array1D<FP_TYPE>&, FP_TYPE);
		FP_TYPE secondDerivative(int, const Array1D<FP_TYPE>&, FP_TYPE);
		FP_TYPE orderDerivative(int, int, const Array1D<FP_TYPE>&, FP_TYPE);

		FP_TYPE firstPolyDeriv(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);
		FP_TYPE secondPolyDeriv(FP_TYPE, const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&);

};


#endif


