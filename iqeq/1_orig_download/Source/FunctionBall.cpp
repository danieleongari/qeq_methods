/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#include "FunctionBall.h"

#include "Array2D.h"
#include "KVector.h"
#include "Column.h"
#include "Tensor.h"

#include <cmath>
#include <cstdlib>

#include <ctime>
#include <cstdlib>
#include <sys/stat.h>

#ifdef ENV_WINDOWS
	#include <windows.h>
	#include <Windows.h>
#else
	#include <unistd.h>
#endif

#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

unsigned long long FunctionBall::random_u = 0;
unsigned long long FunctionBall::random_v = 4101842887655102017LL;
unsigned long long FunctionBall::random_w = 1;

VSLStreamStatePtr FunctionBall::randomStream_val;

FunctionBall::FunctionBall()	{ null(); }
FunctionBall::FunctionBall(const FunctionBall& A)
{
	*this = A;
}
const FunctionBall& FunctionBall::operator=(const FunctionBall& A)
{
	progress_val = A.progress_val;

	return *this;
}
int FunctionBall::operator=(int i)
{
	if(i != 0) 
		throw ErrorBall("Misformed clearing of atom data in FunctionBall::operator=(int i)");
	null();

	return i;
}
void FunctionBall::null()
{
	progress_val = 0;
}

FP_TYPE FunctionBall::factoral(int arg) const			{ return exp(logFactoral(arg)); }
//FP_TYPE FunctionBall::factoral(FP_TYPE arg) const		{ return exp(logFactoral(arg)); }
FP_TYPE FunctionBall::logFactoral(int arg) const
{
	int i;
	FP_TYPE result;

	result = 0;
	for(i = arg; i > 1; i--)  result += log((FP_TYPE)i);

	return result;
}
FP_TYPE FunctionBall::gamma(int arg) const
{
	return factoral(arg-1);
}
FP_TYPE FunctionBall::halfGamma(int n) const
{
	int newN;
	FP_TYPE result;

	if(n%2 == 0)
	{
		newN = n / 2;
		result = gamma(newN);
	}
	else
	{
		newN = (n-1) / 2;
		
		if(newN >= 0)
		{
			result = factoral(2*newN) * sqrt(PI);
			result /= pow((FP_TYPE)4, newN) * factoral(newN);
		}
		else
		{
			result = pow((FP_TYPE)-4, newN) * factoral(newN) * sqrt(PI);
			result /= factoral(2*newN);
		}
	}

	return result;
}
FP_TYPE FunctionBall::halfUpperIncompleteGamma(int n, FP_TYPE arg) const
{
	FP_TYPE result, realN;

	realN = (FP_TYPE)n / 2;

	if(n == 0)
	{
		//result = -expIntEi(-arg);
		result = expIntEn(1, arg);
	}
	else if(n == 1)
	{
		result = sqrt(PI) * erfc(sqrt(arg));
	}
	else if(n == 2)
	{
		result = exp(-arg);
	}
	else if(n < 0)
	{
		result = halfUpperIncompleteGamma(n+2, arg);
		result -= pow(arg, realN) * exp(-arg);
		result /= (FP_TYPE)realN;
	}
	else
	{
		result = (realN-1) * halfUpperIncompleteGamma(n-2, arg);
		result += pow(arg, realN-1) * exp(-arg);
	}

	return result;
}
FP_TYPE FunctionBall::binomial(int upper, int lower) const
{
	FP_TYPE result;
	int a, b;

	if(upper < 0) cout << "BINOMIAL < 0 number" << endl;

	if(lower < 0 || lower > upper) return 0;

	a = (upper < 0) ? 0 : upper;
	b = (lower < 0) ? 0 : lower;
	result = logFactoral(a) - logFactoral(a-b) - logFactoral(b);

	return exp(result);
}
int FunctionBall::signPower(int arg)
{
	if(arg%2 == 0) return 1;
	else return -1;
}
FP_TYPE FunctionBall::round(FP_TYPE arg)
{
	FP_TYPE remain;

	remain = arg - floor(arg);
	if(remain <= 0.50000000) return floor(arg);
	else return ceil(arg);
}
FP_TYPE FunctionBall::erfc(FP_TYPE arg) const
{
	int one = 1;
	FP_TYPE value;

	#ifdef ENV_WINDOWS
		#if FP_PRECISION == FP_SINGLE
			vserfc(&one, &arg, &value);
		#else
			vderfc(&one, &arg, &value);
		#endif
	#else
		#if FP_PRECISION == FP_SINGLE
			vserfc_(&one, &arg, &value);
		#else
			vderfc_(&one, &arg, &value);
		#endif
	#endif

	return value;
}
FP_TYPE FunctionBall::expIntEn(int n, FP_TYPE x) const
{
	int i, ii, maxit;
	FP_TYPE a, b, c, d, del, fact, h, psi, result, error;

	maxit = 100;
	error = 1E-7;

	if (n < 0 || x < 0.0)
	{
		throw ErrorBall("Bad arguments in FunctionBall::expIntEn");
	}
	else 
	{
		//  Special cases
		if (n == 0) result = exp(-x)/x; 
		else if(fabs(x) <= ZERO) result = 1.0 / ((n-1)+ZERO); 
		else 
		{
			// Lentz’s algorithm
			if(x > 1.0) 
			{ 
				b = x + n;
				c = 1.0 / ZERO;
				d = 1.0 / b;
				h = d;

				for(i = 1; i <= maxit; i++) 
				{
					a = -i*(n-1+i);
					b += 2.0;
					d = 1.0 / (a*d+b);	//Denominators cannot be zero.
					c = b + a/c;
					del = c*d;
					h *= del;
					if (fabs(del-1.0) < error) 
					{
						result = h*exp(-x);
						return result;
					}
				}
			
				throw ErrorBall("Continued fraction failed in FunctionBall::expIntEn");
			} 
			
			// Evaluate series.
			else 
			{ 
				//  Set first term
				result = (n-1 != 0) ? (FP_TYPE)1.0/(n-1) : -log(x)-EULER;
				fact = 1.0;
				for(i = 1; i <= maxit; i++) 
				{
					fact *= -x/i;
					if (i != (n-1)) del = -fact/(i-(n-1));
					else 
					{
						//  Compute psi(n).
						psi = -EULER; 
						for (ii = 1; ii <= (n-1); ii++) psi += (FP_TYPE)1.0 / ii;
						del = fact*(-log(x)+psi);
					}
					result += del;
					if (fabs(del) < fabs(result)*error) return result;
				}
				throw ErrorBall("series failed in FunctionBall::expIntEn");
			}
		}
	}
	
	return result;
}
int FunctionBall::intCharLength(int value)
{
	FP_TYPE absValue;
	int length;

	absValue = fabs((FP_TYPE)value);
	absValue += (FP_TYPE)1E-6;
	if(value == 0) length = 2;
	else if(value < 0) length = (int)ceil(log10(absValue))+2;
	else length = (int)ceil(log10(absValue))+1;

	return length;
}
FP_TYPE FunctionBall::cubicSplineInterpolate(int type, FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal)
{
	return cubicSplineInterpolate(x, XVal, YVal, cubicSplineDeriv(type, XVal, YVal));
}
FP_TYPE FunctionBall::cubicSplineFirstDerivative(int type, FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal)
{
	return cubicSplineFirstDerivative(x, XVal, YVal, cubicSplineDeriv(type, XVal, YVal));
}
FP_TYPE FunctionBall::cubicSplineSecondDerivative(int type, FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal)
{
	return cubicSplineSecondDerivative(x, XVal, YVal, cubicSplineDeriv(type, XVal, YVal));
}
FP_TYPE FunctionBall::cubicSplineInterpolate(FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal, const Array1D<FP_TYPE>& Deriv)
{
	int n, klo, khi;
	FP_TYPE h, a, b, c, d, xlo, xhi;
	FP_TYPE result;

	n = XVal.length();

	//  klo and khi now bracket the input value of x.
	h = (XVal[n-1] - XVal[0]) / (n-1);
	klo = (int)floor((x-XVal[0])/h);
	khi = (int)ceil((x-XVal[0])/h) % n;

	//  The xa’s must be distinct.
	if (fabs(h) < 1E-6) throw ErrorBall("Bad xa input to routine FunctionBall::splint"); 
	xlo = XVal[klo];
	xhi = xlo + h;
	a = (xhi-x) / h; 
	b = (x-xlo) / h;	
	c = (a*a*a) - a;
	c *= h * h / 6;
	d = (b*b*b) - b;
	d *= h * h / 6;

	//Cubic spline polynomial is now evaluated.
	result = a*YVal[klo] + b*YVal[khi] + c*Deriv[klo] + d*Deriv[khi];
	
	return result;
}
FP_TYPE FunctionBall::cubicSplineFirstDerivative(FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal, const Array1D<FP_TYPE>& Deriv) 
{
	int n, klo, khi;
	FP_TYPE h, a, b, c, d, xlo, xhi, dx, dy;
	FP_TYPE result;

	n = XVal.length();

	//  klo and khi now bracket the input value of x.
	h = (XVal[n-1] - XVal[0]) / (n-1);
	klo = (int)floor((x-XVal[0])/h);
	khi = (int)ceil((x-XVal[0])/h) % n;
	if(klo == khi && khi < n-1) khi++;
	else klo--;

	//  The xa’s must be distinct.
	if (fabs(h) < 1E-6) throw ErrorBall("Bad xa input to routine FunctionBall::splint"); 
	xlo = XVal[klo];
	xhi = xlo + h;
	dx = XVal[khi] - XVal[klo];
	dy = YVal[khi] - YVal[klo];
	a = (xhi-x) / h; 
	b = (x-xlo) / h;	
	c = ((3*a*a)-1) / 6;
	d = ((3*b*b)-1) / 6;

	//Cubic spline polynomial is now evaluated.
	result = dy / dx;
	result -= c * dx * Deriv[klo];
	result += d * dx * Deriv[khi];
	
	return result;
}
FP_TYPE FunctionBall::cubicSplineSecondDerivative(FP_TYPE x, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal, const Array1D<FP_TYPE>& Deriv)
{
	int n, klo, khi;
	FP_TYPE h, a, b, xlo, xhi;
	FP_TYPE result;

	n = XVal.length();

	//  klo and khi now bracket the input value of x.
	h = (XVal[n-1] - XVal[0]) / (n-1);
	klo = (int)floor((x-XVal[0])/h);
	khi = (int)ceil((x-XVal[0])/h) % n;
	if(klo == khi && khi < n-1) khi++;
	else klo--;

	//  The xa’s must be distinct.
	if (fabs(h) < 1E-6) throw ErrorBall("Bad xa input to routine FunctionBall::splint"); 
	xlo = XVal[klo];
	xhi = xlo + h;
	a = (xhi-x) / h; 
	b = (x-xlo) / h;	

	//Cubic spline polynomial is now evaluated.
	result = (a*Deriv[klo]) + (b*Deriv[khi]);
	
	return result;
}
Array1D<FP_TYPE> FunctionBall::cubicSplineDeriv(int type, const Array1D<FP_TYPE>& XVal, const Array1D<FP_TYPE>& YVal)
{
	int i, k, n;
	FP_TYPE p, sig;
	Array1D<FP_TYPE> U;
	Array1D<FP_TYPE> Deriv;
	Array1D<FP_TYPE> H;

	Matrix XData;
	Column YData, MData;

	if(type == FUNCTIONBALL_CUBIC_SPLINE_FAST)
	{
		n = XVal.length();
		U.setSize(n);
		Deriv.setSize(n);
	
		//  The lower boundary condition is set either to be “natural”
		Deriv[0] = U[0] = 0.0;
	
		//  This is the decomposition loop of the tridiagonal algorithm.
		for(i = 1; i < n-1; i++) 
		{ 
			//  y2 and u are used for temporary storage of the decomposed factors.
			sig = (XVal[i]-XVal[i-1]) / (XVal[i+1]-XVal[i-1]);
			p = (sig*Deriv[i-1]) + (FP_TYPE)2.0;
			Deriv[i] = (sig-(FP_TYPE)1.0) / p;
			U[i] = (YVal[i+1]-YVal[i]) / (XVal[i+1]-XVal[i]) - (YVal[i]-YVal[i-1])/(XVal[i]-XVal[i-1]);
			U[i] = ((FP_TYPE)6.0*U[i]/(XVal[i+1]-XVal[i-1]) - (sig*U[i-1]))/p;
		}

		//  The upper boundary condition is set to be natural
		Deriv[n-1] = 0.0;

		//  This is the backsubstitution loop of the tridiagonal algorithm.
		for (k = n-2; k >= 0 ; k--) 
		{
			Deriv[k] = Deriv[k] * Deriv[k+1] + U[k];
		}
	}
	else 
	{
		n = XVal.length() - 2;

		H.setSize(XVal.length() - 1);
		for(i = 0; i < H.length(); i++) H[i] = XVal[i+1] - XVal[i];

		//  Prepare the X matrix stuff
		XData.setSize(n, n);
		XData.zero();
		if(type == FUNCTIONBALL_CUBIC_SPLINE_NATURAL)
		{
			for(i = 0; i < n; i++)
			{
				XData(i, i) = 2 * (H[i]+H[i+1]);
				if(i < n-1)
				{
					XData(i, i+1) = H[i+1];
					XData(i+1, i) = H[i+1];
				}
			}
		}
		else if(type == FUNCTIONBALL_CUBIC_SPLINE_QUAD_RUNOFF)
		{
			for(i = 0; i < n; i++)
			{
				if(i == 0 || i == n-1) XData(i, i) = 5;
				else XData(i, i) = 4;
			
				if(i < n-1)
				{
					XData(i, i+1) = 1;
					XData(i+1, i) = 1;
				}
			}
		}
		else if(type == FUNCTIONBALL_CUBIC_SPLINE_CUBIC_RUNOFF)
		{
			for(i = 0; i < n; i++)
			{
				if(i == 0 || i == n-1) XData(i, i) = 6;
				else XData(i, i) = 4;
			
				if(i == 0) XData(i+1, i) = 1;
				else if(i == n-2) XData(i, i+1) = 1;
				else if(i < n-1)
				{
					XData(i, i+1) = 1;
					XData(i+1, i) = 1;
				}
			}
		}

		//  Set the Y matrix stuff
		YData.setSize(n);
		
		for(i = 0; i < n; i++)
		{
			YData[i] = (YVal[i+2]-YVal[i+1]) / H[i+1];
			YData[i] -= (YVal[i+1]-YVal[i]) / H[i];
		}
		YData *= 6.0;

		//  Solve to get the M data
		MData = XData.linearSolve(YData);

		//  Now copy to the output array
		Deriv.setSize(MData.elementNo()+2);
		Deriv[0] = 0;
		for(i = 0; i < MData.elementNo(); i++) Deriv[i+1] = MData[i];
		Deriv[i] = 0;
	}

	return Deriv;
}
FP_TYPE FunctionBall::polyInterpolate(FP_TYPE point, const Array1D<FP_TYPE>& Xval, const Array1D<FP_TYPE>& Fval)
{
	int i, j;
	FP_TYPE numerator, denominator, result;

	if(Xval.length() != Fval.length()) throw ErrorBall("Mismatched array lengths in FunctionBall::polyInterpolate(const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&)");

	result = 0;
	for(i = 0; i < Xval.length(); i++)
	{
		numerator = denominator = 1;
		for(j = 0; j < Xval.length(); j++)
		{
			if(i != j)
			{
				numerator *= point - Xval[j];
				denominator *= Xval[i] - Xval[j];
			}
		}

		result += (numerator/denominator) * Fval[i];
	}

	return result;
}
Column FunctionBall::polyFactors(const Array1D<FP_TYPE>& Xval, const Array1D<FP_TYPE>& Fval)
{
	int a, i, n;
	FP_TYPE x;
	Column Result, YValues;
	Matrix XValues;

	if(Xval.length() != Fval.length()) throw ErrorBall("Mismatched array lengths in FunctionBall::polyInterpolate(const Array1D<FP_TYPE>&, const Array1D<FP_TYPE>&)");

	n = Xval.length();
	Result.setSize(n);
	YValues.setSize(n);
	XValues.setSize(n, n);

	for(a = 0; a < n; a++)
	{
		x = Xval[a];
		YValues[a] = Fval[a];

		for(i = 0; i < n; i++)
		{
			XValues(a, i) = pow(x, n-i-1);
		}
	}

	Result = XValues.linearSolve(YValues);

	return Result;
}
