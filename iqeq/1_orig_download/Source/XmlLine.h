/***************************************************************************************
  This code is part of the Equilibrate project
  This program implements the QEq, E-QEq, FC-QEq and I-QEq methods
  This code is an accompanyment to the paper:
  "Charge Equilibration Based on Atomic Ionization in Metal-Organic Frameworks"
  published by Brad Wells and Alan Chaffee.
  Comments/questions about the code can be sent by e-mail to BradAWells@gmail.com
****************************************************************************************/

/****************************************************************************************
  DISCLAIMER:
  This code is provided as is without any type of warranty or guarantee of correctness.
  The purpose of this code is to illustrate the FC-QEQ and I-QEq methods and to provide
  a reference implementation for those who wish to implement the method in their own 
  code.
  The code here is based on a much larger simulation program, and has been removed from
  that project to provide the much smaller program here.  This is not the version of the
  code that was used to generate the results in the original paper.  This program is 
  primarily for pedagogical purposes.  However the code here is relatively fully featured
  and optimised, and therefore may be suitable for some calculations
*****************************************************************************************

/****************************************************************************************
  LICENCE:
  This code is distributed under the Free-BSD Licence.

  Copyright (c) <2014>, <Brad Wells>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  The views and conclusions contained in the software and documentation are those
  of the authors and should not be interpreted as representing official policies, 
  either expressed or implied, of the FreeBSD Project.
****************************************************************************************/

/****************************************************************************************
  Code last modified by Brad Wells
  August 2014
****************************************************************************************/

#ifndef _XMLLINE_H
#define _XMLLINE_H

#include "definitions.h"
#include "String.h"
#include "Array1D.h"

class XmlLine
{
	private:
		String LineName_val;
		Array1D<String> TagName_val;
		Array1D<String> TagValue_val;
		bool end_val;
		int precision_val;

		//  Control variables for printing
		bool printEnd_val;
		int printTabLevel_val;

	public:
		//  Constructors and destructors
		XmlLine();
		XmlLine(const XmlLine&);
		void null();
		const XmlLine& operator=(const XmlLine&);

		//  Readers
		int tagNo() const;
		const String& LineName() const;
		const String& TagName(int) const;
		const String& TagValue(int) const;
		int TagValueInt(int) const;
		FP_TYPE TagValueDouble(int) const;
		bool TagValueBool(int) const;
		const String& TagValue(String const&) const;
		int TagValueInt(String const&) const;
		FP_TYPE TagValueDouble(String const&) const;
		bool TagValueBool(String const&) const;
		const String& TagValue(char*) const;
		int TagValueInt(char*) const;
		FP_TYPE TagValueDouble(char*) const;
		bool TagValueBool(char*) const;
		bool isStart() const;
		bool isStart(const char*) const;
		bool isStart(String const&) const;
		bool isEnd() const;
		bool isEnd(const char*) const;
		bool isEnd(String const&) const;
		bool tagExists(const String&) const;
		bool tagExists(char*) const;
		bool printEnd() const;
		int printTabLevel() const;
		int precision() const;

		//  Writers for forming an Xml Line for output
		void setLineName(const String&);
		void setTag(const String&, const String&);
		void setTag(const String&, const char*);
		void setTag(const String&, int);
		void setTag(const String&, FP_TYPE);
		void setTag(const String&, bool);
		void setEnd(bool);
		void setPrintState(bool, int);
		void setPrecision(int);

		//  The operator that sets all of the data
		XmlLine& operator=(String const&);
		int operator=(int);

		//  Overloads for getting data directly from a file
		friend istream & operator>>(istream &, XmlLine &);

		//  Overloads for putting data into a file
		friend ostream & operator<<(ostream &, XmlLine &);
};

#endif



