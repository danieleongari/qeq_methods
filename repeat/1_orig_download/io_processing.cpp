#include "io_processing.h"
using namespace std;
/////////////////////////////////////////////////////////////////
/* Reading the REPEAT input parameter file if it exists,
   otherwise creating a default one */
void read_input_file(void) {

     char *file_name, *line;
     FILE *default_param_file;

     // Memory allocation
     cube_file_name = create_1d_char_array(MAXLINE,"cube file name");
     line = create_1d_char_array(MAXLINE,"just a line");
     file_name = create_1d_char_array(MAXLINE,"input param file");
     file_name = "REPEAT_param.inp";

     default_param_file = fopen(file_name,"r");
     if(default_param_file != NULL) {       
       fgets(line,MAXLINE,default_param_file);       
       fgets(line,MAXLINE,default_param_file);
       // Read name of cube file
       sscanf(line, "%s\n",cube_file_name);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read fitting flag
       sscanf(line, "%d\n",&fit_flag);       
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read VDW scaling factor
       sscanf(line, "%lg\n",&vdw_fact);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read RESP flag
       sscanf(line, "%d\n",&fit_RESP);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read cutoff flag
       sscanf(line, "%d\n",&flag_cutoff);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read cutoff radius
       sscanf(line, "%lg\n",&R_cutoff);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read symmetry restrain flag
       sscanf(line, "%d\n",&symm_flag);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read Goddard's penalty flag
       sscanf(line, "%d\n",&min_const);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read restrain weights
       sscanf(line, "%lg\n",&lambda_prime);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read vdw_fact_max
       sscanf(line, "%lg\n",&vdw_fact_max);
       fgets(line,MAXLINE,default_param_file);
       fgets(line,MAXLINE,default_param_file);
       // Read total charge
       sscanf(line, "%lg\n",&q_tot);
       fclose(default_param_file);
  
     } else {
       cout << "WARNING: Could not open file " << file_name << "!!!" << endl;

       // Setting the default values for input parameters
       fit_flag = 1;
       vdw_fact = 1.0;
       fit_RESP = 0;
       flag_cutoff = 1;
       R_cutoff = 20.0;
       symm_flag = 0;
       min_const = 0;
       lambda_prime = 0.0;
       vdw_fact_max = 1000;
       q_tot = 0.0;

       // Printing onto file default values for parameters
       default_param_file = fopen(file_name,"w");
       fprintf(default_param_file,"%s\n", \
       "Input ESP file name in cube format");
       fprintf(default_param_file,"%s\n", "REPEAT_ESP.cube");
       fprintf(default_param_file,"%s\n", \
       "Fit molecular(0) or periodic(1:default) system?");
       fprintf(default_param_file,"%d\n",fit_flag);
       fprintf(default_param_file,"%s\n", \
       "van der Waals scaling factor (default = 1.0)");
       fprintf(default_param_file,"%f\n",vdw_fact);
       fprintf(default_param_file,"%s\n", \
       "Apply RESP penalties?, no(0:default), yes(1)");
       fprintf(default_param_file,"%d\n",fit_RESP);
       fprintf(default_param_file,"%s\n", \
       "Read cutoff radius? no(0), yes(1:default)");
       fprintf(default_param_file,"%d\n",flag_cutoff);
       fprintf(default_param_file,"%s\n", \
       "If flag above=1 provide R_cutoff next (in Bohrs)");
       fprintf(default_param_file,"%f\n",R_cutoff);
       fprintf(default_param_file,"%s\n", \
       "Apply symmetry restrain? no(0:default), yes(1)");
       fprintf(default_param_file,"%d\n",symm_flag);
       fprintf(default_param_file,"%s\n", \
       "Use Goddard-type restrains? no(0:default), yes(1)");
       fprintf(default_param_file,"%d\n",min_const);
       fprintf(default_param_file,"%s\n", \
       "If flag above=1 then provide weight next");
       fprintf(default_param_file,"%f\n",lambda_prime);
       fprintf(default_param_file,"%s\n",\
       "van der Waals max scaling factor");
       fprintf(default_param_file,"%f\n",vdw_fact_max);
       fprintf(default_param_file,"%s\n","System total charge");
       fprintf(default_param_file,"%f\n",q_tot);
 
       cout << "Input parameter file " << file_name << \
       " with default values has been created" << endl;
       fclose(default_param_file);
       // Releasing memory
       destroy_1d_char_array(file_name);
     }
     // Releasing memory
     destroy_1d_char_array(line);

}
////////////////////////////////////////////////////////////////////
/* Reading cube file */
void read_cube_file(cube_str* CUBE) {

     int i_atom, i_grid, j_grid, i_grid_2d, k_grid;
     double junk;
     char* line;
     FILE* cube_file;

     // Memory allocation
     line = create_1d_char_array(MAXLINE,"just a line");
     CUBE->n_grid = create_1d_int_array(NDIM,"grid resolution");
     CUBE->axis_zero = create_1d_double_array(NDIM,"origin vector");
     CUBE->axis_vector = create_2d_double_array(NDIM,NDIM,"axis vectors");
 
     cube_file = fopen(cube_file_name,"r");
     if(cube_file != NULL) {
       // Skipping two lines
       fgets(line,MAXLINE,cube_file);
       fgets(line,MAXLINE,cube_file);
       // Reading total # of atoms and origin vector
       fgets(line,MAXLINE,cube_file);
       sscanf(line, "%d %lg %lg %lg\n",&CUBE->n_atoms,&CUBE->axis_zero[0], \
       &CUBE->axis_zero[1],&CUBE->axis_zero[2]);
       // Reading grid info and axis vectors
       fgets(line,MAXLINE,cube_file);
       sscanf(line, "%d %lg %lg %lg\n",&CUBE->n_grid[0], \
       &CUBE->axis_vector[0][0], &CUBE->axis_vector[0][1], \
       &CUBE->axis_vector[0][2]);
       fgets(line,MAXLINE,cube_file);
       sscanf(line, "%d %lg %lg %lg\n",&CUBE->n_grid[1], \
       &CUBE->axis_vector[1][0], &CUBE->axis_vector[1][1], \
       &CUBE->axis_vector[1][2]);
       fgets(line,MAXLINE,cube_file);
       sscanf(line, "%d %lg %lg %lg\n",&CUBE->n_grid[2], \
       &CUBE->axis_vector[2][0],&CUBE->axis_vector[2][1], \
       &CUBE->axis_vector[2][2]);

       // More memory allocation
       CUBE->atom_index = \
       create_1d_int_array(CUBE->n_atoms,"atom types");
       CUBE->atom_pos =  \
       create_2d_double_array(CUBE->n_atoms,NDIM,"atomic positions");      
       CUBE->V_pot = create_2d_double_array(CUBE->n_grid[0]*CUBE->n_grid[1], \
       CUBE->n_grid[2],"ESP grid tabulation");

       // Reading atomic info
       for(i_atom=0;i_atom<CUBE->n_atoms;i_atom++){
          fgets(line,MAXLINE,cube_file);
          sscanf(line,"%d %lg %lg %lg %lg\n",&CUBE->atom_index[i_atom],&junk, \
          &CUBE->atom_pos[i_atom][0],&CUBE->atom_pos[i_atom][1], \
          &CUBE->atom_pos[i_atom][2]);  
       } 
       // Reading ESP grid data 
       for(i_grid=0;i_grid<CUBE->n_grid[0];i_grid++) {
          for(j_grid=0;j_grid<CUBE->n_grid[1];j_grid++) {  
             i_grid_2d = convert_2d_to_1d(i_grid,j_grid,CUBE->n_grid[0]);
             for(k_grid=0;k_grid<CUBE->n_grid[2];k_grid++) {
                fscanf(cube_file,"%lE",&CUBE->V_pot[i_grid_2d][k_grid]);
                //cout << CUBE->V_pot[i_grid_2d][k_grid] << endl;
             }
          }
       }
       fclose(cube_file);

     } else {
       cout << "ERROR: Cannot read your ESP cube file!!!" << endl;

     }

     // Releasing memory
     destroy_1d_char_array(line);

} 
////////////////////////////////////////////////////////////////////
/* Reading RESP penalty data file */
void read_RESP_data_file(void) {

     int line_counter, i_atom;
     char *name, *line;
     FILE* resp_file;

     // Memory allocation
     line = create_1d_char_array(MAXLINE,"just a line");
     name = create_1d_char_array(MAXLINE,"element name");
     str_array = create_1d_double_array(VDW_types,"RESP weight");
     charge_eq_RESP = create_1d_double_array(VDW_types,"RESP charge");

     if(fit_RESP == 1) {
       resp_file = fopen("strs_RESP.ff","r");
       if(resp_file != NULL) {
         line_counter = 0;
         while(fgets(line,MAXLINE,resp_file) != NULL) {
              sscanf(line, "%s %lg %lg\n",name,&str_array[line_counter], \
              &charge_eq_RESP[line_counter]);
              line_counter++;
         }
         fclose(resp_file);
       } else {
        
         cout << "WARNING: Cannot read your RESP parameter file!!!" << endl;
         resp_file = fopen("strs_RESP.ff","w");
         fprintf(resp_file,"%s %f %f\n", "H" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "He" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Li" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Be" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "B" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "C" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "N" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "O" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "F" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ne" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Na" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Mg" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Al" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Si" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "P" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "S" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cl" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ar" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "K" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ca" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Sc" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ti" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "V" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Mn" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Fe" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Co" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ni" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cu" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Zn" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ga" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ge" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "As" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Se" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Br" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Kr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Rb" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Sr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Y" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Zr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Nb" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Mo" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Tc" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ru" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Rh" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pd" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ag" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cd" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "In" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Sn" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Sb" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Te" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "I" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Xe" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cs" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ba" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "La" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ce" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Nd" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pm" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Sm" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Eu" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Gd" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Th" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Dy" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ho" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Er" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Tm" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Yb" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Lu" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Hf" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ta" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "W" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Re" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Os" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ir" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pt" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Au" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Hg" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Tl" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pb" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Bi" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Po" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "At" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Rn" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Fr" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ra" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Ac" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Th" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pa" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "U" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Np" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Pu" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Am" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cm" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Bk" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Cf" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Es" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Fm" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Md" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "No" ,0.0 ,0.0);
         fprintf(resp_file,"%s %f %f\n", "Lw" ,0.0 ,0.0);
         cout << "RESP parameter file strs_RESP.ff with \
         default values has been created" << endl;
         fclose(resp_file);

       }

     } else {
       for(i_atom=0;i_atom<VDW_types;i_atom++) {   
          str_array[i_atom] = 0.0;
          charge_eq_RESP[i_atom] = 0.0;
       }

     }

     // Releasing memory
     destroy_1d_char_array(line);
     destroy_1d_char_array(name);
     
}
////////////////////////////////////////////////////////////////////////
/* Reading connectivity file */
void read_connectivity_file(cube_str *CUBE) {

     int i_type, index, i_index, i_atom;
     char *line;
     FILE *connectivity_file;

     // Memory allocation
     line = create_1d_char_array(MAXLINE,"just a line");
     linking_array = create_1d_int_array(CUBE->n_atoms,"linked charges");
     charges_idem = create_1d_int_array(CUBE->n_atoms,"idem charges");
     charges_equiv = create_1d_int_array(CUBE->n_atoms,"equivalent charges");

     for(i_atom=0;i_atom<CUBE->n_atoms;i_atom++) {
        linking_array[i_atom] = 0;
        charges_idem[i_atom] = i_atom;
        charges_equiv[i_atom] = i_atom;
     }

     // Reading symmetry info
     if(symm_flag == 1) {
       connectivity_file = fopen("connectivity.ff","r");
       if(connectivity_file != NULL) {
         fgets(line,MAXLINE,connectivity_file);
         sscanf(line, "%d\n",&order_type);        
         connectivity_array = create_2d_int_array(order_type,N_symm_max,"");
         for(i_type=0;i_type<order_type;i_type++) {
            fgets(line,MAXLINE,connectivity_file);
            fgets(line,MAXLINE,connectivity_file);
            sscanf(line, "%d\n",&index);
            connectivity_array[i_type][0] = index;
            for(i_index=1;i_index<index+1;i_index++) {
               fscanf(connectivity_file,"%d\n", \
               &connectivity_array[i_type][i_index]);
            }              
            for(i_index=2;i_index<index+1;i_index++) {          
               linking_array[connectivity_array[i_type][i_index]-1] = 1;
               charges_idem[connectivity_array[i_type][i_index]-1] = 
               connectivity_array[i_type][1]-1;
            }       
         }
         for(i_atom=0;i_atom<CUBE->n_atoms;i_atom++) {
            if(linking_array[i_atom] == 0) {
              charges_equiv[i_atom] = index_equiv(i_atom,linking_array);
            } else if(linking_array[i_atom] == 1) {
              charges_equiv[i_atom] = \
              index_equiv(charges_idem[i_atom],linking_array);
            }
         }
         fclose(connectivity_file);

       } else {
         cout << "WARNING: Cannot read your connectivity file!!!" << endl;
         connectivity_file = fopen("connectivity.ff","w");
         fprintf(connectivity_file,"%d\n",2);
         fprintf(connectivity_file,"%s\n","# first tree");
         fprintf(connectivity_file,"%d\n",2);
         fprintf(connectivity_file,"%d %d\n",1,2);
         fprintf(connectivity_file,"%s\n","# second tree");
         fprintf(connectivity_file,"%d\n",1);
         fprintf(connectivity_file,"%d\n",3);
         fclose(connectivity_file);
         cout << "Connectivity file connectivity.ff for H2O " << endl <<
         "has been created. Modify given your needs" << endl;
       }
     }
     destroy_1d_char_array(line); 

}
/////////////////////////////////////////////////////////////////////////
/* Printing stats and Coulomb potential on grid points */
void output_ESP_grid_data(double **inverse_term,double *inverse_term_level) {
  
     int i_grid, i_atom, j_atom;
     double *V_coul, avrg_Coul_ESP, avrg_QM_ESP, V_pot_sq_sum, sq_error;
     char *file_name;
     FILE *Coul_ESP_file;

     file_name = "Coul_ESP.dat";
     V_coul = create_1d_double_array(counter_grid,"");

     avrg_QM_ESP = 0.0;
     avrg_Coul_ESP = 0.0;
     sq_error = 0.0;
     V_pot_sq_sum = 0.0;
     Coul_ESP_file = fopen(file_name,"w");
     if(Coul_ESP_file != NULL) {
       for(i_grid=0;i_grid<counter_grid;i_grid++) {
          V_coul[i_grid] = 0.0;
          for(i_atom=0;i_atom<n_atoms_all;i_atom++) {
             j_atom  = charges_equiv[i_atom];
             V_coul[i_grid] += (inverse_term[i_grid][i_atom] - \
             inverse_term_level[i_atom])*vector_solv[j_atom];
          }
          sq_error += pow((V_coul[i_grid] - V_pot_grid[i_grid]),2);
          V_pot_sq_sum += pow(V_pot_grid[i_grid],2);
          avrg_Coul_ESP += V_coul[i_grid]/counter_grid;
          avrg_QM_ESP += V_pot_grid[i_grid]/counter_grid;
          fprintf(Coul_ESP_file,"%f %f %f %f %f\n",grid_pos[i_grid][0], \
          grid_pos[i_grid][1],grid_pos[i_grid][2],V_coul[i_grid],\
          V_pot_grid[i_grid]);
       }
       printf("\n"); 
       printf("ESP Coulomb average = %le\n",avrg_Coul_ESP);
       printf("ESP QM average = %le\n",avrg_QM_ESP);
       printf("Error of the potential = %f\n",sqrt(sq_error/V_pot_sq_sum));
       fclose(Coul_ESP_file);
     } else {

       printf("ERROR: Cannot write Coulomb ESP grid data!!!");

     }
     destroy_1d_double_array(V_coul);

}

