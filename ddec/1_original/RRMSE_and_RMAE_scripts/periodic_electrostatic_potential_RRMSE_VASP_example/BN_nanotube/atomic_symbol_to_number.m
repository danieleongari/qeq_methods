if strcmpi(atomic_symbol,'H')
    z = 1;
elseif strcmpi(atomic_symbol,'He')
    z = 2;    
elseif strcmpi(atomic_symbol,'Li')
    z = 3;
elseif strcmpi(atomic_symbol,'Be')
    z = 4; 
elseif strcmpi(atomic_symbol,'B')
    z = 5;
elseif strcmpi(atomic_symbol,'C')
    z = 6; 
elseif strcmpi(atomic_symbol,'N')
    z = 7;
elseif strcmpi(atomic_symbol,'O')
    z = 8;
elseif strcmpi(atomic_symbol,'F')
    z = 9;
elseif strcmpi(atomic_symbol,'Ne')
    z = 10;
elseif strcmpi(atomic_symbol,'Na')
    z = 11;
elseif strcmpi(atomic_symbol,'Mg')
    z = 12;
elseif strcmpi(atomic_symbol,'Al')
    z = 13;
elseif strcmpi(atomic_symbol,'Si')
    z = 14;
elseif strcmpi(atomic_symbol,'P')
    z = 15;
elseif strcmpi(atomic_symbol,'S')
    z = 16;
elseif strcmpi(atomic_symbol,'Cl')
    z = 17;
elseif strcmpi(atomic_symbol,'Ar')
    z = 18;
elseif strcmpi(atomic_symbol,'K')
    z = 19;
elseif strcmpi(atomic_symbol,'Ca')
    z = 20;
elseif strcmpi(atomic_symbol,'Sc')
    z = 21;
elseif strcmpi(atomic_symbol,'Ti')
    z = 22;
elseif strcmpi(atomic_symbol,'V')
    z = 23;
elseif strcmpi(atomic_symbol,'Cr')
    z = 24;
elseif strcmpi(atomic_symbol,'Mn')
    z = 25;
elseif strcmpi(atomic_symbol,'Fe')
    z = 26;
elseif strcmpi(atomic_symbol,'Co')
    z = 27;
elseif strcmpi(atomic_symbol,'Ni')
    z = 28;
elseif strcmpi(atomic_symbol,'Cu')
    z = 29;
elseif strcmpi(atomic_symbol,'Zn')
    z = 30;
elseif strcmpi(atomic_symbol,'Ga')
    z = 31;
elseif strcmpi(atomic_symbol,'Ge')
    z = 32;
elseif strcmpi(atomic_symbol,'As')
    z = 33;
elseif strcmpi(atomic_symbol,'Se')
    z = 34;
elseif strcmpi(atomic_symbol,'Br')
    z = 35;
elseif strcmpi(atomic_symbol,'Kr')
    z = 36;
elseif strcmpi(atomic_symbol,'Rb')
    z = 37;
elseif strcmpi(atomic_symbol,'Sr')
    z = 38;
elseif strcmpi(atomic_symbol,'Y')
    z = 39;
elseif strcmpi(atomic_symbol,'Zr')
    z = 40;
elseif strcmpi(atomic_symbol,'Nb')
    z = 41;
elseif strcmpi(atomic_symbol,'Mo')
    z = 42;
elseif strcmpi(atomic_symbol,'Tc')
    z = 43;
elseif strcmpi(atomic_symbol,'Ru')
    z = 44;
elseif strcmpi(atomic_symbol,'Rh')
    z = 45;
elseif strcmpi(atomic_symbol,'Pd')
    z = 46;
elseif strcmpi(atomic_symbol,'Ag')
    z = 47;
elseif strcmpi(atomic_symbol,'Cd')
    z = 48;
elseif strcmpi(atomic_symbol,'In')
    z = 49;
elseif strcmpi(atomic_symbol,'Sn')
    z = 50;
elseif strcmpi(atomic_symbol,'Sb')
    z = 51;
elseif strcmpi(atomic_symbol,'Te')
    z = 52;
elseif strcmpi(atomic_symbol,'I')
    z = 53;
elseif strcmpi(atomic_symbol,'Xe')
    z = 54;
elseif strcmpi(atomic_symbol,'Cs')
    z = 55;
elseif strcmpi(atomic_symbol,'Ba')
    z = 56;
elseif strcmpi(atomic_symbol,'La')
    z = 57;
elseif strcmpi(atomic_symbol,'Ce')
    z = 58;
elseif strcmpi(atomic_symbol,'Pr')
    z = 59;
elseif strcmpi(atomic_symbol,'Nd')
    z = 60;
elseif strcmpi(atomic_symbol,'Pm')
    z = 61;
elseif strcmpi(atomic_symbol,'Sm')
    z = 62;
elseif strcmpi(atomic_symbol,'Eu')
    z = 63;
elseif strcmpi(atomic_symbol,'Gd')
    z = 64;
elseif strcmpi(atomic_symbol,'Tb')
    z = 65;
elseif strcmpi(atomic_symbol,'Dy')
    z = 66;
elseif strcmpi(atomic_symbol,'Ho')
    z = 67;
elseif strcmpi(atomic_symbol,'Er')
    z = 68;
elseif strcmpi(atomic_symbol,'Tm')
    z = 69;
elseif strcmpi(atomic_symbol,'Yb')
    z = 70;
elseif strcmpi(atomic_symbol,'Lu')
    z = 71;
elseif strcmpi(atomic_symbol,'Hf')
    z = 72;
elseif strcmpi(atomic_symbol,'Ta')
    z = 73;
elseif strcmpi(atomic_symbol,'W')
    z = 74;
elseif strcmpi(atomic_symbol,'Re')
    z = 75;
elseif strcmpi(atomic_symbol,'Os')
    z = 76;
elseif strcmpi(atomic_symbol,'Ir')
    z = 77;
elseif strcmpi(atomic_symbol,'Pt')
    z = 78;
elseif strcmpi(atomic_symbol,'Au')
    z = 79;
elseif strcmpi(atomic_symbol,'Hg')
    z = 80;
elseif strcmpi(atomic_symbol,'Tl')
    z = 81;
elseif strcmpi(atomic_symbol,'Pb')
    z = 82;
elseif strcmpi(atomic_symbol,'Bi')
    z = 83;
elseif strcmpi(atomic_symbol,'Po')
    z = 84;
elseif strcmpi(atomic_symbol,'At')
    z = 85;
elseif strcmpi(atomic_symbol,'Rn')
    z = 86;
elseif strcmpi(atomic_symbol,'Fr')
    z = 87;
elseif strcmpi(atomic_symbol,'Ra')
    z = 88;
elseif strcmpi(atomic_symbol,'Ac')
    z = 89;
elseif strcmpi(atomic_symbol,'Th')
    z = 90;
elseif strcmpi(atomic_symbol,'Pa')
    z = 91;
elseif strcmpi(atomic_symbol,'U')
    z = 92;
elseif strcmpi(atomic_symbol,'Np')
    z = 93;
elseif strcmpi(atomic_symbol,'Pu')
    z = 94;
elseif strcmpi(atomic_symbol,'Am')
    z = 95;
elseif strcmpi(atomic_symbol,'Cm')
    z = 96;
elseif strcmpi(atomic_symbol,'Bk')
    z = 97;
elseif strcmpi(atomic_symbol,'Cf')
    z = 98;
elseif strcmpi(atomic_symbol,'Es')
    z = 99;
elseif strcmpi(atomic_symbol,'Fm')
    z = 100;
elseif strcmpi(atomic_symbol,'Md')
    z = 101;
elseif strcmpi(atomic_symbol,'No')
    z = 102;
elseif strcmpi(atomic_symbol,'Lr')
    z = 103;
elseif strcmpi(atomic_symbol,'Rf')
    z = 104;
elseif strcmpi(atomic_symbol,'Db')
    z = 105;
elseif strcmpi(atomic_symbol,'Sg')
    z = 106;
elseif strcmpi(atomic_symbol,'Bh')
    z = 107;
elseif strcmpi(atomic_symbol,'Hs')
    z = 108;
elseif strcmpi(atomic_symbol,'Mt')
    z = 109;
else
    'Atomic symbol not properly identified. Program will terminate.'
    flag = 1
    break
end