## Geoffrey Martin-Noble '16
## v3.3 27 December 2014
## Cleaned up a few things prior to publication

## v3.2 26 July 2013
## outputs the parameters in alphabetical order

## v3.1 9 July 2013
## gives the parameters file a unique name and outputs the sum_of_squares value to the file for easier scanning over alpha

## v3 9 July 2013
## The alpha parameter is now passed as an argument when the script is called in the command line
## e.g. 
## python linear_EQeq+C_v3.py 2.5

## v2 9 July 2013
## The carbon parameter is set to zero throughout the parameterization rather than subtracting it from all of the parameters
## at the end

## 
## v1.1 8 July 2013
## Uses new atom_records generation script, which incidentally switches the position of HI and EQeq charge because it makes more sense that way around
## (at least to me)



## v1 1 July 2013
## Turns EQeq+C parameterization into a linear regression problem



### USING THIS SCRIPT ###
# To run this script type the following command into the command line (not including the dollar sign)
# $python <script name> <alpha parameter>
# So for example:
# $python linear_EQeq+C_v3.3.py 2.10
# In order to run a parameterization using the script as-written, you need a file in the working directory called "atom_records.csv" (the name can be
# changed below or made a parameter passed when calling the script), a csv file formatted with the following columns for each atom in the following order: 
# Compound, Element symbol, Rad(A), X_frac, Y_frac, Z_frac, HI charge, EQeq charge, a, b, c, alpha, beta, gamma
# The name used for the compound (and the elemental symbol) don't need to follow any particular formatting as long as they are consistent and don't
# include commas. The alpha parameter, radii, and unit cell lattice parameters should all use the same length units (probably angstroms).
# X_frac, Y_frac, and Z_frac should all be fractional coordinates, and alpha, beta, and gamma should all be in degrees. The HI charges (or whatever 
# charges you are trying to parameterize the correction factor to) and EQeq charges should be in the same units (probably |e-|).
# If you would like to change the order of the columns, just change the order of the items below, but make sure not to change the names (unless you
# change them where they are used elsewhere in the script.
# The order of the atoms in the atom records file is not important. There should not be a header row, however (to deal with a header row, change the
# way the file is read to skip the first row).
# The output is a file called "parameters_<alpha parameter>.json" (this can be changed below) with the optimal Dz parameters, the alpha parameter, and
# residual sum of squares. It will also print the result of the lstsq which includes the parameters, the residuals, the rank of the constraints matrix, and it's singular values. It also prints the elements for which parameters were determined (in the same order as the parameters).

# If you have any trouble or make any improvements (there are many possibilities), feel free to contact me.



import math
import csv
from numpy import array
from numpy.linalg import lstsq
from sys import argv


# Define column names as numbers for clearer array lookups later
number_of_columns = 14
COMPOUND, SYMBOL, RADIUS, X_FRAC, Y_FRAC, Z_FRAC, HI, EQeq, A, B, C, ALPHA, BETA, GAMMA = range(number_of_columns)

atom_records_file = "atom_records.csv"
alpha_parameter = float(argv[1])
parameters_file = "parameters_{}.json".format(alpha_parameter)

with open(atom_records_file) as f:
    reader = csv.reader(f)
    atom_records = [row for row in reader]




els_to_param = []
Y = [] # List of y values. This is the data that will be fitted

for atom in atom_records:
	element = atom[SYMBOL]
	#Coerce everything into floats so that they can be called later more easily
	atom[RADIUS] = float(atom[RADIUS])
	atom[X_FRAC] = float(atom[X_FRAC])
	atom[Y_FRAC] = float(atom[Y_FRAC])
	atom[Z_FRAC] = float(atom[Z_FRAC])
	atom[EQeq] = float(atom[EQeq])
	atom[HI] = float(atom[HI])
	atom[A] = float(atom[A])
	atom[B] = float(atom[B])
	atom[C] = float(atom[C])
	atom[ALPHA] = float(atom[ALPHA])
	atom[BETA]= float(atom[BETA])
	atom[GAMMA] = float(atom[GAMMA])
	
        yk = atom[HI] - atom[EQeq]  #Calculate the y value (charge discrepancy) for this atom to be used in the linear regression. 
        
        Y.append(yk)
        
        #add new elements other than carbon since it's parameter is set to 0 to ensure a unique solution
	if element not in els_to_param and element!='C':
		els_to_param.append(element)

els_to_param.sort()

Y = array(Y)



## Returns a list of distances between atom 1, atom 2 and the copies of atom 2 in all adjacent unit cells
## Uses the generalized translation vector distance formula
def distance(x1, y1, z1, x2, y2, z2, length_a, length_b, length_c, alpha, beta, gamma):
	alpha_in_radians = (alpha)*(math.pi)/180.0
	beta_in_radians = (beta)*(math.pi)/180.0
	gamma_in_radians = (gamma)*(math.pi)/180.0
	# for unit considerations, the coordinates for the calculation should be in Cartesian as opposed to the fractional coordinates from the atom
	# records file, and should have units (likely angstroms) consistent with the units of the length variables.
	def x_dist(m):
		return length_a*(x2 + m - x1) #breaking down the generalized translantion vector distance formula into smaller parts
	def y_dist(n):
		return length_b*(y2 + n - y1)
	def z_dist(p):
		return length_c*(z2 + p - z1)
	def mapping_function(m,n,p):
		return x_dist(m)**2  + y_dist(n)**2 + z_dist(p)**2 + 2*x_dist(m)*y_dist(n)*math.cos(gamma_in_radians) + 2*y_dist(n)*z_dist(p)*math.cos(alpha_in_radians) + 2*z_dist(p)*x_dist(m)*math.cos(beta_in_radians)

        distance_list = [mapping_function(m,n,p)**0.5 for m in range(-1,2) for n in range(-1,2) for p in range(-1,2)]

	return distance_list 


## this function is designed to return the values of the B_k_k' term in the CM5-like pairwise correction factor
## Returns a list of bkk' values for an atom compared to another atom and its copies in all adjacent unit cells
## Bkk' = e^(-alpha*(Rkk' - rk - rk'))
## Rkk' = Distance between k and k'
## rk = radius of k
## rk' = radius of k'
def B_k_kprime(radius_of_atom_1, radius_of_atom_2, list_distance_between_1_and_2, alpha_param):
        bkk_list = [math.exp(-alpha_param*(list_distance_between_1_and_2[x]-radius_of_atom_1-radius_of_atom_2)) for x in range(len(list_distance_between_1_and_2))]
	return bkk_list



def make_Bkk_value_list_of_lists(atom_records,alpha_param):
	#Pretty sure I could turn this all into a list comprehension, but the clarity/speed tradeoff seems not worth it
        Bkk_value_list_of_lists = []
	for x in range(0,len(atom_records)):  # for every atom in atom records
		Bkk_value_list = []
		atom_x = atom_records[x]      # label it atom_x
		other_atom_contributions = 0  # starts with 0 contribution from other atoms
		for y in range(0,len(atom_records)):  # for every other atom in atom record
			atom_y = atom_records[y]      # call it atom_y
			if x!=y and atom_x[COMPOUND]==atom_y[COMPOUND]:  # check to make sure they're in the same structure and not the same atom
				# Get the list of distances between the two atoms and all copies of the second atom in adjacent cells
				distances = distance(atom_x[X_FRAC],atom_x[Y_FRAC],atom_x[Z_FRAC],atom_y[X_FRAC],atom_y[Y_FRAC],atom_y[Z_FRAC],atom_x[A],atom_x[B],atom_x[C],atom_x[ALPHA],atom_x[BETA],atom_x[GAMMA])
				Bkk_value = sum(B_k_kprime(atom_x[RADIUS],atom_y[RADIUS],distances,alpha_param))  #The Bkk_value is just the sum of all the B_kk' values for the atoms in question (all the copies of atom_y)
			else:
				Bkk_value = None #This value should never be called
			Bkk_value_list.append(Bkk_value)
		Bkk_value_list_of_lists.append(Bkk_value_list)
	return Bkk_value_list_of_lists


## Makes the matrix of x vectors for each atom
## There is an entry in the vector for each element
## See Equation (14) in paper
## yk = DZ(k)*SUM_(k'!=k)[ Bkk'] - SUM_(Z)[DZ*SUM_(k'!=k & Z(k')==Z)[ Bkk'] ]
def make_x_matr(atom_records, Bkk_lol):
    X = []
    for i in range(len(atom_records)):
        atom_i = atom_records[i]
        x = []
        for element in els_to_param:    #one term for each element
            x_el = 0
            if atom_i[SYMBOL]==element:
                #if the atom is that element then add Bkk' for all other atoms that are not the same element
                #the same element terms cancel
                for j in range(len(atom_records)):
                    atom_j = atom_records[j]
                    if atom_j[SYMBOL]!=element and atom_i[COMPOUND]==atom_j[COMPOUND]:
                        x_el += Bkk_lol[i][j]
            
            else:
                #if the atom is not that element, then subtract all the Bkk' terms for other atoms that are that element
                for j in range(len(atom_records)):
                    atom_j = atom_records[j]
                    if atom_j[SYMBOL]==element and atom_i[COMPOUND]==atom_j[COMPOUND]:
                        x_el -= Bkk_lol[i][j]
            x.append(x_el)
        X.append(x)
    
    X = array(X)
    return X


#this can be interesting, but is not used for the parameterization
def compute_chi(Y):
    sum_of_squares = 0
    for res in Y:
        sum_of_squares += (res**2)
    return sum_of_squares



Bkk_lol = make_Bkk_value_list_of_lists(atom_records,alpha_parameter)


X = make_x_matr(atom_records,Bkk_lol)

#use the magic of linear algebra to solve the linear regression
result = lstsq(X,Y)

print(result)
print(els_to_param)

parameters = result[0]
residuals = result[1][0]


#yes this is the silliest way ever to write a .json file, but I wanted
#the element parameter terms to be in alphabetical order (really any consistent order)
#for human readability and I couldn't figure out another way to do that because of a
#dictionary's lack of order. This works. If you know of a better way I would
#love to know because despite working this still really bothers me
with open(parameters_file,'w') as f:
    f.write("{\n")
    for i in range(len(els_to_param)):
        element = els_to_param[i]
        f.write('"{}":{},\n'.format(element,parameters[i])) 

    f.write('"alpha":{},\n'.format(alpha_parameter))
    f.write('"residuals":{}\n'.format(residuals))
    f.write("}\n")
