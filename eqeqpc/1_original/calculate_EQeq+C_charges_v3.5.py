## Geoffrey Martin-Noble '16
## v3.5 26 December 2014
## Cleaned up a little for publication

## v3.4 24 July 2013
## Outputs element and compound MADs sorted alphabetically

## v3.3 12 July 2013
## Outputs the overall EQeq and EQeq+C MAD to the csv file as well

## v3.2 11 July 2013
## Output the atom charges file with coordinates

## v3.1 8 July 2013
## outputs a csv file with MAD by element and compound

## v3 8 July 2013
## Calculates the MAD and chi for the entire dataset
## as well as by compound and element
## Also uses the alpha value in the parameters file instead of a fixed value


## v2.1 8 July 2013
## Switches the order of EQeq and HI charge coming from the atom_records.csv file
## in keeping with make_atom_records_v5 and linear_EQeq_v1.1.py
## Switch made only because that order seems more logical to me


## v2 27 June 2013
## Removes the superfluous data (radius, a, b, c, alpha, beta, gamma) from the charges file
## And calculates the residuals for the EQeq charges and the HI charges


## The output file has columns:
## COMPOUND, SYMBOL, HI Charge, EQeq Charge, EQeq Residual, EQeq+C Charge, EQeq+C residual

## Calculates the EQeq+C charges on all the atoms in the atom records file and creates
## a new file with the EQeq charges appended to the atom records for each row


### USING THIS SCRIPT ###
# To run this script type the following command into the command line (not including the dollar sign)
# $python <script name>
# So for example:
# $python calculate_EQeq+C_charges_v3.5.py
# To run a calculation using the script as-written, you need a file in the working directory called "atom_records.csv" (the name can be
# changed below or made a parameter passed when calling the script), a csv file formatted with the following columns for each atom in the following order: 
# Compound, Element symbol, Rad(A), X_frac, Y_frac, Z_frac, HI charge, EQeq charge, a, b, c, alpha, beta, gamma
# The name used for the compound (and the elemental symbol) don't need to follow any particular formatting as long as they are consistent and don't
# include commas. The alpha parameter, radii, and unit cell lattice parameters should all use the same length units (probably angstroms).
# X_frac, Y_frac, and Z_frac should all be fractional coordinates, and alpha, beta, and gamma should all be in degrees. The HI charges column is not actually
# used in the calculation and is there because I was doing calculations on compounds for which we also had HI charges. It is used however, to calculate residuals
# and MADs. If you edit the script to remove the dependency on the HI charges column, make sure to remove the section at the end that does these calculations
# The EQeq charges should be in the same units (probably |e-|) as the Dz parameters
# If you would like to change the order of the columns, just change the order of the items below, but make sure not to change the names (unless you
# change them where they are used elsewhere in the script.
# The order of the atoms in the atom records file is not important. There should not be a header row, however (to deal with a header row, change the
# way the file is read to skip the first row).
# The calculation also requires a file called "parameters.json" (this can be changed below) with the optimal Dz parameters, and the alpha parameter, formatted
# the same as a python json dictionary output, as follows:
#
 
# {
# "<1st element symbol>": <Dz paramter>,
# "<2nd element symbol>": <Dz paramter>,
# "<3rd element symbol>": <Dz paramter>,
# "alpha": <alpha parameter>
# }

# Having extra items in the dictionary (such as the residuals output by the parameterization script) will not hurt anything.
# The carbon parameter is not necessary as it is assumed to be zero. For example, the parameters.json file  used for the ATMOs was:

# {
# "H":0.0710685441457,
# "N":-0.032284209513,
# "O":-0.100588547373,
# "Se":-0.331389713081,
# "Te":-0.294403409687,
# "V":-0.142764747549,
# "alpha":2.1,
# "residuals":20.9465007925
# }

# The script will output a file called "atom_charges_with_coords.csv" with the following columns for each atom:
# Compound, Element Symbol, HI Charge, EQeq Charge, EQeq Absolute Residual, EQeq+C Charge, EQeq+C Absolute Residual
# Note that the residuals that are calculated are absolute residuals
# The script will also output a file called "residual_info.csv" with the EQeq and EQeq+C MAD for each element and compound
# If you have any trouble or make any improvements (there are many possibilities), feel free to contact me.



import math
import csv
from json import load
from scipy.optimize import minimize

# Define column names as numbers for clearer array lookups later
number_of_columns = 14
COMPOUND, SYMBOL, RADIUS, X_FRAC, Y_FRAC, Z_FRAC, HI, EQeq, A, B, C, ALPHA, BETA, GAMMA = range(number_of_columns)


atom_records_file = "atom_records.csv"
parameters_file = "parameters.json"
charges_file = "atom_charges_with_coords.csv"
output_file = "residual_info.csv"


with open(parameters_file) as file_handle:
	params = load(file_handle)

with open(atom_records_file) as f:
	reader = csv.reader(f)
	atom_records = [row for row in reader]

alpha_parameter = params["alpha"]
params["C"] = 0


for atom in atom_records:
	#Coerce everything into floats for calling later
	atom[RADIUS] = float(atom[RADIUS])
	atom[X_FRAC] = float(atom[X_FRAC])
	atom[Y_FRAC] = float(atom[Y_FRAC])
	atom[Z_FRAC] = float(atom[Z_FRAC])
	atom[HI] = float(atom[HI])
	atom[EQeq] = float(atom[EQeq])
	atom[A] = float(atom[A])
	atom[B] = float(atom[B])
	atom[C] = float(atom[C])
	atom[ALPHA] = float(atom[ALPHA])
	atom[BETA]= float(atom[BETA])
	atom[GAMMA] = float(atom[GAMMA])




## Returns a list of distances between atom 1, atom 2 and the copies of atom 2 in all adjacent unit cells
## Uses the generalized translation vector distance formula
def distance(x1, y1, z1, x2, y2, z2, length_a, length_b, length_c, alpha, beta, gamma):
	alpha_in_radians = (alpha)*(math.pi)/180.0
	beta_in_radians = (beta)*(math.pi)/180.0
	gamma_in_radians = (gamma)*(math.pi)/180.0
	# for unit considerations, the coordinates for the calculation should be in Cartesian as opposed to the fractional coordinates of the atom
	# records file, and should have units (likely angstroms) consistent with the units of the length variables.
	distance_list = [] #empty list into which we will put the distances generated by the loop over the translation vectors
	def x_dist(m):
		return length_a*(x2 + m - x1) #breaking down the generalized translantion vector distance formula into smaller parts
	def y_dist(n):
		return length_b*(y2 + n - y1)
	def z_dist(p):
		return length_c*(z2 + p - z1)
	def mapping_function(m,n,p):
		return x_dist(m)**2  + y_dist(n)**2 + z_dist(p)**2 + 2*x_dist(m)*y_dist(n)*math.cos(gamma_in_radians) + 2*y_dist(n)*z_dist(p)*math.cos(alpha_in_radians) + 2*z_dist(p)*x_dist(m)*math.cos(beta_in_radians)

        distance_list = [mapping_function(m,n,p)**0.5 for m in range(-1,2) for n in range(-1,2) for p in range(-1,2)]

	return distance_list 


## this function is designed to return the values of the B_k_k' term in the CM5-like pairwise correction factor
## Returns a list of bkk' values for an atom compared to another atom and its copies in all adjacent unit cells
## Bkk' = e^(-alpha*(Rkk' - rk - rk'))
## Rkk' = Distance between k and k'
## rk = radius of k
## rk' = radius of k'
def B_k_kprime(radius_of_atom_1, radius_of_atom_2, list_distance_between_1_and_2, alpha_param):
        bkk_list = [math.exp(-alpha_param*(list_distance_between_1_and_2[x]-radius_of_atom_1-radius_of_atom_2)) for x in range(len(list_distance_between_1_and_2))]
	return bkk_list


def M_EQeq_charges(params):
	modified_charges = []
	for x in range(len(atom_records)):
		atom_x = atom_records[x]
		other_atom_contributions = 0
		for y in range(len(atom_records)):
			atom_y = atom_records[y]
			if x!=y and atom_x[COMPOUND]==atom_y[COMPOUND]:  # loop over all atoms y not x and in the same structure
				distances = distance(atom_x[X_FRAC],atom_x[Y_FRAC],atom_x[Z_FRAC],atom_y[X_FRAC],atom_y[Y_FRAC],atom_y[Z_FRAC],atom_x[A],atom_x[B],atom_x[C],atom_x[ALPHA],atom_x[BETA],atom_x[GAMMA])
				T_parameter = params[atom_x[SYMBOL]] - params[atom_y[SYMBOL]]
				Bkk_value = sum(B_k_kprime(atom_x[RADIUS],atom_y[RADIUS],distances,params["alpha"]))
				other_atom_contributions += T_parameter*Bkk_value
		modified_charges.append(atom_x[EQeq] + other_atom_contributions)
	return modified_charges


MEQeq_charges = M_EQeq_charges(params)

new_atom_records = []

compound_dict = {}  #Use these to organize data by compound and by element
element_dict = {}

(EQeq_sum_of_squares, EQeq_sum_of_res, MEQeq_sum_of_squares, MEQeq_sum_of_res) = (0,0,0,0)

for i in range(len(atom_records)):
	MEQeq_charge = MEQeq_charges[i]
	atom_info = atom_records[i]
	compound = atom_info[COMPOUND]
	element = atom_info[SYMBOL]
	x_frac = atom_info[X_FRAC]
	y_frac = atom_info[Y_FRAC]
	z_frac = atom_info[Z_FRAC]
	HI_charge = atom_info[HI]
	EQeq_charge = atom_info[EQeq]
	
	EQeq_abs_res = math.fabs(HI_charge - EQeq_charge)
	MEQeq_abs_res = math.fabs(HI_charge - MEQeq_charge)
	
	EQeq_sum_of_res+=EQeq_abs_res
	MEQeq_sum_of_res+=MEQeq_abs_res
	
	EQeq_sum_of_squares+=EQeq_abs_res**2
	MEQeq_sum_of_squares+=MEQeq_abs_res**2
	
	

	new_atom_info = [compound, element, x_frac, y_frac, z_frac, HI_charge, EQeq_charge, EQeq_abs_res, MEQeq_charge, MEQeq_abs_res]

	try:
		compound_dict[compound].append(new_atom_info)
	except KeyError:
		compound_dict[compound] = [new_atom_info]

	try:
		element_dict[element].append(new_atom_info)
	except KeyError:
		element_dict[element] = [new_atom_info]

	new_atom_records.append(new_atom_info)
	
EQeq_MAD = EQeq_sum_of_res/float(len(atom_records))
MEQeq_MAD = MEQeq_sum_of_res/float(len(atom_records))

print ("EQeq chi: {}\nEQeq+C chi: {}\nEQeq MAD: {}\nEQeq+C MAD: {}\n\n\n".format(EQeq_sum_of_squares, MEQeq_sum_of_squares, EQeq_MAD, MEQeq_MAD))

output_list = [["Overall", EQeq_MAD, MEQeq_MAD]]

for element in sorted(element_dict.keys()):

	(EQeq_sum_of_squares, EQeq_sum_of_res,MEQeq_sum_of_squares, MEQeq_sum_of_res) = (0,0,0,0)
	
	for atom in element_dict[element]:
		EQeq_abs_res = atom[7]
		MEQeq_abs_res = atom[9]

		EQeq_sum_of_res+=EQeq_abs_res
		MEQeq_sum_of_res+=MEQeq_abs_res

		EQeq_sum_of_squares+=EQeq_abs_res**2
		MEQeq_sum_of_squares+=MEQeq_abs_res**2
	
	EQeq_MAD = EQeq_sum_of_res/float(len(element_dict[element]))
	MEQeq_MAD = MEQeq_sum_of_res/float(len(element_dict[element]))
	
	output_list.append([element, EQeq_MAD, MEQeq_MAD])

	print("{}--\n".format(element))
	print("EQeq chi: {}\nEQeq+C chi: {}\nEQeq MAD: {}\nEQeq+C MAD: {}\n\n".format(EQeq_sum_of_squares, MEQeq_sum_of_squares, EQeq_MAD, MEQeq_MAD))

for compound in sorted(compound_dict.keys()):

	(EQeq_sum_of_squares, EQeq_sum_of_res,MEQeq_sum_of_squares, MEQeq_sum_of_res) = (0,0,0,0)
	
	for atom in compound_dict[compound]:
		EQeq_abs_res = atom[7]
		MEQeq_abs_res = atom[9]

		EQeq_sum_of_res+=EQeq_abs_res
		MEQeq_sum_of_res+=MEQeq_abs_res

		EQeq_sum_of_squares+=EQeq_abs_res**2
		MEQeq_sum_of_squares+=MEQeq_abs_res**2
	
	EQeq_MAD = EQeq_sum_of_res/float(len(compound_dict[compound]))
	MEQeq_MAD = MEQeq_sum_of_res/float(len(compound_dict[compound]))

	output_list.append([compound, EQeq_MAD, MEQeq_MAD])
	
	print("{}--\n".format(compound))
	print("EQeq chi: {}\nEQeq+C chi: {}\nEQeq MAD: {}\nEQeq+C MAD: {}\n\n".format(EQeq_sum_of_squares, MEQeq_sum_of_squares, EQeq_MAD, MEQeq_MAD))


with open(output_file, "w") as f:
	for row in output_list:
		csv.writer(f).writerow(row)

with open(charges_file,"w") as f:
	for row in new_atom_records:
		csv.writer(f).writerow(row)
